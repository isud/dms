﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_SHOW
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_SHOW_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล
        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)
            Me.Close()
        End Try

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "Select * from member where user_id like '" + id_user + "';"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While mySqlReader.Read()
                Label87.Text = " " + mySqlReader("name") + "  " + mySqlReader("lastname")
                Label89.Text = " " + mySqlReader("position_name")
                Label92.Text = " " + mySqlReader("user_id")
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Sql.Close()

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewfloor.Items.Clear()
            While (mySqlReader.Read())

                With ListViewfloor.Items.Add(mySqlReader("idfloor"))
                    .SubItems.Add(mySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub btnback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnback.Click
        Me.Hide()
    End Sub

    Private Sub ListViewfloor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewfloor.Click
        idkey = ListViewfloor.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim count As Integer = 1

        mySqlCommand.CommandText = "SELECT * FROM section where idfloor = '" & idkey & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewsection.Items.Clear()

            While (mySqlReader.Read())

                With ListViewsection.Items.Add(mySqlReader("idsection"))
                    .subitems.add(mySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub ListViewsection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewsection.Click
        idkey = ListViewsection.SelectedItems(0).SubItems(0).Text
        'ดึงข้อมูลจากตาราง 4/7/56
        Dim dpname As String
        dpname = ListViewfloor.SelectedItems(0).SubItems(1).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "SELECT * FROM section where idsection = '" & idkey & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim count As Integer = 1

        mySqlCommand.CommandText = "SELECT * FROM data_device where idsection = '" & idkey & "' ; "
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewshowall.Items.Clear()
            While (mySqlReader.Read())
                With ListViewshowall.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "     " + mySqlReader("c_ram") + "     " + mySqlReader("c_comname") + "     ")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "     " + mySqlReader("c_ram") + "     " + mySqlReader("c_comname") + "     ")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    'subitem แสดงข้อมูล dpname ที่เก็บมาแสดงในตาราง
                    .subitems.add(dpname)
                    .subitems.add(mySqlReader("serial"))
                End With
                count = count + 1
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub ListViewshowall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewshowall.Click
        idkey = ListViewshowall.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim count As Integer = 1

        mySqlCommand.CommandText = "SELECT section_name,date_time_book FROM transfer_his join section where  (transfer_his.idsection = section.idsection )  and iddata_device = '" & idkey & "';"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewshowdevice.Items.Clear()

            While (mySqlReader.Read())

                With ListViewshowdevice.Items.Add(mySqlReader("section_name"))
                    .SubItems.Add(mySqlReader("date_time_book"))



                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        idkey = ListViewshowall.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device  where iddata_device = '" & idkey & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                txtedit.Text = mySqlReader("iddata_device")

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT  * FROM maintenance   where    iddata_device = '" & idkey & "';"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewshowhis.Items.Clear()

            While (mySqlReader.Read())

                With ListViewshowhis.Items.Add(mySqlReader("iddata_device"))
                    .SubItems.Add(mySqlReader("history"))
                    .SubItems.Add(mySqlReader("date_maintenance"))
                    .SubItems.Add(mySqlReader("position_main"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

    End Sub

    Private Sub btnblank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnblank.Click
        If txtedit.Text <> "" Then

            savedatablank()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตาราง", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatablank()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Complete")
        If respone = 1 Then
            If txtedit.Text <> "" Then

                If Sql.State = ConnectionState.Closed Then
                    Sql.Open()
                End If

                Try
                    commandText2 = "UPDATE data_device SET state_device = 'ว่าง' , idsection = '0' WHERE iddata_device = " & txtedit.Text & "; "
                    mySqlCommand.CommandText = commandText2
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
            End If
        End If
        Sql.Close()


        ListViewshowall.Items.Clear()
        ListViewshowdevice.Items.Clear()
        ListViewsection.Items.Clear()
        txtedit.Text = ""
    End Sub
End Class