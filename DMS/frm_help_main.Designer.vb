﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_help_main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.txtshowcom = New System.Windows.Forms.TextBox()
        Me.ListViewcom = New System.Windows.Forms.ListView()
        Me.name_container = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.agent = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader37 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader43 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader49 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtsearchcom = New System.Windows.Forms.TextBox()
        Me.btnsearchcom = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.txtshowcomrent = New System.Windows.Forms.TextBox()
        Me.txtsearchcomrent = New System.Windows.Forms.TextBox()
        Me.btnsearchcomrent = New System.Windows.Forms.Button()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.ListViewcomrent = New System.Windows.Forms.ListView()
        Me.ColumnHeader23 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader26 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader27 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader38 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader44 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader50 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtshowprin = New System.Windows.Forms.TextBox()
        Me.txtsearchprin = New System.Windows.Forms.TextBox()
        Me.btnsearch_prin = New System.Windows.Forms.Button()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.ListViewprin = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader39 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader45 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader51 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtshowmonitor = New System.Windows.Forms.TextBox()
        Me.txtsearchmonitor = New System.Windows.Forms.TextBox()
        Me.btnsearch_monitor = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ListViewmonitor = New System.Windows.Forms.ListView()
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader40 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader46 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader52 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btngoadd = New System.Windows.Forms.TabPage()
        Me.ListViewother = New System.Windows.Forms.ListView()
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader35 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader36 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader41 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader47 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader53 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtshowother = New System.Windows.Forms.TextBox()
        Me.txtsearchother = New System.Windows.Forms.TextBox()
        Me.btnsearch_other = New System.Windows.Forms.Button()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.txtshowlicense = New System.Windows.Forms.TextBox()
        Me.txtsearchlicense = New System.Windows.Forms.TextBox()
        Me.btnsearch_license = New System.Windows.Forms.Button()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.ListViewlicense = New System.Windows.Forms.ListView()
        Me.ColumnHeader18 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader19 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader20 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader42 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader48 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader54 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.btngoadd.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(917, 614)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(132, 76)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.btngoadd)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(1, 2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1391, 644)
        Me.TabControl1.TabIndex = 356
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label67)
        Me.TabPage1.Controls.Add(Me.txtshowcom)
        Me.TabPage1.Controls.Add(Me.ListViewcom)
        Me.TabPage1.Controls.Add(Me.txtsearchcom)
        Me.TabPage1.Controls.Add(Me.btnsearchcom)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 35)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1383, 605)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Computer"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(617, 19)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(45, 26)
        Me.Label67.TabIndex = 181
        Me.Label67.Text = "Count"
        '
        'txtshowcom
        '
        Me.txtshowcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowcom.Location = New System.Drawing.Point(668, 16)
        Me.txtshowcom.Name = "txtshowcom"
        Me.txtshowcom.ReadOnly = True
        Me.txtshowcom.Size = New System.Drawing.Size(54, 34)
        Me.txtshowcom.TabIndex = 180
        '
        'ListViewcom
        '
        Me.ListViewcom.BackColor = System.Drawing.Color.White
        Me.ListViewcom.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.name_container, Me.agent, Me.ColumnHeader1, Me.ColumnHeader2, Me.size, Me.ColumnHeader37, Me.ColumnHeader43, Me.ColumnHeader49})
        Me.ListViewcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewcom.FullRowSelect = True
        Me.ListViewcom.GridLines = True
        Me.ListViewcom.HideSelection = False
        Me.ListViewcom.Location = New System.Drawing.Point(6, 56)
        Me.ListViewcom.Name = "ListViewcom"
        Me.ListViewcom.Size = New System.Drawing.Size(725, 543)
        Me.ListViewcom.TabIndex = 117
        Me.ListViewcom.UseCompatibleStateImageBehavior = False
        Me.ListViewcom.View = System.Windows.Forms.View.Details
        '
        'name_container
        '
        Me.name_container.Text = "ID"
        '
        'agent
        '
        Me.agent.Text = "Type"
        Me.agent.Width = 80
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Brand"
        Me.ColumnHeader1.Width = 75
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Detail"
        Me.ColumnHeader2.Width = 160
        '
        'size
        '
        Me.size.Text = "Status"
        Me.size.Width = 70
        '
        'ColumnHeader37
        '
        Me.ColumnHeader37.Text = "Officer Add"
        Me.ColumnHeader37.Width = 90
        '
        'ColumnHeader43
        '
        Me.ColumnHeader43.Text = "Section"
        Me.ColumnHeader43.Width = 90
        '
        'ColumnHeader49
        '
        Me.ColumnHeader49.Text = "Officer Edit"
        Me.ColumnHeader49.Width = 90
        '
        'txtsearchcom
        '
        Me.txtsearchcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtsearchcom.Location = New System.Drawing.Point(86, 14)
        Me.txtsearchcom.Name = "txtsearchcom"
        Me.txtsearchcom.Size = New System.Drawing.Size(305, 34)
        Me.txtsearchcom.TabIndex = 115
        '
        'btnsearchcom
        '
        Me.btnsearchcom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearchcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearchcom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearchcom.Location = New System.Drawing.Point(397, 13)
        Me.btnsearchcom.Name = "btnsearchcom"
        Me.btnsearchcom.Size = New System.Drawing.Size(68, 34)
        Me.btnsearchcom.TabIndex = 116
        Me.btnsearchcom.Text = "Search"
        Me.btnsearchcom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearchcom.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 34)
        Me.Label2.TabIndex = 114
        Me.Label2.Text = "Search :"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Label72)
        Me.TabPage6.Controls.Add(Me.txtshowcomrent)
        Me.TabPage6.Controls.Add(Me.txtsearchcomrent)
        Me.TabPage6.Controls.Add(Me.btnsearchcomrent)
        Me.TabPage6.Controls.Add(Me.Label54)
        Me.TabPage6.Controls.Add(Me.ListViewcomrent)
        Me.TabPage6.Location = New System.Drawing.Point(4, 35)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1383, 605)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Computer Rent"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(617, 24)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(45, 26)
        Me.Label72.TabIndex = 183
        Me.Label72.Text = "Count"
        '
        'txtshowcomrent
        '
        Me.txtshowcomrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowcomrent.Location = New System.Drawing.Point(668, 21)
        Me.txtshowcomrent.Name = "txtshowcomrent"
        Me.txtshowcomrent.ReadOnly = True
        Me.txtshowcomrent.Size = New System.Drawing.Size(54, 34)
        Me.txtshowcomrent.TabIndex = 182
        '
        'txtsearchcomrent
        '
        Me.txtsearchcomrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchcomrent.Location = New System.Drawing.Point(86, 17)
        Me.txtsearchcomrent.Name = "txtsearchcomrent"
        Me.txtsearchcomrent.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchcomrent.TabIndex = 134
        '
        'btnsearchcomrent
        '
        Me.btnsearchcomrent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearchcomrent.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearchcomrent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearchcomrent.Location = New System.Drawing.Point(385, 17)
        Me.btnsearchcomrent.Name = "btnsearchcomrent"
        Me.btnsearchcomrent.Size = New System.Drawing.Size(68, 34)
        Me.btnsearchcomrent.TabIndex = 135
        Me.btnsearchcomrent.Text = "Search"
        Me.btnsearchcomrent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearchcomrent.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label54.Location = New System.Drawing.Point(6, 15)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(74, 34)
        Me.Label54.TabIndex = 133
        Me.Label54.Text = "Search :"
        '
        'ListViewcomrent
        '
        Me.ListViewcomrent.BackColor = System.Drawing.Color.White
        Me.ListViewcomrent.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader23, Me.ColumnHeader24, Me.ColumnHeader25, Me.ColumnHeader26, Me.ColumnHeader27, Me.ColumnHeader38, Me.ColumnHeader44, Me.ColumnHeader50})
        Me.ListViewcomrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewcomrent.FullRowSelect = True
        Me.ListViewcomrent.GridLines = True
        Me.ListViewcomrent.HideSelection = False
        Me.ListViewcomrent.Location = New System.Drawing.Point(6, 61)
        Me.ListViewcomrent.Name = "ListViewcomrent"
        Me.ListViewcomrent.Size = New System.Drawing.Size(718, 538)
        Me.ListViewcomrent.TabIndex = 132
        Me.ListViewcomrent.UseCompatibleStateImageBehavior = False
        Me.ListViewcomrent.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader23
        '
        Me.ColumnHeader23.Text = "ID"
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "Type"
        Me.ColumnHeader24.Width = 80
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "Brand"
        Me.ColumnHeader25.Width = 80
        '
        'ColumnHeader26
        '
        Me.ColumnHeader26.Text = "Detail"
        Me.ColumnHeader26.Width = 140
        '
        'ColumnHeader27
        '
        Me.ColumnHeader27.Text = "Status"
        Me.ColumnHeader27.Width = 70
        '
        'ColumnHeader38
        '
        Me.ColumnHeader38.Text = "Officer Add"
        Me.ColumnHeader38.Width = 90
        '
        'ColumnHeader44
        '
        Me.ColumnHeader44.Text = "Section"
        Me.ColumnHeader44.Width = 100
        '
        'ColumnHeader50
        '
        Me.ColumnHeader50.Text = "Officer Edit"
        Me.ColumnHeader50.Width = 90
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label31)
        Me.TabPage2.Controls.Add(Me.txtshowprin)
        Me.TabPage2.Controls.Add(Me.txtsearchprin)
        Me.TabPage2.Controls.Add(Me.btnsearch_prin)
        Me.TabPage2.Controls.Add(Me.Label49)
        Me.TabPage2.Controls.Add(Me.ListViewprin)
        Me.TabPage2.Location = New System.Drawing.Point(4, 35)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1383, 605)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Printer"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(671, 23)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(45, 26)
        Me.Label31.TabIndex = 204
        Me.Label31.Text = "Count"
        '
        'txtshowprin
        '
        Me.txtshowprin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowprin.Location = New System.Drawing.Point(722, 20)
        Me.txtshowprin.Name = "txtshowprin"
        Me.txtshowprin.ReadOnly = True
        Me.txtshowprin.Size = New System.Drawing.Size(54, 34)
        Me.txtshowprin.TabIndex = 203
        '
        'txtsearchprin
        '
        Me.txtsearchprin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchprin.Location = New System.Drawing.Point(86, 14)
        Me.txtsearchprin.Name = "txtsearchprin"
        Me.txtsearchprin.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchprin.TabIndex = 201
        '
        'btnsearch_prin
        '
        Me.btnsearch_prin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_prin.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_prin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_prin.Location = New System.Drawing.Point(385, 14)
        Me.btnsearch_prin.Name = "btnsearch_prin"
        Me.btnsearch_prin.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_prin.TabIndex = 202
        Me.btnsearch_prin.Text = "Search"
        Me.btnsearch_prin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_prin.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label49.Location = New System.Drawing.Point(6, 12)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(74, 34)
        Me.Label49.TabIndex = 200
        Me.Label49.Text = "Search :"
        '
        'ListViewprin
        '
        Me.ListViewprin.BackColor = System.Drawing.Color.White
        Me.ListViewprin.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader39, Me.ColumnHeader45, Me.ColumnHeader51})
        Me.ListViewprin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewprin.FullRowSelect = True
        Me.ListViewprin.GridLines = True
        Me.ListViewprin.HideSelection = False
        Me.ListViewprin.Location = New System.Drawing.Point(6, 60)
        Me.ListViewprin.Name = "ListViewprin"
        Me.ListViewprin.Size = New System.Drawing.Size(770, 539)
        Me.ListViewprin.TabIndex = 199
        Me.ListViewprin.UseCompatibleStateImageBehavior = False
        Me.ListViewprin.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "ID"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Type"
        Me.ColumnHeader4.Width = 90
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Brand"
        Me.ColumnHeader5.Width = 75
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Detail"
        Me.ColumnHeader6.Width = 150
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Status"
        Me.ColumnHeader7.Width = 90
        '
        'ColumnHeader39
        '
        Me.ColumnHeader39.Text = "Officer Add"
        Me.ColumnHeader39.Width = 110
        '
        'ColumnHeader45
        '
        Me.ColumnHeader45.Text = "Section"
        Me.ColumnHeader45.Width = 100
        '
        'ColumnHeader51
        '
        Me.ColumnHeader51.Text = "Officer Edit"
        Me.ColumnHeader51.Width = 90
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.txtshowmonitor)
        Me.TabPage3.Controls.Add(Me.txtsearchmonitor)
        Me.TabPage3.Controls.Add(Me.btnsearch_monitor)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.ListViewmonitor)
        Me.TabPage3.Location = New System.Drawing.Point(4, 35)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1383, 605)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Monitor"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(717, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 26)
        Me.Label4.TabIndex = 210
        Me.Label4.Text = "Count"
        '
        'txtshowmonitor
        '
        Me.txtshowmonitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowmonitor.Location = New System.Drawing.Point(768, 21)
        Me.txtshowmonitor.Name = "txtshowmonitor"
        Me.txtshowmonitor.ReadOnly = True
        Me.txtshowmonitor.Size = New System.Drawing.Size(54, 34)
        Me.txtshowmonitor.TabIndex = 209
        '
        'txtsearchmonitor
        '
        Me.txtsearchmonitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchmonitor.Location = New System.Drawing.Point(86, 18)
        Me.txtsearchmonitor.Name = "txtsearchmonitor"
        Me.txtsearchmonitor.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchmonitor.TabIndex = 207
        '
        'btnsearch_monitor
        '
        Me.btnsearch_monitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_monitor.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_monitor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_monitor.Location = New System.Drawing.Point(385, 18)
        Me.btnsearch_monitor.Name = "btnsearch_monitor"
        Me.btnsearch_monitor.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_monitor.TabIndex = 208
        Me.btnsearch_monitor.Text = "Search"
        Me.btnsearch_monitor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_monitor.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 34)
        Me.Label5.TabIndex = 206
        Me.Label5.Text = "Search :"
        '
        'ListViewmonitor
        '
        Me.ListViewmonitor.BackColor = System.Drawing.Color.White
        Me.ListViewmonitor.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader40, Me.ColumnHeader46, Me.ColumnHeader52})
        Me.ListViewmonitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewmonitor.FullRowSelect = True
        Me.ListViewmonitor.GridLines = True
        Me.ListViewmonitor.HideSelection = False
        Me.ListViewmonitor.Location = New System.Drawing.Point(6, 62)
        Me.ListViewmonitor.Name = "ListViewmonitor"
        Me.ListViewmonitor.Size = New System.Drawing.Size(815, 540)
        Me.ListViewmonitor.TabIndex = 205
        Me.ListViewmonitor.UseCompatibleStateImageBehavior = False
        Me.ListViewmonitor.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "ID"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Type"
        Me.ColumnHeader9.Width = 100
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Brand"
        Me.ColumnHeader10.Width = 100
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Detail"
        Me.ColumnHeader11.Width = 150
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Status"
        Me.ColumnHeader12.Width = 100
        '
        'ColumnHeader40
        '
        Me.ColumnHeader40.Text = "Officer Add"
        Me.ColumnHeader40.Width = 110
        '
        'ColumnHeader46
        '
        Me.ColumnHeader46.Text = "Section"
        Me.ColumnHeader46.Width = 100
        '
        'ColumnHeader52
        '
        Me.ColumnHeader52.Text = "Officer Edit"
        Me.ColumnHeader52.Width = 90
        '
        'btngoadd
        '
        Me.btngoadd.Controls.Add(Me.ListViewother)
        Me.btngoadd.Controls.Add(Me.Label3)
        Me.btngoadd.Controls.Add(Me.txtshowother)
        Me.btngoadd.Controls.Add(Me.txtsearchother)
        Me.btngoadd.Controls.Add(Me.btnsearch_other)
        Me.btngoadd.Controls.Add(Me.Label33)
        Me.btngoadd.Location = New System.Drawing.Point(4, 35)
        Me.btngoadd.Name = "btngoadd"
        Me.btngoadd.Padding = New System.Windows.Forms.Padding(3)
        Me.btngoadd.Size = New System.Drawing.Size(1383, 605)
        Me.btngoadd.TabIndex = 3
        Me.btngoadd.Text = "Other"
        Me.btngoadd.UseVisualStyleBackColor = True
        '
        'ListViewother
        '
        Me.ListViewother.BackColor = System.Drawing.Color.White
        Me.ListViewother.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader13, Me.ColumnHeader14, Me.ColumnHeader15, Me.ColumnHeader16, Me.ColumnHeader17, Me.ColumnHeader35, Me.ColumnHeader36, Me.ColumnHeader41, Me.ColumnHeader47, Me.ColumnHeader53})
        Me.ListViewother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewother.FullRowSelect = True
        Me.ListViewother.GridLines = True
        Me.ListViewother.HideSelection = False
        Me.ListViewother.Location = New System.Drawing.Point(6, 60)
        Me.ListViewother.Name = "ListViewother"
        Me.ListViewother.Size = New System.Drawing.Size(1015, 539)
        Me.ListViewother.TabIndex = 1061
        Me.ListViewother.UseCompatibleStateImageBehavior = False
        Me.ListViewother.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "ID"
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Type"
        Me.ColumnHeader14.Width = 90
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Brand"
        Me.ColumnHeader15.Width = 100
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Detail"
        Me.ColumnHeader16.Width = 120
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Status"
        Me.ColumnHeader17.Width = 90
        '
        'ColumnHeader35
        '
        Me.ColumnHeader35.Text = "Serial Number"
        Me.ColumnHeader35.Width = 150
        '
        'ColumnHeader36
        '
        Me.ColumnHeader36.Text = "Type Data"
        Me.ColumnHeader36.Width = 100
        '
        'ColumnHeader41
        '
        Me.ColumnHeader41.Text = "Officer Add"
        Me.ColumnHeader41.Width = 120
        '
        'ColumnHeader47
        '
        Me.ColumnHeader47.Text = "Section"
        Me.ColumnHeader47.Width = 90
        '
        'ColumnHeader53
        '
        Me.ColumnHeader53.Text = "Ofiicer Edit"
        Me.ColumnHeader53.Width = 90
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(916, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 26)
        Me.Label3.TabIndex = 216
        Me.Label3.Text = "Count"
        '
        'txtshowother
        '
        Me.txtshowother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowother.Location = New System.Drawing.Point(967, 20)
        Me.txtshowother.Name = "txtshowother"
        Me.txtshowother.ReadOnly = True
        Me.txtshowother.Size = New System.Drawing.Size(54, 34)
        Me.txtshowother.TabIndex = 215
        '
        'txtsearchother
        '
        Me.txtsearchother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchother.Location = New System.Drawing.Point(86, 15)
        Me.txtsearchother.Name = "txtsearchother"
        Me.txtsearchother.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchother.TabIndex = 213
        '
        'btnsearch_other
        '
        Me.btnsearch_other.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_other.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_other.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_other.Location = New System.Drawing.Point(385, 13)
        Me.btnsearch_other.Name = "btnsearch_other"
        Me.btnsearch_other.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_other.TabIndex = 214
        Me.btnsearch_other.Text = "Search"
        Me.btnsearch_other.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_other.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label33.Location = New System.Drawing.Point(6, 13)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(74, 34)
        Me.Label33.TabIndex = 212
        Me.Label33.Text = "Search :"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label45)
        Me.TabPage5.Controls.Add(Me.txtshowlicense)
        Me.TabPage5.Controls.Add(Me.txtsearchlicense)
        Me.TabPage5.Controls.Add(Me.btnsearch_license)
        Me.TabPage5.Controls.Add(Me.Label70)
        Me.TabPage5.Controls.Add(Me.ListViewlicense)
        Me.TabPage5.Location = New System.Drawing.Point(4, 35)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(1383, 605)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "License"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(671, 24)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(45, 26)
        Me.Label45.TabIndex = 222
        Me.Label45.Text = "Count"
        '
        'txtshowlicense
        '
        Me.txtshowlicense.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowlicense.Location = New System.Drawing.Point(722, 21)
        Me.txtshowlicense.Name = "txtshowlicense"
        Me.txtshowlicense.ReadOnly = True
        Me.txtshowlicense.Size = New System.Drawing.Size(54, 34)
        Me.txtshowlicense.TabIndex = 221
        '
        'txtsearchlicense
        '
        Me.txtsearchlicense.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchlicense.Location = New System.Drawing.Point(86, 17)
        Me.txtsearchlicense.Name = "txtsearchlicense"
        Me.txtsearchlicense.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchlicense.TabIndex = 219
        '
        'btnsearch_license
        '
        Me.btnsearch_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_license.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_license.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_license.Location = New System.Drawing.Point(385, 17)
        Me.btnsearch_license.Name = "btnsearch_license"
        Me.btnsearch_license.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_license.TabIndex = 220
        Me.btnsearch_license.Text = "Search"
        Me.btnsearch_license.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_license.UseVisualStyleBackColor = True
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.Location = New System.Drawing.Point(6, 15)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(74, 34)
        Me.Label70.TabIndex = 218
        Me.Label70.Text = "Search :"
        '
        'ListViewlicense
        '
        Me.ListViewlicense.BackColor = System.Drawing.Color.White
        Me.ListViewlicense.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader18, Me.ColumnHeader19, Me.ColumnHeader20, Me.ColumnHeader21, Me.ColumnHeader22, Me.ColumnHeader42, Me.ColumnHeader48, Me.ColumnHeader54})
        Me.ListViewlicense.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewlicense.FullRowSelect = True
        Me.ListViewlicense.GridLines = True
        Me.ListViewlicense.HideSelection = False
        Me.ListViewlicense.Location = New System.Drawing.Point(6, 61)
        Me.ListViewlicense.Name = "ListViewlicense"
        Me.ListViewlicense.Size = New System.Drawing.Size(769, 538)
        Me.ListViewlicense.TabIndex = 217
        Me.ListViewlicense.UseCompatibleStateImageBehavior = False
        Me.ListViewlicense.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader18
        '
        Me.ColumnHeader18.Text = "ID"
        '
        'ColumnHeader19
        '
        Me.ColumnHeader19.Text = "Type"
        Me.ColumnHeader19.Width = 100
        '
        'ColumnHeader20
        '
        Me.ColumnHeader20.Text = "Brand"
        Me.ColumnHeader20.Width = 75
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Detail"
        Me.ColumnHeader21.Width = 150
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Status"
        Me.ColumnHeader22.Width = 90
        '
        'ColumnHeader42
        '
        Me.ColumnHeader42.Text = "Officer Add"
        Me.ColumnHeader42.Width = 100
        '
        'ColumnHeader48
        '
        Me.ColumnHeader48.Text = "Section"
        Me.ColumnHeader48.Width = 100
        '
        'ColumnHeader54
        '
        Me.ColumnHeader54.Text = "Officer Edit"
        Me.ColumnHeader54.Width = 90
        '
        'frm_help_main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1257, 702)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "frm_help_main"
        Me.Text = "frm_help_main"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.btngoadd.ResumeLayout(False)
        Me.btngoadd.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents txtshowcom As System.Windows.Forms.TextBox
    Friend WithEvents ListViewcom As System.Windows.Forms.ListView
    Friend WithEvents name_container As System.Windows.Forms.ColumnHeader
    Friend WithEvents agent As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents size As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader37 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader43 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader49 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtsearchcom As System.Windows.Forms.TextBox
    Friend WithEvents btnsearchcom As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents txtshowcomrent As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchcomrent As System.Windows.Forms.TextBox
    Friend WithEvents btnsearchcomrent As System.Windows.Forms.Button
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents ListViewcomrent As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader23 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader24 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader25 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader26 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader27 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader38 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader44 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader50 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtshowprin As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchprin As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_prin As System.Windows.Forms.Button
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents ListViewprin As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader39 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader45 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader51 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtshowmonitor As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchmonitor As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_monitor As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ListViewmonitor As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader40 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader46 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader52 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btngoadd As System.Windows.Forms.TabPage
    Friend WithEvents ListViewother As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader35 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader36 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader41 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader47 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader53 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtshowother As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchother As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_other As System.Windows.Forms.Button
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtshowlicense As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchlicense As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_license As System.Windows.Forms.Button
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents ListViewlicense As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader18 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader19 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader20 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader42 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader48 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader54 As System.Windows.Forms.ColumnHeader
End Class
