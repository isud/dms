﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_ADD_DEVICE
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_add_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล
        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)
            Me.Close()
        End Try

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        mySqlCommand.CommandText = "Select * from member where user_id like '" + id_user + "';"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try

            mySqlReader = mySqlCommand.ExecuteReader
            While mySqlReader.Read()
                Label87.Text = " " + mySqlReader("name") + "  " + mySqlReader("lastname")
                Label89.Text = " " + mySqlReader("position_name")
                Label92.Text = " " + mySqlReader("user_id")

            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Sql.Close()

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mysqlcommand.CommandText = "SELECT * FROM brand order by idbrand;"
        mysqlcommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mysqlcommand

        Try
            mysqlreader = mysqlcommand.ExecuteReader
            ComboBoxbrand1.Items.Clear()
            ComboBoxbrand2.Items.Clear()
            ComboBoxbrand3.Items.Clear()
            ComboBoxbrand4.Items.Clear()
            ComboBoxbrand5.Items.Clear()
            While (mysqlreader.Read())
                ComboBoxbrand1.Items.Add(mySqlReader("brand_name"))
                ComboBoxbrand2.Items.Add(mysqlreader("brand_name"))
                ComboBoxbrand3.Items.Add(mysqlreader("brand_name"))
                ComboBoxbrand4.Items.Add(mySqlReader("brand_name"))
                ComboBoxbrand5.Items.Add(mySqlReader("brand_name"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If


        mySqlCommand.CommandText = "SELECT * FROM type order by idtype ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ComboBoxtype_other.Items.Clear()
            While (mySqlReader.Read())
                ComboBoxtype_other.Items.Add(mySqlReader("type_name"))
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub btn_save_com_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_com.Click
        If txt1_cpu.Text <> "" Or txt_store.Text = "" Then

            savedatacom()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub savedatacom()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,model,c_cpu,c_mainboard,c_ram,c_hdd,c_vga,c_ps,c_cd,c_ip,c_comname,c_windows,c_office,c_case,serial,other,note,price,brand_data,state_device,date,position,detail,startbuy,warrant,idsection,store,section_name) VALUES ('Computer','" & txt1_model.Text & "', '" & txt1_cpu.Text & "', '" & txt1_mainboard.Text & "','" & txt1_ram.Text & "','" & txt1_hdd.Text & "','" & txt1_vga.Text & "','" & txt1_ps.Text & "','" & txt1_cd.Text & "','" & txt1_ip.Text & "','" & txt1_comname.Text & "','" & txt1_windows.Text & "','" & txt1_office.Text & "','" & txt1_case.Text & "','" & txt1_serial.Text & "','" & txt1_other.Text & "','" & txt1_note_com.Text & "','" & txt1_price.Text & "','" & ComboBoxbrand1.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','" + id_user + "','" & txt1_detail.Text & "','" + DateTimePicker5.Value.Day.ToString + "/" + DateTimePicker5.Value.Month.ToString + "/" + DateTimePicker5.Value.Year.ToString + "','" + DateTimePicker6.Value.Day.ToString + "/" + DateTimePicker6.Value.Month.ToString + "/" + DateTimePicker6.Value.Year.ToString + "','0','" & txt_store.Text & "','ว่าง');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย ", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        cleardatacom()

    End Sub
    Private Sub cleardatacom()
        txt1_cpu.Text = ""
        txt1_case.Text = ""
        txt1_cd.Text = ""
        txt1_comname.Text = ""
        txt1_detail.Text = ""
        txt1_hdd.Text = ""
        txt1_ip.Text = ""
        txt1_mainboard.Text = ""
        txt1_model.Text = ""
        txt1_note_com.Text = ""
        txt1_office.Text = ""
        txt1_other.Text = ""
        txt1_price.Text = ""
        txt1_ps.Text = ""
        txt1_ram.Text = ""
        txt1_serial.Text = ""
        txt1_vga.Text = ""
        txt1_windows.Text = ""
        ComboBoxbrand1.Text = ""
        txt_store.Text = ""
    End Sub

    Private Sub btn_save_comr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_comr.Click
        If txt2_cpu.Text <> "" Then

            savedatacom2()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatacom2()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,model,c_cpu,c_mainboard,c_ram,c_hdd,c_vga,c_ps,c_cd,c_ip,c_comname,c_windows,c_office,c_case,serial,other,note,price,brand_data,state_device,startbuy,warrant,position,detail,idsection,store,section_name,position) VALUES ('Computer Rent','" & txt2_model.Text & "', '" & txt2_cpu.Text & "', '" & txt2_mainboard.Text & "','" & txt2_ram.Text & "','" & txt2_hdd.Text & "','" & txt2_vga.Text & "','" & txt2_ps.Text & "','" & txt2_cd.Text & "','" & txt2_ip.Text & "','" & txt2_comname.Text & "','" & txt2_windows.Text & "','" & txt2_office.Text & "','" & txt2_case.Text & "','" & txt2_serial.Text & "','" & txt2_other.Text & "','" & txt2_note_comr.Text & "','" & txt2_price.Text & "','" & ComboBoxbrand2.Text & "','ว่าง','" + DateTimePicker3.Value.Day.ToString + "/" + DateTimePicker3.Value.Month.ToString + "/" + DateTimePicker3.Value.Year.ToString + "','" + DateTimePicker4.Value.Day.ToString + "/" + DateTimePicker4.Value.Month.ToString + "/" + DateTimePicker4.Value.Year.ToString + "','" + id_user + "','" & txt2_detail.Text & "','0','" & txt_store_rent.Text & "','ว่าง','" + id_user + "');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย ", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        cleardatacom2()
    End Sub
    Private Sub cleardatacom2()
        txt2_cpu.Text = ""
        txt2_case.Text = ""
        txt2_cd.Text = ""
        txt2_comname.Text = ""
        txt2_detail.Text = ""
        txt2_hdd.Text = ""
        txt2_ip.Text = ""
        txt2_mainboard.Text = ""
        txt2_model.Text = ""
        txt2_note_comr.Text = ""
        txt2_office.Text = ""
        txt2_other.Text = ""
        txt2_price.Text = ""
        txt2_ps.Text = ""
        txt2_ram.Text = ""
        txt2_serial.Text = ""
        txt2_vga.Text = ""
        txt2_windows.Text = ""
        ComboBoxbrand2.Text = ""
        txt_store_rent.Text = ""
    End Sub

    Private Sub btn_cancel_com_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel_com.Click
        cleardatacom()
    End Sub

    Private Sub btnback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnback.Click
        Me.Hide()
    End Sub

    Private Sub btn_save_prin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_prin.Click
        If txt3_model.Text <> "" Then

            savedataprin4()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedataprin4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim printer As String

        If Radiolaserc.Checked = True Then
            printer = "Laser Printer"
        End If
        If Radiodot.Checked = True Then
            printer = "Dot-matrix Printer"
        End If
        If Radioinkjet.Checked = True Then
            printer = "Inkjet Printer"
        End If


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,model,serial,price,detail,p_type,note,state_device,date,idsection,section_name,position) VALUES ('Printer','" & ComboBoxbrand3.Text & "', '" & txt3_model.Text & "','" & txt3_serial.Text & "','" & txt3_price.Text & "','" & txt3_detail.Text & "','" & printer & "','" & txt3_note_prin.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','0','ว่าง','" + id_user + "');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
        cleardataprin3()
    End Sub
    Private Sub cleardataprin3()
        Radiolaserc.Checked = False
        Radiodot.Checked = False
        Radioinkjet.Checked = False
        ComboBoxbrand3.Text = ""
        txt3_detail.Text = ""
        txt3_model.Text = ""
        txt3_note_prin.Text = ""
        txt3_price.Text = ""
        txt3_serial.Text = ""
    End Sub

    Private Sub btn_cencel_prin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cencel_prin.Click
        cleardataprin3()
    End Sub

    Private Sub btn_cancel_comr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel_comr.Click
        cleardatacom2()
    End Sub

    Private Sub btn_save_monitor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_monitor.Click
        If txt4_model.Text <> "" Then

            savedatamonitor4()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatamonitor4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            If txt4_model.Text <> "" Then
                mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,model,m_size,detail,serial,price,note,state_device,date,idsection,section_name,position) VALUES ('Monitor','" & ComboBoxbrand4.Text & "', '" & txt4_model.Text & "', '" & txt4_size.Text & "','" & txt4_detail.Text & "','" & txt4_serial.Text & "','" & txt4_price.Text & "','" & txt4_note_monitor.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','0','ว่าง','" + id_user + "');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        cleardatamonitor4()
    End Sub

    Private Sub btn_cancel_monitor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel_monitor.Click
        txt4_detail.Text = ""
        txt4_model.Text = ""
        txt4_note_monitor.Text = ""
        txt4_price.Text = ""
        txt4_serial.Text = ""
        txt4_size.Text = ""
        ComboBoxbrand4.Text = ""
    End Sub
    Private Sub cleardatamonitor4()
        txt4_detail.Text = ""
        txt4_model.Text = ""
        txt4_note_monitor.Text = ""
        txt4_price.Text = ""
        txt4_serial.Text = ""
        txt4_size.Text = ""
        ComboBoxbrand4.Text = ""
    End Sub

    Private Sub btn_save_other_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_other.Click
        If txt5_model.Text <> "" Then

            savedataspare5()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedataspare5()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            If txt5_model.Text <> "" Then
                mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,model,detail,serial,type_data,price,note,state_device,date,idsection,position) VALUES ('Other','" & ComboBoxbrand5.Text & "', '" & txt5_model.Text & "', '" & txt5_detail.Text & "','" & txt5_serial.Text & "','" & ComboBoxtype_other.Text & "','" & txt5_price.Text & "','" & txt5_note_other.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','0','ว่าง','" + id_user + "');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
        cleardataother5()
    End Sub

    Private Sub cleardataother5()
        txt5_detail.Text = ""
        txt5_model.Text = ""
        txt5_note_other.Text = ""
        txt5_price.Text = ""
        txt5_serial.Text = ""
        ComboBoxtype_other.Text = ""
        ComboBoxbrand5.Text = ""

    End Sub

    Private Sub btn_cancel_other_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel_other.Click
        cleardataother5()

    End Sub

    Private Sub btn_save_license_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_license.Click
        If txt6_brand.Text <> "" And Radioapp.Checked <> False Or Radioos.Checked <> False Then

            savedatalicens6()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatalicens6()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim windows_application As String
        If Radioos.Checked = True Then
            windows_application = "OS"
        End If
        If Radioapp.Checked = True Then
            windows_application = "Application"
        End If

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,price,detail,amount,windows_application,date,startbuy,warrant,note,state_device,position) VALUES ('License','" & txt6_brand.Text & "','" & txt6_price.Text & "','" & txt6_detail.Text & "','" & txt6_amount.Text & "','" & windows_application & "','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','" & DateTimePicker1.Value.Date.ToString & "','" & DateTimePicker2.Value.Date.ToString & "','" & txt6_note_license.Text & "','ยังไม่ถูกใช้งาน','" + id_user + "');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
        cleardatalicense6()
    End Sub
    Private Sub cleardatalicense6()
        txt6_amount.Text = ""
        txt6_brand.Text = ""
        txt6_detail.Text = ""
        txt6_note_license.Text = ""
        txt6_price.Text = ""
        Radioapp.Checked = False
        Radioos.Checked = False
    End Sub

    Private Sub btn_cancel_license_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel_license.Click
        cleardatalicense6()
    End Sub
End Class