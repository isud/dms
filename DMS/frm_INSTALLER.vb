﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_INSTALLER
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Protected ConnectionDB As connectdb = connectdb.NewConnection
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_INSTALLER_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล
        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)
            Me.Close()
        End Try
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        'mySqlCommand.CommandText = "Select * from member where user_id like '" + id_user + "';"
        'mySqlCommand.Connection = Sql
        'mySqlAdaptor.SelectCommand = mySqlCommand
        mySqlReader = ConnectionDB.ExecuteReader("Select * from member where user_id like '" + id_user + "'")
        Try
            'mySqlReader = mySqlCommand.ExecuteReader
            While mySqlReader.Read()
                Label87.Text = " " + mySqlReader("name") + "  " + mySqlReader("lastname")
                Label89.Text = " " + mySqlReader("position_name")
                Label92.Text = " " + mySqlReader("user_id")
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Sql.Close()
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where state_device = 'ว่าง' order by iddata_device ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewshow.Items.Clear()

            While (mySqlReader.Read())

                With ListViewshow.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("serial"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewfloor.Items.Clear()
            While (mySqlReader.Read())

                With ListViewfloor.Items.Add(mySqlReader("idfloor"))
                    .SubItems.Add(mySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()


    End Sub

    Private Sub ListViewshow_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewshow.DoubleClick
        add_aoi()
        'เรียกข้อมูลอีอ้อย
        Dim Listview3_Index As Integer
        Listview3_Index = ListViewshow.SelectedIndices(0)
        'ListViewshow.Items.RemoveAt(Listview3_Index)
    End Sub
    Private Sub add_aoi()
        Dim iddata_device As String = ListViewshow.SelectedItems(0).SubItems(0).Text
        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If
        Dim CheckIndex As Integer
        Dim i As Integer
        Dim CheckData As Boolean

        CheckData = False
        CheckIndex = ListViewshowdata.Items.Count


        For i = 0 To CheckIndex - 1
            If iddata_device <> ListViewshowdata.Items(i).SubItems(0).Text Then

                CheckData = True

            Else

                CheckData = False
                Exit For
            End If
        Next i

        If CheckData = False And CheckIndex <> 0 Then
            MsgBox("มีข้อมูลนี้อยู่แล้ว", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Warning Message")

        End If

        If CheckData = True Or CheckIndex = 0 Then

            Dim mySqlCommand As New MySqlCommand
            Dim mySqlAdaptor As New MySqlDataAdapter
            Dim mySqlReader As MySqlDataReader

            mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device= '" & iddata_device & "';"
            mySqlCommand.Connection = sql
            mySqlAdaptor.SelectCommand = mySqlCommand

            Try
                mySqlReader = mySqlCommand.ExecuteReader
                While (mySqlReader.Read())

                    With ListViewshowdata.Items.Add(mySqlReader("iddata_device"))
                         If mySqlReader("data_type") = "Computer" Then
                            .SubItems.Add("Computer")
                        End If
                        If mySqlReader("data_type") = "Computer Rent" Then
                            .SubItems.Add("Computer Rent")
                        End If
                        If mySqlReader("data_type") = "Printer" Then
                            .SubItems.Add("Printer")
                        End If
                        If mySqlReader("data_type") = "Monitor" Then
                            .SubItems.Add("Monitor")
                        End If
                        If mySqlReader("data_type") = "Other" Then
                            .SubItems.Add("Other")
                        End If
                        If mySqlReader("data_type") = "License" Then
                            .SubItems.add("License")
                        End If
                        .SubItems.Add(mySqlReader("brand_data"))

                         If mySqlReader("data_type") = "Computer" Then
                            .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                        End If
                        If mySqlReader("data_type") = "Computer Rent" Then
                            .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                        End If
                        If mySqlReader("data_type") = "Printer" Then
                            .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                        End If
                        If mySqlReader("data_type") = "Monitor" Then
                            .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                        End If
                        If mySqlReader("data_type") = "Other" Then
                            .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                        End If
                        If mySqlReader("data_type") = "License" Then
                            .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                        End If
                        If mySqlReader("state_device") IsNot DBNull.Value Then
                            .SubItems.Add(mySqlReader("state_device"))
                        End If
                    End With
                End While
            Catch ex As Exception
                MsgBox(ex.ToString)
                MsgBox("ห้ามใส่เครื่องหมาย ' ในช่องค้นหา", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Warning Message")
            End Try
            sql.Close()
        End If
    End Sub

    Private Sub ListViewshowdata_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewshowdata.DoubleClick
        Dim Listview3_Index As Integer
        Listview3_Index = ListViewshowdata.SelectedIndices(0)
        ListViewshowdata.Items.RemoveAt(Listview3_Index)
    End Sub

    Private Sub ListViewfloor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewfloor.Click
        idkey = ListViewfloor.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim count As Integer = 1

        mySqlCommand.CommandText = "SELECT * FROM section where idfloor = '" & idkey & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewsection.Items.Clear()
            While (mySqlReader.Read())

                With ListViewsection.Items.Add(mySqlReader("idsection"))
                    .subitems.add(mySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub ListViewsection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewsection.Click
        idkey = ListViewsection.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim count As Integer = 1

        mySqlCommand.CommandText = "SELECT * FROM section where idsection = '" & idkey & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                txt_id_section.Text = mySqlReader("idsection")
                txtsection.Text = mySqlReader("section_name")

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    'Private Sub ListViewlocat_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    idkey = ListViewlocat.SelectedItems(0).SubItems(0).Text

    '    If Sql.State = ConnectionState.Closed Then
    '        Sql.Open()
    '    End If

    '    Dim mySqlCommand As New MySqlCommand
    '    Dim mySqlAdaptor As New MySqlDataAdapter
    '    Dim mySqlReader As MySqlDataReader

    '    mySqlCommand.CommandText = "SELECT * FROM location  where idlocation = '" & idkey & "' ;"
    '    mySqlCommand.Connection = Sql
    '    mySqlAdaptor.SelectCommand = mySqlCommand

    '    Try
    '        mySqlReader = mySqlCommand.ExecuteReader
    '        While (mySqlReader.Read())
    '            txtidlocation.Text = mySqlReader("idlocation")
    '            txtlocation.Text = mySqlReader("location_name")

    '        End While
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    '    Sql.Close()
    'End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If txt_id_section.Text <> "" And ListViewshowdata.Items.Count > 0 Then

            savedata()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตาราง", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub savedata()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object
        Dim commandText As String
        Dim commandText2 As String
        Dim commandText3 As String
        Dim counter1 As Integer = 0
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")

        For counter1 = 0 To ListViewshowdata.Items.Count - 1

            If Sql.State = ConnectionState.Closed Then
                Sql.Open()
            End If

            commandText = "UPDATE data_device SET state_device = 'ถูกใช้งาน' ,idsection = '" & txt_id_section.Text & "',section_data = '" & txtsection.Text & "'  WHERE iddata_device = '" & ListViewshowdata.Items(counter1).SubItems(0).Text & "' "
            mySqlCommand.CommandText = commandText
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            Sql.Close()

            If Sql.State = ConnectionState.Closed Then
                Sql.Open()
            End If

            Try
                mySqlCommand.CommandText = "INSERT INTO transfer_his (iddata_device,idsection,time_instal) VALUES ('" & ListViewshowdata.Items(counter1).SubItems(0).Text & "',  '" & txt_id_section.Text & "','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)

            End Try
            Sql.Close()
        Next counter1
        ListViewsection.Items.Clear()
        showdata()
    End Sub
    Private Sub showdata()

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM data_device where state_device = 'ว่าง' order by iddata_device ;"
        MySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = MySqlCommand
        Try
            mySqlReader = MySqlCommand.ExecuteReader

            ListViewshow.Items.Clear()

            While (mySqlReader.Read())

                With ListViewshow.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("serial"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        ListViewshowdata.Items.Clear()
        txt_id_section.Text = ""
        txtsection.Text = ""
        ListViewsection.Items.Clear()
    End Sub

    Private Sub btnback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnback.Click
        Me.Hide()
    End Sub
End Class
