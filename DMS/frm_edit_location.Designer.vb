﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_edit_location
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_edit_location))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_add_location = New System.Windows.Forms.TextBox()
        Me.ListViewsection = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btn_clear_location = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btn_add_location = New System.Windows.Forms.Button()
        Me.txtidfloor = New System.Windows.Forms.TextBox()
        Me.txtfloor = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtidsection = New System.Windows.Forms.TextBox()
        Me.txtsection = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txt_id_location = New System.Windows.Forms.TextBox()
        Me.txt_save_location = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_id_floor2 = New System.Windows.Forms.TextBox()
        Me.txt_floor2 = New System.Windows.Forms.TextBox()
        Me.btn_home = New System.Windows.Forms.Button()
        Me.btn_delete_location = New System.Windows.Forms.Button()
        Me.btn_save_location = New System.Windows.Forms.Button()
        Me.txt_id_section = New System.Windows.Forms.TextBox()
        Me.ListViewlocation = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txt_edit_section = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_add_location)
        Me.GroupBox1.Controls.Add(Me.ListViewsection)
        Me.GroupBox1.Controls.Add(Me.btn_clear_location)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.btn_add_location)
        Me.GroupBox1.Controls.Add(Me.txtidfloor)
        Me.GroupBox1.Controls.Add(Me.txtfloor)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtidsection)
        Me.GroupBox1.Controls.Add(Me.txtsection)
        Me.GroupBox1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(25, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(397, 715)
        Me.GroupBox1.TabIndex = 359
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "คลิกเลือก Section เพื่อบันทึก Location ::"
        '
        'txt_add_location
        '
        Me.txt_add_location.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_add_location.Location = New System.Drawing.Point(119, 596)
        Me.txt_add_location.Multiline = True
        Me.txt_add_location.Name = "txt_add_location"
        Me.txt_add_location.Size = New System.Drawing.Size(181, 32)
        Me.txt_add_location.TabIndex = 375
        '
        'ListViewsection
        '
        Me.ListViewsection.BackColor = System.Drawing.Color.White
        Me.ListViewsection.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader6})
        Me.ListViewsection.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewsection.FullRowSelect = True
        Me.ListViewsection.GridLines = True
        Me.ListViewsection.HideSelection = False
        Me.ListViewsection.Location = New System.Drawing.Point(19, 36)
        Me.ListViewsection.Name = "ListViewsection"
        Me.ListViewsection.Size = New System.Drawing.Size(348, 458)
        Me.ListViewsection.TabIndex = 353
        Me.ListViewsection.UseCompatibleStateImageBehavior = False
        Me.ListViewsection.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "ID"
        Me.ColumnHeader1.Width = 70
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Floor"
        Me.ColumnHeader2.Width = 130
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Section"
        Me.ColumnHeader6.Width = 140
        '
        'btn_clear_location
        '
        Me.btn_clear_location.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_clear_location.Location = New System.Drawing.Point(189, 655)
        Me.btn_clear_location.Name = "btn_clear_location"
        Me.btn_clear_location.Size = New System.Drawing.Size(111, 37)
        Me.btn_clear_location.TabIndex = 373
        Me.btn_clear_location.Text = "ล้างข้อมูล"
        Me.btn_clear_location.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(37, 596)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 29)
        Me.Label7.TabIndex = 367
        Me.Label7.Text = "Location :"
        '
        'btn_add_location
        '
        Me.btn_add_location.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_add_location.Location = New System.Drawing.Point(72, 655)
        Me.btn_add_location.Name = "btn_add_location"
        Me.btn_add_location.Size = New System.Drawing.Size(111, 37)
        Me.btn_add_location.TabIndex = 372
        Me.btn_add_location.Text = "บันทึก"
        Me.btn_add_location.UseVisualStyleBackColor = True
        '
        'txtidfloor
        '
        Me.txtidfloor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidfloor.Location = New System.Drawing.Point(312, 520)
        Me.txtidfloor.Multiline = True
        Me.txtidfloor.Name = "txtidfloor"
        Me.txtidfloor.ReadOnly = True
        Me.txtidfloor.Size = New System.Drawing.Size(55, 32)
        Me.txtidfloor.TabIndex = 368
        '
        'txtfloor
        '
        Me.txtfloor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfloor.Location = New System.Drawing.Point(119, 520)
        Me.txtfloor.Multiline = True
        Me.txtfloor.Name = "txtfloor"
        Me.txtfloor.ReadOnly = True
        Me.txtfloor.Size = New System.Drawing.Size(181, 32)
        Me.txtfloor.TabIndex = 366
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(60, 520)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 29)
        Me.Label6.TabIndex = 365
        Me.Label6.Text = "Floor :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(45, 558)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 29)
        Me.Label5.TabIndex = 356
        Me.Label5.Text = "Section :"
        '
        'txtidsection
        '
        Me.txtidsection.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidsection.Location = New System.Drawing.Point(312, 558)
        Me.txtidsection.Multiline = True
        Me.txtidsection.Name = "txtidsection"
        Me.txtidsection.ReadOnly = True
        Me.txtidsection.Size = New System.Drawing.Size(55, 32)
        Me.txtidsection.TabIndex = 369
        '
        'txtsection
        '
        Me.txtsection.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsection.Location = New System.Drawing.Point(119, 558)
        Me.txtsection.Multiline = True
        Me.txtsection.Name = "txtsection"
        Me.txtsection.ReadOnly = True
        Me.txtsection.Size = New System.Drawing.Size(181, 32)
        Me.txtsection.TabIndex = 357
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txt_id_location)
        Me.GroupBox2.Controls.Add(Me.txt_save_location)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txt_id_floor2)
        Me.GroupBox2.Controls.Add(Me.txt_floor2)
        Me.GroupBox2.Controls.Add(Me.btn_home)
        Me.GroupBox2.Controls.Add(Me.btn_delete_location)
        Me.GroupBox2.Controls.Add(Me.btn_save_location)
        Me.GroupBox2.Controls.Add(Me.txt_id_section)
        Me.GroupBox2.Controls.Add(Me.ListViewlocation)
        Me.GroupBox2.Controls.Add(Me.txt_edit_section)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(428, 35)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(397, 710)
        Me.GroupBox2.TabIndex = 360
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "คลิกเลือก Section เพื่อแก้ไข ::"
        '
        'txt_id_location
        '
        Me.txt_id_location.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id_location.Location = New System.Drawing.Point(303, 606)
        Me.txt_id_location.Name = "txt_id_location"
        Me.txt_id_location.ReadOnly = True
        Me.txt_id_location.Size = New System.Drawing.Size(61, 37)
        Me.txt_id_location.TabIndex = 378
        '
        'txt_save_location
        '
        Me.txt_save_location.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_save_location.Location = New System.Drawing.Point(103, 606)
        Me.txt_save_location.Multiline = True
        Me.txt_save_location.Name = "txt_save_location"
        Me.txt_save_location.Size = New System.Drawing.Size(194, 37)
        Me.txt_save_location.TabIndex = 377
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 606)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 29)
        Me.Label3.TabIndex = 376
        Me.Label3.Text = "Location :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(44, 520)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 29)
        Me.Label2.TabIndex = 225
        Me.Label2.Text = "Floor :"
        '
        'txt_id_floor2
        '
        Me.txt_id_floor2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id_floor2.Location = New System.Drawing.Point(303, 520)
        Me.txt_id_floor2.Name = "txt_id_floor2"
        Me.txt_id_floor2.ReadOnly = True
        Me.txt_id_floor2.Size = New System.Drawing.Size(61, 37)
        Me.txt_id_floor2.TabIndex = 224
        '
        'txt_floor2
        '
        Me.txt_floor2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_floor2.Location = New System.Drawing.Point(103, 520)
        Me.txt_floor2.Name = "txt_floor2"
        Me.txt_floor2.ReadOnly = True
        Me.txt_floor2.Size = New System.Drawing.Size(194, 37)
        Me.txt_floor2.TabIndex = 223
        '
        'btn_home
        '
        Me.btn_home.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_home.Location = New System.Drawing.Point(266, 655)
        Me.btn_home.Name = "btn_home"
        Me.btn_home.Size = New System.Drawing.Size(111, 37)
        Me.btn_home.TabIndex = 222
        Me.btn_home.Text = "กลับหน้าแรก"
        Me.btn_home.UseVisualStyleBackColor = True
        '
        'btn_delete_location
        '
        Me.btn_delete_location.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_delete_location.Location = New System.Drawing.Point(149, 655)
        Me.btn_delete_location.Name = "btn_delete_location"
        Me.btn_delete_location.Size = New System.Drawing.Size(111, 37)
        Me.btn_delete_location.TabIndex = 221
        Me.btn_delete_location.Text = "ลบข้อมูล"
        Me.btn_delete_location.UseVisualStyleBackColor = True
        '
        'btn_save_location
        '
        Me.btn_save_location.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_save_location.Location = New System.Drawing.Point(32, 655)
        Me.btn_save_location.Name = "btn_save_location"
        Me.btn_save_location.Size = New System.Drawing.Size(111, 37)
        Me.btn_save_location.TabIndex = 220
        Me.btn_save_location.Text = "ยืนยัน"
        Me.btn_save_location.UseVisualStyleBackColor = True
        '
        'txt_id_section
        '
        Me.txt_id_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id_section.Location = New System.Drawing.Point(303, 563)
        Me.txt_id_section.Name = "txt_id_section"
        Me.txt_id_section.ReadOnly = True
        Me.txt_id_section.Size = New System.Drawing.Size(61, 37)
        Me.txt_id_section.TabIndex = 219
        '
        'ListViewlocation
        '
        Me.ListViewlocation.BackColor = System.Drawing.Color.White
        Me.ListViewlocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader5, Me.ColumnHeader4})
        Me.ListViewlocation.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewlocation.FullRowSelect = True
        Me.ListViewlocation.GridLines = True
        Me.ListViewlocation.HideSelection = False
        Me.ListViewlocation.Location = New System.Drawing.Point(27, 36)
        Me.ListViewlocation.Name = "ListViewlocation"
        Me.ListViewlocation.Size = New System.Drawing.Size(345, 458)
        Me.ListViewlocation.TabIndex = 218
        Me.ListViewlocation.UseCompatibleStateImageBehavior = False
        Me.ListViewlocation.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "ID"
        Me.ColumnHeader3.Width = 50
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Section"
        Me.ColumnHeader5.Width = 120
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Location"
        Me.ColumnHeader4.Width = 170
        '
        'txt_edit_section
        '
        Me.txt_edit_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_edit_section.Location = New System.Drawing.Point(103, 563)
        Me.txt_edit_section.Name = "txt_edit_section"
        Me.txt_edit_section.ReadOnly = True
        Me.txt_edit_section.Size = New System.Drawing.Size(194, 37)
        Me.txt_edit_section.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 563)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Section :"
        '
        'frm_edit_location
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 762)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_edit_location"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Management Location"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtfloor As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtsection As System.Windows.Forms.TextBox
    Friend WithEvents txtidsection As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtidfloor As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btn_clear_location As System.Windows.Forms.Button
    Friend WithEvents btn_add_location As System.Windows.Forms.Button
    Friend WithEvents ListViewsection As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txt_add_location As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_id_floor2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_floor2 As System.Windows.Forms.TextBox
    Friend WithEvents btn_home As System.Windows.Forms.Button
    Friend WithEvents btn_delete_location As System.Windows.Forms.Button
    Friend WithEvents btn_save_location As System.Windows.Forms.Button
    Friend WithEvents txt_id_section As System.Windows.Forms.TextBox
    Friend WithEvents ListViewlocation As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txt_edit_section As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_id_location As System.Windows.Forms.TextBox
    Friend WithEvents txt_save_location As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
End Class
