﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_edit_section
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_edit_section))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_id_floor2 = New System.Windows.Forms.TextBox()
        Me.txt_floor2 = New System.Windows.Forms.TextBox()
        Me.btn_home = New System.Windows.Forms.Button()
        Me.btn_delete = New System.Windows.Forms.Button()
        Me.btn_save_section = New System.Windows.Forms.Button()
        Me.txt_id_section = New System.Windows.Forms.TextBox()
        Me.ListViewsection = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txt_edit_section = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_delete_section = New System.Windows.Forms.Button()
        Me.btn_add_section = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_id_floor1 = New System.Windows.Forms.TextBox()
        Me.txt_floor1 = New System.Windows.Forms.TextBox()
        Me.txt_add_section = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ListViewfloor = New System.Windows.Forms.ListView()
        Me.ColumnHeader20 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txt_id_floor2)
        Me.GroupBox1.Controls.Add(Me.txt_floor2)
        Me.GroupBox1.Controls.Add(Me.btn_home)
        Me.GroupBox1.Controls.Add(Me.btn_delete)
        Me.GroupBox1.Controls.Add(Me.btn_save_section)
        Me.GroupBox1.Controls.Add(Me.txt_id_section)
        Me.GroupBox1.Controls.Add(Me.ListViewsection)
        Me.GroupBox1.Controls.Add(Me.txt_edit_section)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(352, 40)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(397, 710)
        Me.GroupBox1.TabIndex = 224
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "คลิกเลือก Section เพื่อแก้ไข ::"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(57, 555)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 29)
        Me.Label2.TabIndex = 225
        Me.Label2.Text = "Floor :"
        '
        'txt_id_floor2
        '
        Me.txt_id_floor2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id_floor2.Location = New System.Drawing.Point(316, 555)
        Me.txt_id_floor2.Name = "txt_id_floor2"
        Me.txt_id_floor2.ReadOnly = True
        Me.txt_id_floor2.Size = New System.Drawing.Size(61, 37)
        Me.txt_id_floor2.TabIndex = 224
        '
        'txt_floor2
        '
        Me.txt_floor2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_floor2.Location = New System.Drawing.Point(116, 555)
        Me.txt_floor2.Name = "txt_floor2"
        Me.txt_floor2.ReadOnly = True
        Me.txt_floor2.Size = New System.Drawing.Size(194, 37)
        Me.txt_floor2.TabIndex = 223
        '
        'btn_home
        '
        Me.btn_home.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_home.Location = New System.Drawing.Point(266, 655)
        Me.btn_home.Name = "btn_home"
        Me.btn_home.Size = New System.Drawing.Size(111, 37)
        Me.btn_home.TabIndex = 222
        Me.btn_home.Text = "กลับหน้าแรก"
        Me.btn_home.UseVisualStyleBackColor = True
        '
        'btn_delete
        '
        Me.btn_delete.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_delete.Location = New System.Drawing.Point(149, 655)
        Me.btn_delete.Name = "btn_delete"
        Me.btn_delete.Size = New System.Drawing.Size(111, 37)
        Me.btn_delete.TabIndex = 221
        Me.btn_delete.Text = "ลบข้อมูล"
        Me.btn_delete.UseVisualStyleBackColor = True
        '
        'btn_save_section
        '
        Me.btn_save_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_save_section.Location = New System.Drawing.Point(32, 655)
        Me.btn_save_section.Name = "btn_save_section"
        Me.btn_save_section.Size = New System.Drawing.Size(111, 37)
        Me.btn_save_section.TabIndex = 220
        Me.btn_save_section.Text = "ยืนยัน"
        Me.btn_save_section.UseVisualStyleBackColor = True
        '
        'txt_id_section
        '
        Me.txt_id_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id_section.Location = New System.Drawing.Point(316, 598)
        Me.txt_id_section.Name = "txt_id_section"
        Me.txt_id_section.ReadOnly = True
        Me.txt_id_section.Size = New System.Drawing.Size(61, 37)
        Me.txt_id_section.TabIndex = 219
        '
        'ListViewsection
        '
        Me.ListViewsection.BackColor = System.Drawing.Color.White
        Me.ListViewsection.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader5, Me.ColumnHeader1})
        Me.ListViewsection.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewsection.FullRowSelect = True
        Me.ListViewsection.GridLines = True
        Me.ListViewsection.HideSelection = False
        Me.ListViewsection.Location = New System.Drawing.Point(27, 36)
        Me.ListViewsection.Name = "ListViewsection"
        Me.ListViewsection.Size = New System.Drawing.Size(345, 500)
        Me.ListViewsection.TabIndex = 218
        Me.ListViewsection.UseCompatibleStateImageBehavior = False
        Me.ListViewsection.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "ID"
        Me.ColumnHeader2.Width = 50
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Floor"
        Me.ColumnHeader5.Width = 120
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Section"
        Me.ColumnHeader1.Width = 170
        '
        'txt_edit_section
        '
        Me.txt_edit_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_edit_section.Location = New System.Drawing.Point(116, 598)
        Me.txt_edit_section.Name = "txt_edit_section"
        Me.txt_edit_section.Size = New System.Drawing.Size(194, 37)
        Me.txt_edit_section.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(42, 598)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Section :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_delete_section)
        Me.GroupBox2.Controls.Add(Me.btn_add_section)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txt_id_floor1)
        Me.GroupBox2.Controls.Add(Me.txt_floor1)
        Me.GroupBox2.Controls.Add(Me.txt_add_section)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.ListViewfloor)
        Me.GroupBox2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(23, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(323, 491)
        Me.GroupBox2.TabIndex = 358
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "คลิกเลือก Floor เพื่อบันทึก Section ::"
        '
        'btn_delete_section
        '
        Me.btn_delete_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_delete_section.Location = New System.Drawing.Point(170, 429)
        Me.btn_delete_section.Name = "btn_delete_section"
        Me.btn_delete_section.Size = New System.Drawing.Size(111, 37)
        Me.btn_delete_section.TabIndex = 360
        Me.btn_delete_section.Text = "ล้างข้อมูล"
        Me.btn_delete_section.UseVisualStyleBackColor = True
        '
        'btn_add_section
        '
        Me.btn_add_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_add_section.Location = New System.Drawing.Point(53, 429)
        Me.btn_add_section.Name = "btn_add_section"
        Me.btn_add_section.Size = New System.Drawing.Size(111, 37)
        Me.btn_add_section.TabIndex = 359
        Me.btn_add_section.Text = "บันทึก"
        Me.btn_add_section.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(28, 326)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 29)
        Me.Label3.TabIndex = 358
        Me.Label3.Text = "Floor :"
        '
        'txt_id_floor1
        '
        Me.txt_id_floor1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_id_floor1.Location = New System.Drawing.Point(245, 326)
        Me.txt_id_floor1.Name = "txt_id_floor1"
        Me.txt_id_floor1.ReadOnly = True
        Me.txt_id_floor1.Size = New System.Drawing.Size(61, 37)
        Me.txt_id_floor1.TabIndex = 357
        '
        'txt_floor1
        '
        Me.txt_floor1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_floor1.Location = New System.Drawing.Point(87, 326)
        Me.txt_floor1.Name = "txt_floor1"
        Me.txt_floor1.ReadOnly = True
        Me.txt_floor1.Size = New System.Drawing.Size(152, 37)
        Me.txt_floor1.TabIndex = 356
        '
        'txt_add_section
        '
        Me.txt_add_section.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_add_section.Location = New System.Drawing.Point(87, 369)
        Me.txt_add_section.Name = "txt_add_section"
        Me.txt_add_section.Size = New System.Drawing.Size(219, 37)
        Me.txt_add_section.TabIndex = 354
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 369)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 29)
        Me.Label4.TabIndex = 353
        Me.Label4.Text = "Section :"
        '
        'ListViewfloor
        '
        Me.ListViewfloor.BackColor = System.Drawing.Color.White
        Me.ListViewfloor.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader20, Me.ColumnHeader21})
        Me.ListViewfloor.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewfloor.FullRowSelect = True
        Me.ListViewfloor.GridLines = True
        Me.ListViewfloor.HideSelection = False
        Me.ListViewfloor.Location = New System.Drawing.Point(30, 36)
        Me.ListViewfloor.Name = "ListViewfloor"
        Me.ListViewfloor.Size = New System.Drawing.Size(257, 273)
        Me.ListViewfloor.TabIndex = 352
        Me.ListViewfloor.UseCompatibleStateImageBehavior = False
        Me.ListViewfloor.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader20
        '
        Me.ColumnHeader20.Text = "ID"
        Me.ColumnHeader20.Width = 70
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Floor"
        Me.ColumnHeader21.Width = 180
        '
        'frm_edit_section
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 762)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_edit_section"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Management Section"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ListViewfloor As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader20 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_id_floor2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_floor2 As System.Windows.Forms.TextBox
    Friend WithEvents btn_home As System.Windows.Forms.Button
    Friend WithEvents btn_delete As System.Windows.Forms.Button
    Friend WithEvents btn_save_section As System.Windows.Forms.Button
    Friend WithEvents txt_id_section As System.Windows.Forms.TextBox
    Friend WithEvents ListViewsection As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txt_edit_section As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_delete_section As System.Windows.Forms.Button
    Friend WithEvents btn_add_section As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_id_floor1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_floor1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_add_section As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
End Class
