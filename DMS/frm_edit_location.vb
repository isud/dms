﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_edit_location
    'ประกาศตัวแปรตัวเชื่อมต่อ
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Dim idkey2 As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_edit_location_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล

        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)

            Me.Close()
        End Try

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor);"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewsection.Items.Clear()
            While (MySqlReader.Read())

                With ListViewsection.Items.Add(MySqlReader("idsection"))
                    .SubItems.Add(MySqlReader("floor_name"))
                    .SubItems.Add(MySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()


        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If


        MySqlCommand.CommandText = "SELECT * FROM location join section join floor where (location.idfloor = floor.idfloor and section.idfloor = floor.idfloor and location.idsection = section.idsection ) ;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewlocation.Items.Clear()
            While (MySqlReader.Read())

                With ListViewlocation.Items.Add(MySqlReader("idlocation"))
                    .subitems.add(MySqlReader("section_name"))
                    .subitems.add(MySqlReader("location_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

    End Sub
    Private Sub btn_hide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Hide()
    End Sub

    Private Sub ListViewsection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewsection.Click
        idkey = ListViewsection.SelectedItems(0).SubItems(0).Text

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If


        mySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor)and  idsection = '" & idkey & "' ;"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                txtidsection.Text = mySqlReader("idsection")
                txtfloor.Text = mySqlReader("floor_name")
                txtsection.Text = mySqlReader("section_name")
                txtidfloor.Text = mySqlReader("idFloor")

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_save_location_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_location.Click
        If txt_add_location.Text <> "" Then

            savedata()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะบันทึก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedata()
        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Try
            If txt_add_location.Text <> "" Then
                MySqlCommand.CommandText = "INSERT INTO location (location_name,idfloor,idsection) VALUES ('" & txt_add_location.Text & "','" & txtidfloor.Text & "','" & txtidsection.Text & "' );"
                MySqlCommand.CommandType = CommandType.Text
                MySqlCommand.Connection = sql
                MySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor);"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewsection.Items.Clear()
            While (MySqlReader.Read())

                With ListViewsection.Items.Add(MySqlReader("idsection"))
                    .SubItems.Add(MySqlReader("floor_name"))
                    .SubItems.Add(MySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If


        MySqlCommand.CommandText = "SELECT * FROM location join section join floor where (location.idfloor = floor.idfloor and section.idfloor = floor.idfloor and location.idsection = section.idsection ) ;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewlocation.Items.Clear()
            While (MySqlReader.Read())

                With ListViewlocation.Items.Add(MySqlReader("idlocation"))
                    .subitems.add(MySqlReader("section_name"))
                    .subitems.add(MySqlReader("location_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
        txtfloor.Text = ""
        txtidfloor.Text = ""
        txtsection.Text = ""
        txtidsection.Text = ""
        txt_add_location.Text = ""
    End Sub

    Private Sub btn_delete_location_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_clear_location.Click
        txtfloor.Text = ""
        txtidfloor.Text = ""
        txtsection.Text = ""
        txtidsection.Text = ""
        txt_add_location.Text = ""
        txt_floor2.Text = ""
        txt_id_floor2.Text = ""
        txt_edit_section.Text = ""
        txt_id_section.Text = ""
        txt_save_location.Text = ""
        txt_id_location.Text = ""
    End Sub

    Private Sub ListViewlocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewlocation.Click
        idkey2 = ListViewlocation.SelectedItems(0).SubItems(0).Text

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        MySqlCommand.CommandText = "SELECT * FROM location join section join floor where (location.idfloor = floor.idfloor and section.idfloor = floor.idfloor and location.idsection = section.idsection )and idlocation = '" & idkey2 & "' ;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            While (MySqlReader.Read())
                txt_id_floor2.Text = (MySqlReader("idfloor"))
                txt_floor2.Text = (MySqlReader("floor_name"))
                txt_edit_section.Text = (MySqlReader("section_name"))
                txt_id_section.Text = (MySqlReader("idsection"))
                txt_save_location.Text = (MySqlReader("location_name"))
                txt_id_location.Text = (MySqlReader("idlocation"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_save_location_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_location.Click
        If txt_save_location.Text <> "" Then

            savedatalocation()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedatalocation()

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        Dim commandText2 As String

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then

            If txt_save_location.Text <> "" Then
                Try
                    commandText2 = "UPDATE location SET location_name = '" & txt_save_location.Text & "'  WHERE idlocation = " & txt_id_location.Text & "; "
                    mySqlCommand.CommandText = commandText2
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If
            End If
        End If

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If


        mySqlCommand.CommandText = "SELECT * FROM location join section join floor where (location.idfloor = floor.idfloor and section.idfloor = floor.idfloor and location.idsection = section.idsection ) ;"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewlocation.Items.Clear()
            While (mySqlReader.Read())

                With ListViewlocation.Items.Add(mySqlReader("idlocation"))
                    .subitems.add(mySqlReader("section_name"))
                    .subitems.add(mySqlReader("location_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
        txt_floor2.Text = ""
        txt_id_floor2.Text = ""
        txt_edit_section.Text = ""
        txt_id_section.Text = ""
        txt_save_location.Text = ""
        txt_id_location.Text = ""

    End Sub

    Private Sub btn_delete_location_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete_location.Click
        If txt_id_location.Text <> "" Then

            DeleteData()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteData()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt_id_location.Text <> "" Then

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If
                Try

                    mySqlCommand.CommandText = "DELETE FROM location where idlocation = '" & txt_id_location.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = sql

                    mySqlCommand.ExecuteNonQuery()
                    sql.Close()
                Catch ex As Exception

                    MsgBox(ex.ToString)
                    Exit Sub
                End Try

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If
            End If
        End If
        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If


        mySqlCommand.CommandText = "SELECT * FROM location join section join floor where (location.idfloor = floor.idfloor and section.idfloor = floor.idfloor and location.idsection = section.idsection ) ;"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewlocation.Items.Clear()
            While (mySqlReader.Read())

                With ListViewlocation.Items.Add(mySqlReader("idlocation"))
                    .subitems.add(mySqlReader("section_name"))
                    .subitems.add(mySqlReader("location_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
        txt_floor2.Text = ""
        txt_id_floor2.Text = ""
        txt_edit_section.Text = ""
        txt_id_section.Text = ""
        txt_save_location.Text = ""
        txt_id_location.Text = ""
    End Sub

    Private Sub btn_home_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_home.Click
        Me.Hide()
    End Sub

End Class