﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_edit_type
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_edit_type_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล

        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)

            Me.Close()
        End Try

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        mySqlCommand.CommandText = "SELECT * FROM type order by idtype;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewtype.Items.Clear()

            While (mySqlReader.Read())

                With ListViewtype.Items.Add(mySqlReader("idtype"))
                    .SubItems.Add(mySqlReader("type_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub showdatatype()
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        mySqlCommand.CommandText = "SELECT * FROM type order by idtype;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewtype.Items.Clear()

            While (mySqlReader.Read())

                With ListViewtype.Items.Add(mySqlReader("idtype"))
                    .SubItems.Add(mySqlReader("type_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub btn_add_type_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_type.Click
        Dim Input As String
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            Input = InputBox("เพิ่มข้อมูลประเภท")
            If Input.Length > 0 Then

                mySqlCommand.CommandText = "INSERT INTO type (type_name) VALUES ('" & Input & "');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        showdatatype()
    End Sub

    Private Sub btn_home_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_home.Click
        Me.Hide()
    End Sub

    Private Sub ListViewtype_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewtype.Click
        idkey = ListViewtype.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        MySqlCommand.CommandText = "SELECT * FROM type  where idtype = '" & idkey & "' ;"
        MySqlCommand.Connection = Sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            While (MySqlReader.Read())
                txt_id_type.Text = (MySqlReader("idtype"))
                txt_type.Text = (MySqlReader("type_name"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save.Click
        If txt_id_type.Text <> "" Then

            savedatatype()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedatatype()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")

        If respone = 1 Then
            Try
                commandText2 = "UPDATE type SET type_name = '" & txt_type.Text & "'  WHERE idtype = " & txt_id_type.Text & "; "

                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        Sql.Close()
        txt_type.Text = ""
        txt_id_type.Text = ""
        showdatatype()
    End Sub

    Private Sub btn_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete.Click
        If txt_id_type.Text <> "" Then

            DeleteData()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteData()

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object

        respone = MsgBox("คุณต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt_id_type.Text <> "" Then

                If Sql.State = ConnectionState.Closed Then
                    Sql.Open()
                End If

                Try

                    mySqlCommand.CommandText = "DELETE FROM type where idtype = '" & txt_id_type.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                    txt_id_type.Text = ""
                    txt_type.Text = ""
                Catch ex As Exception

                    MsgBox(ex.ToString)

                End Try
                Sql.Close()
                'txt_count.Text = ListViewshow.Items.Count.ToString
                showdatatype()
                'เรียกข้อมูลของ brand มาแสดงใน listviewbrand
            End If
        End If
    End Sub
End Class