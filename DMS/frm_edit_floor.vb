﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_edit_floor
    'ประกาศตัวแปรตัวเชื่อมต่อ
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_edit_floor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล

        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)

            Me.Close()
        End Try

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        MySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = MySqlCommand

        Try
            mySqlReader = MySqlCommand.ExecuteReader
            ListViewfloor.Items.Clear()
            While (mySqlReader.Read())

                With ListViewfloor.Items.Add(mySqlReader("idfloor"))
                    .SubItems.Add(mySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

    End Sub
    Private Sub btn_add_floor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_floor.Click
        Dim Input As String
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Try
            Input = InputBox("เพิ่มข้อมูลชั้น")
            If Input.Length > 0 Then

                mySqlCommand.CommandText = "INSERT INTO floor (floor_name) VALUES ('" & Input & "');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewfloor.Items.Clear()

            While (mySqlReader.Read())

                With ListViewfloor.Items.Add(mySqlReader("idfloor"))
                    .SubItems.Add(mySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_home_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_home.Click
        Me.Hide()
    End Sub

    Private Sub ListViewfloor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewfloor.Click
        idkey = ListViewfloor.SelectedItems(0).SubItems(0).Text

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        MySqlCommand.CommandText = "SELECT * FROM floor  where idfloor = '" & idkey & "' ;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            While (MySqlReader.Read())
                txt_id_floor.Text = (MySqlReader("idfloor"))
                txt_floor.Text = (MySqlReader("floor_name"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save.Click
        If txt_id_floor.Text <> "" Then

            savedata()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedata()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")

        If respone = 1 Then
            Try
                commandText2 = "UPDATE floor SET floor_name = '" & txt_floor.Text & "'  WHERE idfloor = " & txt_id_floor.Text & "; "

                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = sql
                mySqlCommand.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        sql.Close()
        txt_floor.Text = ""
        txt_id_floor.Text = ""


        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewfloor.Items.Clear()

            While (mySqlReader.Read())

                With ListViewfloor.Items.Add(mySqlReader("idfloor"))
                    .SubItems.Add(mySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete.Click
        If txt_id_floor.Text <> "" Then

            DeleteData()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteData()

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        respone = MsgBox("คุณต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt_id_floor.Text <> "" Then

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If

                Try

                    mySqlCommand.CommandText = "DELETE FROM floor where idfloor = '" & txt_id_floor.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = sql
                    mySqlCommand.ExecuteNonQuery()

                    txt_floor.Text = ""

                Catch ex As Exception

                    MsgBox(ex.ToString)

                End Try
                sql.Close()

                'txt_count.Text = ListViewshow.Items.Count.ToString

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If

                mySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
                mySqlCommand.Connection = sql
                mySqlAdaptor.SelectCommand = mySqlCommand

                Try
                    mySqlReader = mySqlCommand.ExecuteReader

                    ListViewfloor.Items.Clear()

                    While (mySqlReader.Read())

                        With ListViewfloor.Items.Add(mySqlReader("idfloor"))
                            .SubItems.Add(mySqlReader("floor_name"))

                        End With
                    End While
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                sql.Close()

            End If
        End If
    End Sub
End Class