﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ADD_DEVICE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_ADD_DEVICE))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txt_store = New System.Windows.Forms.TextBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.DateTimePicker6 = New System.Windows.Forms.DateTimePicker()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.DateTimePicker5 = New System.Windows.Forms.DateTimePicker()
        Me.btn_cancel_com = New System.Windows.Forms.Button()
        Me.btn_save_com = New System.Windows.Forms.Button()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txt1_note_com = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand1 = New System.Windows.Forms.ComboBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txt1_price = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.tab3detailsoftware = New System.Windows.Forms.Label()
        Me.txt1_other = New System.Windows.Forms.TextBox()
        Me.txt1_comname = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txt1_office = New System.Windows.Forms.TextBox()
        Me.txt1_windows = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txt1_detail = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt1_vga = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt1_serial = New System.Windows.Forms.TextBox()
        Me.txt1_hdd = New System.Windows.Forms.TextBox()
        Me.txt1_ram = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txt1_ip = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txt1_mainboard = New System.Windows.Forms.TextBox()
        Me.txt1_cpu = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt1_case = New System.Windows.Forms.TextBox()
        Me.txt1_model = New System.Windows.Forms.TextBox()
        Me.txt1_ps = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt1_cd = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.txt_store_rent = New System.Windows.Forms.TextBox()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker()
        Me.btn_cancel_comr = New System.Windows.Forms.Button()
        Me.btn_save_comr = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt2_note_comr = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand2 = New System.Windows.Forms.ComboBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txt2_price = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txt2_other = New System.Windows.Forms.TextBox()
        Me.txt2_comname = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txt2_office = New System.Windows.Forms.TextBox()
        Me.txt2_windows = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txt2_detail = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txt2_vga = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txt2_serial = New System.Windows.Forms.TextBox()
        Me.txt2_hdd = New System.Windows.Forms.TextBox()
        Me.txt2_ram = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.txt2_ip = New System.Windows.Forms.TextBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.txt2_mainboard = New System.Windows.Forms.TextBox()
        Me.txt2_cpu = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.txt2_case = New System.Windows.Forms.TextBox()
        Me.txt2_model = New System.Windows.Forms.TextBox()
        Me.txt2_ps = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.txt2_cd = New System.Windows.Forms.TextBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Radiolaserc = New System.Windows.Forms.RadioButton()
        Me.Radiodot = New System.Windows.Forms.RadioButton()
        Me.Radioinkjet = New System.Windows.Forms.RadioButton()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.txt3_note_prin = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand3 = New System.Windows.Forms.ComboBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.btn_cencel_prin = New System.Windows.Forms.Button()
        Me.txt3_price = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt3_serial = New System.Windows.Forms.TextBox()
        Me.txt3_detail = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt3_model = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_save_prin = New System.Windows.Forms.Button()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.txt4_note_monitor = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand4 = New System.Windows.Forms.ComboBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.btn_cancel_monitor = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt4_price = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txt4_serial = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btn_save_monitor = New System.Windows.Forms.Button()
        Me.txt4_model = New System.Windows.Forms.TextBox()
        Me.txt4_detail = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txt4_size = New System.Windows.Forms.TextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.txt5_note_other = New System.Windows.Forms.RichTextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.ComboBoxtype_other = New System.Windows.Forms.ComboBox()
        Me.ComboBoxbrand5 = New System.Windows.Forms.ComboBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.btn_cancel_other = New System.Windows.Forms.Button()
        Me.txt5_price = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txt5_serial = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt5_detail = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txt5_model = New System.Windows.Forms.TextBox()
        Me.btn_save_other = New System.Windows.Forms.Button()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Radioapp = New System.Windows.Forms.RadioButton()
        Me.Radioos = New System.Windows.Forms.RadioButton()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.txt6_note_license = New System.Windows.Forms.RichTextBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.btn_cancel_license = New System.Windows.Forms.Button()
        Me.txt6_amount = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.btn_save_license = New System.Windows.Forms.Button()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txt6_detail = New System.Windows.Forms.TextBox()
        Me.txt6_price = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txt6_brand = New System.Windows.Forms.TextBox()
        Me.btnback = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.llogin = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 97)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1079, 580)
        Me.TabControl1.TabIndex = 316
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txt_store)
        Me.TabPage1.Controls.Add(Me.Label94)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.btn_cancel_com)
        Me.TabPage1.Controls.Add(Me.btn_save_com)
        Me.TabPage1.Controls.Add(Me.Label77)
        Me.TabPage1.Controls.Add(Me.txt1_note_com)
        Me.TabPage1.Controls.Add(Me.ComboBoxbrand1)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.Label61)
        Me.TabPage1.Controls.Add(Me.Label54)
        Me.TabPage1.Controls.Add(Me.txt1_price)
        Me.TabPage1.Controls.Add(Me.Label34)
        Me.TabPage1.Controls.Add(Me.tab3detailsoftware)
        Me.TabPage1.Controls.Add(Me.txt1_other)
        Me.TabPage1.Controls.Add(Me.txt1_comname)
        Me.TabPage1.Controls.Add(Me.Label30)
        Me.TabPage1.Controls.Add(Me.Label29)
        Me.TabPage1.Controls.Add(Me.txt1_office)
        Me.TabPage1.Controls.Add(Me.txt1_windows)
        Me.TabPage1.Controls.Add(Me.Label28)
        Me.TabPage1.Controls.Add(Me.txt1_detail)
        Me.TabPage1.Controls.Add(Me.Label27)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.txt1_vga)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.txt1_serial)
        Me.TabPage1.Controls.Add(Me.txt1_hdd)
        Me.TabPage1.Controls.Add(Me.txt1_ram)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.txt1_ip)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label24)
        Me.TabPage1.Controls.Add(Me.txt1_mainboard)
        Me.TabPage1.Controls.Add(Me.txt1_cpu)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.txt1_case)
        Me.TabPage1.Controls.Add(Me.txt1_model)
        Me.TabPage1.Controls.Add(Me.txt1_ps)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.txt1_cd)
        Me.TabPage1.Controls.Add(Me.Label25)
        Me.TabPage1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 35)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1071, 541)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Computer"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txt_store
        '
        Me.txt_store.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_store.Location = New System.Drawing.Point(504, 372)
        Me.txt_store.Multiline = True
        Me.txt_store.Name = "txt_store"
        Me.txt_store.Size = New System.Drawing.Size(202, 34)
        Me.txt_store.TabIndex = 20
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(436, 372)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(62, 26)
        Me.Label94.TabIndex = 1011
        Me.Label94.Text = "ชื่อผู้ขาย :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label96)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker6)
        Me.GroupBox2.Controls.Add(Me.Label86)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker5)
        Me.GroupBox2.Location = New System.Drawing.Point(742, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(312, 162)
        Me.GroupBox2.TabIndex = 1009
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "เลือกวันที่"
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Location = New System.Drawing.Point(29, 95)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(79, 26)
        Me.Label96.TabIndex = 101
        Me.Label96.Text = "หมดประกัน :"
        '
        'DateTimePicker6
        '
        Me.DateTimePicker6.Location = New System.Drawing.Point(114, 95)
        Me.DateTimePicker6.Name = "DateTimePicker6"
        Me.DateTimePicker6.Size = New System.Drawing.Size(154, 34)
        Me.DateTimePicker6.TabIndex = 102
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(53, 39)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(55, 26)
        Me.Label86.TabIndex = 6
        Me.Label86.Text = "วันที่ซื้อ :"
        '
        'DateTimePicker5
        '
        Me.DateTimePicker5.Location = New System.Drawing.Point(114, 39)
        Me.DateTimePicker5.Name = "DateTimePicker5"
        Me.DateTimePicker5.Size = New System.Drawing.Size(154, 34)
        Me.DateTimePicker5.TabIndex = 100
        '
        'btn_cancel_com
        '
        Me.btn_cancel_com.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel_com.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel_com.Location = New System.Drawing.Point(583, 495)
        Me.btn_cancel_com.Name = "btn_cancel_com"
        Me.btn_cancel_com.Size = New System.Drawing.Size(80, 40)
        Me.btn_cancel_com.TabIndex = 22
        Me.btn_cancel_com.Text = "Cancel"
        Me.btn_cancel_com.UseVisualStyleBackColor = True
        '
        'btn_save_com
        '
        Me.btn_save_com.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save_com.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_com.Location = New System.Drawing.Point(497, 495)
        Me.btn_save_com.Name = "btn_save_com"
        Me.btn_save_com.Size = New System.Drawing.Size(80, 40)
        Me.btn_save_com.TabIndex = 21
        Me.btn_save_com.Text = "Save"
        Me.btn_save_com.UseVisualStyleBackColor = True
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(454, 311)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(44, 26)
        Me.Label77.TabIndex = 60
        Me.Label77.Text = "Note :"
        '
        'txt1_note_com
        '
        Me.txt1_note_com.Location = New System.Drawing.Point(504, 314)
        Me.txt1_note_com.Name = "txt1_note_com"
        Me.txt1_note_com.Size = New System.Drawing.Size(202, 52)
        Me.txt1_note_com.TabIndex = 19
        Me.txt1_note_com.Text = ""
        '
        'ComboBoxbrand1
        '
        Me.ComboBoxbrand1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ComboBoxbrand1.FormattingEnabled = True
        Me.ComboBoxbrand1.Location = New System.Drawing.Point(131, 31)
        Me.ComboBoxbrand1.Name = "ComboBoxbrand1"
        Me.ComboBoxbrand1.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand1.TabIndex = 1
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(499, 419)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(115, 26)
        Me.Label62.TabIndex = 57
        Me.Label62.Text = "=  บังคับกรอกข้อมูล"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label61.ForeColor = System.Drawing.Color.Red
        Me.Label61.Location = New System.Drawing.Point(478, 425)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(15, 20)
        Me.Label61.TabIndex = 56
        Me.Label61.Text = "*"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label54.ForeColor = System.Drawing.Color.Red
        Me.Label54.Location = New System.Drawing.Point(339, 117)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(15, 20)
        Me.Label54.TabIndex = 55
        Me.Label54.Text = "*"
        '
        'txt1_price
        '
        Me.txt1_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_price.Location = New System.Drawing.Point(504, 274)
        Me.txt1_price.Name = "txt1_price"
        Me.txt1_price.Size = New System.Drawing.Size(73, 34)
        Me.txt1_price.TabIndex = 18
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(451, 274)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(47, 26)
        Me.Label34.TabIndex = 52
        Me.Label34.Text = "Price :"
        '
        'tab3detailsoftware
        '
        Me.tab3detailsoftware.AutoSize = True
        Me.tab3detailsoftware.Location = New System.Drawing.Point(449, 192)
        Me.tab3detailsoftware.Name = "tab3detailsoftware"
        Me.tab3detailsoftware.Size = New System.Drawing.Size(49, 26)
        Me.tab3detailsoftware.TabIndex = 49
        Me.tab3detailsoftware.Text = "Other :"
        '
        'txt1_other
        '
        Me.txt1_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_other.Location = New System.Drawing.Point(504, 192)
        Me.txt1_other.Multiline = True
        Me.txt1_other.Name = "txt1_other"
        Me.txt1_other.Size = New System.Drawing.Size(202, 35)
        Me.txt1_other.TabIndex = 16
        '
        'txt1_comname
        '
        Me.txt1_comname.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_comname.Location = New System.Drawing.Point(504, 32)
        Me.txt1_comname.Name = "txt1_comname"
        Me.txt1_comname.Size = New System.Drawing.Size(202, 34)
        Me.txt1_comname.TabIndex = 12
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(390, 31)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(108, 26)
        Me.Label30.TabIndex = 44
        Me.Label30.Text = "ComputerName :"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(447, 152)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(51, 26)
        Me.Label29.TabIndex = 40
        Me.Label29.Text = "Office :"
        '
        'txt1_office
        '
        Me.txt1_office.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_office.Location = New System.Drawing.Point(504, 152)
        Me.txt1_office.Name = "txt1_office"
        Me.txt1_office.Size = New System.Drawing.Size(202, 34)
        Me.txt1_office.TabIndex = 15
        '
        'txt1_windows
        '
        Me.txt1_windows.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_windows.Location = New System.Drawing.Point(504, 112)
        Me.txt1_windows.Name = "txt1_windows"
        Me.txt1_windows.Size = New System.Drawing.Size(202, 34)
        Me.txt1_windows.TabIndex = 14
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(429, 112)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(69, 26)
        Me.Label28.TabIndex = 36
        Me.Label28.Text = "Windows :"
        '
        'txt1_detail
        '
        Me.txt1_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_detail.Location = New System.Drawing.Point(504, 234)
        Me.txt1_detail.Multiline = True
        Me.txt1_detail.Name = "txt1_detail"
        Me.txt1_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt1_detail.TabIndex = 17
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(26, 430)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(99, 26)
        Me.Label27.TabIndex = 35
        Me.Label27.Text = "Serial Number :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(56, 234)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(69, 26)
        Me.Label20.TabIndex = 16
        Me.Label20.Text = "Harddisk :"
        '
        'txt1_vga
        '
        Me.txt1_vga.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_vga.Location = New System.Drawing.Point(131, 271)
        Me.txt1_vga.Name = "txt1_vga"
        Me.txt1_vga.Size = New System.Drawing.Size(202, 34)
        Me.txt1_vga.TabIndex = 7
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(81, 191)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(44, 26)
        Me.Label19.TabIndex = 15
        Me.Label19.Text = "Ram :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(54, 271)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(71, 26)
        Me.Label21.TabIndex = 18
        Me.Label21.Text = "Vga Card :"
        '
        'txt1_serial
        '
        Me.txt1_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_serial.Location = New System.Drawing.Point(131, 430)
        Me.txt1_serial.Name = "txt1_serial"
        Me.txt1_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt1_serial.TabIndex = 11
        '
        'txt1_hdd
        '
        Me.txt1_hdd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_hdd.Location = New System.Drawing.Point(131, 231)
        Me.txt1_hdd.Name = "txt1_hdd"
        Me.txt1_hdd.Size = New System.Drawing.Size(202, 34)
        Me.txt1_hdd.TabIndex = 6
        '
        'txt1_ram
        '
        Me.txt1_ram.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ram.Location = New System.Drawing.Point(131, 191)
        Me.txt1_ram.Name = "txt1_ram"
        Me.txt1_ram.Size = New System.Drawing.Size(202, 34)
        Me.txt1_ram.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(420, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 26)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "IP Address :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(47, 151)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 26)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "Mainboard :"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(78, 390)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(47, 26)
        Me.Label26.TabIndex = 33
        Me.Label26.Text = "Case :"
        '
        'txt1_ip
        '
        Me.txt1_ip.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ip.Location = New System.Drawing.Point(504, 72)
        Me.txt1_ip.Name = "txt1_ip"
        Me.txt1_ip.Size = New System.Drawing.Size(202, 34)
        Me.txt1_ip.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(81, 111)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 26)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "CPU :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(448, 234)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 26)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Detail :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(33, 311)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(92, 26)
        Me.Label24.TabIndex = 28
        Me.Label24.Text = "PowerSupply :"
        '
        'txt1_mainboard
        '
        Me.txt1_mainboard.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_mainboard.Location = New System.Drawing.Point(131, 151)
        Me.txt1_mainboard.Name = "txt1_mainboard"
        Me.txt1_mainboard.Size = New System.Drawing.Size(202, 34)
        Me.txt1_mainboard.TabIndex = 4
        '
        'txt1_cpu
        '
        Me.txt1_cpu.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_cpu.Location = New System.Drawing.Point(131, 111)
        Me.txt1_cpu.Name = "txt1_cpu"
        Me.txt1_cpu.Size = New System.Drawing.Size(202, 34)
        Me.txt1_cpu.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(73, 71)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 26)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Model :"
        '
        'txt1_case
        '
        Me.txt1_case.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_case.Location = New System.Drawing.Point(131, 390)
        Me.txt1_case.Name = "txt1_case"
        Me.txt1_case.Size = New System.Drawing.Size(202, 34)
        Me.txt1_case.TabIndex = 10
        '
        'txt1_model
        '
        Me.txt1_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_model.Location = New System.Drawing.Point(131, 71)
        Me.txt1_model.Name = "txt1_model"
        Me.txt1_model.Size = New System.Drawing.Size(202, 34)
        Me.txt1_model.TabIndex = 2
        '
        'txt1_ps
        '
        Me.txt1_ps.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ps.Location = New System.Drawing.Point(131, 311)
        Me.txt1_ps.Name = "txt1_ps"
        Me.txt1_ps.Size = New System.Drawing.Size(202, 34)
        Me.txt1_ps.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(73, 31)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 26)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Brand :"
        '
        'txt1_cd
        '
        Me.txt1_cd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_cd.Location = New System.Drawing.Point(131, 350)
        Me.txt1_cd.Name = "txt1_cd"
        Me.txt1_cd.Size = New System.Drawing.Size(202, 34)
        Me.txt1_cd.TabIndex = 9
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(61, 350)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(64, 26)
        Me.Label25.TabIndex = 31
        Me.Label25.Text = "CD/DVD :"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.txt_store_rent)
        Me.TabPage6.Controls.Add(Me.Label95)
        Me.TabPage6.Controls.Add(Me.GroupBox1)
        Me.TabPage6.Controls.Add(Me.btn_cancel_comr)
        Me.TabPage6.Controls.Add(Me.btn_save_comr)
        Me.TabPage6.Controls.Add(Me.Label4)
        Me.TabPage6.Controls.Add(Me.txt2_note_comr)
        Me.TabPage6.Controls.Add(Me.ComboBoxbrand2)
        Me.TabPage6.Controls.Add(Me.Label42)
        Me.TabPage6.Controls.Add(Me.Label43)
        Me.TabPage6.Controls.Add(Me.Label44)
        Me.TabPage6.Controls.Add(Me.txt2_price)
        Me.TabPage6.Controls.Add(Me.Label45)
        Me.TabPage6.Controls.Add(Me.Label46)
        Me.TabPage6.Controls.Add(Me.txt2_other)
        Me.TabPage6.Controls.Add(Me.txt2_comname)
        Me.TabPage6.Controls.Add(Me.Label47)
        Me.TabPage6.Controls.Add(Me.Label48)
        Me.TabPage6.Controls.Add(Me.txt2_office)
        Me.TabPage6.Controls.Add(Me.txt2_windows)
        Me.TabPage6.Controls.Add(Me.Label49)
        Me.TabPage6.Controls.Add(Me.txt2_detail)
        Me.TabPage6.Controls.Add(Me.Label50)
        Me.TabPage6.Controls.Add(Me.Label51)
        Me.TabPage6.Controls.Add(Me.txt2_vga)
        Me.TabPage6.Controls.Add(Me.Label52)
        Me.TabPage6.Controls.Add(Me.Label53)
        Me.TabPage6.Controls.Add(Me.txt2_serial)
        Me.TabPage6.Controls.Add(Me.txt2_hdd)
        Me.TabPage6.Controls.Add(Me.txt2_ram)
        Me.TabPage6.Controls.Add(Me.Label59)
        Me.TabPage6.Controls.Add(Me.Label60)
        Me.TabPage6.Controls.Add(Me.Label71)
        Me.TabPage6.Controls.Add(Me.txt2_ip)
        Me.TabPage6.Controls.Add(Me.Label72)
        Me.TabPage6.Controls.Add(Me.Label73)
        Me.TabPage6.Controls.Add(Me.Label74)
        Me.TabPage6.Controls.Add(Me.txt2_mainboard)
        Me.TabPage6.Controls.Add(Me.txt2_cpu)
        Me.TabPage6.Controls.Add(Me.Label76)
        Me.TabPage6.Controls.Add(Me.txt2_case)
        Me.TabPage6.Controls.Add(Me.txt2_model)
        Me.TabPage6.Controls.Add(Me.txt2_ps)
        Me.TabPage6.Controls.Add(Me.Label82)
        Me.TabPage6.Controls.Add(Me.txt2_cd)
        Me.TabPage6.Controls.Add(Me.Label83)
        Me.TabPage6.Location = New System.Drawing.Point(4, 35)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1071, 541)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Computer Rent"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'txt_store_rent
        '
        Me.txt_store_rent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_store_rent.Location = New System.Drawing.Point(497, 371)
        Me.txt_store_rent.Multiline = True
        Me.txt_store_rent.Name = "txt_store_rent"
        Me.txt_store_rent.Size = New System.Drawing.Size(202, 34)
        Me.txt_store_rent.TabIndex = 20
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(429, 371)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(62, 26)
        Me.Label95.TabIndex = 1013
        Me.Label95.Text = "ชื่อผู้ขาย :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label84)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker3)
        Me.GroupBox1.Controls.Add(Me.Label85)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker4)
        Me.GroupBox1.Location = New System.Drawing.Point(743, 22)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(315, 162)
        Me.GroupBox1.TabIndex = 1008
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "เลือกวันที่"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(54, 30)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(59, 26)
        Me.Label84.TabIndex = 6
        Me.Label84.Text = "วันที่เช่า :"
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Location = New System.Drawing.Point(119, 31)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(159, 34)
        Me.DateTimePicker3.TabIndex = 100
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(32, 97)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(81, 26)
        Me.Label85.TabIndex = 7
        Me.Label85.Text = "หมดสัญญา :"
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.Location = New System.Drawing.Point(119, 97)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(159, 34)
        Me.DateTimePicker4.TabIndex = 150
        '
        'btn_cancel_comr
        '
        Me.btn_cancel_comr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel_comr.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel_comr.Location = New System.Drawing.Point(586, 498)
        Me.btn_cancel_comr.Name = "btn_cancel_comr"
        Me.btn_cancel_comr.Size = New System.Drawing.Size(80, 40)
        Me.btn_cancel_comr.TabIndex = 22
        Me.btn_cancel_comr.Text = "Cancel"
        Me.btn_cancel_comr.UseVisualStyleBackColor = True
        '
        'btn_save_comr
        '
        Me.btn_save_comr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save_comr.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_comr.Location = New System.Drawing.Point(500, 498)
        Me.btn_save_comr.Name = "btn_save_comr"
        Me.btn_save_comr.Size = New System.Drawing.Size(80, 40)
        Me.btn_save_comr.TabIndex = 21
        Me.btn_save_comr.Text = "Save"
        Me.btn_save_comr.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(447, 310)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 26)
        Me.Label4.TabIndex = 105
        Me.Label4.Text = "Note :"
        '
        'txt2_note_comr
        '
        Me.txt2_note_comr.Location = New System.Drawing.Point(497, 313)
        Me.txt2_note_comr.Name = "txt2_note_comr"
        Me.txt2_note_comr.Size = New System.Drawing.Size(202, 52)
        Me.txt2_note_comr.TabIndex = 19
        Me.txt2_note_comr.Text = ""
        '
        'ComboBoxbrand2
        '
        Me.ComboBoxbrand2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ComboBoxbrand2.FormattingEnabled = True
        Me.ComboBoxbrand2.Location = New System.Drawing.Point(124, 30)
        Me.ComboBoxbrand2.Name = "ComboBoxbrand2"
        Me.ComboBoxbrand2.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand2.TabIndex = 1
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(488, 416)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(115, 26)
        Me.Label42.TabIndex = 102
        Me.Label42.Text = "=  บังคับกรอกข้อมูล"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.Red
        Me.Label43.Location = New System.Drawing.Point(467, 422)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(15, 20)
        Me.Label43.TabIndex = 101
        Me.Label43.Text = "*"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.Red
        Me.Label44.Location = New System.Drawing.Point(332, 116)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(15, 20)
        Me.Label44.TabIndex = 100
        Me.Label44.Text = "*"
        '
        'txt2_price
        '
        Me.txt2_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_price.Location = New System.Drawing.Point(497, 273)
        Me.txt2_price.Name = "txt2_price"
        Me.txt2_price.Size = New System.Drawing.Size(55, 34)
        Me.txt2_price.TabIndex = 18
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(444, 270)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(47, 26)
        Me.Label45.TabIndex = 99
        Me.Label45.Text = "Price :"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(442, 191)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(49, 26)
        Me.Label46.TabIndex = 98
        Me.Label46.Text = "Other :"
        '
        'txt2_other
        '
        Me.txt2_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_other.Location = New System.Drawing.Point(497, 191)
        Me.txt2_other.Multiline = True
        Me.txt2_other.Name = "txt2_other"
        Me.txt2_other.Size = New System.Drawing.Size(202, 35)
        Me.txt2_other.TabIndex = 16
        '
        'txt2_comname
        '
        Me.txt2_comname.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_comname.Location = New System.Drawing.Point(497, 31)
        Me.txt2_comname.Name = "txt2_comname"
        Me.txt2_comname.Size = New System.Drawing.Size(202, 34)
        Me.txt2_comname.TabIndex = 12
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(383, 30)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(108, 26)
        Me.Label47.TabIndex = 97
        Me.Label47.Text = "ComputerName :"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(440, 151)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(51, 26)
        Me.Label48.TabIndex = 96
        Me.Label48.Text = "Office :"
        '
        'txt2_office
        '
        Me.txt2_office.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_office.Location = New System.Drawing.Point(497, 151)
        Me.txt2_office.Name = "txt2_office"
        Me.txt2_office.Size = New System.Drawing.Size(202, 34)
        Me.txt2_office.TabIndex = 15
        '
        'txt2_windows
        '
        Me.txt2_windows.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_windows.Location = New System.Drawing.Point(497, 111)
        Me.txt2_windows.Name = "txt2_windows"
        Me.txt2_windows.Size = New System.Drawing.Size(202, 34)
        Me.txt2_windows.TabIndex = 14
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(422, 111)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(69, 26)
        Me.Label49.TabIndex = 95
        Me.Label49.Text = "Windows :"
        '
        'txt2_detail
        '
        Me.txt2_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_detail.Location = New System.Drawing.Point(497, 233)
        Me.txt2_detail.Multiline = True
        Me.txt2_detail.Name = "txt2_detail"
        Me.txt2_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt2_detail.TabIndex = 17
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(19, 429)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(99, 26)
        Me.Label50.TabIndex = 94
        Me.Label50.Text = "Serial Number :"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(49, 233)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(69, 26)
        Me.Label51.TabIndex = 84
        Me.Label51.Text = "Harddisk :"
        '
        'txt2_vga
        '
        Me.txt2_vga.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_vga.Location = New System.Drawing.Point(124, 270)
        Me.txt2_vga.Name = "txt2_vga"
        Me.txt2_vga.Size = New System.Drawing.Size(202, 34)
        Me.txt2_vga.TabIndex = 7
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(74, 190)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(44, 26)
        Me.Label52.TabIndex = 82
        Me.Label52.Text = "Ram :"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(47, 270)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(71, 26)
        Me.Label53.TabIndex = 87
        Me.Label53.Text = "Vga Card :"
        '
        'txt2_serial
        '
        Me.txt2_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_serial.Location = New System.Drawing.Point(124, 429)
        Me.txt2_serial.Name = "txt2_serial"
        Me.txt2_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt2_serial.TabIndex = 11
        '
        'txt2_hdd
        '
        Me.txt2_hdd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_hdd.Location = New System.Drawing.Point(124, 230)
        Me.txt2_hdd.Name = "txt2_hdd"
        Me.txt2_hdd.Size = New System.Drawing.Size(202, 34)
        Me.txt2_hdd.TabIndex = 6
        '
        'txt2_ram
        '
        Me.txt2_ram.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_ram.Location = New System.Drawing.Point(124, 190)
        Me.txt2_ram.Name = "txt2_ram"
        Me.txt2_ram.Size = New System.Drawing.Size(202, 34)
        Me.txt2_ram.TabIndex = 5
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(413, 70)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(78, 26)
        Me.Label59.TabIndex = 89
        Me.Label59.Text = "IP Address :"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(40, 150)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(78, 26)
        Me.Label60.TabIndex = 76
        Me.Label60.Text = "Mainboard :"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(71, 389)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(47, 26)
        Me.Label71.TabIndex = 93
        Me.Label71.Text = "Case :"
        '
        'txt2_ip
        '
        Me.txt2_ip.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_ip.Location = New System.Drawing.Point(497, 71)
        Me.txt2_ip.Name = "txt2_ip"
        Me.txt2_ip.Size = New System.Drawing.Size(202, 34)
        Me.txt2_ip.TabIndex = 13
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(74, 110)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(44, 26)
        Me.Label72.TabIndex = 74
        Me.Label72.Text = "CPU :"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(441, 230)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(50, 26)
        Me.Label73.TabIndex = 90
        Me.Label73.Text = "Detail :"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(26, 310)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(92, 26)
        Me.Label74.TabIndex = 91
        Me.Label74.Text = "PowerSupply :"
        '
        'txt2_mainboard
        '
        Me.txt2_mainboard.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_mainboard.Location = New System.Drawing.Point(124, 150)
        Me.txt2_mainboard.Name = "txt2_mainboard"
        Me.txt2_mainboard.Size = New System.Drawing.Size(202, 34)
        Me.txt2_mainboard.TabIndex = 4
        '
        'txt2_cpu
        '
        Me.txt2_cpu.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_cpu.Location = New System.Drawing.Point(124, 110)
        Me.txt2_cpu.Name = "txt2_cpu"
        Me.txt2_cpu.Size = New System.Drawing.Size(202, 34)
        Me.txt2_cpu.TabIndex = 3
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(66, 70)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(52, 26)
        Me.Label76.TabIndex = 70
        Me.Label76.Text = "Model :"
        '
        'txt2_case
        '
        Me.txt2_case.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_case.Location = New System.Drawing.Point(124, 389)
        Me.txt2_case.Name = "txt2_case"
        Me.txt2_case.Size = New System.Drawing.Size(202, 34)
        Me.txt2_case.TabIndex = 10
        '
        'txt2_model
        '
        Me.txt2_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_model.Location = New System.Drawing.Point(124, 70)
        Me.txt2_model.Name = "txt2_model"
        Me.txt2_model.Size = New System.Drawing.Size(202, 34)
        Me.txt2_model.TabIndex = 2
        '
        'txt2_ps
        '
        Me.txt2_ps.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_ps.Location = New System.Drawing.Point(124, 310)
        Me.txt2_ps.Name = "txt2_ps"
        Me.txt2_ps.Size = New System.Drawing.Size(202, 34)
        Me.txt2_ps.TabIndex = 8
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(66, 30)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(52, 26)
        Me.Label82.TabIndex = 68
        Me.Label82.Text = "Brand :"
        '
        'txt2_cd
        '
        Me.txt2_cd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_cd.Location = New System.Drawing.Point(124, 349)
        Me.txt2_cd.Name = "txt2_cd"
        Me.txt2_cd.Size = New System.Drawing.Size(202, 34)
        Me.txt2_cd.TabIndex = 9
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(54, 349)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(64, 26)
        Me.Label83.TabIndex = 92
        Me.Label83.Text = "CD/DVD :"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.Label78)
        Me.TabPage2.Controls.Add(Me.txt3_note_prin)
        Me.TabPage2.Controls.Add(Me.ComboBoxbrand3)
        Me.TabPage2.Controls.Add(Me.Label63)
        Me.TabPage2.Controls.Add(Me.Label64)
        Me.TabPage2.Controls.Add(Me.Label55)
        Me.TabPage2.Controls.Add(Me.btn_cencel_prin)
        Me.TabPage2.Controls.Add(Me.txt3_price)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.txt3_serial)
        Me.TabPage2.Controls.Add(Me.txt3_detail)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.txt3_model)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.btn_save_prin)
        Me.TabPage2.Location = New System.Drawing.Point(4, 35)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1071, 541)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Printer"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Radiolaserc)
        Me.GroupBox3.Controls.Add(Me.Radiodot)
        Me.GroupBox3.Controls.Add(Me.Radioinkjet)
        Me.GroupBox3.Location = New System.Drawing.Point(294, 21)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(578, 88)
        Me.GroupBox3.TabIndex = 63
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "เลือกประเภท"
        '
        'Radiolaserc
        '
        Me.Radiolaserc.AutoSize = True
        Me.Radiolaserc.Location = New System.Drawing.Point(37, 33)
        Me.Radiolaserc.Name = "Radiolaserc"
        Me.Radiolaserc.Size = New System.Drawing.Size(100, 30)
        Me.Radiolaserc.TabIndex = 8
        Me.Radiolaserc.TabStop = True
        Me.Radiolaserc.Text = "Laser Printer"
        Me.Radiolaserc.UseVisualStyleBackColor = True
        '
        'Radiodot
        '
        Me.Radiodot.AutoSize = True
        Me.Radiodot.Location = New System.Drawing.Point(218, 33)
        Me.Radiodot.Name = "Radiodot"
        Me.Radiodot.Size = New System.Drawing.Size(126, 30)
        Me.Radiodot.TabIndex = 10
        Me.Radiodot.TabStop = True
        Me.Radiodot.Text = "Dot-matrix Printer"
        Me.Radiodot.UseVisualStyleBackColor = True
        '
        'Radioinkjet
        '
        Me.Radioinkjet.AutoSize = True
        Me.Radioinkjet.Location = New System.Drawing.Point(425, 33)
        Me.Radioinkjet.Name = "Radioinkjet"
        Me.Radioinkjet.Size = New System.Drawing.Size(98, 30)
        Me.Radioinkjet.TabIndex = 11
        Me.Radioinkjet.TabStop = True
        Me.Radioinkjet.Text = "Inkjet Printer"
        Me.Radioinkjet.UseVisualStyleBackColor = True
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(426, 336)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(44, 26)
        Me.Label78.TabIndex = 62
        Me.Label78.Text = "Note :"
        '
        'txt3_note_prin
        '
        Me.txt3_note_prin.Location = New System.Drawing.Point(476, 336)
        Me.txt3_note_prin.Name = "txt3_note_prin"
        Me.txt3_note_prin.Size = New System.Drawing.Size(202, 52)
        Me.txt3_note_prin.TabIndex = 6
        Me.txt3_note_prin.Text = ""
        '
        'ComboBoxbrand3
        '
        Me.ComboBoxbrand3.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxbrand3.FormattingEnabled = True
        Me.ComboBoxbrand3.Location = New System.Drawing.Point(476, 135)
        Me.ComboBoxbrand3.Name = "ComboBoxbrand3"
        Me.ComboBoxbrand3.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand3.TabIndex = 1
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(471, 432)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(115, 26)
        Me.Label63.TabIndex = 59
        Me.Label63.Text = "=  บังคับกรอกข้อมูล"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.Color.Red
        Me.Label64.Location = New System.Drawing.Point(447, 432)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(18, 29)
        Me.Label64.TabIndex = 58
        Me.Label64.Text = "*"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.ForeColor = System.Drawing.Color.Red
        Me.Label55.Location = New System.Drawing.Point(684, 175)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(18, 29)
        Me.Label55.TabIndex = 56
        Me.Label55.Text = "*"
        '
        'btn_cencel_prin
        '
        Me.btn_cencel_prin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cencel_prin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_cencel_prin.Location = New System.Drawing.Point(585, 495)
        Me.btn_cencel_prin.Name = "btn_cencel_prin"
        Me.btn_cencel_prin.Size = New System.Drawing.Size(80, 40)
        Me.btn_cencel_prin.TabIndex = 8
        Me.btn_cencel_prin.Text = "Cancel"
        Me.btn_cencel_prin.UseVisualStyleBackColor = True
        '
        'txt3_price
        '
        Me.txt3_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_price.Location = New System.Drawing.Point(476, 296)
        Me.txt3_price.Name = "txt3_price"
        Me.txt3_price.Size = New System.Drawing.Size(55, 34)
        Me.txt3_price.TabIndex = 5
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(423, 296)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(47, 26)
        Me.Label35.TabIndex = 54
        Me.Label35.Text = "Price :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(375, 216)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(95, 26)
        Me.Label18.TabIndex = 19
        Me.Label18.Text = "Serial Nunber :"
        '
        'txt3_serial
        '
        Me.txt3_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_serial.Location = New System.Drawing.Point(476, 216)
        Me.txt3_serial.Name = "txt3_serial"
        Me.txt3_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt3_serial.TabIndex = 3
        '
        'txt3_detail
        '
        Me.txt3_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_detail.Location = New System.Drawing.Point(476, 256)
        Me.txt3_detail.Multiline = True
        Me.txt3_detail.Name = "txt3_detail"
        Me.txt3_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt3_detail.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(420, 256)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 26)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Detail :"
        '
        'txt3_model
        '
        Me.txt3_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_model.Location = New System.Drawing.Point(476, 175)
        Me.txt3_model.Name = "txt3_model"
        Me.txt3_model.Size = New System.Drawing.Size(202, 34)
        Me.txt3_model.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(418, 175)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 26)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Model :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(418, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 26)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Brand :"
        '
        'btn_save_prin
        '
        Me.btn_save_prin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save_prin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_save_prin.Location = New System.Drawing.Point(499, 495)
        Me.btn_save_prin.Name = "btn_save_prin"
        Me.btn_save_prin.Size = New System.Drawing.Size(80, 40)
        Me.btn_save_prin.TabIndex = 7
        Me.btn_save_prin.Text = "Save"
        Me.btn_save_prin.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label79)
        Me.TabPage3.Controls.Add(Me.txt4_note_monitor)
        Me.TabPage3.Controls.Add(Me.ComboBoxbrand4)
        Me.TabPage3.Controls.Add(Me.Label65)
        Me.TabPage3.Controls.Add(Me.Label66)
        Me.TabPage3.Controls.Add(Me.Label56)
        Me.TabPage3.Controls.Add(Me.btn_cancel_monitor)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.txt4_price)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Controls.Add(Me.txt4_serial)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.btn_save_monitor)
        Me.TabPage3.Controls.Add(Me.txt4_model)
        Me.TabPage3.Controls.Add(Me.txt4_detail)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.txt4_size)
        Me.TabPage3.Location = New System.Drawing.Point(4, 35)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1071, 541)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Monitor"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(436, 270)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(44, 26)
        Me.Label79.TabIndex = 63
        Me.Label79.Text = "Note :"
        '
        'txt4_note_monitor
        '
        Me.txt4_note_monitor.Location = New System.Drawing.Point(486, 270)
        Me.txt4_note_monitor.Name = "txt4_note_monitor"
        Me.txt4_note_monitor.Size = New System.Drawing.Size(180, 52)
        Me.txt4_note_monitor.TabIndex = 7
        Me.txt4_note_monitor.Text = ""
        '
        'ComboBoxbrand4
        '
        Me.ComboBoxbrand4.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxbrand4.FormattingEnabled = True
        Me.ComboBoxbrand4.Location = New System.Drawing.Point(486, 30)
        Me.ComboBoxbrand4.Name = "ComboBoxbrand4"
        Me.ComboBoxbrand4.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand4.TabIndex = 1
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.Location = New System.Drawing.Point(481, 336)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(115, 26)
        Me.Label65.TabIndex = 59
        Me.Label65.Text = "=  บังคับกรอกข้อมูล"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.Red
        Me.Label66.Location = New System.Drawing.Point(460, 337)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(15, 20)
        Me.Label66.TabIndex = 58
        Me.Label66.Text = "*"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label56.ForeColor = System.Drawing.Color.Red
        Me.Label56.Location = New System.Drawing.Point(694, 76)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(15, 20)
        Me.Label56.TabIndex = 57
        Me.Label56.Text = "*"
        '
        'btn_cancel_monitor
        '
        Me.btn_cancel_monitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel_monitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel_monitor.Location = New System.Drawing.Point(583, 497)
        Me.btn_cancel_monitor.Name = "btn_cancel_monitor"
        Me.btn_cancel_monitor.Size = New System.Drawing.Size(80, 38)
        Me.btn_cancel_monitor.TabIndex = 9
        Me.btn_cancel_monitor.Text = "Cancel"
        Me.btn_cancel_monitor.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(547, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 26)
        Me.Label2.TabIndex = 55
        Me.Label2.Text = "inch"
        '
        'txt4_price
        '
        Me.txt4_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_price.Location = New System.Drawing.Point(486, 230)
        Me.txt4_price.Name = "txt4_price"
        Me.txt4_price.Size = New System.Drawing.Size(55, 34)
        Me.txt4_price.TabIndex = 6
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(433, 230)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(47, 26)
        Me.Label36.TabIndex = 54
        Me.Label36.Text = "Price :"
        '
        'txt4_serial
        '
        Me.txt4_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_serial.Location = New System.Drawing.Point(486, 150)
        Me.txt4_serial.Name = "txt4_serial"
        Me.txt4_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt4_serial.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(428, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 26)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Brand :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(381, 150)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(99, 26)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "Serial Number :"
        '
        'btn_save_monitor
        '
        Me.btn_save_monitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save_monitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_monitor.Location = New System.Drawing.Point(497, 497)
        Me.btn_save_monitor.Name = "btn_save_monitor"
        Me.btn_save_monitor.Size = New System.Drawing.Size(80, 38)
        Me.btn_save_monitor.TabIndex = 8
        Me.btn_save_monitor.Text = "Save"
        Me.btn_save_monitor.UseVisualStyleBackColor = True
        '
        'txt4_model
        '
        Me.txt4_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_model.Location = New System.Drawing.Point(486, 70)
        Me.txt4_model.Name = "txt4_model"
        Me.txt4_model.Size = New System.Drawing.Size(202, 34)
        Me.txt4_model.TabIndex = 2
        '
        'txt4_detail
        '
        Me.txt4_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_detail.Location = New System.Drawing.Point(486, 190)
        Me.txt4_detail.Multiline = True
        Me.txt4_detail.Name = "txt4_detail"
        Me.txt4_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt4_detail.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(430, 190)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 26)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Detail :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(428, 70)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(52, 26)
        Me.Label14.TabIndex = 5
        Me.Label14.Text = "Model :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(438, 109)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(42, 26)
        Me.Label22.TabIndex = 13
        Me.Label22.Text = "Size :"
        '
        'txt4_size
        '
        Me.txt4_size.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_size.Location = New System.Drawing.Point(486, 110)
        Me.txt4_size.Name = "txt4_size"
        Me.txt4_size.Size = New System.Drawing.Size(55, 34)
        Me.txt4_size.TabIndex = 3
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Label80)
        Me.TabPage4.Controls.Add(Me.txt5_note_other)
        Me.TabPage4.Controls.Add(Me.Label75)
        Me.TabPage4.Controls.Add(Me.ComboBoxtype_other)
        Me.TabPage4.Controls.Add(Me.ComboBoxbrand5)
        Me.TabPage4.Controls.Add(Me.Label67)
        Me.TabPage4.Controls.Add(Me.Label68)
        Me.TabPage4.Controls.Add(Me.Label57)
        Me.TabPage4.Controls.Add(Me.btn_cancel_other)
        Me.TabPage4.Controls.Add(Me.txt5_price)
        Me.TabPage4.Controls.Add(Me.Label37)
        Me.TabPage4.Controls.Add(Me.txt5_serial)
        Me.TabPage4.Controls.Add(Me.Label23)
        Me.TabPage4.Controls.Add(Me.Label16)
        Me.TabPage4.Controls.Add(Me.txt5_detail)
        Me.TabPage4.Controls.Add(Me.Label15)
        Me.TabPage4.Controls.Add(Me.Label31)
        Me.TabPage4.Controls.Add(Me.txt5_model)
        Me.TabPage4.Controls.Add(Me.btn_save_other)
        Me.TabPage4.Location = New System.Drawing.Point(4, 35)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1071, 541)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Other"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(430, 267)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(44, 26)
        Me.Label80.TabIndex = 66
        Me.Label80.Text = "Note :"
        '
        'txt5_note_other
        '
        Me.txt5_note_other.Location = New System.Drawing.Point(480, 267)
        Me.txt5_note_other.Name = "txt5_note_other"
        Me.txt5_note_other.Size = New System.Drawing.Size(180, 52)
        Me.txt5_note_other.TabIndex = 7
        Me.txt5_note_other.Text = ""
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(429, 187)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(45, 26)
        Me.Label75.TabIndex = 64
        Me.Label75.Text = "Type :"
        '
        'ComboBoxtype_other
        '
        Me.ComboBoxtype_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxtype_other.FormattingEnabled = True
        Me.ComboBoxtype_other.Location = New System.Drawing.Point(480, 187)
        Me.ComboBoxtype_other.Name = "ComboBoxtype_other"
        Me.ComboBoxtype_other.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxtype_other.TabIndex = 5
        '
        'ComboBoxbrand5
        '
        Me.ComboBoxbrand5.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxbrand5.FormattingEnabled = True
        Me.ComboBoxbrand5.Location = New System.Drawing.Point(480, 27)
        Me.ComboBoxbrand5.Name = "ComboBoxbrand5"
        Me.ComboBoxbrand5.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand5.TabIndex = 1
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(477, 332)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(115, 26)
        Me.Label67.TabIndex = 59
        Me.Label67.Text = "=  บังคับกรอกข้อมูล"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.Red
        Me.Label68.Location = New System.Drawing.Point(456, 338)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(15, 20)
        Me.Label68.TabIndex = 58
        Me.Label68.Text = "*"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label57.ForeColor = System.Drawing.Color.Red
        Me.Label57.Location = New System.Drawing.Point(688, 111)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(15, 20)
        Me.Label57.TabIndex = 56
        Me.Label57.Text = "*"
        '
        'btn_cancel_other
        '
        Me.btn_cancel_other.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel_other.Location = New System.Drawing.Point(582, 495)
        Me.btn_cancel_other.Name = "btn_cancel_other"
        Me.btn_cancel_other.Size = New System.Drawing.Size(80, 40)
        Me.btn_cancel_other.TabIndex = 9
        Me.btn_cancel_other.Text = "Cancel"
        Me.btn_cancel_other.UseVisualStyleBackColor = True
        '
        'txt5_price
        '
        Me.txt5_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_price.Location = New System.Drawing.Point(480, 227)
        Me.txt5_price.Name = "txt5_price"
        Me.txt5_price.Size = New System.Drawing.Size(55, 34)
        Me.txt5_price.TabIndex = 6
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(427, 227)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(47, 26)
        Me.Label37.TabIndex = 54
        Me.Label37.Text = "Price :"
        '
        'txt5_serial
        '
        Me.txt5_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_serial.Location = New System.Drawing.Point(480, 107)
        Me.txt5_serial.Name = "txt5_serial"
        Me.txt5_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt5_serial.TabIndex = 3
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(375, 111)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(99, 26)
        Me.Label23.TabIndex = 10
        Me.Label23.Text = "Serial Number :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(424, 147)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(50, 26)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Detail :"
        '
        'txt5_detail
        '
        Me.txt5_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_detail.Location = New System.Drawing.Point(480, 147)
        Me.txt5_detail.Multiline = True
        Me.txt5_detail.Name = "txt5_detail"
        Me.txt5_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt5_detail.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(422, 67)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 26)
        Me.Label15.TabIndex = 4
        Me.Label15.Text = "Model :"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(422, 27)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(52, 26)
        Me.Label31.TabIndex = 3
        Me.Label31.Text = "Brand :"
        '
        'txt5_model
        '
        Me.txt5_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_model.Location = New System.Drawing.Point(480, 67)
        Me.txt5_model.Name = "txt5_model"
        Me.txt5_model.Size = New System.Drawing.Size(202, 34)
        Me.txt5_model.TabIndex = 2
        '
        'btn_save_other
        '
        Me.btn_save_other.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_save_other.Location = New System.Drawing.Point(496, 495)
        Me.btn_save_other.Name = "btn_save_other"
        Me.btn_save_other.Size = New System.Drawing.Size(80, 40)
        Me.btn_save_other.TabIndex = 8
        Me.btn_save_other.Text = "Save"
        Me.btn_save_other.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.GroupBox5)
        Me.TabPage5.Controls.Add(Me.GroupBox4)
        Me.TabPage5.Controls.Add(Me.Label81)
        Me.TabPage5.Controls.Add(Me.txt6_note_license)
        Me.TabPage5.Controls.Add(Me.Label69)
        Me.TabPage5.Controls.Add(Me.Label70)
        Me.TabPage5.Controls.Add(Me.Label58)
        Me.TabPage5.Controls.Add(Me.btn_cancel_license)
        Me.TabPage5.Controls.Add(Me.txt6_amount)
        Me.TabPage5.Controls.Add(Me.Label40)
        Me.TabPage5.Controls.Add(Me.btn_save_license)
        Me.TabPage5.Controls.Add(Me.Label39)
        Me.TabPage5.Controls.Add(Me.txt6_detail)
        Me.TabPage5.Controls.Add(Me.txt6_price)
        Me.TabPage5.Controls.Add(Me.Label38)
        Me.TabPage5.Controls.Add(Me.Label41)
        Me.TabPage5.Controls.Add(Me.txt6_brand)
        Me.TabPage5.Location = New System.Drawing.Point(4, 35)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(1071, 541)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "License"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label32)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox5.Controls.Add(Me.Label33)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox5.Location = New System.Drawing.Point(237, 22)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(367, 162)
        Me.GroupBox5.TabIndex = 1007
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Choose Date"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(58, 31)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(55, 26)
        Me.Label32.TabIndex = 6
        Me.Label32.Text = "วันที่ซื้อ :"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(119, 31)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 34)
        Me.DateTimePicker1.TabIndex = 100
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(15, 97)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(98, 26)
        Me.Label33.TabIndex = 7
        Me.Label33.Text = "หมดประกันเมื่อ :"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(119, 97)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(200, 34)
        Me.DateTimePicker2.TabIndex = 150
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Radioapp)
        Me.GroupBox4.Controls.Add(Me.Radioos)
        Me.GroupBox4.Location = New System.Drawing.Point(610, 22)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(199, 81)
        Me.GroupBox4.TabIndex = 1006
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Choose Type"
        '
        'Radioapp
        '
        Me.Radioapp.AutoSize = True
        Me.Radioapp.Location = New System.Drawing.Point(93, 33)
        Me.Radioapp.Name = "Radioapp"
        Me.Radioapp.Size = New System.Drawing.Size(93, 30)
        Me.Radioapp.TabIndex = 999
        Me.Radioapp.TabStop = True
        Me.Radioapp.Text = "Application"
        Me.Radioapp.UseVisualStyleBackColor = True
        '
        'Radioos
        '
        Me.Radioos.AutoSize = True
        Me.Radioos.Location = New System.Drawing.Point(22, 33)
        Me.Radioos.Name = "Radioos"
        Me.Radioos.Size = New System.Drawing.Size(48, 30)
        Me.Radioos.TabIndex = 998
        Me.Radioos.TabStop = True
        Me.Radioos.Text = "OS"
        Me.Radioos.UseVisualStyleBackColor = True
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(352, 366)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(44, 26)
        Me.Label81.TabIndex = 1005
        Me.Label81.Text = "Note :"
        '
        'txt6_note_license
        '
        Me.txt6_note_license.Location = New System.Drawing.Point(402, 366)
        Me.txt6_note_license.Name = "txt6_note_license"
        Me.txt6_note_license.Size = New System.Drawing.Size(180, 52)
        Me.txt6_note_license.TabIndex = 5
        Me.txt6_note_license.Text = ""
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(399, 433)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(115, 26)
        Me.Label69.TabIndex = 1003
        Me.Label69.Text = "=  บังคับกรอกข้อมูล"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.ForeColor = System.Drawing.Color.Red
        Me.Label70.Location = New System.Drawing.Point(381, 433)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(15, 20)
        Me.Label70.TabIndex = 1002
        Me.Label70.Text = "*"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label58.ForeColor = System.Drawing.Color.Red
        Me.Label58.Location = New System.Drawing.Point(610, 212)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(15, 20)
        Me.Label58.TabIndex = 1001
        Me.Label58.Text = "*"
        '
        'btn_cancel_license
        '
        Me.btn_cancel_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_cancel_license.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_cancel_license.Location = New System.Drawing.Point(584, 495)
        Me.btn_cancel_license.Name = "btn_cancel_license"
        Me.btn_cancel_license.Size = New System.Drawing.Size(80, 40)
        Me.btn_cancel_license.TabIndex = 7
        Me.btn_cancel_license.Text = "Cancel"
        Me.btn_cancel_license.UseVisualStyleBackColor = True
        '
        'txt6_amount
        '
        Me.txt6_amount.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_amount.Location = New System.Drawing.Point(402, 326)
        Me.txt6_amount.Name = "txt6_amount"
        Me.txt6_amount.Size = New System.Drawing.Size(55, 34)
        Me.txt6_amount.TabIndex = 4
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(336, 326)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(60, 26)
        Me.Label40.TabIndex = 61
        Me.Label40.Text = "Amount :"
        '
        'btn_save_license
        '
        Me.btn_save_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_save_license.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_save_license.Location = New System.Drawing.Point(498, 495)
        Me.btn_save_license.Name = "btn_save_license"
        Me.btn_save_license.Size = New System.Drawing.Size(80, 40)
        Me.btn_save_license.TabIndex = 6
        Me.btn_save_license.Text = "Save"
        Me.btn_save_license.UseVisualStyleBackColor = True
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(346, 246)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(50, 26)
        Me.Label39.TabIndex = 59
        Me.Label39.Text = "Detail :"
        '
        'txt6_detail
        '
        Me.txt6_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_detail.Location = New System.Drawing.Point(402, 246)
        Me.txt6_detail.Multiline = True
        Me.txt6_detail.Name = "txt6_detail"
        Me.txt6_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt6_detail.TabIndex = 2
        '
        'txt6_price
        '
        Me.txt6_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_price.Location = New System.Drawing.Point(402, 286)
        Me.txt6_price.Name = "txt6_price"
        Me.txt6_price.Size = New System.Drawing.Size(55, 34)
        Me.txt6_price.TabIndex = 3
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(349, 286)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(47, 26)
        Me.Label38.TabIndex = 56
        Me.Label38.Text = "Price :"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(344, 206)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(52, 26)
        Me.Label41.TabIndex = 3
        Me.Label41.Text = "Brand :"
        '
        'txt6_brand
        '
        Me.txt6_brand.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_brand.Location = New System.Drawing.Point(402, 206)
        Me.txt6_brand.Name = "txt6_brand"
        Me.txt6_brand.Size = New System.Drawing.Size(202, 34)
        Me.txt6_brand.TabIndex = 1
        '
        'btnback
        '
        Me.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnback.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnback.Location = New System.Drawing.Point(995, 25)
        Me.btnback.Name = "btnback"
        Me.btnback.Size = New System.Drawing.Size(80, 40)
        Me.btnback.TabIndex = 1010
        Me.btnback.Text = "Home"
        Me.btnback.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label93)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.llogin)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1371, 92)
        Me.Panel1.TabIndex = 317
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label93.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label93.Location = New System.Drawing.Point(507, 59)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(334, 15)
        Me.Label93.TabIndex = 348
        Me.Label93.Text = " Developer By Computer @Diana Complex Shopping Center"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(374, 91)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 347
        Me.PictureBox1.TabStop = False
        '
        'llogin
        '
        Me.llogin.AutoSize = True
        Me.llogin.BackColor = System.Drawing.Color.Transparent
        Me.llogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.llogin.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.llogin.Location = New System.Drawing.Point(504, 24)
        Me.llogin.Name = "llogin"
        Me.llogin.Size = New System.Drawing.Size(388, 33)
        Me.llogin.TabIndex = 346
        Me.llogin.Text = "Device Management System"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel2.Controls.Add(Me.btnback)
        Me.Panel2.Controls.Add(Me.Label92)
        Me.Panel2.Controls.Add(Me.Label91)
        Me.Panel2.Controls.Add(Me.Label87)
        Me.Panel2.Controls.Add(Me.Label88)
        Me.Panel2.Controls.Add(Me.Label89)
        Me.Panel2.Controls.Add(Me.Label90)
        Me.Panel2.Location = New System.Drawing.Point(0, 678)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1095, 92)
        Me.Panel2.TabIndex = 348
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label92.Location = New System.Drawing.Point(133, 27)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(72, 34)
        Me.Label92.TabIndex = 345
        Me.Label92.Text = "Label92"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.BackColor = System.Drawing.Color.Transparent
        Me.Label91.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label91.ForeColor = System.Drawing.Color.Black
        Me.Label91.Location = New System.Drawing.Point(31, 31)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(107, 30)
        Me.Label91.TabIndex = 344
        Me.Label91.Text = "รหัสผู้เข้าใช้ :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.Location = New System.Drawing.Point(375, 29)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(72, 34)
        Me.Label87.TabIndex = 343
        Me.Label87.Text = "Label87"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.Color.Transparent
        Me.Label88.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.ForeColor = System.Drawing.Color.Black
        Me.Label88.Location = New System.Drawing.Point(644, 31)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(84, 30)
        Me.Label88.TabIndex = 3
        Me.Label88.Text = "ตำแหน่ง :"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label89.Location = New System.Drawing.Point(722, 29)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(72, 34)
        Me.Label89.TabIndex = 342
        Me.Label89.Text = "Label89"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.BackColor = System.Drawing.Color.Transparent
        Me.Label90.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label90.ForeColor = System.Drawing.Color.Black
        Me.Label90.Location = New System.Drawing.Point(333, 31)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(48, 30)
        Me.Label90.TabIndex = 1
        Me.Label90.Text = "คุณ :"
        '
        'frm_ADD_DEVICE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1084, 771)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_ADD_DEVICE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Add"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btn_cancel_com As System.Windows.Forms.Button
    Friend WithEvents btn_save_com As System.Windows.Forms.Button
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents txt1_note_com As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txt1_price As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents tab3detailsoftware As System.Windows.Forms.Label
    Friend WithEvents txt1_other As System.Windows.Forms.TextBox
    Friend WithEvents txt1_comname As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txt1_office As System.Windows.Forms.TextBox
    Friend WithEvents txt1_windows As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txt1_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txt1_vga As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txt1_serial As System.Windows.Forms.TextBox
    Friend WithEvents txt1_hdd As System.Windows.Forms.TextBox
    Friend WithEvents txt1_ram As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txt1_ip As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txt1_mainboard As System.Windows.Forms.TextBox
    Friend WithEvents txt1_cpu As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt1_case As System.Windows.Forms.TextBox
    Friend WithEvents txt1_model As System.Windows.Forms.TextBox
    Friend WithEvents txt1_ps As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt1_cd As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Radiolaserc As System.Windows.Forms.RadioButton
    Friend WithEvents Radiodot As System.Windows.Forms.RadioButton
    Friend WithEvents Radioinkjet As System.Windows.Forms.RadioButton
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents txt3_note_prin As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents btn_cencel_prin As System.Windows.Forms.Button
    Friend WithEvents txt3_price As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt3_serial As System.Windows.Forms.TextBox
    Friend WithEvents txt3_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt3_model As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btn_save_prin As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents txt4_note_monitor As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents btn_cancel_monitor As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt4_price As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txt4_serial As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btn_save_monitor As System.Windows.Forms.Button
    Friend WithEvents txt4_model As System.Windows.Forms.TextBox
    Friend WithEvents txt4_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txt4_size As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents txt5_note_other As System.Windows.Forms.RichTextBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxtype_other As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxbrand5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents btn_cancel_other As System.Windows.Forms.Button
    Friend WithEvents txt5_price As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txt5_serial As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt5_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txt5_model As System.Windows.Forms.TextBox
    Friend WithEvents btn_save_other As System.Windows.Forms.Button
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Radioapp As System.Windows.Forms.RadioButton
    Friend WithEvents Radioos As System.Windows.Forms.RadioButton
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents txt6_note_license As System.Windows.Forms.RichTextBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents btn_cancel_license As System.Windows.Forms.Button
    Friend WithEvents txt6_amount As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents btn_save_license As System.Windows.Forms.Button
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txt6_detail As System.Windows.Forms.TextBox
    Friend WithEvents txt6_price As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txt6_brand As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents llogin As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents btn_cancel_comr As System.Windows.Forms.Button
    Friend WithEvents btn_save_comr As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt2_note_comr As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txt2_price As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txt2_other As System.Windows.Forms.TextBox
    Friend WithEvents txt2_comname As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txt2_office As System.Windows.Forms.TextBox
    Friend WithEvents txt2_windows As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txt2_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents txt2_vga As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents txt2_serial As System.Windows.Forms.TextBox
    Friend WithEvents txt2_hdd As System.Windows.Forms.TextBox
    Friend WithEvents txt2_ram As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents txt2_ip As System.Windows.Forms.TextBox
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents txt2_mainboard As System.Windows.Forms.TextBox
    Friend WithEvents txt2_cpu As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents txt2_case As System.Windows.Forms.TextBox
    Friend WithEvents txt2_model As System.Windows.Forms.TextBox
    Friend WithEvents txt2_ps As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents txt2_cd As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnback As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents txt_store As System.Windows.Forms.TextBox
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents txt_store_rent As System.Windows.Forms.TextBox
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker6 As System.Windows.Forms.DateTimePicker
End Class
