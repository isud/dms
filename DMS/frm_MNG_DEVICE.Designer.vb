﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MNG_DEVICE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_MNG_DEVICE))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.llogin = New System.Windows.Forms.Label()
        Me.gp = New System.Windows.Forms.Panel()
        Me.btnback = New System.Windows.Forms.Button()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.txtwarrant = New System.Windows.Forms.TextBox()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.txtdatebuy = New System.Windows.Forms.TextBox()
        Me.txt1_store = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txt1_serial = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txt1_case = New System.Windows.Forms.TextBox()
        Me.txt1_cd = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt1_note_com = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand1 = New System.Windows.Forms.ComboBox()
        Me.txt1_price = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tab3detailsoftware = New System.Windows.Forms.Label()
        Me.txt1_other = New System.Windows.Forms.TextBox()
        Me.txt1_comname = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txt1_office = New System.Windows.Forms.TextBox()
        Me.txt1_windows = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txt1_detail = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt1_vga = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt1_hdd = New System.Windows.Forms.TextBox()
        Me.txt1_ram = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt1_ip = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txt1_mainboard = New System.Windows.Forms.TextBox()
        Me.txt1_cpu = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txt1_model = New System.Windows.Forms.TextBox()
        Me.txt1_ps = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnaddnew_com = New System.Windows.Forms.Button()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.txtshowcom = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txt1_idcom = New System.Windows.Forms.TextBox()
        Me.ListViewcom = New System.Windows.Forms.ListView()
        Me.name_container = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.agent = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader37 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader43 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader49 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtsearchcom = New System.Windows.Forms.TextBox()
        Me.btnsearchcom = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnsavecom = New System.Windows.Forms.Button()
        Me.btndeletecom = New System.Windows.Forms.Button()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.txtwarrantcomr = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.txtdatebutcomr = New System.Windows.Forms.TextBox()
        Me.txt2_store_rent = New System.Windows.Forms.TextBox()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txt2_serial = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txt2_idcomr = New System.Windows.Forms.TextBox()
        Me.btnaddnew_comrent = New System.Windows.Forms.Button()
        Me.btnsave_comrent = New System.Windows.Forms.Button()
        Me.btndelete_comr = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txt2_note_comr = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand2 = New System.Windows.Forms.ComboBox()
        Me.txt2_price = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txt2_other = New System.Windows.Forms.TextBox()
        Me.txt2_comname = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.txt2_office = New System.Windows.Forms.TextBox()
        Me.txt2_windows = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.txt2_detail = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.txt2_vga = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.txt2_hdd = New System.Windows.Forms.TextBox()
        Me.txt2_ram = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.txt2_ip = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.txt2_mainboard = New System.Windows.Forms.TextBox()
        Me.txt2_cpu = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.txt2_case = New System.Windows.Forms.TextBox()
        Me.txt2_model = New System.Windows.Forms.TextBox()
        Me.txt2_ps = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.txt2_cd = New System.Windows.Forms.TextBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.txtshowcomrent = New System.Windows.Forms.TextBox()
        Me.txtsearchcomrent = New System.Windows.Forms.TextBox()
        Me.btnsearchcomrent = New System.Windows.Forms.Button()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.ListViewcomrent = New System.Windows.Forms.ListView()
        Me.ColumnHeader23 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader26 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader27 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader38 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader44 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader50 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnaddnew_prinnew = New System.Windows.Forms.Button()
        Me.btnsave_print = New System.Windows.Forms.Button()
        Me.btndelete_print = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Radiolaserc = New System.Windows.Forms.RadioButton()
        Me.Radiodot = New System.Windows.Forms.RadioButton()
        Me.Radioinkjet = New System.Windows.Forms.RadioButton()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.txt3_note_prin = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand3 = New System.Windows.Forms.ComboBox()
        Me.txt3_price = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt3_serial = New System.Windows.Forms.TextBox()
        Me.txt3_detail = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt3_model = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtshowprin = New System.Windows.Forms.TextBox()
        Me.txtsearchprin = New System.Windows.Forms.TextBox()
        Me.btnsearch_prin = New System.Windows.Forms.Button()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.ListViewprin = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader39 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader45 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader51 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txt3_idprint = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnaddnew_monitor = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.btn_delete_monitor = New System.Windows.Forms.Button()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.txt4_note_monitor = New System.Windows.Forms.RichTextBox()
        Me.ComboBoxbrand4 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt4_price = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt4_serial = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txt4_model = New System.Windows.Forms.TextBox()
        Me.txt4_detail = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.txt4_size = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtshowmonitor = New System.Windows.Forms.TextBox()
        Me.txtsearchmonitor = New System.Windows.Forms.TextBox()
        Me.btnsearch_monitor = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ListViewmonitor = New System.Windows.Forms.ListView()
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader40 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader46 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader52 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txt4_idmonitor = New System.Windows.Forms.TextBox()
        Me.btngoadd = New System.Windows.Forms.TabPage()
        Me.ListViewother = New System.Windows.Forms.ListView()
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader35 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader36 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader41 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader47 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader53 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnaddnew_other = New System.Windows.Forms.Button()
        Me.btnsaveother = New System.Windows.Forms.Button()
        Me.btn_delete_other = New System.Windows.Forms.Button()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.txt5_note_other = New System.Windows.Forms.RichTextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.ComboBoxtype_other = New System.Windows.Forms.ComboBox()
        Me.ComboBoxbrand5 = New System.Windows.Forms.ComboBox()
        Me.txt5_price = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt5_serial = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txt5_detail = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.txt5_model = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtshowother = New System.Windows.Forms.TextBox()
        Me.txtsearchother = New System.Windows.Forms.TextBox()
        Me.btnsearch_other = New System.Windows.Forms.Button()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txt5_idother = New System.Windows.Forms.TextBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.txt6_idlicense = New System.Windows.Forms.TextBox()
        Me.btnaddnew_license = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.btn_delete_license = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Radioapp = New System.Windows.Forms.RadioButton()
        Me.Radioos = New System.Windows.Forms.RadioButton()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.txt6_note_license = New System.Windows.Forms.RichTextBox()
        Me.txt6_amount = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txt6_detail = New System.Windows.Forms.TextBox()
        Me.txt6_price = New System.Windows.Forms.TextBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txt6_brand = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.txtshowlicense = New System.Windows.Forms.TextBox()
        Me.txtsearchlicense = New System.Windows.Forms.TextBox()
        Me.btnsearch_license = New System.Windows.Forms.Button()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.ListViewlicense = New System.Windows.Forms.ListView()
        Me.ColumnHeader18 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader19 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader20 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader42 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader48 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader54 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader28 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader29 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader30 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader31 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader32 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader33 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader34 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gp.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.btngoadd.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label93)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.llogin)
        Me.Panel1.Location = New System.Drawing.Point(1, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1371, 92)
        Me.Panel1.TabIndex = 318
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label93.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label93.Location = New System.Drawing.Point(542, 61)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(334, 15)
        Me.Label93.TabIndex = 348
        Me.Label93.Text = " Developer By Computer @Diana Complex Shopping Center"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(374, 91)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 347
        Me.PictureBox1.TabStop = False
        '
        'llogin
        '
        Me.llogin.AutoSize = True
        Me.llogin.BackColor = System.Drawing.Color.Transparent
        Me.llogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.llogin.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.llogin.Location = New System.Drawing.Point(539, 26)
        Me.llogin.Name = "llogin"
        Me.llogin.Size = New System.Drawing.Size(388, 33)
        Me.llogin.TabIndex = 346
        Me.llogin.Text = "Device Management System"
        '
        'gp
        '
        Me.gp.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gp.Controls.Add(Me.btnback)
        Me.gp.Controls.Add(Me.Label92)
        Me.gp.Controls.Add(Me.Label91)
        Me.gp.Controls.Add(Me.Label87)
        Me.gp.Controls.Add(Me.Label88)
        Me.gp.Controls.Add(Me.Label89)
        Me.gp.Controls.Add(Me.Label90)
        Me.gp.Location = New System.Drawing.Point(1, 720)
        Me.gp.Name = "gp"
        Me.gp.Size = New System.Drawing.Size(1382, 92)
        Me.gp.TabIndex = 349
        '
        'btnback
        '
        Me.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnback.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnback.Location = New System.Drawing.Point(1262, 25)
        Me.btnback.Name = "btnback"
        Me.btnback.Size = New System.Drawing.Size(80, 40)
        Me.btnback.TabIndex = 1010
        Me.btnback.Text = "Home"
        Me.btnback.UseVisualStyleBackColor = True
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label92.Location = New System.Drawing.Point(133, 27)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(72, 34)
        Me.Label92.TabIndex = 345
        Me.Label92.Text = "Label92"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.BackColor = System.Drawing.Color.Transparent
        Me.Label91.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label91.ForeColor = System.Drawing.Color.Black
        Me.Label91.Location = New System.Drawing.Point(31, 31)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(107, 30)
        Me.Label91.TabIndex = 344
        Me.Label91.Text = "รหัสผู้เข้าใช้ :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.Location = New System.Drawing.Point(375, 29)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(72, 34)
        Me.Label87.TabIndex = 343
        Me.Label87.Text = "Label87"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.Color.Transparent
        Me.Label88.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.ForeColor = System.Drawing.Color.Black
        Me.Label88.Location = New System.Drawing.Point(644, 31)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(84, 30)
        Me.Label88.TabIndex = 3
        Me.Label88.Text = "ตำแหน่ง :"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label89.Location = New System.Drawing.Point(722, 29)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(72, 34)
        Me.Label89.TabIndex = 342
        Me.Label89.Text = "Label89"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.BackColor = System.Drawing.Color.Transparent
        Me.Label90.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label90.ForeColor = System.Drawing.Color.Black
        Me.Label90.Location = New System.Drawing.Point(333, 31)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(48, 30)
        Me.Label90.TabIndex = 1
        Me.Label90.Text = "คุณ :"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.btngoadd)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(3, 94)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1380, 625)
        Me.TabControl1.TabIndex = 350
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label100)
        Me.TabPage1.Controls.Add(Me.Label99)
        Me.TabPage1.Controls.Add(Me.txtwarrant)
        Me.TabPage1.Controls.Add(Me.Label97)
        Me.TabPage1.Controls.Add(Me.Label98)
        Me.TabPage1.Controls.Add(Me.txtdatebuy)
        Me.TabPage1.Controls.Add(Me.txt1_store)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label27)
        Me.TabPage1.Controls.Add(Me.txt1_serial)
        Me.TabPage1.Controls.Add(Me.Label38)
        Me.TabPage1.Controls.Add(Me.txt1_case)
        Me.TabPage1.Controls.Add(Me.txt1_cd)
        Me.TabPage1.Controls.Add(Me.Label43)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txt1_note_com)
        Me.TabPage1.Controls.Add(Me.ComboBoxbrand1)
        Me.TabPage1.Controls.Add(Me.txt1_price)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.tab3detailsoftware)
        Me.TabPage1.Controls.Add(Me.txt1_other)
        Me.TabPage1.Controls.Add(Me.txt1_comname)
        Me.TabPage1.Controls.Add(Me.Label30)
        Me.TabPage1.Controls.Add(Me.Label29)
        Me.TabPage1.Controls.Add(Me.txt1_office)
        Me.TabPage1.Controls.Add(Me.txt1_windows)
        Me.TabPage1.Controls.Add(Me.Label28)
        Me.TabPage1.Controls.Add(Me.txt1_detail)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.txt1_vga)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.txt1_hdd)
        Me.TabPage1.Controls.Add(Me.txt1_ram)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.txt1_ip)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label24)
        Me.TabPage1.Controls.Add(Me.txt1_mainboard)
        Me.TabPage1.Controls.Add(Me.txt1_cpu)
        Me.TabPage1.Controls.Add(Me.Label25)
        Me.TabPage1.Controls.Add(Me.txt1_model)
        Me.TabPage1.Controls.Add(Me.txt1_ps)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.btnaddnew_com)
        Me.TabPage1.Controls.Add(Me.Label67)
        Me.TabPage1.Controls.Add(Me.txtshowcom)
        Me.TabPage1.Controls.Add(Me.Label37)
        Me.TabPage1.Controls.Add(Me.txt1_idcom)
        Me.TabPage1.Controls.Add(Me.ListViewcom)
        Me.TabPage1.Controls.Add(Me.txtsearchcom)
        Me.TabPage1.Controls.Add(Me.btnsearchcom)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.btnsavecom)
        Me.TabPage1.Controls.Add(Me.btndeletecom)
        Me.TabPage1.Location = New System.Drawing.Point(4, 35)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1372, 586)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Computer"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(1259, 436)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(72, 26)
        Me.Label100.TabIndex = 1057
        Me.Label100.Text = "*14/6/2014"
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(1259, 393)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(72, 26)
        Me.Label99.TabIndex = 1056
        Me.Label99.Text = "*14/6/2014"
        '
        'txtwarrant
        '
        Me.txtwarrant.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtwarrant.Location = New System.Drawing.Point(1157, 433)
        Me.txtwarrant.Multiline = True
        Me.txtwarrant.Name = "txtwarrant"
        Me.txtwarrant.Size = New System.Drawing.Size(96, 34)
        Me.txtwarrant.TabIndex = 1054
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(1072, 433)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(79, 26)
        Me.Label97.TabIndex = 1055
        Me.Label97.Text = "หมดประกัน :"
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(1096, 396)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(55, 26)
        Me.Label98.TabIndex = 1053
        Me.Label98.Text = "วันที่ซื้อ :"
        '
        'txtdatebuy
        '
        Me.txtdatebuy.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdatebuy.Location = New System.Drawing.Point(1157, 393)
        Me.txtdatebuy.Name = "txtdatebuy"
        Me.txtdatebuy.Size = New System.Drawing.Size(96, 34)
        Me.txtdatebuy.TabIndex = 1052
        '
        'txt1_store
        '
        Me.txt1_store.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_store.Location = New System.Drawing.Point(835, 493)
        Me.txt1_store.Multiline = True
        Me.txt1_store.Name = "txt1_store"
        Me.txt1_store.Size = New System.Drawing.Size(202, 34)
        Me.txt1_store.TabIndex = 1050
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(767, 493)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 26)
        Me.Label15.TabIndex = 1051
        Me.Label15.Text = "ชื่อผู้ขาย :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(730, 453)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(99, 26)
        Me.Label27.TabIndex = 1049
        Me.Label27.Text = "Serial Number :"
        '
        'txt1_serial
        '
        Me.txt1_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_serial.Location = New System.Drawing.Point(835, 453)
        Me.txt1_serial.Name = "txt1_serial"
        Me.txt1_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt1_serial.TabIndex = 1046
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(782, 413)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(47, 26)
        Me.Label38.TabIndex = 1048
        Me.Label38.Text = "Case :"
        '
        'txt1_case
        '
        Me.txt1_case.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_case.Location = New System.Drawing.Point(835, 413)
        Me.txt1_case.Name = "txt1_case"
        Me.txt1_case.Size = New System.Drawing.Size(202, 34)
        Me.txt1_case.TabIndex = 1045
        '
        'txt1_cd
        '
        Me.txt1_cd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_cd.Location = New System.Drawing.Point(835, 373)
        Me.txt1_cd.Name = "txt1_cd"
        Me.txt1_cd.Size = New System.Drawing.Size(202, 34)
        Me.txt1_cd.TabIndex = 1044
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(765, 373)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(64, 26)
        Me.Label43.TabIndex = 1047
        Me.Label43.Text = "CD/DVD :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1107, 332)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 26)
        Me.Label1.TabIndex = 1042
        Me.Label1.Text = "Note :"
        '
        'txt1_note_com
        '
        Me.txt1_note_com.Location = New System.Drawing.Point(1157, 335)
        Me.txt1_note_com.Name = "txt1_note_com"
        Me.txt1_note_com.Size = New System.Drawing.Size(180, 52)
        Me.txt1_note_com.TabIndex = 1032
        Me.txt1_note_com.Text = ""
        '
        'ComboBoxbrand1
        '
        Me.ComboBoxbrand1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ComboBoxbrand1.FormattingEnabled = True
        Me.ComboBoxbrand1.Location = New System.Drawing.Point(835, 53)
        Me.ComboBoxbrand1.Name = "ComboBoxbrand1"
        Me.ComboBoxbrand1.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand1.TabIndex = 1010
        '
        'txt1_price
        '
        Me.txt1_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_price.Location = New System.Drawing.Point(1157, 295)
        Me.txt1_price.Name = "txt1_price"
        Me.txt1_price.Size = New System.Drawing.Size(55, 34)
        Me.txt1_price.TabIndex = 1031
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(1104, 292)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 26)
        Me.Label6.TabIndex = 1040
        Me.Label6.Text = "Price :"
        '
        'tab3detailsoftware
        '
        Me.tab3detailsoftware.AutoSize = True
        Me.tab3detailsoftware.Location = New System.Drawing.Point(1102, 213)
        Me.tab3detailsoftware.Name = "tab3detailsoftware"
        Me.tab3detailsoftware.Size = New System.Drawing.Size(49, 26)
        Me.tab3detailsoftware.TabIndex = 1039
        Me.tab3detailsoftware.Text = "Other :"
        '
        'txt1_other
        '
        Me.txt1_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_other.Location = New System.Drawing.Point(1157, 213)
        Me.txt1_other.Multiline = True
        Me.txt1_other.Name = "txt1_other"
        Me.txt1_other.Size = New System.Drawing.Size(202, 35)
        Me.txt1_other.TabIndex = 1028
        '
        'txt1_comname
        '
        Me.txt1_comname.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_comname.Location = New System.Drawing.Point(1157, 53)
        Me.txt1_comname.Name = "txt1_comname"
        Me.txt1_comname.Size = New System.Drawing.Size(202, 34)
        Me.txt1_comname.TabIndex = 1022
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(1043, 52)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(108, 26)
        Me.Label30.TabIndex = 1038
        Me.Label30.Text = "ComputerName :"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(1100, 173)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(51, 26)
        Me.Label29.TabIndex = 1037
        Me.Label29.Text = "Office :"
        '
        'txt1_office
        '
        Me.txt1_office.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_office.Location = New System.Drawing.Point(1157, 173)
        Me.txt1_office.Name = "txt1_office"
        Me.txt1_office.Size = New System.Drawing.Size(202, 34)
        Me.txt1_office.TabIndex = 1025
        '
        'txt1_windows
        '
        Me.txt1_windows.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_windows.Location = New System.Drawing.Point(1157, 133)
        Me.txt1_windows.Name = "txt1_windows"
        Me.txt1_windows.Size = New System.Drawing.Size(202, 34)
        Me.txt1_windows.TabIndex = 1024
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(1082, 133)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(69, 26)
        Me.Label28.TabIndex = 1036
        Me.Label28.Text = "Windows :"
        '
        'txt1_detail
        '
        Me.txt1_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_detail.Location = New System.Drawing.Point(1157, 255)
        Me.txt1_detail.Multiline = True
        Me.txt1_detail.Name = "txt1_detail"
        Me.txt1_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt1_detail.TabIndex = 1029
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(760, 256)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(69, 26)
        Me.Label20.TabIndex = 1027
        Me.Label20.Text = "Harddisk :"
        '
        'txt1_vga
        '
        Me.txt1_vga.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_vga.Location = New System.Drawing.Point(835, 293)
        Me.txt1_vga.Name = "txt1_vga"
        Me.txt1_vga.Size = New System.Drawing.Size(202, 34)
        Me.txt1_vga.TabIndex = 1018
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(785, 213)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(44, 26)
        Me.Label19.TabIndex = 1026
        Me.Label19.Text = "Ram :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(758, 293)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(71, 26)
        Me.Label21.TabIndex = 1030
        Me.Label21.Text = "Vga Card :"
        '
        'txt1_hdd
        '
        Me.txt1_hdd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_hdd.Location = New System.Drawing.Point(835, 253)
        Me.txt1_hdd.Name = "txt1_hdd"
        Me.txt1_hdd.Size = New System.Drawing.Size(202, 34)
        Me.txt1_hdd.TabIndex = 1017
        '
        'txt1_ram
        '
        Me.txt1_ram.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ram.Location = New System.Drawing.Point(835, 213)
        Me.txt1_ram.Name = "txt1_ram"
        Me.txt1_ram.Size = New System.Drawing.Size(202, 34)
        Me.txt1_ram.TabIndex = 1015
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(1073, 92)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(78, 26)
        Me.Label10.TabIndex = 1033
        Me.Label10.Text = "IP Address :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(751, 173)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 26)
        Me.Label13.TabIndex = 1021
        Me.Label13.Text = "Mainboard :"
        '
        'txt1_ip
        '
        Me.txt1_ip.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ip.Location = New System.Drawing.Point(1157, 93)
        Me.txt1_ip.Name = "txt1_ip"
        Me.txt1_ip.Size = New System.Drawing.Size(202, 34)
        Me.txt1_ip.TabIndex = 1023
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(785, 133)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 26)
        Me.Label12.TabIndex = 1020
        Me.Label12.Text = "CPU :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(1101, 255)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(50, 26)
        Me.Label11.TabIndex = 1034
        Me.Label11.Text = "Detail :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(737, 333)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(92, 26)
        Me.Label24.TabIndex = 1035
        Me.Label24.Text = "PowerSupply :"
        '
        'txt1_mainboard
        '
        Me.txt1_mainboard.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_mainboard.Location = New System.Drawing.Point(835, 173)
        Me.txt1_mainboard.Name = "txt1_mainboard"
        Me.txt1_mainboard.Size = New System.Drawing.Size(202, 34)
        Me.txt1_mainboard.TabIndex = 1014
        '
        'txt1_cpu
        '
        Me.txt1_cpu.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_cpu.Location = New System.Drawing.Point(835, 133)
        Me.txt1_cpu.Name = "txt1_cpu"
        Me.txt1_cpu.Size = New System.Drawing.Size(202, 34)
        Me.txt1_cpu.TabIndex = 1012
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(777, 93)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(52, 26)
        Me.Label25.TabIndex = 1016
        Me.Label25.Text = "Model :"
        '
        'txt1_model
        '
        Me.txt1_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_model.Location = New System.Drawing.Point(835, 93)
        Me.txt1_model.Name = "txt1_model"
        Me.txt1_model.Size = New System.Drawing.Size(202, 34)
        Me.txt1_model.TabIndex = 1011
        '
        'txt1_ps
        '
        Me.txt1_ps.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt1_ps.Location = New System.Drawing.Point(835, 333)
        Me.txt1_ps.Name = "txt1_ps"
        Me.txt1_ps.Size = New System.Drawing.Size(202, 34)
        Me.txt1_ps.TabIndex = 1019
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(777, 53)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(52, 26)
        Me.Label26.TabIndex = 1013
        Me.Label26.Text = "Brand :"
        '
        'btnaddnew_com
        '
        Me.btnaddnew_com.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaddnew_com.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddnew_com.Location = New System.Drawing.Point(1039, 534)
        Me.btnaddnew_com.Name = "btnaddnew_com"
        Me.btnaddnew_com.Size = New System.Drawing.Size(85, 39)
        Me.btnaddnew_com.TabIndex = 182
        Me.btnaddnew_com.Text = "ADD NEW"
        Me.btnaddnew_com.UseVisualStyleBackColor = True
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(617, 19)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(45, 26)
        Me.Label67.TabIndex = 181
        Me.Label67.Text = "Count"
        '
        'txtshowcom
        '
        Me.txtshowcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowcom.Location = New System.Drawing.Point(668, 16)
        Me.txtshowcom.Name = "txtshowcom"
        Me.txtshowcom.ReadOnly = True
        Me.txtshowcom.Size = New System.Drawing.Size(54, 34)
        Me.txtshowcom.TabIndex = 180
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(799, 16)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(30, 26)
        Me.Label37.TabIndex = 158
        Me.Label37.Text = "ID :"
        '
        'txt1_idcom
        '
        Me.txt1_idcom.Location = New System.Drawing.Point(835, 13)
        Me.txt1_idcom.Name = "txt1_idcom"
        Me.txt1_idcom.ReadOnly = True
        Me.txt1_idcom.Size = New System.Drawing.Size(55, 34)
        Me.txt1_idcom.TabIndex = 157
        '
        'ListViewcom
        '
        Me.ListViewcom.BackColor = System.Drawing.Color.White
        Me.ListViewcom.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.name_container, Me.agent, Me.ColumnHeader1, Me.ColumnHeader2, Me.size, Me.ColumnHeader37, Me.ColumnHeader43, Me.ColumnHeader49})
        Me.ListViewcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewcom.FullRowSelect = True
        Me.ListViewcom.GridLines = True
        Me.ListViewcom.HideSelection = False
        Me.ListViewcom.Location = New System.Drawing.Point(6, 56)
        Me.ListViewcom.Name = "ListViewcom"
        Me.ListViewcom.Size = New System.Drawing.Size(718, 527)
        Me.ListViewcom.TabIndex = 117
        Me.ListViewcom.UseCompatibleStateImageBehavior = False
        Me.ListViewcom.View = System.Windows.Forms.View.Details
        '
        'name_container
        '
        Me.name_container.Text = "ID"
        '
        'agent
        '
        Me.agent.Text = "Type"
        Me.agent.Width = 80
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Brand"
        Me.ColumnHeader1.Width = 75
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Detail"
        Me.ColumnHeader2.Width = 160
        '
        'size
        '
        Me.size.Text = "Status"
        Me.size.Width = 70
        '
        'ColumnHeader37
        '
        Me.ColumnHeader37.Text = "Officer Add"
        Me.ColumnHeader37.Width = 90
        '
        'ColumnHeader43
        '
        Me.ColumnHeader43.Text = "Section"
        Me.ColumnHeader43.Width = 90
        '
        'ColumnHeader49
        '
        Me.ColumnHeader49.Text = "Officer Edit"
        Me.ColumnHeader49.Width = 90
        '
        'txtsearchcom
        '
        Me.txtsearchcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtsearchcom.Location = New System.Drawing.Point(86, 14)
        Me.txtsearchcom.Name = "txtsearchcom"
        Me.txtsearchcom.Size = New System.Drawing.Size(305, 34)
        Me.txtsearchcom.TabIndex = 115
        '
        'btnsearchcom
        '
        Me.btnsearchcom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearchcom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearchcom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearchcom.Location = New System.Drawing.Point(397, 13)
        Me.btnsearchcom.Name = "btnsearchcom"
        Me.btnsearchcom.Size = New System.Drawing.Size(68, 34)
        Me.btnsearchcom.TabIndex = 116
        Me.btnsearchcom.Text = "Search"
        Me.btnsearchcom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearchcom.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 34)
        Me.Label2.TabIndex = 114
        Me.Label2.Text = "Search :"
        '
        'btnsavecom
        '
        Me.btnsavecom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsavecom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsavecom.Location = New System.Drawing.Point(1130, 534)
        Me.btnsavecom.Name = "btnsavecom"
        Me.btnsavecom.Size = New System.Drawing.Size(85, 39)
        Me.btnsavecom.TabIndex = 18
        Me.btnsavecom.Text = "EDIT NOW"
        Me.btnsavecom.UseVisualStyleBackColor = True
        '
        'btndeletecom
        '
        Me.btndeletecom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btndeletecom.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndeletecom.ForeColor = System.Drawing.Color.Black
        Me.btndeletecom.Location = New System.Drawing.Point(1221, 534)
        Me.btndeletecom.Name = "btndeletecom"
        Me.btndeletecom.Size = New System.Drawing.Size(85, 39)
        Me.btndeletecom.TabIndex = 19
        Me.btndeletecom.Text = "Delete"
        Me.btndeletecom.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Label84)
        Me.TabPage6.Controls.Add(Me.Label85)
        Me.TabPage6.Controls.Add(Me.txtwarrantcomr)
        Me.TabPage6.Controls.Add(Me.Label86)
        Me.TabPage6.Controls.Add(Me.Label96)
        Me.TabPage6.Controls.Add(Me.txtdatebutcomr)
        Me.TabPage6.Controls.Add(Me.txt2_store_rent)
        Me.TabPage6.Controls.Add(Me.Label95)
        Me.TabPage6.Controls.Add(Me.Label42)
        Me.TabPage6.Controls.Add(Me.txt2_serial)
        Me.TabPage6.Controls.Add(Me.Label48)
        Me.TabPage6.Controls.Add(Me.txt2_idcomr)
        Me.TabPage6.Controls.Add(Me.btnaddnew_comrent)
        Me.TabPage6.Controls.Add(Me.btnsave_comrent)
        Me.TabPage6.Controls.Add(Me.btndelete_comr)
        Me.TabPage6.Controls.Add(Me.Label44)
        Me.TabPage6.Controls.Add(Me.txt2_note_comr)
        Me.TabPage6.Controls.Add(Me.ComboBoxbrand2)
        Me.TabPage6.Controls.Add(Me.txt2_price)
        Me.TabPage6.Controls.Add(Me.Label51)
        Me.TabPage6.Controls.Add(Me.Label52)
        Me.TabPage6.Controls.Add(Me.txt2_other)
        Me.TabPage6.Controls.Add(Me.txt2_comname)
        Me.TabPage6.Controls.Add(Me.Label53)
        Me.TabPage6.Controls.Add(Me.Label55)
        Me.TabPage6.Controls.Add(Me.txt2_office)
        Me.TabPage6.Controls.Add(Me.txt2_windows)
        Me.TabPage6.Controls.Add(Me.Label59)
        Me.TabPage6.Controls.Add(Me.txt2_detail)
        Me.TabPage6.Controls.Add(Me.Label60)
        Me.TabPage6.Controls.Add(Me.txt2_vga)
        Me.TabPage6.Controls.Add(Me.Label61)
        Me.TabPage6.Controls.Add(Me.Label62)
        Me.TabPage6.Controls.Add(Me.txt2_hdd)
        Me.TabPage6.Controls.Add(Me.txt2_ram)
        Me.TabPage6.Controls.Add(Me.Label63)
        Me.TabPage6.Controls.Add(Me.Label64)
        Me.TabPage6.Controls.Add(Me.Label65)
        Me.TabPage6.Controls.Add(Me.txt2_ip)
        Me.TabPage6.Controls.Add(Me.Label66)
        Me.TabPage6.Controls.Add(Me.Label73)
        Me.TabPage6.Controls.Add(Me.Label74)
        Me.TabPage6.Controls.Add(Me.txt2_mainboard)
        Me.TabPage6.Controls.Add(Me.txt2_cpu)
        Me.TabPage6.Controls.Add(Me.Label76)
        Me.TabPage6.Controls.Add(Me.txt2_case)
        Me.TabPage6.Controls.Add(Me.txt2_model)
        Me.TabPage6.Controls.Add(Me.txt2_ps)
        Me.TabPage6.Controls.Add(Me.Label82)
        Me.TabPage6.Controls.Add(Me.txt2_cd)
        Me.TabPage6.Controls.Add(Me.Label83)
        Me.TabPage6.Controls.Add(Me.Label72)
        Me.TabPage6.Controls.Add(Me.txtshowcomrent)
        Me.TabPage6.Controls.Add(Me.txtsearchcomrent)
        Me.TabPage6.Controls.Add(Me.btnsearchcomrent)
        Me.TabPage6.Controls.Add(Me.Label54)
        Me.TabPage6.Controls.Add(Me.ListViewcomrent)
        Me.TabPage6.Location = New System.Drawing.Point(4, 35)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1372, 586)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Computer Rent"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(1259, 446)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(72, 26)
        Me.Label84.TabIndex = 1063
        Me.Label84.Text = "*14/6/2014"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(1259, 403)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(72, 26)
        Me.Label85.TabIndex = 1062
        Me.Label85.Text = "*14/6/2014"
        '
        'txtwarrantcomr
        '
        Me.txtwarrantcomr.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtwarrantcomr.Location = New System.Drawing.Point(1157, 443)
        Me.txtwarrantcomr.Multiline = True
        Me.txtwarrantcomr.Name = "txtwarrantcomr"
        Me.txtwarrantcomr.Size = New System.Drawing.Size(96, 34)
        Me.txtwarrantcomr.TabIndex = 1060
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(1072, 443)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(79, 26)
        Me.Label86.TabIndex = 1061
        Me.Label86.Text = "หมดประกัน :"
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Location = New System.Drawing.Point(1092, 406)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(59, 26)
        Me.Label96.TabIndex = 1059
        Me.Label96.Text = "วันที่เช่า :"
        '
        'txtdatebutcomr
        '
        Me.txtdatebutcomr.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdatebutcomr.Location = New System.Drawing.Point(1157, 403)
        Me.txtdatebutcomr.Name = "txtdatebutcomr"
        Me.txtdatebutcomr.Size = New System.Drawing.Size(96, 34)
        Me.txtdatebutcomr.TabIndex = 1058
        '
        'txt2_store_rent
        '
        Me.txt2_store_rent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_store_rent.Location = New System.Drawing.Point(835, 502)
        Me.txt2_store_rent.Multiline = True
        Me.txt2_store_rent.Name = "txt2_store_rent"
        Me.txt2_store_rent.Size = New System.Drawing.Size(202, 34)
        Me.txt2_store_rent.TabIndex = 1056
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(767, 502)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(62, 26)
        Me.Label95.TabIndex = 1057
        Me.Label95.Text = "ชื่อผู้ขาย :"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(730, 462)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(99, 26)
        Me.Label42.TabIndex = 1055
        Me.Label42.Text = "Serial Number :"
        '
        'txt2_serial
        '
        Me.txt2_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_serial.Location = New System.Drawing.Point(835, 462)
        Me.txt2_serial.Name = "txt2_serial"
        Me.txt2_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt2_serial.TabIndex = 1054
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(799, 25)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(30, 26)
        Me.Label48.TabIndex = 1053
        Me.Label48.Text = "ID :"
        '
        'txt2_idcomr
        '
        Me.txt2_idcomr.Location = New System.Drawing.Point(835, 23)
        Me.txt2_idcomr.Name = "txt2_idcomr"
        Me.txt2_idcomr.ReadOnly = True
        Me.txt2_idcomr.Size = New System.Drawing.Size(55, 34)
        Me.txt2_idcomr.TabIndex = 1052
        '
        'btnaddnew_comrent
        '
        Me.btnaddnew_comrent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaddnew_comrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddnew_comrent.Location = New System.Drawing.Point(1039, 541)
        Me.btnaddnew_comrent.Name = "btnaddnew_comrent"
        Me.btnaddnew_comrent.Size = New System.Drawing.Size(85, 39)
        Me.btnaddnew_comrent.TabIndex = 1051
        Me.btnaddnew_comrent.Text = "ADD NEW"
        Me.btnaddnew_comrent.UseVisualStyleBackColor = True
        '
        'btnsave_comrent
        '
        Me.btnsave_comrent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsave_comrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave_comrent.Location = New System.Drawing.Point(1130, 541)
        Me.btnsave_comrent.Name = "btnsave_comrent"
        Me.btnsave_comrent.Size = New System.Drawing.Size(85, 39)
        Me.btnsave_comrent.TabIndex = 1049
        Me.btnsave_comrent.Text = "EDIT NOW"
        Me.btnsave_comrent.UseVisualStyleBackColor = True
        '
        'btndelete_comr
        '
        Me.btndelete_comr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btndelete_comr.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete_comr.ForeColor = System.Drawing.Color.Black
        Me.btndelete_comr.Location = New System.Drawing.Point(1221, 541)
        Me.btndelete_comr.Name = "btndelete_comr"
        Me.btndelete_comr.Size = New System.Drawing.Size(85, 39)
        Me.btndelete_comr.TabIndex = 1050
        Me.btndelete_comr.Text = "Delete"
        Me.btndelete_comr.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(1107, 342)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(44, 26)
        Me.Label44.TabIndex = 1047
        Me.Label44.Text = "Note :"
        '
        'txt2_note_comr
        '
        Me.txt2_note_comr.Location = New System.Drawing.Point(1157, 345)
        Me.txt2_note_comr.Name = "txt2_note_comr"
        Me.txt2_note_comr.Size = New System.Drawing.Size(180, 52)
        Me.txt2_note_comr.TabIndex = 1026
        Me.txt2_note_comr.Text = ""
        '
        'ComboBoxbrand2
        '
        Me.ComboBoxbrand2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ComboBoxbrand2.FormattingEnabled = True
        Me.ComboBoxbrand2.Location = New System.Drawing.Point(835, 63)
        Me.ComboBoxbrand2.Name = "ComboBoxbrand2"
        Me.ComboBoxbrand2.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand2.TabIndex = 1009
        '
        'txt2_price
        '
        Me.txt2_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_price.Location = New System.Drawing.Point(1157, 305)
        Me.txt2_price.Name = "txt2_price"
        Me.txt2_price.Size = New System.Drawing.Size(55, 34)
        Me.txt2_price.TabIndex = 1025
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(1104, 302)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(47, 26)
        Me.Label51.TabIndex = 1043
        Me.Label51.Text = "Price :"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(1102, 223)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(49, 26)
        Me.Label52.TabIndex = 1042
        Me.Label52.Text = "Other :"
        '
        'txt2_other
        '
        Me.txt2_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_other.Location = New System.Drawing.Point(1157, 223)
        Me.txt2_other.Multiline = True
        Me.txt2_other.Name = "txt2_other"
        Me.txt2_other.Size = New System.Drawing.Size(202, 35)
        Me.txt2_other.TabIndex = 1023
        '
        'txt2_comname
        '
        Me.txt2_comname.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_comname.Location = New System.Drawing.Point(1157, 63)
        Me.txt2_comname.Name = "txt2_comname"
        Me.txt2_comname.Size = New System.Drawing.Size(202, 34)
        Me.txt2_comname.TabIndex = 1019
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(1043, 62)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(108, 26)
        Me.Label53.TabIndex = 1041
        Me.Label53.Text = "ComputerName :"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(1100, 183)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(51, 26)
        Me.Label55.TabIndex = 1040
        Me.Label55.Text = "Office :"
        '
        'txt2_office
        '
        Me.txt2_office.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_office.Location = New System.Drawing.Point(1157, 183)
        Me.txt2_office.Name = "txt2_office"
        Me.txt2_office.Size = New System.Drawing.Size(202, 34)
        Me.txt2_office.TabIndex = 1022
        '
        'txt2_windows
        '
        Me.txt2_windows.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_windows.Location = New System.Drawing.Point(1157, 143)
        Me.txt2_windows.Name = "txt2_windows"
        Me.txt2_windows.Size = New System.Drawing.Size(202, 34)
        Me.txt2_windows.TabIndex = 1021
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(1082, 143)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(69, 26)
        Me.Label59.TabIndex = 1039
        Me.Label59.Text = "Windows :"
        '
        'txt2_detail
        '
        Me.txt2_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_detail.Location = New System.Drawing.Point(1157, 265)
        Me.txt2_detail.Multiline = True
        Me.txt2_detail.Name = "txt2_detail"
        Me.txt2_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt2_detail.TabIndex = 1024
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(760, 266)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(69, 26)
        Me.Label60.TabIndex = 1032
        Me.Label60.Text = "Harddisk :"
        '
        'txt2_vga
        '
        Me.txt2_vga.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_vga.Location = New System.Drawing.Point(835, 303)
        Me.txt2_vga.Name = "txt2_vga"
        Me.txt2_vga.Size = New System.Drawing.Size(202, 34)
        Me.txt2_vga.TabIndex = 1015
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(785, 223)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(44, 26)
        Me.Label61.TabIndex = 1031
        Me.Label61.Text = "Ram :"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(758, 303)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(71, 26)
        Me.Label62.TabIndex = 1033
        Me.Label62.Text = "Vga Card :"
        '
        'txt2_hdd
        '
        Me.txt2_hdd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_hdd.Location = New System.Drawing.Point(835, 263)
        Me.txt2_hdd.Name = "txt2_hdd"
        Me.txt2_hdd.Size = New System.Drawing.Size(202, 34)
        Me.txt2_hdd.TabIndex = 1014
        '
        'txt2_ram
        '
        Me.txt2_ram.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_ram.Location = New System.Drawing.Point(835, 223)
        Me.txt2_ram.Name = "txt2_ram"
        Me.txt2_ram.Size = New System.Drawing.Size(202, 34)
        Me.txt2_ram.TabIndex = 1013
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(1073, 102)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(78, 26)
        Me.Label63.TabIndex = 1034
        Me.Label63.Text = "IP Address :"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(751, 183)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(78, 26)
        Me.Label64.TabIndex = 1030
        Me.Label64.Text = "Mainboard :"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(782, 422)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(47, 26)
        Me.Label65.TabIndex = 1038
        Me.Label65.Text = "Case :"
        '
        'txt2_ip
        '
        Me.txt2_ip.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_ip.Location = New System.Drawing.Point(1157, 103)
        Me.txt2_ip.Name = "txt2_ip"
        Me.txt2_ip.Size = New System.Drawing.Size(202, 34)
        Me.txt2_ip.TabIndex = 1020
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(785, 143)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(44, 26)
        Me.Label66.TabIndex = 1029
        Me.Label66.Text = "CPU :"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(1101, 262)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(50, 26)
        Me.Label73.TabIndex = 1035
        Me.Label73.Text = "Detail :"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(737, 343)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(92, 26)
        Me.Label74.TabIndex = 1036
        Me.Label74.Text = "PowerSupply :"
        '
        'txt2_mainboard
        '
        Me.txt2_mainboard.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_mainboard.Location = New System.Drawing.Point(835, 183)
        Me.txt2_mainboard.Name = "txt2_mainboard"
        Me.txt2_mainboard.Size = New System.Drawing.Size(202, 34)
        Me.txt2_mainboard.TabIndex = 1012
        '
        'txt2_cpu
        '
        Me.txt2_cpu.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_cpu.Location = New System.Drawing.Point(835, 143)
        Me.txt2_cpu.Name = "txt2_cpu"
        Me.txt2_cpu.Size = New System.Drawing.Size(202, 34)
        Me.txt2_cpu.TabIndex = 1011
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(777, 103)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(52, 26)
        Me.Label76.TabIndex = 1028
        Me.Label76.Text = "Model :"
        '
        'txt2_case
        '
        Me.txt2_case.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_case.Location = New System.Drawing.Point(835, 422)
        Me.txt2_case.Name = "txt2_case"
        Me.txt2_case.Size = New System.Drawing.Size(202, 34)
        Me.txt2_case.TabIndex = 1018
        '
        'txt2_model
        '
        Me.txt2_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_model.Location = New System.Drawing.Point(835, 103)
        Me.txt2_model.Name = "txt2_model"
        Me.txt2_model.Size = New System.Drawing.Size(202, 34)
        Me.txt2_model.TabIndex = 1010
        '
        'txt2_ps
        '
        Me.txt2_ps.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_ps.Location = New System.Drawing.Point(835, 343)
        Me.txt2_ps.Name = "txt2_ps"
        Me.txt2_ps.Size = New System.Drawing.Size(202, 34)
        Me.txt2_ps.TabIndex = 1016
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(777, 63)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(52, 26)
        Me.Label82.TabIndex = 1027
        Me.Label82.Text = "Brand :"
        '
        'txt2_cd
        '
        Me.txt2_cd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt2_cd.Location = New System.Drawing.Point(835, 382)
        Me.txt2_cd.Name = "txt2_cd"
        Me.txt2_cd.Size = New System.Drawing.Size(202, 34)
        Me.txt2_cd.TabIndex = 1017
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(765, 382)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(64, 26)
        Me.Label83.TabIndex = 1037
        Me.Label83.Text = "CD/DVD :"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(617, 24)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(45, 26)
        Me.Label72.TabIndex = 183
        Me.Label72.Text = "Count"
        '
        'txtshowcomrent
        '
        Me.txtshowcomrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowcomrent.Location = New System.Drawing.Point(668, 21)
        Me.txtshowcomrent.Name = "txtshowcomrent"
        Me.txtshowcomrent.ReadOnly = True
        Me.txtshowcomrent.Size = New System.Drawing.Size(54, 34)
        Me.txtshowcomrent.TabIndex = 182
        '
        'txtsearchcomrent
        '
        Me.txtsearchcomrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchcomrent.Location = New System.Drawing.Point(86, 17)
        Me.txtsearchcomrent.Name = "txtsearchcomrent"
        Me.txtsearchcomrent.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchcomrent.TabIndex = 134
        '
        'btnsearchcomrent
        '
        Me.btnsearchcomrent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearchcomrent.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearchcomrent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearchcomrent.Location = New System.Drawing.Point(385, 17)
        Me.btnsearchcomrent.Name = "btnsearchcomrent"
        Me.btnsearchcomrent.Size = New System.Drawing.Size(68, 34)
        Me.btnsearchcomrent.TabIndex = 135
        Me.btnsearchcomrent.Text = "Search"
        Me.btnsearchcomrent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearchcomrent.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label54.Location = New System.Drawing.Point(6, 15)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(74, 34)
        Me.Label54.TabIndex = 133
        Me.Label54.Text = "Search :"
        '
        'ListViewcomrent
        '
        Me.ListViewcomrent.BackColor = System.Drawing.Color.White
        Me.ListViewcomrent.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader23, Me.ColumnHeader24, Me.ColumnHeader25, Me.ColumnHeader26, Me.ColumnHeader27, Me.ColumnHeader38, Me.ColumnHeader44, Me.ColumnHeader50})
        Me.ListViewcomrent.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewcomrent.FullRowSelect = True
        Me.ListViewcomrent.GridLines = True
        Me.ListViewcomrent.HideSelection = False
        Me.ListViewcomrent.Location = New System.Drawing.Point(6, 61)
        Me.ListViewcomrent.Name = "ListViewcomrent"
        Me.ListViewcomrent.Size = New System.Drawing.Size(718, 525)
        Me.ListViewcomrent.TabIndex = 132
        Me.ListViewcomrent.UseCompatibleStateImageBehavior = False
        Me.ListViewcomrent.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader23
        '
        Me.ColumnHeader23.Text = "ID"
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "Type"
        Me.ColumnHeader24.Width = 80
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "Brand"
        Me.ColumnHeader25.Width = 80
        '
        'ColumnHeader26
        '
        Me.ColumnHeader26.Text = "Detail"
        Me.ColumnHeader26.Width = 140
        '
        'ColumnHeader27
        '
        Me.ColumnHeader27.Text = "Status"
        Me.ColumnHeader27.Width = 70
        '
        'ColumnHeader38
        '
        Me.ColumnHeader38.Text = "Officer Add"
        Me.ColumnHeader38.Width = 90
        '
        'ColumnHeader44
        '
        Me.ColumnHeader44.Text = "Section"
        Me.ColumnHeader44.Width = 100
        '
        'ColumnHeader50
        '
        Me.ColumnHeader50.Text = "Officer Edit"
        Me.ColumnHeader50.Width = 90
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnaddnew_prinnew)
        Me.TabPage2.Controls.Add(Me.btnsave_print)
        Me.TabPage2.Controls.Add(Me.btndelete_print)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.Label78)
        Me.TabPage2.Controls.Add(Me.txt3_note_prin)
        Me.TabPage2.Controls.Add(Me.ComboBoxbrand3)
        Me.TabPage2.Controls.Add(Me.txt3_price)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.txt3_serial)
        Me.TabPage2.Controls.Add(Me.txt3_detail)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.txt3_model)
        Me.TabPage2.Controls.Add(Me.Label56)
        Me.TabPage2.Controls.Add(Me.Label68)
        Me.TabPage2.Controls.Add(Me.Label31)
        Me.TabPage2.Controls.Add(Me.txtshowprin)
        Me.TabPage2.Controls.Add(Me.txtsearchprin)
        Me.TabPage2.Controls.Add(Me.btnsearch_prin)
        Me.TabPage2.Controls.Add(Me.Label49)
        Me.TabPage2.Controls.Add(Me.ListViewprin)
        Me.TabPage2.Controls.Add(Me.Label36)
        Me.TabPage2.Controls.Add(Me.txt3_idprint)
        Me.TabPage2.Location = New System.Drawing.Point(4, 35)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1372, 586)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Printer"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnaddnew_prinnew
        '
        Me.btnaddnew_prinnew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaddnew_prinnew.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddnew_prinnew.Location = New System.Drawing.Point(1039, 541)
        Me.btnaddnew_prinnew.Name = "btnaddnew_prinnew"
        Me.btnaddnew_prinnew.Size = New System.Drawing.Size(85, 39)
        Me.btnaddnew_prinnew.TabIndex = 1054
        Me.btnaddnew_prinnew.Text = "ADD NEW"
        Me.btnaddnew_prinnew.UseVisualStyleBackColor = True
        '
        'btnsave_print
        '
        Me.btnsave_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsave_print.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave_print.Location = New System.Drawing.Point(1130, 541)
        Me.btnsave_print.Name = "btnsave_print"
        Me.btnsave_print.Size = New System.Drawing.Size(85, 39)
        Me.btnsave_print.TabIndex = 1052
        Me.btnsave_print.Text = "EDIT NOW"
        Me.btnsave_print.UseVisualStyleBackColor = True
        '
        'btndelete_print
        '
        Me.btndelete_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btndelete_print.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete_print.ForeColor = System.Drawing.Color.Black
        Me.btndelete_print.Location = New System.Drawing.Point(1221, 541)
        Me.btndelete_print.Name = "btndelete_print"
        Me.btndelete_print.Size = New System.Drawing.Size(85, 39)
        Me.btndelete_print.TabIndex = 1053
        Me.btndelete_print.Text = "Delete"
        Me.btndelete_print.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Radiolaserc)
        Me.GroupBox2.Controls.Add(Me.Radiodot)
        Me.GroupBox2.Controls.Add(Me.Radioinkjet)
        Me.GroupBox2.Location = New System.Drawing.Point(810, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(549, 88)
        Me.GroupBox2.TabIndex = 218
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "เลือกประเภท"
        '
        'Radiolaserc
        '
        Me.Radiolaserc.AutoSize = True
        Me.Radiolaserc.Location = New System.Drawing.Point(37, 33)
        Me.Radiolaserc.Name = "Radiolaserc"
        Me.Radiolaserc.Size = New System.Drawing.Size(100, 30)
        Me.Radiolaserc.TabIndex = 8
        Me.Radiolaserc.TabStop = True
        Me.Radiolaserc.Text = "Laser Printer"
        Me.Radiolaserc.UseVisualStyleBackColor = True
        '
        'Radiodot
        '
        Me.Radiodot.AutoSize = True
        Me.Radiodot.Location = New System.Drawing.Point(218, 33)
        Me.Radiodot.Name = "Radiodot"
        Me.Radiodot.Size = New System.Drawing.Size(126, 30)
        Me.Radiodot.TabIndex = 10
        Me.Radiodot.TabStop = True
        Me.Radiodot.Text = "Dot-matrix Printer"
        Me.Radiodot.UseVisualStyleBackColor = True
        '
        'Radioinkjet
        '
        Me.Radioinkjet.AutoSize = True
        Me.Radioinkjet.Location = New System.Drawing.Point(425, 33)
        Me.Radioinkjet.Name = "Radioinkjet"
        Me.Radioinkjet.Size = New System.Drawing.Size(98, 30)
        Me.Radioinkjet.TabIndex = 11
        Me.Radioinkjet.TabStop = True
        Me.Radioinkjet.Text = "Inkjet Printer"
        Me.Radioinkjet.UseVisualStyleBackColor = True
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(970, 381)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(44, 26)
        Me.Label78.TabIndex = 217
        Me.Label78.Text = "Note :"
        '
        'txt3_note_prin
        '
        Me.txt3_note_prin.Location = New System.Drawing.Point(1020, 381)
        Me.txt3_note_prin.Name = "txt3_note_prin"
        Me.txt3_note_prin.Size = New System.Drawing.Size(202, 52)
        Me.txt3_note_prin.TabIndex = 212
        Me.txt3_note_prin.Text = ""
        '
        'ComboBoxbrand3
        '
        Me.ComboBoxbrand3.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxbrand3.FormattingEnabled = True
        Me.ComboBoxbrand3.Location = New System.Drawing.Point(1020, 180)
        Me.ComboBoxbrand3.Name = "ComboBoxbrand3"
        Me.ComboBoxbrand3.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand3.TabIndex = 205
        '
        'txt3_price
        '
        Me.txt3_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_price.Location = New System.Drawing.Point(1020, 341)
        Me.txt3_price.Name = "txt3_price"
        Me.txt3_price.Size = New System.Drawing.Size(55, 34)
        Me.txt3_price.TabIndex = 210
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(967, 341)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 26)
        Me.Label8.TabIndex = 215
        Me.Label8.Text = "Price :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(919, 261)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(95, 26)
        Me.Label18.TabIndex = 214
        Me.Label18.Text = "Serial Nunber :"
        '
        'txt3_serial
        '
        Me.txt3_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_serial.Location = New System.Drawing.Point(1020, 261)
        Me.txt3_serial.Name = "txt3_serial"
        Me.txt3_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt3_serial.TabIndex = 208
        '
        'txt3_detail
        '
        Me.txt3_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_detail.Location = New System.Drawing.Point(1020, 301)
        Me.txt3_detail.Multiline = True
        Me.txt3_detail.Name = "txt3_detail"
        Me.txt3_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt3_detail.TabIndex = 209
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(964, 301)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 26)
        Me.Label9.TabIndex = 213
        Me.Label9.Text = "Detail :"
        '
        'txt3_model
        '
        Me.txt3_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt3_model.Location = New System.Drawing.Point(1020, 220)
        Me.txt3_model.Name = "txt3_model"
        Me.txt3_model.Size = New System.Drawing.Size(202, 34)
        Me.txt3_model.TabIndex = 206
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(962, 220)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(52, 26)
        Me.Label56.TabIndex = 211
        Me.Label56.Text = "Model :"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(962, 183)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(52, 26)
        Me.Label68.TabIndex = 207
        Me.Label68.Text = "Brand :"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(671, 23)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(45, 26)
        Me.Label31.TabIndex = 204
        Me.Label31.Text = "Count"
        '
        'txtshowprin
        '
        Me.txtshowprin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowprin.Location = New System.Drawing.Point(722, 20)
        Me.txtshowprin.Name = "txtshowprin"
        Me.txtshowprin.ReadOnly = True
        Me.txtshowprin.Size = New System.Drawing.Size(54, 34)
        Me.txtshowprin.TabIndex = 203
        '
        'txtsearchprin
        '
        Me.txtsearchprin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchprin.Location = New System.Drawing.Point(86, 14)
        Me.txtsearchprin.Name = "txtsearchprin"
        Me.txtsearchprin.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchprin.TabIndex = 201
        '
        'btnsearch_prin
        '
        Me.btnsearch_prin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_prin.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_prin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_prin.Location = New System.Drawing.Point(385, 14)
        Me.btnsearch_prin.Name = "btnsearch_prin"
        Me.btnsearch_prin.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_prin.TabIndex = 202
        Me.btnsearch_prin.Text = "Search"
        Me.btnsearch_prin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_prin.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label49.Location = New System.Drawing.Point(6, 12)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(74, 34)
        Me.Label49.TabIndex = 200
        Me.Label49.Text = "Search :"
        '
        'ListViewprin
        '
        Me.ListViewprin.BackColor = System.Drawing.Color.White
        Me.ListViewprin.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader39, Me.ColumnHeader45, Me.ColumnHeader51})
        Me.ListViewprin.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewprin.FullRowSelect = True
        Me.ListViewprin.GridLines = True
        Me.ListViewprin.HideSelection = False
        Me.ListViewprin.Location = New System.Drawing.Point(6, 60)
        Me.ListViewprin.Name = "ListViewprin"
        Me.ListViewprin.Size = New System.Drawing.Size(770, 520)
        Me.ListViewprin.TabIndex = 199
        Me.ListViewprin.UseCompatibleStateImageBehavior = False
        Me.ListViewprin.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "ID"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Type"
        Me.ColumnHeader4.Width = 90
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Brand"
        Me.ColumnHeader5.Width = 75
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Detail"
        Me.ColumnHeader6.Width = 150
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Status"
        Me.ColumnHeader7.Width = 90
        '
        'ColumnHeader39
        '
        Me.ColumnHeader39.Text = "Officer Add"
        Me.ColumnHeader39.Width = 110
        '
        'ColumnHeader45
        '
        Me.ColumnHeader45.Text = "Section"
        Me.ColumnHeader45.Width = 100
        '
        'ColumnHeader51
        '
        Me.ColumnHeader51.Text = "Officer Edit"
        Me.ColumnHeader51.Width = 90
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(984, 140)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(30, 26)
        Me.Label36.TabIndex = 130
        Me.Label36.Text = "ID :"
        '
        'txt3_idprint
        '
        Me.txt3_idprint.Location = New System.Drawing.Point(1020, 140)
        Me.txt3_idprint.Name = "txt3_idprint"
        Me.txt3_idprint.ReadOnly = True
        Me.txt3_idprint.Size = New System.Drawing.Size(55, 34)
        Me.txt3_idprint.TabIndex = 129
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnaddnew_monitor)
        Me.TabPage3.Controls.Add(Me.Button10)
        Me.TabPage3.Controls.Add(Me.btn_delete_monitor)
        Me.TabPage3.Controls.Add(Me.Label79)
        Me.TabPage3.Controls.Add(Me.txt4_note_monitor)
        Me.TabPage3.Controls.Add(Me.ComboBoxbrand4)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.txt4_price)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.txt4_serial)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.txt4_model)
        Me.TabPage3.Controls.Add(Me.txt4_detail)
        Me.TabPage3.Controls.Add(Me.Label32)
        Me.TabPage3.Controls.Add(Me.Label50)
        Me.TabPage3.Controls.Add(Me.Label57)
        Me.TabPage3.Controls.Add(Me.txt4_size)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.txtshowmonitor)
        Me.TabPage3.Controls.Add(Me.txtsearchmonitor)
        Me.TabPage3.Controls.Add(Me.btnsearch_monitor)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.ListViewmonitor)
        Me.TabPage3.Controls.Add(Me.Label35)
        Me.TabPage3.Controls.Add(Me.txt4_idmonitor)
        Me.TabPage3.Location = New System.Drawing.Point(4, 35)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1372, 586)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Monitor"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btnaddnew_monitor
        '
        Me.btnaddnew_monitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaddnew_monitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddnew_monitor.Location = New System.Drawing.Point(1039, 541)
        Me.btnaddnew_monitor.Name = "btnaddnew_monitor"
        Me.btnaddnew_monitor.Size = New System.Drawing.Size(85, 39)
        Me.btnaddnew_monitor.TabIndex = 1057
        Me.btnaddnew_monitor.Text = "ADD NEW"
        Me.btnaddnew_monitor.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(1130, 541)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(85, 39)
        Me.Button10.TabIndex = 1055
        Me.Button10.Text = "EDIT NOW"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'btn_delete_monitor
        '
        Me.btn_delete_monitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_delete_monitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_delete_monitor.ForeColor = System.Drawing.Color.Black
        Me.btn_delete_monitor.Location = New System.Drawing.Point(1221, 541)
        Me.btn_delete_monitor.Name = "btn_delete_monitor"
        Me.btn_delete_monitor.Size = New System.Drawing.Size(85, 39)
        Me.btn_delete_monitor.TabIndex = 1056
        Me.btn_delete_monitor.Text = "Delete"
        Me.btn_delete_monitor.UseVisualStyleBackColor = True
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(1003, 345)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(44, 26)
        Me.Label79.TabIndex = 226
        Me.Label79.Text = "Note :"
        '
        'txt4_note_monitor
        '
        Me.txt4_note_monitor.Location = New System.Drawing.Point(1053, 345)
        Me.txt4_note_monitor.Name = "txt4_note_monitor"
        Me.txt4_note_monitor.Size = New System.Drawing.Size(180, 52)
        Me.txt4_note_monitor.TabIndex = 219
        Me.txt4_note_monitor.Text = ""
        '
        'ComboBoxbrand4
        '
        Me.ComboBoxbrand4.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxbrand4.FormattingEnabled = True
        Me.ComboBoxbrand4.Location = New System.Drawing.Point(1053, 105)
        Me.ComboBoxbrand4.Name = "ComboBoxbrand4"
        Me.ComboBoxbrand4.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand4.TabIndex = 211
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(1114, 185)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 26)
        Me.Label7.TabIndex = 224
        Me.Label7.Text = "inch"
        '
        'txt4_price
        '
        Me.txt4_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_price.Location = New System.Drawing.Point(1053, 305)
        Me.txt4_price.Name = "txt4_price"
        Me.txt4_price.Size = New System.Drawing.Size(55, 34)
        Me.txt4_price.TabIndex = 218
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(1000, 305)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(47, 26)
        Me.Label14.TabIndex = 223
        Me.Label14.Text = "Price :"
        '
        'txt4_serial
        '
        Me.txt4_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_serial.Location = New System.Drawing.Point(1053, 225)
        Me.txt4_serial.Name = "txt4_serial"
        Me.txt4_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt4_serial.TabIndex = 215
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(995, 105)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(52, 26)
        Me.Label17.TabIndex = 213
        Me.Label17.Text = "Brand :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(948, 225)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(99, 26)
        Me.Label22.TabIndex = 222
        Me.Label22.Text = "Serial Number :"
        '
        'txt4_model
        '
        Me.txt4_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_model.Location = New System.Drawing.Point(1053, 145)
        Me.txt4_model.Name = "txt4_model"
        Me.txt4_model.Size = New System.Drawing.Size(202, 34)
        Me.txt4_model.TabIndex = 212
        '
        'txt4_detail
        '
        Me.txt4_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_detail.Location = New System.Drawing.Point(1053, 265)
        Me.txt4_detail.Multiline = True
        Me.txt4_detail.Name = "txt4_detail"
        Me.txt4_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt4_detail.TabIndex = 216
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(997, 265)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(50, 26)
        Me.Label32.TabIndex = 221
        Me.Label32.Text = "Detail :"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(995, 145)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(52, 26)
        Me.Label50.TabIndex = 217
        Me.Label50.Text = "Model :"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(1005, 184)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(42, 26)
        Me.Label57.TabIndex = 220
        Me.Label57.Text = "Size :"
        '
        'txt4_size
        '
        Me.txt4_size.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4_size.Location = New System.Drawing.Point(1053, 185)
        Me.txt4_size.Name = "txt4_size"
        Me.txt4_size.Size = New System.Drawing.Size(55, 34)
        Me.txt4_size.TabIndex = 214
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(717, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 26)
        Me.Label4.TabIndex = 210
        Me.Label4.Text = "Count"
        '
        'txtshowmonitor
        '
        Me.txtshowmonitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowmonitor.Location = New System.Drawing.Point(768, 21)
        Me.txtshowmonitor.Name = "txtshowmonitor"
        Me.txtshowmonitor.ReadOnly = True
        Me.txtshowmonitor.Size = New System.Drawing.Size(54, 34)
        Me.txtshowmonitor.TabIndex = 209
        '
        'txtsearchmonitor
        '
        Me.txtsearchmonitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchmonitor.Location = New System.Drawing.Point(86, 18)
        Me.txtsearchmonitor.Name = "txtsearchmonitor"
        Me.txtsearchmonitor.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchmonitor.TabIndex = 207
        '
        'btnsearch_monitor
        '
        Me.btnsearch_monitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_monitor.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_monitor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_monitor.Location = New System.Drawing.Point(385, 18)
        Me.btnsearch_monitor.Name = "btnsearch_monitor"
        Me.btnsearch_monitor.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_monitor.TabIndex = 208
        Me.btnsearch_monitor.Text = "Search"
        Me.btnsearch_monitor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_monitor.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 34)
        Me.Label5.TabIndex = 206
        Me.Label5.Text = "Search :"
        '
        'ListViewmonitor
        '
        Me.ListViewmonitor.BackColor = System.Drawing.Color.White
        Me.ListViewmonitor.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader40, Me.ColumnHeader46, Me.ColumnHeader52})
        Me.ListViewmonitor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewmonitor.FullRowSelect = True
        Me.ListViewmonitor.GridLines = True
        Me.ListViewmonitor.HideSelection = False
        Me.ListViewmonitor.Location = New System.Drawing.Point(6, 62)
        Me.ListViewmonitor.Name = "ListViewmonitor"
        Me.ListViewmonitor.Size = New System.Drawing.Size(815, 518)
        Me.ListViewmonitor.TabIndex = 205
        Me.ListViewmonitor.UseCompatibleStateImageBehavior = False
        Me.ListViewmonitor.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "ID"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Type"
        Me.ColumnHeader9.Width = 100
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Brand"
        Me.ColumnHeader10.Width = 100
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Detail"
        Me.ColumnHeader11.Width = 150
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Status"
        Me.ColumnHeader12.Width = 100
        '
        'ColumnHeader40
        '
        Me.ColumnHeader40.Text = "Officer Add"
        Me.ColumnHeader40.Width = 110
        '
        'ColumnHeader46
        '
        Me.ColumnHeader46.Text = "Section"
        Me.ColumnHeader46.Width = 100
        '
        'ColumnHeader52
        '
        Me.ColumnHeader52.Text = "Officer Edit"
        Me.ColumnHeader52.Width = 90
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(1017, 68)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(30, 26)
        Me.Label35.TabIndex = 130
        Me.Label35.Text = "ID :"
        '
        'txt4_idmonitor
        '
        Me.txt4_idmonitor.Location = New System.Drawing.Point(1053, 65)
        Me.txt4_idmonitor.Name = "txt4_idmonitor"
        Me.txt4_idmonitor.ReadOnly = True
        Me.txt4_idmonitor.Size = New System.Drawing.Size(57, 34)
        Me.txt4_idmonitor.TabIndex = 129
        '
        'btngoadd
        '
        Me.btngoadd.Controls.Add(Me.ListViewother)
        Me.btngoadd.Controls.Add(Me.btnaddnew_other)
        Me.btngoadd.Controls.Add(Me.btnsaveother)
        Me.btngoadd.Controls.Add(Me.btn_delete_other)
        Me.btngoadd.Controls.Add(Me.Label80)
        Me.btngoadd.Controls.Add(Me.txt5_note_other)
        Me.btngoadd.Controls.Add(Me.Label75)
        Me.btngoadd.Controls.Add(Me.ComboBoxtype_other)
        Me.btngoadd.Controls.Add(Me.ComboBoxbrand5)
        Me.btngoadd.Controls.Add(Me.txt5_price)
        Me.btngoadd.Controls.Add(Me.Label16)
        Me.btngoadd.Controls.Add(Me.txt5_serial)
        Me.btngoadd.Controls.Add(Me.Label23)
        Me.btngoadd.Controls.Add(Me.Label39)
        Me.btngoadd.Controls.Add(Me.txt5_detail)
        Me.btngoadd.Controls.Add(Me.Label58)
        Me.btngoadd.Controls.Add(Me.Label69)
        Me.btngoadd.Controls.Add(Me.txt5_model)
        Me.btngoadd.Controls.Add(Me.Label3)
        Me.btngoadd.Controls.Add(Me.txtshowother)
        Me.btngoadd.Controls.Add(Me.txtsearchother)
        Me.btngoadd.Controls.Add(Me.btnsearch_other)
        Me.btngoadd.Controls.Add(Me.Label33)
        Me.btngoadd.Controls.Add(Me.Label34)
        Me.btngoadd.Controls.Add(Me.txt5_idother)
        Me.btngoadd.Location = New System.Drawing.Point(4, 35)
        Me.btngoadd.Name = "btngoadd"
        Me.btngoadd.Padding = New System.Windows.Forms.Padding(3)
        Me.btngoadd.Size = New System.Drawing.Size(1372, 586)
        Me.btngoadd.TabIndex = 3
        Me.btngoadd.Text = "Other"
        Me.btngoadd.UseVisualStyleBackColor = True
        '
        'ListViewother
        '
        Me.ListViewother.BackColor = System.Drawing.Color.White
        Me.ListViewother.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader13, Me.ColumnHeader14, Me.ColumnHeader15, Me.ColumnHeader16, Me.ColumnHeader17, Me.ColumnHeader35, Me.ColumnHeader36, Me.ColumnHeader41, Me.ColumnHeader47, Me.ColumnHeader53})
        Me.ListViewother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewother.FullRowSelect = True
        Me.ListViewother.GridLines = True
        Me.ListViewother.HideSelection = False
        Me.ListViewother.Location = New System.Drawing.Point(6, 60)
        Me.ListViewother.Name = "ListViewother"
        Me.ListViewother.Size = New System.Drawing.Size(1015, 523)
        Me.ListViewother.TabIndex = 1061
        Me.ListViewother.UseCompatibleStateImageBehavior = False
        Me.ListViewother.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "ID"
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Type"
        Me.ColumnHeader14.Width = 90
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Brand"
        Me.ColumnHeader15.Width = 100
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Detail"
        Me.ColumnHeader16.Width = 120
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Status"
        Me.ColumnHeader17.Width = 90
        '
        'ColumnHeader35
        '
        Me.ColumnHeader35.Text = "Serial Number"
        Me.ColumnHeader35.Width = 150
        '
        'ColumnHeader36
        '
        Me.ColumnHeader36.Text = "Type Data"
        Me.ColumnHeader36.Width = 100
        '
        'ColumnHeader41
        '
        Me.ColumnHeader41.Text = "Officer Add"
        Me.ColumnHeader41.Width = 120
        '
        'ColumnHeader47
        '
        Me.ColumnHeader47.Text = "Section"
        Me.ColumnHeader47.Width = 90
        '
        'ColumnHeader53
        '
        Me.ColumnHeader53.Text = "Ofiicer Edit"
        Me.ColumnHeader53.Width = 90
        '
        'btnaddnew_other
        '
        Me.btnaddnew_other.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaddnew_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddnew_other.Location = New System.Drawing.Point(1039, 541)
        Me.btnaddnew_other.Name = "btnaddnew_other"
        Me.btnaddnew_other.Size = New System.Drawing.Size(85, 39)
        Me.btnaddnew_other.TabIndex = 1060
        Me.btnaddnew_other.Text = "ADD NEW"
        Me.btnaddnew_other.UseVisualStyleBackColor = True
        '
        'btnsaveother
        '
        Me.btnsaveother.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsaveother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsaveother.Location = New System.Drawing.Point(1130, 541)
        Me.btnsaveother.Name = "btnsaveother"
        Me.btnsaveother.Size = New System.Drawing.Size(85, 39)
        Me.btnsaveother.TabIndex = 1058
        Me.btnsaveother.Text = "EDIT NOW"
        Me.btnsaveother.UseVisualStyleBackColor = True
        '
        'btn_delete_other
        '
        Me.btn_delete_other.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_delete_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_delete_other.ForeColor = System.Drawing.Color.Black
        Me.btn_delete_other.Location = New System.Drawing.Point(1221, 541)
        Me.btn_delete_other.Name = "btn_delete_other"
        Me.btn_delete_other.Size = New System.Drawing.Size(85, 39)
        Me.btn_delete_other.TabIndex = 1059
        Me.btn_delete_other.Text = "Delete"
        Me.btn_delete_other.UseVisualStyleBackColor = True
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(1098, 323)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(44, 26)
        Me.Label80.TabIndex = 231
        Me.Label80.Text = "Note :"
        '
        'txt5_note_other
        '
        Me.txt5_note_other.Location = New System.Drawing.Point(1148, 323)
        Me.txt5_note_other.Name = "txt5_note_other"
        Me.txt5_note_other.Size = New System.Drawing.Size(180, 52)
        Me.txt5_note_other.TabIndex = 226
        Me.txt5_note_other.Text = ""
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(1097, 243)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(45, 26)
        Me.Label75.TabIndex = 230
        Me.Label75.Text = "Type :"
        '
        'ComboBoxtype_other
        '
        Me.ComboBoxtype_other.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxtype_other.FormattingEnabled = True
        Me.ComboBoxtype_other.Location = New System.Drawing.Point(1148, 243)
        Me.ComboBoxtype_other.Name = "ComboBoxtype_other"
        Me.ComboBoxtype_other.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxtype_other.TabIndex = 223
        '
        'ComboBoxbrand5
        '
        Me.ComboBoxbrand5.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxbrand5.FormattingEnabled = True
        Me.ComboBoxbrand5.Location = New System.Drawing.Point(1148, 83)
        Me.ComboBoxbrand5.Name = "ComboBoxbrand5"
        Me.ComboBoxbrand5.Size = New System.Drawing.Size(202, 34)
        Me.ComboBoxbrand5.TabIndex = 217
        '
        'txt5_price
        '
        Me.txt5_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_price.Location = New System.Drawing.Point(1148, 283)
        Me.txt5_price.Name = "txt5_price"
        Me.txt5_price.Size = New System.Drawing.Size(55, 34)
        Me.txt5_price.TabIndex = 224
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(1095, 283)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 26)
        Me.Label16.TabIndex = 228
        Me.Label16.Text = "Price :"
        '
        'txt5_serial
        '
        Me.txt5_serial.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_serial.Location = New System.Drawing.Point(1148, 163)
        Me.txt5_serial.Name = "txt5_serial"
        Me.txt5_serial.Size = New System.Drawing.Size(202, 34)
        Me.txt5_serial.TabIndex = 219
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(1043, 167)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(99, 26)
        Me.Label23.TabIndex = 227
        Me.Label23.Text = "Serial Number :"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(1092, 203)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(50, 26)
        Me.Label39.TabIndex = 225
        Me.Label39.Text = "Detail :"
        '
        'txt5_detail
        '
        Me.txt5_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_detail.Location = New System.Drawing.Point(1148, 203)
        Me.txt5_detail.Multiline = True
        Me.txt5_detail.Name = "txt5_detail"
        Me.txt5_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt5_detail.TabIndex = 221
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(1090, 123)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(52, 26)
        Me.Label58.TabIndex = 222
        Me.Label58.Text = "Model :"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(1090, 83)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(52, 26)
        Me.Label69.TabIndex = 220
        Me.Label69.Text = "Brand :"
        '
        'txt5_model
        '
        Me.txt5_model.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt5_model.Location = New System.Drawing.Point(1148, 123)
        Me.txt5_model.Name = "txt5_model"
        Me.txt5_model.Size = New System.Drawing.Size(202, 34)
        Me.txt5_model.TabIndex = 218
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(916, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 26)
        Me.Label3.TabIndex = 216
        Me.Label3.Text = "Count"
        '
        'txtshowother
        '
        Me.txtshowother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowother.Location = New System.Drawing.Point(967, 20)
        Me.txtshowother.Name = "txtshowother"
        Me.txtshowother.ReadOnly = True
        Me.txtshowother.Size = New System.Drawing.Size(54, 34)
        Me.txtshowother.TabIndex = 215
        '
        'txtsearchother
        '
        Me.txtsearchother.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchother.Location = New System.Drawing.Point(86, 15)
        Me.txtsearchother.Name = "txtsearchother"
        Me.txtsearchother.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchother.TabIndex = 213
        '
        'btnsearch_other
        '
        Me.btnsearch_other.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_other.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_other.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_other.Location = New System.Drawing.Point(385, 13)
        Me.btnsearch_other.Name = "btnsearch_other"
        Me.btnsearch_other.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_other.TabIndex = 214
        Me.btnsearch_other.Text = "Search"
        Me.btnsearch_other.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_other.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label33.Location = New System.Drawing.Point(6, 13)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(74, 34)
        Me.Label33.TabIndex = 212
        Me.Label33.Text = "Search :"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(1112, 46)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(30, 26)
        Me.Label34.TabIndex = 128
        Me.Label34.Text = "ID :"
        '
        'txt5_idother
        '
        Me.txt5_idother.Location = New System.Drawing.Point(1148, 43)
        Me.txt5_idother.Name = "txt5_idother"
        Me.txt5_idother.ReadOnly = True
        Me.txt5_idother.Size = New System.Drawing.Size(55, 34)
        Me.txt5_idother.TabIndex = 127
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label94)
        Me.TabPage5.Controls.Add(Me.txt6_idlicense)
        Me.TabPage5.Controls.Add(Me.btnaddnew_license)
        Me.TabPage5.Controls.Add(Me.Button18)
        Me.TabPage5.Controls.Add(Me.btn_delete_license)
        Me.TabPage5.Controls.Add(Me.GroupBox5)
        Me.TabPage5.Controls.Add(Me.GroupBox4)
        Me.TabPage5.Controls.Add(Me.Label81)
        Me.TabPage5.Controls.Add(Me.txt6_note_license)
        Me.TabPage5.Controls.Add(Me.txt6_amount)
        Me.TabPage5.Controls.Add(Me.Label46)
        Me.TabPage5.Controls.Add(Me.Label47)
        Me.TabPage5.Controls.Add(Me.txt6_detail)
        Me.TabPage5.Controls.Add(Me.txt6_price)
        Me.TabPage5.Controls.Add(Me.Label71)
        Me.TabPage5.Controls.Add(Me.Label77)
        Me.TabPage5.Controls.Add(Me.txt6_brand)
        Me.TabPage5.Controls.Add(Me.Label45)
        Me.TabPage5.Controls.Add(Me.txtshowlicense)
        Me.TabPage5.Controls.Add(Me.txtsearchlicense)
        Me.TabPage5.Controls.Add(Me.btnsearch_license)
        Me.TabPage5.Controls.Add(Me.Label70)
        Me.TabPage5.Controls.Add(Me.ListViewlicense)
        Me.TabPage5.Location = New System.Drawing.Point(4, 35)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(1372, 586)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "License"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(873, 235)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(30, 26)
        Me.Label94.TabIndex = 1065
        Me.Label94.Text = "ID :"
        '
        'txt6_idlicense
        '
        Me.txt6_idlicense.Location = New System.Drawing.Point(909, 232)
        Me.txt6_idlicense.Name = "txt6_idlicense"
        Me.txt6_idlicense.ReadOnly = True
        Me.txt6_idlicense.Size = New System.Drawing.Size(55, 34)
        Me.txt6_idlicense.TabIndex = 1064
        '
        'btnaddnew_license
        '
        Me.btnaddnew_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaddnew_license.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaddnew_license.Location = New System.Drawing.Point(1039, 541)
        Me.btnaddnew_license.Name = "btnaddnew_license"
        Me.btnaddnew_license.Size = New System.Drawing.Size(85, 39)
        Me.btnaddnew_license.TabIndex = 1063
        Me.btnaddnew_license.Text = "ADD NEW"
        Me.btnaddnew_license.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button18.Location = New System.Drawing.Point(1130, 541)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(85, 39)
        Me.Button18.TabIndex = 1061
        Me.Button18.Text = "EDIT NOW"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'btn_delete_license
        '
        Me.btn_delete_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_delete_license.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_delete_license.ForeColor = System.Drawing.Color.Black
        Me.btn_delete_license.Location = New System.Drawing.Point(1221, 541)
        Me.btn_delete_license.Name = "btn_delete_license"
        Me.btn_delete_license.Size = New System.Drawing.Size(85, 39)
        Me.btn_delete_license.TabIndex = 1062
        Me.btn_delete_license.Text = "Delete"
        Me.btn_delete_license.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label40)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox5.Controls.Add(Me.Label41)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox5.Location = New System.Drawing.Point(790, 61)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(367, 162)
        Me.GroupBox5.TabIndex = 1020
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Choose Date"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(58, 31)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(55, 26)
        Me.Label40.TabIndex = 6
        Me.Label40.Text = "วันที่ซื้อ :"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(119, 31)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 34)
        Me.DateTimePicker1.TabIndex = 100
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(15, 97)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(98, 26)
        Me.Label41.TabIndex = 7
        Me.Label41.Text = "หมดประกันเมื่อ :"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(119, 97)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(200, 34)
        Me.DateTimePicker2.TabIndex = 150
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Radioapp)
        Me.GroupBox4.Controls.Add(Me.Radioos)
        Me.GroupBox4.Location = New System.Drawing.Point(1163, 61)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(199, 81)
        Me.GroupBox4.TabIndex = 1019
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Choose Type"
        '
        'Radioapp
        '
        Me.Radioapp.AutoSize = True
        Me.Radioapp.Location = New System.Drawing.Point(93, 33)
        Me.Radioapp.Name = "Radioapp"
        Me.Radioapp.Size = New System.Drawing.Size(93, 30)
        Me.Radioapp.TabIndex = 999
        Me.Radioapp.TabStop = True
        Me.Radioapp.Text = "Application"
        Me.Radioapp.UseVisualStyleBackColor = True
        '
        'Radioos
        '
        Me.Radioos.AutoSize = True
        Me.Radioos.Location = New System.Drawing.Point(22, 33)
        Me.Radioos.Name = "Radioos"
        Me.Radioos.Size = New System.Drawing.Size(48, 30)
        Me.Radioos.TabIndex = 998
        Me.Radioos.TabStop = True
        Me.Radioos.Text = "OS"
        Me.Radioos.UseVisualStyleBackColor = True
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(859, 429)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(44, 26)
        Me.Label81.TabIndex = 1018
        Me.Label81.Text = "Note :"
        '
        'txt6_note_license
        '
        Me.txt6_note_license.Location = New System.Drawing.Point(909, 429)
        Me.txt6_note_license.Name = "txt6_note_license"
        Me.txt6_note_license.Size = New System.Drawing.Size(180, 52)
        Me.txt6_note_license.TabIndex = 1013
        Me.txt6_note_license.Text = ""
        '
        'txt6_amount
        '
        Me.txt6_amount.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_amount.Location = New System.Drawing.Point(909, 389)
        Me.txt6_amount.Name = "txt6_amount"
        Me.txt6_amount.Size = New System.Drawing.Size(55, 34)
        Me.txt6_amount.TabIndex = 1012
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(843, 389)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(60, 26)
        Me.Label46.TabIndex = 1016
        Me.Label46.Text = "Amount :"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(853, 309)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(50, 26)
        Me.Label47.TabIndex = 1015
        Me.Label47.Text = "Detail :"
        '
        'txt6_detail
        '
        Me.txt6_detail.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_detail.Location = New System.Drawing.Point(909, 309)
        Me.txt6_detail.Multiline = True
        Me.txt6_detail.Name = "txt6_detail"
        Me.txt6_detail.Size = New System.Drawing.Size(202, 34)
        Me.txt6_detail.TabIndex = 1009
        '
        'txt6_price
        '
        Me.txt6_price.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_price.Location = New System.Drawing.Point(909, 349)
        Me.txt6_price.Name = "txt6_price"
        Me.txt6_price.Size = New System.Drawing.Size(55, 34)
        Me.txt6_price.TabIndex = 1010
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(856, 349)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(47, 26)
        Me.Label71.TabIndex = 1014
        Me.Label71.Text = "Price :"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(851, 269)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(52, 26)
        Me.Label77.TabIndex = 1011
        Me.Label77.Text = "Brand :"
        '
        'txt6_brand
        '
        Me.txt6_brand.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6_brand.Location = New System.Drawing.Point(909, 269)
        Me.txt6_brand.Name = "txt6_brand"
        Me.txt6_brand.Size = New System.Drawing.Size(202, 34)
        Me.txt6_brand.TabIndex = 1008
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(671, 24)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(45, 26)
        Me.Label45.TabIndex = 222
        Me.Label45.Text = "Count"
        '
        'txtshowlicense
        '
        Me.txtshowlicense.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtshowlicense.Location = New System.Drawing.Point(722, 21)
        Me.txtshowlicense.Name = "txtshowlicense"
        Me.txtshowlicense.ReadOnly = True
        Me.txtshowlicense.Size = New System.Drawing.Size(54, 34)
        Me.txtshowlicense.TabIndex = 221
        '
        'txtsearchlicense
        '
        Me.txtsearchlicense.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearchlicense.Location = New System.Drawing.Point(86, 17)
        Me.txtsearchlicense.Name = "txtsearchlicense"
        Me.txtsearchlicense.Size = New System.Drawing.Size(293, 34)
        Me.txtsearchlicense.TabIndex = 219
        '
        'btnsearch_license
        '
        Me.btnsearch_license.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsearch_license.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnsearch_license.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsearch_license.Location = New System.Drawing.Point(385, 17)
        Me.btnsearch_license.Name = "btnsearch_license"
        Me.btnsearch_license.Size = New System.Drawing.Size(68, 34)
        Me.btnsearch_license.TabIndex = 220
        Me.btnsearch_license.Text = "Search"
        Me.btnsearch_license.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsearch_license.UseVisualStyleBackColor = True
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label70.Location = New System.Drawing.Point(6, 15)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(74, 34)
        Me.Label70.TabIndex = 218
        Me.Label70.Text = "Search :"
        '
        'ListViewlicense
        '
        Me.ListViewlicense.BackColor = System.Drawing.Color.White
        Me.ListViewlicense.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader18, Me.ColumnHeader19, Me.ColumnHeader20, Me.ColumnHeader21, Me.ColumnHeader22, Me.ColumnHeader42, Me.ColumnHeader48, Me.ColumnHeader54})
        Me.ListViewlicense.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewlicense.FullRowSelect = True
        Me.ListViewlicense.GridLines = True
        Me.ListViewlicense.HideSelection = False
        Me.ListViewlicense.Location = New System.Drawing.Point(6, 61)
        Me.ListViewlicense.Name = "ListViewlicense"
        Me.ListViewlicense.Size = New System.Drawing.Size(769, 519)
        Me.ListViewlicense.TabIndex = 217
        Me.ListViewlicense.UseCompatibleStateImageBehavior = False
        Me.ListViewlicense.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader18
        '
        Me.ColumnHeader18.Text = "ID"
        '
        'ColumnHeader19
        '
        Me.ColumnHeader19.Text = "Type"
        Me.ColumnHeader19.Width = 100
        '
        'ColumnHeader20
        '
        Me.ColumnHeader20.Text = "Brand"
        Me.ColumnHeader20.Width = 75
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Detail"
        Me.ColumnHeader21.Width = 150
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Status"
        Me.ColumnHeader22.Width = 90
        '
        'ColumnHeader42
        '
        Me.ColumnHeader42.Text = "Officer Add"
        Me.ColumnHeader42.Width = 100
        '
        'ColumnHeader48
        '
        Me.ColumnHeader48.Text = "Section"
        Me.ColumnHeader48.Width = 100
        '
        'ColumnHeader54
        '
        Me.ColumnHeader54.Text = "Officer Edit"
        Me.ColumnHeader54.Width = 90
        '
        'ColumnHeader28
        '
        Me.ColumnHeader28.DisplayIndex = 0
        Me.ColumnHeader28.Text = "ID"
        '
        'ColumnHeader29
        '
        Me.ColumnHeader29.DisplayIndex = 1
        Me.ColumnHeader29.Text = "TYPE"
        Me.ColumnHeader29.Width = 100
        '
        'ColumnHeader30
        '
        Me.ColumnHeader30.DisplayIndex = 2
        Me.ColumnHeader30.Text = "NAME"
        Me.ColumnHeader30.Width = 100
        '
        'ColumnHeader31
        '
        Me.ColumnHeader31.DisplayIndex = 3
        Me.ColumnHeader31.Text = "Detail"
        Me.ColumnHeader31.Width = 120
        '
        'ColumnHeader32
        '
        Me.ColumnHeader32.DisplayIndex = 4
        Me.ColumnHeader32.Text = "Status"
        Me.ColumnHeader32.Width = 90
        '
        'ColumnHeader33
        '
        Me.ColumnHeader33.DisplayIndex = 5
        Me.ColumnHeader33.Text = "Serial Number"
        Me.ColumnHeader33.Width = 140
        '
        'ColumnHeader34
        '
        Me.ColumnHeader34.DisplayIndex = 6
        Me.ColumnHeader34.Text = "TYPE"
        Me.ColumnHeader34.Width = 100
        '
        'frm_MNG_DEVICE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1378, 813)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.gp)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_MNG_DEVICE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Management Device"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gp.ResumeLayout(False)
        Me.gp.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.btngoadd.ResumeLayout(False)
        Me.btngoadd.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents llogin As System.Windows.Forms.Label
    Friend WithEvents gp As System.Windows.Forms.Panel
    Friend WithEvents btnback As System.Windows.Forms.Button
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnaddnew_com As System.Windows.Forms.Button
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents txtshowcom As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txt1_idcom As System.Windows.Forms.TextBox
    Friend WithEvents ListViewcom As System.Windows.Forms.ListView
    Friend WithEvents name_container As System.Windows.Forms.ColumnHeader
    Friend WithEvents agent As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents size As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtsearchcom As System.Windows.Forms.TextBox
    Friend WithEvents btnsearchcom As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnsavecom As System.Windows.Forms.Button
    Friend WithEvents btndeletecom As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txt3_idprint As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txt4_idmonitor As System.Windows.Forms.TextBox
    Friend WithEvents btngoadd As System.Windows.Forms.TabPage
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txt5_idother As System.Windows.Forms.TextBox
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents txtshowcomrent As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchcomrent As System.Windows.Forms.TextBox
    Friend WithEvents btnsearchcomrent As System.Windows.Forms.Button
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents ListViewcomrent As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader23 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader24 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader25 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader26 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader27 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt1_note_com As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand1 As System.Windows.Forms.ComboBox
    Friend WithEvents txt1_price As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tab3detailsoftware As System.Windows.Forms.Label
    Friend WithEvents txt1_other As System.Windows.Forms.TextBox
    Friend WithEvents txt1_comname As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txt1_office As System.Windows.Forms.TextBox
    Friend WithEvents txt1_windows As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txt1_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txt1_vga As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txt1_hdd As System.Windows.Forms.TextBox
    Friend WithEvents txt1_ram As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt1_ip As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txt1_mainboard As System.Windows.Forms.TextBox
    Friend WithEvents txt1_cpu As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txt1_model As System.Windows.Forms.TextBox
    Friend WithEvents txt1_ps As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txt1_serial As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txt1_case As System.Windows.Forms.TextBox
    Friend WithEvents txt1_cd As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader28 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader29 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader30 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader31 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader32 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader33 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader34 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txt2_note_comr As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand2 As System.Windows.Forms.ComboBox
    Friend WithEvents txt2_price As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents txt2_other As System.Windows.Forms.TextBox
    Friend WithEvents txt2_comname As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents txt2_office As System.Windows.Forms.TextBox
    Friend WithEvents txt2_windows As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents txt2_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents txt2_vga As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents txt2_hdd As System.Windows.Forms.TextBox
    Friend WithEvents txt2_ram As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents txt2_ip As System.Windows.Forms.TextBox
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents txt2_mainboard As System.Windows.Forms.TextBox
    Friend WithEvents txt2_cpu As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents txt2_case As System.Windows.Forms.TextBox
    Friend WithEvents txt2_model As System.Windows.Forms.TextBox
    Friend WithEvents txt2_ps As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents txt2_cd As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txt2_idcomr As System.Windows.Forms.TextBox
    Friend WithEvents btnaddnew_comrent As System.Windows.Forms.Button
    Friend WithEvents btnsave_comrent As System.Windows.Forms.Button
    Friend WithEvents btndelete_comr As System.Windows.Forms.Button
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtshowprin As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchprin As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_prin As System.Windows.Forms.Button
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents ListViewprin As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Radiolaserc As System.Windows.Forms.RadioButton
    Friend WithEvents Radiodot As System.Windows.Forms.RadioButton
    Friend WithEvents Radioinkjet As System.Windows.Forms.RadioButton
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents txt3_note_prin As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand3 As System.Windows.Forms.ComboBox
    Friend WithEvents txt3_price As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt3_serial As System.Windows.Forms.TextBox
    Friend WithEvents txt3_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt3_model As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents btnaddnew_prinnew As System.Windows.Forms.Button
    Friend WithEvents btnsave_print As System.Windows.Forms.Button
    Friend WithEvents btndelete_print As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtshowmonitor As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchmonitor As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_monitor As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ListViewmonitor As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnaddnew_monitor As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents btn_delete_monitor As System.Windows.Forms.Button
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents txt4_note_monitor As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBoxbrand4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt4_price As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt4_serial As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txt4_model As System.Windows.Forms.TextBox
    Friend WithEvents txt4_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents txt4_size As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtshowother As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchother As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_other As System.Windows.Forms.Button
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents ListViewother As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader35 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader36 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnaddnew_other As System.Windows.Forms.Button
    Friend WithEvents btnsaveother As System.Windows.Forms.Button
    Friend WithEvents btn_delete_other As System.Windows.Forms.Button
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents txt5_note_other As System.Windows.Forms.RichTextBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxtype_other As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxbrand5 As System.Windows.Forms.ComboBox
    Friend WithEvents txt5_price As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt5_serial As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txt5_detail As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents txt5_model As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtshowlicense As System.Windows.Forms.TextBox
    Friend WithEvents txtsearchlicense As System.Windows.Forms.TextBox
    Friend WithEvents btnsearch_license As System.Windows.Forms.Button
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents ListViewlicense As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader18 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader19 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader20 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnaddnew_license As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents btn_delete_license As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Radioapp As System.Windows.Forms.RadioButton
    Friend WithEvents Radioos As System.Windows.Forms.RadioButton
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents txt6_note_license As System.Windows.Forms.RichTextBox
    Friend WithEvents txt6_amount As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txt6_detail As System.Windows.Forms.TextBox
    Friend WithEvents txt6_price As System.Windows.Forms.TextBox
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents txt6_brand As System.Windows.Forms.TextBox
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents txt6_idlicense As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txt2_serial As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txt1_store As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt2_store_rent As System.Windows.Forms.TextBox
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader37 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader38 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader39 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader40 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader41 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader42 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents txtwarrant As System.Windows.Forms.TextBox
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents txtdatebuy As System.Windows.Forms.TextBox
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader43 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader44 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader45 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader46 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader47 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader48 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents txtwarrantcomr As System.Windows.Forms.TextBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents txtdatebutcomr As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader49 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader50 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader51 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader52 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader53 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader54 As System.Windows.Forms.ColumnHeader
End Class
