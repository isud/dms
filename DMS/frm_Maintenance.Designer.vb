﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Maintenance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Maintenance))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.llogin = New System.Windows.Forms.Label()
        Me.gp = New System.Windows.Forms.Panel()
        Me.btnback = New System.Windows.Forms.Button()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ListViewsection = New System.Windows.Forms.ListView()
        Me.ColumnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ListViewfloor = New System.Windows.Forms.ListView()
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ListViewshowhis = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.ListViewshowall = New System.Windows.Forms.ListView()
        Me.name_container = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.agent = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_section = New System.Windows.Forms.TextBox()
        Me.txt_detail = New System.Windows.Forms.RichTextBox()
        Me.btnsavemain = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtid = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gp.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label93)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.llogin)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1371, 92)
        Me.Panel1.TabIndex = 354
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label93.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label93.Location = New System.Drawing.Point(542, 61)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(334, 15)
        Me.Label93.TabIndex = 348
        Me.Label93.Text = " Developer By Computer @Diana Complex Shopping Center"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(374, 91)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 347
        Me.PictureBox1.TabStop = False
        '
        'llogin
        '
        Me.llogin.AutoSize = True
        Me.llogin.BackColor = System.Drawing.Color.Transparent
        Me.llogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.llogin.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.llogin.Location = New System.Drawing.Point(539, 26)
        Me.llogin.Name = "llogin"
        Me.llogin.Size = New System.Drawing.Size(388, 33)
        Me.llogin.TabIndex = 346
        Me.llogin.Text = "Device Management System"
        '
        'gp
        '
        Me.gp.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.gp.Controls.Add(Me.btnback)
        Me.gp.Controls.Add(Me.Label92)
        Me.gp.Controls.Add(Me.Label91)
        Me.gp.Controls.Add(Me.Label87)
        Me.gp.Controls.Add(Me.Label88)
        Me.gp.Controls.Add(Me.Label89)
        Me.gp.Controls.Add(Me.Label90)
        Me.gp.Location = New System.Drawing.Point(0, 652)
        Me.gp.Name = "gp"
        Me.gp.Size = New System.Drawing.Size(1346, 92)
        Me.gp.TabIndex = 355
        '
        'btnback
        '
        Me.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnback.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnback.Location = New System.Drawing.Point(1247, 27)
        Me.btnback.Name = "btnback"
        Me.btnback.Size = New System.Drawing.Size(80, 40)
        Me.btnback.TabIndex = 1010
        Me.btnback.Text = "Home"
        Me.btnback.UseVisualStyleBackColor = True
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label92.Location = New System.Drawing.Point(133, 27)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(72, 34)
        Me.Label92.TabIndex = 345
        Me.Label92.Text = "Label92"
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.BackColor = System.Drawing.Color.Transparent
        Me.Label91.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label91.ForeColor = System.Drawing.Color.Black
        Me.Label91.Location = New System.Drawing.Point(31, 31)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(107, 30)
        Me.Label91.TabIndex = 344
        Me.Label91.Text = "รหัสผู้เข้าใช้ :"
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.Location = New System.Drawing.Point(375, 29)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(72, 34)
        Me.Label87.TabIndex = 343
        Me.Label87.Text = "Label87"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.Color.Transparent
        Me.Label88.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label88.ForeColor = System.Drawing.Color.Black
        Me.Label88.Location = New System.Drawing.Point(644, 31)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(84, 30)
        Me.Label88.TabIndex = 3
        Me.Label88.Text = "ตำแหน่ง :"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label89.Location = New System.Drawing.Point(722, 29)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(72, 34)
        Me.Label89.TabIndex = 342
        Me.Label89.Text = "Label89"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.BackColor = System.Drawing.Color.Transparent
        Me.Label90.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label90.ForeColor = System.Drawing.Color.Black
        Me.Label90.Location = New System.Drawing.Point(333, 31)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(48, 30)
        Me.Label90.TabIndex = 1
        Me.Label90.Text = "คุณ :"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ListViewsection)
        Me.GroupBox3.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(181, 98)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(266, 261)
        Me.GroupBox3.TabIndex = 379
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "คลิกเลือก Section"
        '
        'ListViewsection
        '
        Me.ListViewsection.BackColor = System.Drawing.Color.White
        Me.ListViewsection.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader24, Me.ColumnHeader25})
        Me.ListViewsection.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewsection.FullRowSelect = True
        Me.ListViewsection.GridLines = True
        Me.ListViewsection.HideSelection = False
        Me.ListViewsection.Location = New System.Drawing.Point(15, 36)
        Me.ListViewsection.Name = "ListViewsection"
        Me.ListViewsection.Size = New System.Drawing.Size(236, 218)
        Me.ListViewsection.TabIndex = 367
        Me.ListViewsection.UseCompatibleStateImageBehavior = False
        Me.ListViewsection.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "ID"
        Me.ColumnHeader24.Width = 50
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "Section"
        Me.ColumnHeader25.Width = 180
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ListViewfloor)
        Me.GroupBox2.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 98)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(163, 260)
        Me.GroupBox2.TabIndex = 378
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "คลิกเลือก Floor"
        '
        'ListViewfloor
        '
        Me.ListViewfloor.BackColor = System.Drawing.Color.White
        Me.ListViewfloor.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader11, Me.ColumnHeader12})
        Me.ListViewfloor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewfloor.FullRowSelect = True
        Me.ListViewfloor.GridLines = True
        Me.ListViewfloor.HideSelection = False
        Me.ListViewfloor.Location = New System.Drawing.Point(21, 36)
        Me.ListViewfloor.Name = "ListViewfloor"
        Me.ListViewfloor.Size = New System.Drawing.Size(136, 218)
        Me.ListViewfloor.TabIndex = 366
        Me.ListViewfloor.UseCompatibleStateImageBehavior = False
        Me.ListViewfloor.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "ID"
        Me.ColumnHeader11.Width = 40
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Floor"
        Me.ColumnHeader12.Width = 90
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ListViewshowhis)
        Me.GroupBox4.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(461, 98)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(873, 261)
        Me.GroupBox4.TabIndex = 390
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "ประวัติการซ่อม"
        '
        'ListViewshowhis
        '
        Me.ListViewshowhis.BackColor = System.Drawing.Color.White
        Me.ListViewshowhis.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader4})
        Me.ListViewshowhis.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ListViewshowhis.FullRowSelect = True
        Me.ListViewshowhis.GridLines = True
        Me.ListViewshowhis.HideSelection = False
        Me.ListViewshowhis.Location = New System.Drawing.Point(17, 33)
        Me.ListViewshowhis.Name = "ListViewshowhis"
        Me.ListViewshowhis.Size = New System.Drawing.Size(845, 218)
        Me.ListViewshowhis.TabIndex = 350
        Me.ListViewshowhis.UseCompatibleStateImageBehavior = False
        Me.ListViewshowhis.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "ID"
        Me.ColumnHeader3.Width = 70
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "History"
        Me.ColumnHeader6.Width = 480
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Date"
        Me.ColumnHeader7.Width = 120
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Officer"
        Me.ColumnHeader4.Width = 170
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.ListViewshowall)
        Me.GroupBox5.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(12, 364)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(976, 279)
        Me.GroupBox5.TabIndex = 391
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "แสดงข้อมูลอุปกรณ์ตาม Section"
        '
        'ListViewshowall
        '
        Me.ListViewshowall.BackColor = System.Drawing.Color.White
        Me.ListViewshowall.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.name_container, Me.agent, Me.ColumnHeader1, Me.ColumnHeader2, Me.size, Me.ColumnHeader22, Me.ColumnHeader5})
        Me.ListViewshowall.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewshowall.FullRowSelect = True
        Me.ListViewshowall.GridLines = True
        Me.ListViewshowall.HideSelection = False
        Me.ListViewshowall.Location = New System.Drawing.Point(10, 29)
        Me.ListViewshowall.Name = "ListViewshowall"
        Me.ListViewshowall.Size = New System.Drawing.Size(957, 241)
        Me.ListViewshowall.TabIndex = 359
        Me.ListViewshowall.UseCompatibleStateImageBehavior = False
        Me.ListViewshowall.View = System.Windows.Forms.View.Details
        '
        'name_container
        '
        Me.name_container.Text = "ID"
        '
        'agent
        '
        Me.agent.Text = "Type"
        Me.agent.Width = 110
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Brand"
        Me.ColumnHeader1.Width = 110
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Detail"
        Me.ColumnHeader2.Width = 260
        '
        'size
        '
        Me.size.Text = "Status"
        Me.size.Width = 90
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Section"
        Me.ColumnHeader22.Width = 150
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Serial Number"
        Me.ColumnHeader5.Width = 170
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_section)
        Me.GroupBox1.Controls.Add(Me.txt_detail)
        Me.GroupBox1.Controls.Add(Me.btnsavemain)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtid)
        Me.GroupBox1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(994, 365)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(333, 205)
        Me.GroupBox1.TabIndex = 392
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "กรอกข้อมูลการ Maintenance"
        '
        'txt_section
        '
        Me.txt_section.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_section.Location = New System.Drawing.Point(138, 39)
        Me.txt_section.Name = "txt_section"
        Me.txt_section.ReadOnly = True
        Me.txt_section.Size = New System.Drawing.Size(56, 26)
        Me.txt_section.TabIndex = 353
        '
        'txt_detail
        '
        Me.txt_detail.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_detail.Location = New System.Drawing.Point(76, 74)
        Me.txt_detail.Name = "txt_detail"
        Me.txt_detail.Size = New System.Drawing.Size(226, 58)
        Me.txt_detail.TabIndex = 352
        Me.txt_detail.Text = ""
        '
        'btnsavemain
        '
        Me.btnsavemain.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsavemain.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsavemain.Location = New System.Drawing.Point(76, 138)
        Me.btnsavemain.Name = "btnsavemain"
        Me.btnsavemain.Size = New System.Drawing.Size(80, 40)
        Me.btnsavemain.TabIndex = 351
        Me.btnsavemain.Text = "SAVE"
        Me.btnsavemain.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 26)
        Me.Label2.TabIndex = 350
        Me.Label2.Text = "DETAIL :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(40, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 26)
        Me.Label1.TabIndex = 349
        Me.Label1.Text = "ID :"
        '
        'txtid
        '
        Me.txtid.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtid.Location = New System.Drawing.Point(76, 39)
        Me.txtid.Name = "txtid"
        Me.txtid.ReadOnly = True
        Me.txtid.Size = New System.Drawing.Size(56, 26)
        Me.txtid.TabIndex = 348
        '
        'frm_Maintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1339, 745)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gp)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_Maintenance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Maintenance"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gp.ResumeLayout(False)
        Me.gp.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents llogin As System.Windows.Forms.Label
    Friend WithEvents gp As System.Windows.Forms.Panel
    Friend WithEvents btnback As System.Windows.Forms.Button
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ListViewsection As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader24 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader25 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ListViewfloor As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ListViewshowhis As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents ListViewshowall As System.Windows.Forms.ListView
    Friend WithEvents name_container As System.Windows.Forms.ColumnHeader
    Friend WithEvents agent As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents size As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_section As System.Windows.Forms.TextBox
    Friend WithEvents txt_detail As System.Windows.Forms.RichTextBox
    Friend WithEvents btnsavemain As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtid As System.Windows.Forms.TextBox
End Class
