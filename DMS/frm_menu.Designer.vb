﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class btn_add_user
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(btn_add_user))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnnextedit = New System.Windows.Forms.Button()
        Me.btnshowdevice = New System.Windows.Forms.Button()
        Me.btneditsectiongroup = New System.Windows.Forms.Button()
        Me.btnadd_device_group = New System.Windows.Forms.Button()
        Me.btnaddhistory = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btntype = New System.Windows.Forms.Button()
        Me.btnbrand = New System.Windows.Forms.Button()
        Me.btnadddepartment = New System.Windows.Forms.Button()
        Me.btnaddfloor = New System.Windows.Forms.Button()
        Me.btnadd = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.สวัสดี = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.llogin = New System.Windows.Forms.Label()
        Me.btn_regis = New System.Windows.Forms.Button()
        Me.btn_loguot = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.ForeColor = System.Drawing.Color.White
        Me.TabControl1.Location = New System.Drawing.Point(1, 100)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(974, 230)
        Me.TabControl1.TabIndex = 340
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.Controls.Add(Me.btnnextedit)
        Me.TabPage1.Controls.Add(Me.btnshowdevice)
        Me.TabPage1.Controls.Add(Me.btneditsectiongroup)
        Me.TabPage1.Controls.Add(Me.btnadd_device_group)
        Me.TabPage1.Controls.Add(Me.btnaddhistory)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabPage1.ForeColor = System.Drawing.Color.Black
        Me.TabPage1.Location = New System.Drawing.Point(4, 35)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(966, 191)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "DEVICE MANAGEMENT"
        '
        'btnnextedit
        '
        Me.btnnextedit.BackColor = System.Drawing.SystemColors.Control
        Me.btnnextedit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnnextedit.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnnextedit.ForeColor = System.Drawing.Color.Black
        Me.btnnextedit.Location = New System.Drawing.Point(25, 53)
        Me.btnnextedit.Name = "btnnextedit"
        Me.btnnextedit.Size = New System.Drawing.Size(117, 72)
        Me.btnnextedit.TabIndex = 341
        Me.btnnextedit.Text = "Management Device"
        Me.btnnextedit.UseVisualStyleBackColor = False
        '
        'btnshowdevice
        '
        Me.btnshowdevice.BackColor = System.Drawing.SystemColors.Control
        Me.btnshowdevice.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnshowdevice.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnshowdevice.ForeColor = System.Drawing.Color.Black
        Me.btnshowdevice.Location = New System.Drawing.Point(517, 53)
        Me.btnshowdevice.Name = "btnshowdevice"
        Me.btnshowdevice.Size = New System.Drawing.Size(117, 72)
        Me.btnshowdevice.TabIndex = 340
        Me.btnshowdevice.Text = "ALL DATABASE"
        Me.btnshowdevice.UseVisualStyleBackColor = False
        '
        'btneditsectiongroup
        '
        Me.btneditsectiongroup.BackColor = System.Drawing.SystemColors.Control
        Me.btneditsectiongroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btneditsectiongroup.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btneditsectiongroup.ForeColor = System.Drawing.Color.Black
        Me.btneditsectiongroup.Location = New System.Drawing.Point(271, 53)
        Me.btneditsectiongroup.Name = "btneditsectiongroup"
        Me.btneditsectiongroup.Size = New System.Drawing.Size(117, 72)
        Me.btneditsectiongroup.TabIndex = 340
        Me.btneditsectiongroup.Text = "TRANSFER"
        Me.btneditsectiongroup.UseVisualStyleBackColor = False
        '
        'btnadd_device_group
        '
        Me.btnadd_device_group.BackColor = System.Drawing.SystemColors.Control
        Me.btnadd_device_group.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnadd_device_group.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnadd_device_group.ForeColor = System.Drawing.Color.Black
        Me.btnadd_device_group.Location = New System.Drawing.Point(148, 53)
        Me.btnadd_device_group.Name = "btnadd_device_group"
        Me.btnadd_device_group.Size = New System.Drawing.Size(117, 72)
        Me.btnadd_device_group.TabIndex = 336
        Me.btnadd_device_group.Text = "INSTALLER"
        Me.btnadd_device_group.UseVisualStyleBackColor = False
        '
        'btnaddhistory
        '
        Me.btnaddhistory.BackColor = System.Drawing.SystemColors.Control
        Me.btnaddhistory.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnaddhistory.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnaddhistory.ForeColor = System.Drawing.Color.Black
        Me.btnaddhistory.Location = New System.Drawing.Point(394, 53)
        Me.btnaddhistory.Name = "btnaddhistory"
        Me.btnaddhistory.Size = New System.Drawing.Size(117, 72)
        Me.btnaddhistory.TabIndex = 333
        Me.btnaddhistory.Text = "MAINTENANCE"
        Me.btnaddhistory.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.btntype)
        Me.TabPage2.Controls.Add(Me.btnbrand)
        Me.TabPage2.Controls.Add(Me.btnadddepartment)
        Me.TabPage2.Controls.Add(Me.btnaddfloor)
        Me.TabPage2.ForeColor = System.Drawing.Color.White
        Me.TabPage2.Location = New System.Drawing.Point(4, 35)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(966, 191)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "LOCATION MANAGEMENT"
        '
        'btntype
        '
        Me.btntype.BackColor = System.Drawing.SystemColors.Control
        Me.btntype.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btntype.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btntype.ForeColor = System.Drawing.Color.Black
        Me.btntype.Location = New System.Drawing.Point(399, 55)
        Me.btntype.Name = "btntype"
        Me.btntype.Size = New System.Drawing.Size(117, 72)
        Me.btntype.TabIndex = 343
        Me.btntype.Text = "Management Type"
        Me.btntype.UseVisualStyleBackColor = False
        '
        'btnbrand
        '
        Me.btnbrand.BackColor = System.Drawing.SystemColors.Control
        Me.btnbrand.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnbrand.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnbrand.ForeColor = System.Drawing.Color.Black
        Me.btnbrand.Location = New System.Drawing.Point(276, 55)
        Me.btnbrand.Name = "btnbrand"
        Me.btnbrand.Size = New System.Drawing.Size(117, 72)
        Me.btnbrand.TabIndex = 342
        Me.btnbrand.Text = "Management Brand"
        Me.btnbrand.UseVisualStyleBackColor = False
        '
        'btnadddepartment
        '
        Me.btnadddepartment.BackColor = System.Drawing.SystemColors.Control
        Me.btnadddepartment.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnadddepartment.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnadddepartment.ForeColor = System.Drawing.Color.Black
        Me.btnadddepartment.Location = New System.Drawing.Point(153, 55)
        Me.btnadddepartment.Name = "btnadddepartment"
        Me.btnadddepartment.Size = New System.Drawing.Size(117, 72)
        Me.btnadddepartment.TabIndex = 332
        Me.btnadddepartment.Text = "Management Section"
        Me.btnadddepartment.UseVisualStyleBackColor = False
        '
        'btnaddfloor
        '
        Me.btnaddfloor.BackColor = System.Drawing.SystemColors.Control
        Me.btnaddfloor.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnaddfloor.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnaddfloor.ForeColor = System.Drawing.Color.Black
        Me.btnaddfloor.Location = New System.Drawing.Point(30, 55)
        Me.btnaddfloor.Name = "btnaddfloor"
        Me.btnaddfloor.Size = New System.Drawing.Size(117, 72)
        Me.btnaddfloor.TabIndex = 338
        Me.btnaddfloor.Text = "Management Floor"
        Me.btnaddfloor.UseVisualStyleBackColor = False
        '
        'btnadd
        '
        Me.btnadd.BackColor = System.Drawing.SystemColors.Control
        Me.btnadd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnadd.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnadd.ForeColor = System.Drawing.Color.Black
        Me.btnadd.Location = New System.Drawing.Point(30, 345)
        Me.btnadd.Name = "btnadd"
        Me.btnadd.Size = New System.Drawing.Size(117, 72)
        Me.btnadd.TabIndex = 330
        Me.btnadd.Text = "ADD"
        Me.btnadd.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(100, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 34)
        Me.Label4.TabIndex = 343
        Me.Label4.Text = "Label4"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(100, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 34)
        Me.Label3.TabIndex = 342
        Me.Label3.Text = "Label3"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(10, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 30)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "ตำแหน่ง :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(10, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 30)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "คุณ :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(683, 373)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(289, 83)
        Me.Panel1.TabIndex = 341
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel2.Controls.Add(Me.สวัสดี)
        Me.Panel2.Location = New System.Drawing.Point(683, 332)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(289, 41)
        Me.Panel2.TabIndex = 342
        '
        'สวัสดี
        '
        Me.สวัสดี.AutoSize = True
        Me.สวัสดี.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.สวัสดี.Location = New System.Drawing.Point(100, 5)
        Me.สวัสดี.Name = "สวัสดี"
        Me.สวัสดี.Size = New System.Drawing.Size(101, 30)
        Me.สวัสดี.TabIndex = 0
        Me.สวัสดี.Text = "ยินดีต้อนรับ"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Controls.Add(Me.llogin)
        Me.Panel3.Location = New System.Drawing.Point(1, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(982, 94)
        Me.Panel3.TabIndex = 343
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(374, 91)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'llogin
        '
        Me.llogin.AutoSize = True
        Me.llogin.BackColor = System.Drawing.Color.Transparent
        Me.llogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.llogin.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.llogin.Location = New System.Drawing.Point(397, 28)
        Me.llogin.Name = "llogin"
        Me.llogin.Size = New System.Drawing.Size(388, 33)
        Me.llogin.TabIndex = 0
        Me.llogin.Text = "Device Management System"
        '
        'btn_regis
        '
        Me.btn_regis.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.btn_regis.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_regis.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_regis.ForeColor = System.Drawing.Color.Black
        Me.btn_regis.Location = New System.Drawing.Point(683, 462)
        Me.btn_regis.Name = "btn_regis"
        Me.btn_regis.Size = New System.Drawing.Size(289, 48)
        Me.btn_regis.TabIndex = 344
        Me.btn_regis.Text = "Form Register"
        Me.btn_regis.UseVisualStyleBackColor = False
        '
        'btn_loguot
        '
        Me.btn_loguot.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.btn_loguot.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_loguot.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btn_loguot.ForeColor = System.Drawing.Color.Black
        Me.btn_loguot.Location = New System.Drawing.Point(683, 516)
        Me.btn_loguot.Name = "btn_loguot"
        Me.btn_loguot.Size = New System.Drawing.Size(289, 48)
        Me.btn_loguot.TabIndex = 345
        Me.btn_loguot.Text = "Log out"
        Me.btn_loguot.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Control
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(166, 343)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(117, 72)
        Me.Button1.TabIndex = 346
        Me.Button1.Text = "Report"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btn_add_user
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(977, 593)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn_loguot)
        Me.Controls.Add(Me.btn_regis)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.btnadd)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "btn_add_user"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form Menu"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btnnextedit As System.Windows.Forms.Button
    Friend WithEvents btnshowdevice As System.Windows.Forms.Button
    Friend WithEvents btneditsectiongroup As System.Windows.Forms.Button
    Friend WithEvents btnadd As System.Windows.Forms.Button
    Friend WithEvents btnadd_device_group As System.Windows.Forms.Button
    Friend WithEvents btnaddhistory As System.Windows.Forms.Button
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btnadddepartment As System.Windows.Forms.Button
    Friend WithEvents btnaddfloor As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btntype As System.Windows.Forms.Button
    Friend WithEvents btnbrand As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents สวัสดี As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents llogin As System.Windows.Forms.Label
    Friend WithEvents btn_regis As System.Windows.Forms.Button
    Friend WithEvents btn_loguot As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
