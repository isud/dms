﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_MNG_DEVICE
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkeycom1 As String
    Dim idkeycomr2 As String
    Dim idkeyprin3 As String
    Dim idkeymonitor4 As String
    Dim idkeyother5 As String
    Dim idkeylicense6 As String
    Dim idkey2 As String
    Dim idkey3 As String
    Dim idkey8 As String
    Dim idkey9 As String
    Dim idkey10 As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_MNG_DEVICE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล
        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)
            Me.Close()
        End Try
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        mySqlCommand.CommandText = "Select * from member where user_id like '" + id_user + "';"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try

            mySqlReader = mySqlCommand.ExecuteReader
            While mySqlReader.Read()
                Label87.Text = " " + mySqlReader("name") + "  " + mySqlReader("lastname")
                Label89.Text = " " + mySqlReader("position_name")
                Label92.Text = " " + mySqlReader("user_id")

            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Sql.Close()

        ''เริ่มต้นหน้าใหม่ TYPE "Computer"

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Computer' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewcom.Items.Clear()

            While (mySqlReader.Read())

                With ListViewcom.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        ''เริ่มต้นหน้าใหม่ TYPE "Computer Rent"

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Computer Rent' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewcomrent.Items.Clear()

            While (mySqlReader.Read())

                With ListViewcomrent.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        ''เริ่มต้นหน้าใหม่ TYPE "Printer"

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Printer' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewprin.Items.Clear()

            While (mySqlReader.Read())

                With ListViewprin.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("    " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        ''เริ่มต้นหน้าใหม่ TYPE "Monitor"
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Monitor' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewmonitor.Items.Clear()

            While (mySqlReader.Read())

                With ListViewmonitor.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()


        ''เริ่มต้นหน้าใหม่ TYPE "Other"


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Other' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewother.Items.Clear()

            While (mySqlReader.Read())

                With ListViewother.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("serial"))
                    If mySqlReader("type_data") IsNot DBNull.Value Then
                        .subitems.add(mySqlReader("type_data"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With

            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()


        ''เริ่มต้นหน้าใหม่ TYPE "License"
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'License' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewlicense.Items.Clear()

            While (mySqlReader.Read())

                With ListViewlicense.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("     " + mySqlReader("detail") + "     " + mySqlReader("price") + "     " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txtshowcom.Text = ListViewcom.Items.Count.ToString
        txtshowcomrent.Text = ListViewcomrent.Items.Count.ToString
        txtshowlicense.Text = ListViewlicense.Items.Count.ToString
        txtshowmonitor.Text = ListViewmonitor.Items.Count.ToString
        txtshowother.Text = ListViewother.Items.Count.ToString
        txtshowprin.Text = ListViewprin.Items.Count.ToString
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM brand order by idbrand;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ComboBoxbrand1.Items.Clear()
            ComboBoxbrand2.Items.Clear()
            ComboBoxbrand3.Items.Clear()
            ComboBoxbrand4.Items.Clear()
            ComboBoxbrand5.Items.Clear()
            While (mySqlReader.Read())
                ComboBoxbrand1.Items.Add(mySqlReader("brand_name"))
                ComboBoxbrand2.Items.Add(mySqlReader("brand_name"))
                ComboBoxbrand3.Items.Add(mySqlReader("brand_name"))
                ComboBoxbrand4.Items.Add(mySqlReader("brand_name"))
                ComboBoxbrand5.Items.Add(mySqlReader("brand_name"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If


        mySqlCommand.CommandText = "SELECT * FROM type order by idtype ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ComboBoxtype_other.Items.Clear()
            While (mySqlReader.Read())
                ComboBoxtype_other.Items.Add(mySqlReader("type_name"))
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub showdatacom1()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Computer' order by iddata_device  DESC;"
        MySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = MySqlCommand.ExecuteReader

            ListViewcom.Items.Clear()

            While (mySqlReader.Read())

                With ListViewcom.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .SubItems.Add(mySqlReader("position"))
                    .SubItems.Add(mySqlReader("section_data"))
                    .SubItems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txtshowcom.Text = ListViewcom.Items.Count.ToString
    End Sub
    Private Sub showdatacomr2()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Computer Rent' order by iddata_device  DESC;"
        MySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = MySqlCommand
        Try
            mySqlReader = MySqlCommand.ExecuteReader

            ListViewcomrent.Items.Clear()

            While (mySqlReader.Read())

                With ListViewcomrent.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub showdataprin3()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Printer' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewprin.Items.Clear()

            While (mySqlReader.Read())

                With ListViewprin.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("    " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub showdatamonitor4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Monitor' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewmonitor.Items.Clear()

            While (mySqlReader.Read())

                With ListViewmonitor.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

    End Sub
    Private Sub showdataother5()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'Other' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewother.Items.Clear()

            While (mySqlReader.Read())

                With ListViewother.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("serial"))
                    If mySqlReader("type_data") IsNot DBNull.Value Then
                        .subitems.add(mySqlReader("type_data"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub showdatalicense6()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        mySqlCommand.CommandText = "SELECT * FROM data_device where data_type = 'License' order by iddata_device  DESC;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewlicense.Items.Clear()

            While (mySqlReader.Read())

                With ListViewlicense.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("     " + mySqlReader("detail") + "     " + mySqlReader("price") + "     " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

    End Sub
    Private Sub ListViewcom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewcom.Click
        idkeycom1 = ListViewcom.SelectedItems(0).SubItems(0).Text
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device = '" & idkeycom1 & "' ;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                ComboBoxbrand1.Text = mySqlReader("brand_data")
                txt1_case.Text = mySqlReader("c_case")
                txt1_cd.Text = mySqlReader("c_cd")
                txt1_comname.Text = mySqlReader("c_comname")
                txt1_cpu.Text = mySqlReader("c_cpu")
                txt1_detail.Text = mySqlReader("detail")
                txt1_hdd.Text = mySqlReader("c_hdd")
                txt1_idcom.Text = mySqlReader("iddata_device")
                txt1_ip.Text = mySqlReader("c_ip")
                txt1_mainboard.Text = mySqlReader("c_mainboard")
                txt1_model.Text = mySqlReader("model")
                txt1_note_com.Text = mySqlReader("note")
                txt1_office.Text = mySqlReader("c_office")
                txt1_other.Text = mySqlReader("other")
                txt1_price.Text = mySqlReader("price")
                txt1_ps.Text = mySqlReader("c_ps")
                txt1_ram.Text = mySqlReader("c_ram")
                txt1_serial.Text = mySqlReader("serial")
                txt1_vga.Text = mySqlReader("c_vga")
                txt1_windows.Text = mySqlReader("c_windows")
                txt1_store.Text = mySqlReader("store")
                txtdatebuy.Text = mySqlReader("startbuy")
                txtwarrant.Text = mySqlReader("warrant")

                'Dim date_as As Date
                'date_as = "#" + mySqlReader("startbuy") + "#"
                'DateTimePicker5.Value = date_as
                'date_as = "#" + mySqlReader("warrant") + "#"
                'DateTimePicker6.Value = date_as
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub ListViewcomrent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewcomrent.Click
        idkeycomr2 = ListViewcomrent.SelectedItems(0).SubItems(0).Text
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device = '" & idkeycomr2 & "' ;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                ComboBoxbrand2.Text = mySqlReader("brand_data")
                txt2_case.Text = mySqlReader("c_case")
                txt2_cd.Text = mySqlReader("c_cd")
                txt2_comname.Text = mySqlReader("c_comname")
                txt2_cpu.Text = mySqlReader("c_cpu")
                txt2_detail.Text = mySqlReader("detail")
                txt2_hdd.Text = mySqlReader("c_hdd")
                txt2_idcomr.Text = mySqlReader("iddata_device")
                txt2_ip.Text = mySqlReader("c_ip")
                txt2_mainboard.Text = mySqlReader("c_mainboard")
                txt2_model.Text = mySqlReader("model")
                txt2_note_comr.Text = mySqlReader("note")
                txt2_office.Text = mySqlReader("c_office")
                txt2_other.Text = mySqlReader("other")
                txt2_price.Text = mySqlReader("price")
                txt2_ps.Text = mySqlReader("c_ps")
                txt2_ram.Text = mySqlReader("c_ram")
                txt2_serial.Text = mySqlReader("serial")
                txt2_vga.Text = mySqlReader("c_vga")
                txt2_windows.Text = mySqlReader("c_windows")
                txt2_store_rent.Text = mySqlReader("store")
                txtdatebutcomr.Text = mySqlReader("startbuy")
                txtwarrantcomr.Text = mySqlReader("warrant")
                'Dim date_as As Date
                'date_as = "#" + mySqlReader("startbuy") + "#"
                'DateTimePicker3.Value = date_as
                'date_as = "#" + mySqlReader("warrant") + "#"
                'DateTimePicker4.Value = date_as
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub ListViewprin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewprin.Click
        idkeyprin3 = ListViewprin.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device = '" & idkeyprin3 & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                ComboBoxbrand3.Text = mySqlReader("brand_data")
                txt3_model.Text = mySqlReader("model")
                txt3_idprint.Text = mySqlReader("iddata_device")
                txt3_detail.Text = mySqlReader("detail")
                txt3_serial.Text = mySqlReader("serial")
                txt3_price.Text = mySqlReader("price")
                txt3_note_prin.Text = mySqlReader("note")
                If mySqlReader("p_type") = "Laser Printer" Then
                    Radiolaserc.Checked = True
                Else
                    Radiolaserc.Checked = False
                End If
                If mySqlReader("p_type") = "Dot-matrix Printer" Then
                    Radiodot.Checked = True
                Else
                    Radiodot.Checked = False
                End If
                If mySqlReader("p_type") = "Inkjet Printer" Then
                    Radioinkjet.Checked = True
                Else
                    Radioinkjet.Checked = False
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub ListViewmonitor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewmonitor.Click
        idkeymonitor4 = ListViewmonitor.SelectedItems(0).SubItems(0).Text
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device = '" & idkeymonitor4 & "' ;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                ComboBoxbrand4.Text = mySqlReader("brand_data")
                txt4_model.Text = mySqlReader("model")
                txt4_idmonitor.Text = mySqlReader("iddata_device")
                txt4_detail.Text = mySqlReader("detail")
                txt4_serial.Text = mySqlReader("serial")
                txt4_size.Text = mySqlReader("m_size")
                txt4_price.Text = mySqlReader("price")
                txt4_note_monitor.Text = mySqlReader("note")
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub ListViewother_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewother.Click
        idkeyother5 = ListViewother.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device = '" & idkeyother5 & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                ComboBoxbrand5.Text = mySqlReader("brand_data")
                txt5_model.Text = mySqlReader("model")
                txt5_idother.Text = mySqlReader("iddata_device")
                txt5_detail.Text = mySqlReader("detail")
                txt5_serial.Text = mySqlReader("serial")
                txt5_price.Text = mySqlReader("price")
                txt5_note_other.Text = mySqlReader("note")
                ComboBoxtype_other.Text = mySqlReader("type_data")

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub ListViewlicense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewlicense.Click
        idkeylicense6 = ListViewlicense.SelectedItems(0).SubItems(0).Text
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where iddata_device = '" & idkeylicense6 & "' ;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                txt6_brand.Text = mySqlReader("brand_data")
                txt6_detail.Text = mySqlReader("detail")
                txt6_idlicense.Text = mySqlReader("iddata_device")
                txt6_amount.Text = mySqlReader("amount")
                txt6_price.Text = mySqlReader("price")
                txt6_note_license.Text = mySqlReader("note")
                If mySqlReader("windows_application") = "OS" Then
                    Radioos.Checked = True
                Else
                    Radioos.Checked = False
                End If
                If mySqlReader("windows_application") = "Application" Then
                    Radioapp.Checked = True
                Else
                    Radioapp.Checked = False
                End If
                ''โค้ดปุ่มเลือกเวลา
                Dim date_as As Date
                date_as = "#" + mySqlReader("startbuy") + "#"
                DateTimePicker1.Value = date_as

                date_as = "#" + mySqlReader("warrant") + "#"
                DateTimePicker2.Value = date_as
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
    Private Sub txtsearchcom_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearchcom.KeyDown
        If e.KeyCode = "13" Then
            searchdatacom1()
        End If
    End Sub
    Private Sub searchdatacom1()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim key As String
        Dim count As Integer

        count = 0
        key = txtsearchcom.Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where  data_type = 'Computer' and ( brand_data like '%" + key + "%'  or  model like '%" + key + "%'   or  c_cpu like '%" + key + "%'  or  c_mainboard like '%" + key + "%' or  c_ram like '%" + key + "%' or  c_hdd like '%" + key + "%' or  c_vga like '%" + key + "%'  or  state_device like '%" + key + "%' or  p_type like '%" + key + "%'or  m_size like '%" + key + "%' or  detail like '%" + key + "%'or  c_ip like '%" + key + "%' or  c_ps like '%" + key + "%' or  c_cd like '%" + key + "%' or  c_case like '%" + key + "%' or  serial like '%" + key + "%' or  c_comname like '%" + key + "%'or  c_windows like '%" + key + "%'or  c_office like '%" + key + "%'or  other like '%" + key + "%')  order by iddata_device DESC;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewcom.Items.Clear()

            While (mySqlReader.Read())

                With ListViewcom.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))

                End With
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txtshowcom.Text = ListViewcom.Items.Count.ToString
    End Sub
    Private Sub searchdatacomrent2()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim key As String
        Dim count As Integer

        count = 0
        key = txtsearchcomrent.Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where  data_type = 'Computer Rent' and ( brand_data like '%" + key + "%'  or  model like '%" + key + "%'   or  c_cpu like '%" + key + "%'  or  c_mainboard like '%" + key + "%' or  c_ram like '%" + key + "%' or  c_hdd like '%" + key + "%' or  c_vga like '%" + key + "%'  or  state_device like '%" + key + "%' or  p_type like '%" + key + "%'or  m_size like '%" + key + "%' or  detail like '%" + key + "%'or  c_ip like '%" + key + "%' or  c_ps like '%" + key + "%' or  c_cd like '%" + key + "%' or  c_case like '%" + key + "%' or  serial like '%" + key + "%' or  c_comname like '%" + key + "%'or  c_windows like '%" + key + "%'or  c_office like '%" + key + "%'or  other like '%" + key + "%')  order by iddata_device DESC;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewcomrent.Items.Clear()

            While (mySqlReader.Read())

                With ListViewcomrent.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txtshowcom.Text = ListViewcom.Items.Count.ToString
    End Sub
    Private Sub searchdataprin3()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim key As String
        Dim count As Integer

        count = 0
        key = txtsearchprin.Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where  data_type = 'Printer' and ( brand_data like '%" + key + "%'  or  model like '%" + key + "%'   or  c_cpu like '%" + key + "%'  or  c_mainboard like '%" + key + "%' or  c_ram like '%" + key + "%' or  c_hdd like '%" + key + "%' or  c_vga like '%" + key + "%'  or  state_device like '%" + key + "%' or  p_type like '%" + key + "%'or  m_size like '%" + key + "%' or  detail like '%" + key + "%'or  c_ip like '%" + key + "%' or  c_ps like '%" + key + "%' or  c_cd like '%" + key + "%' or  c_case like '%" + key + "%' or  serial like '%" + key + "%' or  c_comname like '%" + key + "%'or  c_windows like '%" + key + "%'or  c_office like '%" + key + "%'or  other like '%" + key + "%')  order by iddata_device DESC;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewprin.Items.Clear()

            While (mySqlReader.Read())

                With ListViewprin.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txt3_serial.Text = ListViewprin.Items.Count.ToString
    End Sub
    Private Sub searchdatamonitor4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim key As String
        Dim count As Integer

        count = 0
        key = txtsearchmonitor.Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where  data_type = 'Monitor' and ( brand_data like '%" + key + "%'  or  model like '%" + key + "%'   or  c_cpu like '%" + key + "%'  or  c_mainboard like '%" + key + "%' or  c_ram like '%" + key + "%' or  c_hdd like '%" + key + "%' or  c_vga like '%" + key + "%'  or  state_device like '%" + key + "%' or  p_type like '%" + key + "%'or  m_size like '%" + key + "%' or  detail like '%" + key + "%'or  c_ip like '%" + key + "%' or  c_ps like '%" + key + "%' or  c_cd like '%" + key + "%' or  c_case like '%" + key + "%' or  serial like '%" + key + "%' or  c_comname like '%" + key + "%'or  c_windows like '%" + key + "%'or  c_office like '%" + key + "%'or  other like '%" + key + "%')  order by iddata_device DESC;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewmonitor.Items.Clear()

            While (mySqlReader.Read())

                With ListViewmonitor.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txtshowmonitor.Text = ListViewmonitor.Items.Count.ToString
    End Sub
    Private Sub searchdataother5()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim key As String
        Dim count As Integer

        count = 0
        key = txtsearchother.Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where  data_type = 'Other' and ( brand_data like '%" + key + "%'  or  model like '%" + key + "%'   or  c_cpu like '%" + key + "%'  or  c_mainboard like '%" + key + "%' or  c_ram like '%" + key + "%' or  c_hdd like '%" + key + "%' or  c_vga like '%" + key + "%'  or  state_device like '%" + key + "%' or  p_type like '%" + key + "%'or  m_size like '%" + key + "%' or  detail like '%" + key + "%'or  c_ip like '%" + key + "%' or  c_ps like '%" + key + "%' or  c_cd like '%" + key + "%' or  c_case like '%" + key + "%' or  serial like '%" + key + "%' or  c_comname like '%" + key + "%'or  c_windows like '%" + key + "%'or  c_office like '%" + key + "%'or  other like '%" + key + "%')  order by iddata_device DESC;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewother.Items.Clear()

            While (mySqlReader.Read())

                With ListViewother.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("serial"))
                    If mySqlReader("type_data") IsNot DBNull.Value Then
                        .subitems.add(mySqlReader("type_data"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txt5_note_other.Text = ListViewother.Items.Count.ToString
    End Sub
    Private Sub searchdatalicense6()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim key As String
        Dim count As Integer

        count = 0
        key = txtsearchlicense.Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM data_device where  data_type = 'Other' and ( brand_data like '%" + key + "%'  or  model like '%" + key + "%'   or  c_cpu like '%" + key + "%'  or  c_mainboard like '%" + key + "%' or  c_ram like '%" + key + "%' or  c_hdd like '%" + key + "%' or  c_vga like '%" + key + "%'  or  state_device like '%" + key + "%' or  p_type like '%" + key + "%'or  m_size like '%" + key + "%' or  detail like '%" + key + "%'or  c_ip like '%" + key + "%' or  c_ps like '%" + key + "%' or  c_cd like '%" + key + "%' or  c_case like '%" + key + "%' or  serial like '%" + key + "%' or  c_comname like '%" + key + "%'or  c_windows like '%" + key + "%'or  c_office like '%" + key + "%'or  other like '%" + key + "%')  order by iddata_device DESC;"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewlicense.Items.Clear()

            While (mySqlReader.Read())

                With ListViewlicense.Items.Add(mySqlReader("iddata_device"))
                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add("Computer")
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add("Computer Rent")
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Printer")
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Monitor")
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Other")
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.add("License")
                    End If
                    .SubItems.Add(mySqlReader("brand_data"))

                    If mySqlReader("data_type") = "Computer" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Computer Rent" Then
                        .SubItems.Add(mySqlReader("model") + "     " + mySqlReader("c_cpu") + "      " + mySqlReader("c_mainboard") + "       " + mySqlReader("c_ram") + "       " + mySqlReader("c_hdd") + "      " + mySqlReader("c_vga") + "Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "PowerSupply : " + mySqlReader("c_ps") + "CD/DVD : " + mySqlReader("c_cd") + "Case : " + mySqlReader("c_case") + "Computername : " + mySqlReader("c_comname") + " Windows : " + mySqlReader("c_windows") + "Office : " + mySqlReader("c_office") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Printer" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Type : " + mySqlReader("p_type") + "   Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Monitor" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Size : " + mySqlReader("m_size") + "  Detail : " + mySqlReader("detail") + "Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "Other" Then
                        .SubItems.Add("Model : " + mySqlReader("model") + "Detail : " + mySqlReader("detail") + "  Serial Number : " + mySqlReader("serial") + "Price : " + mySqlReader("price"))
                    End If
                    If mySqlReader("data_type") = "License" Then
                        .SubItems.Add("Detail : " + mySqlReader("detail") + "Price : " + mySqlReader("price") + "  Amount : " + mySqlReader("amount"))
                    End If
                    If mySqlReader("state_device") IsNot DBNull.Value Then
                        .SubItems.Add(mySqlReader("state_device"))
                    End If
                    .subitems.add(mySqlReader("position"))
                    .subitems.add(mySqlReader("section_data"))
                    .subitems.add(mySqlReader("position_edit"))
                End With
            End While

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        txtshowlicense.Text = ListViewlicense.Items.Count.ToString
    End Sub
    Private Sub txtsearchcomrent_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearchcomrent.KeyDown
        If e.KeyCode = "13" Then
            searchdatacomrent2()
        End If
        txtshowcomrent.Text = ListViewcomrent.Items.Count.ToString
    End Sub
    Private Sub txtsearchprin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearchprin.KeyDown
        If e.KeyCode = "13" Then
            searchdataprin3()
        End If
        txt3_serial.Text = ListViewprin.Items.Count.ToString
    End Sub
    Private Sub txtsearchmonitor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearchmonitor.KeyDown
        If e.KeyCode = "13" Then
            searchdatamonitor4()
        End If
        txtshowmonitor.Text = ListViewmonitor.Items.Count.ToString
    End Sub
    Private Sub txtsearchother_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearchother.KeyDown
        If e.KeyCode = "13" Then
            searchdataother5()
        End If
        txt5_note_other.Text = ListViewother.Items.Count.ToString
    End Sub
    Private Sub txtsearchlicense_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtsearchlicense.KeyDown
        If e.KeyCode = "13" Then
            searchdatalicense6()
        End If
        txtshowlicense.Text = ListViewlicense.Items.Count.ToString
    End Sub
    Private Sub btnsavecom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsavecom.Click
        If txt1_idcom.Text <> "" Then

            savedatacom1()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedatacom1()
        Dim respone As Object
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")

        If respone = 1 Then
            Try
                commandText2 = "UPDATE data_device SET brand_data = '" & ComboBoxbrand1.Text & "' , model = '" & txt1_model.Text & "' , c_cpu = '" & txt1_cpu.Text & "' , c_mainboard = '" & txt1_mainboard.Text & "', c_ram = '" & txt1_ram.Text & "' ,c_hdd = '" & txt1_hdd.Text & "' ,c_vga = '" & txt1_vga.Text & "',c_ip = '" & txt1_ip.Text & "',detail = '" & txt1_detail.Text & "',c_ps = '" & txt1_ps.Text & "' ,c_cd = '" & txt1_cd.Text & "',c_case = '" & txt1_case.Text & "',serial = '" & txt1_serial.Text & "',price = '" & txt1_price.Text & "',c_comname = '" & txt1_comname.Text & "',c_windows = '" & txt1_windows.Text & "',c_office = '" & txt1_office.Text & "',other = '" & txt1_other.Text & "',note = '" & txt1_note_com.Text & "',store = '" & txt1_store.Text & "',startbuy = '" & txtdatebuy.Text & "' ,warrant = '" & txtwarrant.Text & "',position_edit = '" + id_user + "'  WHERE iddata_device = " & txt1_idcom.Text & "; "
                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("บันทึกเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        showdatacom1()
        cleardatacom1()
    End Sub
    Private Sub btnsave_comrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave_comrent.Click
        If txt2_idcomr.Text <> "" Then

            savedatacomrent2()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedatacomrent2()
        Dim respone As Object
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")

        If respone = 1 Then
            Try
                commandText2 = "UPDATE data_device SET brand_data = '" & ComboBoxbrand2.Text & "' , model = '" & txt2_model.Text & "' , c_cpu = '" & txt2_cpu.Text & "' , c_mainboard = '" & txt2_mainboard.Text & "', c_ram = '" & txt2_ram.Text & "' ,c_hdd = '" & txt2_hdd.Text & "' ,c_vga = '" & txt2_vga.Text & "',c_ip = '" & txt2_ip.Text & "',detail = '" & txt2_detail.Text & "',c_ps = '" & txt2_ps.Text & "' ,c_cd = '" & txt2_cd.Text & "',c_case = '" & txt2_case.Text & "',serial = '" & txt2_serial.Text & "',price = '" & txt2_price.Text & "',c_comname = '" & txt2_comname.Text & "',c_windows = '" & txt2_windows.Text & "',c_office = '" & txt2_office.Text & "',other = '" & txt2_other.Text & "',note = '" & txt2_note_comr.Text & "',store = '" & txt2_store_rent.Text & "',startbuy = '" & txtdatebutcomr.Text & "' ,warrant = '" & txtwarrantcomr.Text & "',position_edit = '" + id_user + "' WHERE iddata_device = " & txt2_idcomr.Text & "; "
                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("บันทึกเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        showdatacomr2()
        cleardatacomr2()
    End Sub
    Private Sub btnsave_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave_print.Click
        If txt3_idprint.Text <> "" Then

            savedataprin3()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedataprin3()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object
        Dim printer As String
        If Radiolaserc.Checked = True Then
            printer = "Laser Printer"
        End If
        If Radiodot.Checked = True Then
            printer = "Dot-matrix Printer"
        End If
        If Radioinkjet.Checked = True Then
            printer = "Inkjet Printer"
        End If
        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Complete")
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        If respone = 1 Then


            Try
                commandText2 = "UPDATE data_device SET brand_data = '" & ComboBoxbrand3.Text & "' , model = '" & txt3_model.Text & "' , serial = '" & txt3_serial.Text & "', detail = '" & txt3_detail.Text & "', p_type = '" & printer & "', price = '" & txt3_price.Text & "', note = '" & txt3_note_prin.Text & "',position_edit = '" + id_user + "' WHERE iddata_device = " & txt3_idprint.Text & "; "
                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("บันทึกเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        showdataprin3()
        cleardataprin3()
    End Sub
    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If txt4_idmonitor.Text <> "" Then

            savedatamonitor4()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedatamonitor4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง by โตน", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Complete")
        If respone = 1 Then
            Try
                commandText2 = "UPDATE data_device SET brand_data = '" & ComboBoxbrand4.Text & "' , model = '" & txt4_model.Text & "' , serial = '" & txt4_serial.Text & "', detail = '" & txt4_detail.Text & "', m_size = '" & txt4_size.Text & "',price = '" & txt4_price.Text & "',note = '" & txt4_note_monitor.Text & "',position_edit = '" + id_user + "' WHERE iddata_device = " & txt4_idmonitor.Text & "; "
                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("บันทึกเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        showdatamonitor4()
        cleardatamonitor4()
    End Sub
    Private Sub btnsaveother_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsaveother.Click
        If txt5_idother.Text <> "" Then

            savedataother5()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedataother5()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            Try
                commandText2 = "UPDATE data_device SET brand_data = '" & ComboBoxbrand5.Text & "' , model = '" & txt5_model.Text & "' , serial = '" & txt5_serial.Text & "', detail = '" & txt5_detail.Text & "', price = '" & txt5_price.Text & "' , type_data = '" & ComboBoxtype_other.Text & "' , note = '" & txt5_note_other.Text & "',position_edit = '" + id_user + "'  WHERE iddata_device = " & txt5_idother.Text & "; "
                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("บันทึกเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        showdataother5()
        cleardataother5()
    End Sub
    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        If txt6_idlicense.Text <> "" Then

            savedatalicense6()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub savedatalicense6()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object
        Dim windows_application As String
        If Radioos.Checked = True Then
            windows_application = "OS"
        End If
        If Radioapp.Checked = True Then
            windows_application = "Application"
        End If
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            Try
                commandText2 = "UPDATE data_device SET brand_data = '" & txt6_brand.Text & "' , amount = '" & txt6_amount.Text & "' , price = '" & txt6_price.Text & "', detail = '" & txt6_detail.Text & "', note = '" & txt6_note_license.Text & "',windows_application = '" & windows_application & "',position_edit = '" + id_user + "' WHERE iddata_device = " & txt6_idlicense.Text & ";"
                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("บันทึกเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        showdatalicense6()
    End Sub
    Private Sub btnsearchcom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearchcom.Click
        searchdatacom1()
    End Sub
    Private Sub btnsearchcomrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearchcomrent.Click
        searchdatacomrent2()
    End Sub
    Private Sub btnsearch_prin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch_prin.Click
        searchdataprin3()
    End Sub
    Private Sub btnsearch_monitor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch_monitor.Click
        searchdatamonitor4()
    End Sub
    Private Sub btnsearch_other_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch_other.Click
        searchdataother5()
    End Sub
    Private Sub btnsearch_license_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch_license.Click
        searchdatalicense6()
    End Sub
    Private Sub cleardatacom1()
        txt1_cpu.Text = ""
        txt1_case.Text = ""
        txt1_cd.Text = ""
        txt1_comname.Text = ""
        txt1_detail.Text = ""
        txt1_hdd.Text = ""
        txt1_ip.Text = ""
        txt1_mainboard.Text = ""
        txt1_model.Text = ""
        txt1_note_com.Text = ""
        txt1_office.Text = ""
        txt1_other.Text = ""
        txt1_price.Text = ""
        txt1_ps.Text = ""
        txt1_ram.Text = ""
        txt1_serial.Text = ""
        txt1_vga.Text = ""
        txt1_windows.Text = ""
        ComboBoxbrand1.Text = ""
        txt1_idcom.Text = ""
        txt1_store.Text = ""
    End Sub
    Private Sub cleardatacomr2()
        txt2_cpu.Text = ""
        txt2_case.Text = ""
        txt2_cd.Text = ""
        txt2_comname.Text = ""
        txt2_detail.Text = ""
        txt2_hdd.Text = ""
        txt2_ip.Text = ""
        txt2_mainboard.Text = ""
        txt2_model.Text = ""
        txt2_note_comr.Text = ""
        txt2_office.Text = ""
        txt2_other.Text = ""
        txt2_price.Text = ""
        txt2_ps.Text = ""
        txt2_ram.Text = ""
        txt2_serial.Text = ""
        txt2_vga.Text = ""
        txt2_windows.Text = ""
        ComboBoxbrand2.Text = ""
        txt2_idcomr.Text = ""
        txt2_store_rent.Text = ""

    End Sub
    Private Sub cleardataprin3()
        Radiolaserc.Checked = False
        Radiodot.Checked = False
        Radioinkjet.Checked = False
        ComboBoxbrand3.Text = ""
        txt3_detail.Text = ""
        txt3_model.Text = ""
        txt3_note_prin.Text = ""
        txt3_price.Text = ""
        txt3_serial.Text = ""
        txt3_idprint.Text = ""
    End Sub
    Private Sub cleardatamonitor4()
        txt4_detail.Text = ""
        txt4_model.Text = ""
        txt4_note_monitor.Text = ""
        txt4_price.Text = ""
        txt4_serial.Text = ""
        txt4_size.Text = ""
        ComboBoxbrand4.Text = ""
        txt4_idmonitor.Text = ""
    End Sub
    Private Sub cleardataother5()
        txt5_detail.Text = ""
        txt5_model.Text = ""
        txt5_note_other.Text = ""
        txt5_price.Text = ""
        txt5_serial.Text = ""
        ComboBoxtype_other.Text = ""
        ComboBoxbrand5.Text = ""
        txt5_idother.Text = ""
    End Sub
    Private Sub cleardatalicense6()
        txt6_amount.Text = ""
        txt6_brand.Text = ""
        txt6_detail.Text = ""
        txt6_note_license.Text = ""
        txt6_price.Text = ""
        Radioapp.Checked = False
        Radioos.Checked = False
        txt6_idlicense.Text = ""
    End Sub
    Private Sub btndeletecom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndeletecom.Click
        If txt1_idcom.Text <> "" Then
            DeleteDatacom1()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteDatacom1()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt1_idcom.Text <> "" Then
                Try
                    mySqlCommand.CommandText = "DELETE FROM data_device where iddata_device = '" & txt1_idcom.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                Sql.Close()
            End If
        End If
        showdatacom1()
        cleardatacom1()
    End Sub
    Private Sub btndelete_comr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete_comr.Click
        If txt2_idcomr.Text <> "" Then
            DeleteDatacomr2()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteDatacomr2()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt2_idcomr.Text <> "" Then
                Try
                    mySqlCommand.CommandText = "DELETE FROM data_device where iddata_device = '" & txt2_idcomr.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                Sql.Close()
            End If
        End If
        showdatacomr2()
        cleardatacomr2()
    End Sub

    Private Sub btndelete_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete_print.Click
        If txt3_idprint.Text <> "" Then
            DeleteDataprin3()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteDataprin3()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt1_idcom.Text <> "" Then
                Try
                    mySqlCommand.CommandText = "DELETE FROM data_device where iddata_device = '" & txt1_idcom.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                Sql.Close()
            End If
        End If
        showdataprin3()
        cleardataprin3()
    End Sub

    Private Sub btn_delete_monitor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete_monitor.Click
        If txt4_idmonitor.Text <> "" Then
            DeleteDatamonitor4()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteDatamonitor4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt4_idmonitor.Text <> "" Then
                Try
                    mySqlCommand.CommandText = "DELETE FROM data_device where iddata_device = '" & txt4_idmonitor.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                Sql.Close()
            End If
        End If
        showdatamonitor4()
        cleardatamonitor4()
    End Sub

    Private Sub btn_delete_other_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete_other.Click
        If txt5_idother.Text <> "" Then
            DeleteDataother4()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteDataother4()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt5_idother.Text <> "" Then
                Try
                    mySqlCommand.CommandText = "DELETE FROM data_device where iddata_device = '" & txt5_idother.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                Sql.Close()
            End If
        End If
        showdataother5()
        cleardataother5()
    End Sub

    Private Sub btn_delete_license_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete_license.Click
        If txt6_idlicense.Text <> "" Then
            DeleteDatalicense6()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteDatalicense6()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt6_idlicense.Text <> "" Then
                Try
                    mySqlCommand.CommandText = "DELETE FROM data_device where iddata_device = '" & txt6_idlicense.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                Sql.Close()
            End If
        End If
        showdatalicense6()
        cleardatalicense6()
    End Sub

    Private Sub btnaddnewcom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddnew_com.Click

        If txt1_cpu.Text <> "" Then

            savedatacomnew()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วนนะจ้า!! By โตน ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatacomnew()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,model,c_cpu,c_mainboard,c_ram,c_hdd,c_vga,c_ps,c_cd,c_ip,c_comname,c_windows,c_office,c_case,serial,other,note,price,brand_data,state_device,date,position,detail,startbuy,warrant,idsection,store,section_data,position_edit) VALUES ('Computer','" & txt1_model.Text & "', '" & txt1_cpu.Text & "', '" & txt1_mainboard.Text & "','" & txt1_ram.Text & "','" & txt1_hdd.Text & "','" & txt1_vga.Text & "','" & txt1_ps.Text & "','" & txt1_cd.Text & "','" & txt1_ip.Text & "','" & txt1_comname.Text & "','" & txt1_windows.Text & "','" & txt1_office.Text & "','" & txt1_case.Text & "','" & txt1_serial.Text & "','" & txt1_other.Text & "','" & txt1_note_com.Text & "','" & txt1_price.Text & "','" & ComboBoxbrand1.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','" + id_user + "','" & txt1_detail.Text & "','" & txtdatebuy.Text & "','" & txtwarrant.Text & "','0','" & txt1_store.Text & "','ว่าง','-');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย ", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        cleardatacom1()
        showdatacom1()
    End Sub

    Private Sub btnaddnew_comrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddnew_comrent.Click
        If txt2_cpu.Text <> "" Then

            savedatacomrnew()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วนนะจ้า!! By โตน ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatacomrnew()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,model,c_cpu,c_mainboard,c_ram,c_hdd,c_vga,c_ps,c_cd,c_ip,c_comname,c_windows,c_office,c_case,serial,other,note,price,brand_data,state_device,startbuy,warrant,position,detail,idsection,store,section_data,position_edit,date) VALUES ('Computer Rent','" & txt2_model.Text & "', '" & txt2_cpu.Text & "', '" & txt2_mainboard.Text & "','" & txt2_ram.Text & "','" & txt2_hdd.Text & "','" & txt2_vga.Text & "','" & txt2_ps.Text & "','" & txt2_cd.Text & "','" & txt2_ip.Text & "','" & txt2_comname.Text & "','" & txt2_windows.Text & "','" & txt2_office.Text & "','" & txt2_case.Text & "','" & txt2_serial.Text & "','" & txt2_other.Text & "','" & txt2_note_comr.Text & "','" & txt2_price.Text & "','" & ComboBoxbrand2.Text & "','ว่าง','" & txtdatebutcomr.Text & "','" & txtwarrantcomr.Text & "','" + id_user + "','" & txt2_detail.Text & "','0','" & txt2_store_rent.Text & "','ว่าง','-','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย ", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        cleardatacomr2()
        showdatacomr2()
    End Sub

    Private Sub btnaddnew_prinnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddnew_prinnew.Click
        If txt3_model.Text <> "" Then

            savedataprinnew()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วนนะจ้า!! By โตน ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedataprinnew()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim printer As String
        If Radiolaserc.Checked = True Then
            printer = "Laser Printer"
        End If
        If Radiodot.Checked = True Then
            printer = "Dot-matrix Printer"
        End If
        If Radioinkjet.Checked = True Then
            printer = "Inkjet Printer"
        End If
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,model,serial,price,detail,p_type,note,state_device,date,idsection,section_data,position,position_edit) VALUES ('Printer','" & ComboBoxbrand3.Text & "', '" & txt3_model.Text & "','" & txt3_serial.Text & "','" & txt3_price.Text & "','" & txt3_detail.Text & "','" & printer & "','" & txt3_note_prin.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','0','ว่าง','" + id_user + "','-');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
        showdataprin3()
        cleardataprin3()
    End Sub

    Private Sub btnaddnew_monitor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddnew_monitor.Click
        If txt4_model.Text <> "" Then

            savedatamonitornew()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วนนะจ้า!! By โตน ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatamonitornew()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            If txt4_model.Text <> "" Then
                mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,model,m_size,detail,serial,price,note,state_device,date,idsection,section_data,position,position_edit) VALUES ('Monitor','" & ComboBoxbrand4.Text & "', '" & txt4_model.Text & "', '" & txt4_size.Text & "','" & txt4_detail.Text & "','" & txt4_serial.Text & "','" & txt4_price.Text & "','" & txt4_note_monitor.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','0','ว่าง','" + id_user + "','-');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
        cleardatamonitor4()
        showdatamonitor4()
    End Sub

    Private Sub btnaddnew_other_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddnew_other.Click
        If txt5_model.Text <> "" Then

            savedataothernew()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedataothernew()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim check_sum As String = 0

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        'เช็คซ้ำ
        For i = 0 To ListViewother.Items.Count - 1

            If txt5_serial.Text = ListViewother.Items(i).SubItems(5).Text Then
                check_sum = "1"
            End If
        Next
        Try
            If idkey2 <> txt5_serial.Text And check_sum = "0" Then
                If txt5_serial.Text <> "" Then
                    mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,model,detail,serial,type_data,price,note,state_device,date,idsection,section_data,position,position_edit) VALUES ('Other','" & ComboBoxbrand5.Text & "', '" & txt5_model.Text & "', '" & txt5_detail.Text & "','" & txt5_serial.Text & "','" & ComboBoxtype_other.Text & "','" & txt5_price.Text & "','" & txt5_note_other.Text & "','ว่าง','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','0','ว่าง','" + id_user + "','-');"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                    MessageBox.Show("เก็บเข้าฐานข้อมูลแล้ว", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    showdataother5()
                    idkey3 = txt5_serial.Text
                Else
                    MessageBox.Show("กรุณาเช็คข้อมูลให้ถูกต้อง", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("กรุณาเช็คข้อมูลให้ถูกต้อง", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
    End Sub

    Private Sub btnaddnew_license_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddnew_license.Click
        If txt6_brand.Text <> "" Then

            savedatalicensenew()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedatalicensenew()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim windows_application As String
        If Radioos.Checked = True Then
            windows_application = "OS"
        End If
        If Radioapp.Checked = True Then
            windows_application = "Application"
        End If
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        Try
            mySqlCommand.CommandText = "INSERT INTO data_device (data_type,brand_data,price,detail,amount,windows_application,state_device,date,startbuy,warrant,note,idsection,position,section_data,position_edit) VALUES ('License','" & txt6_brand.Text & "','" & txt6_price.Text & "','" & txt6_detail.Text & "','" & txt6_amount.Text & "','" & windows_application & "','ยังไม่ถูกใช้งาน','" + Date.Now.Day.ToString + "/" + Date.Now.Month.ToString + "/" + Date.Now.Year.ToString + "','" & DateTimePicker1.Value.Date.ToString & "','" & DateTimePicker2.Value.Date.ToString & "','" & txt6_note_license.Text & "','0','ว่าง','" + id_user + "','-');"
            mySqlCommand.CommandType = CommandType.Text
            mySqlCommand.Connection = Sql
            mySqlCommand.ExecuteNonQuery()
            MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
        cleardatalicense6()
        showdatalicense6()
    End Sub
    Private Sub btnback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnback.Click
        Me.Hide()
    End Sub

    Private Sub txtshowcomrent_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtshowcomrent.KeyDown
        searchdatacomrent2()
    End Sub
End Class