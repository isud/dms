﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_ADD_USER
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey2 As String
    Dim idkey3 As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_ADD_USER_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล
        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)
            Me.Close()
        End Try

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM member order by idmember;"
        MySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = MySqlCommand

        Try
            mySqlReader = MySqlCommand.ExecuteReader
            ListViewuser.Items.Clear()
            While (mySqlReader.Read())

                With ListViewuser.Items.Add(mySqlReader("idmember"))
                    .SubItems.Add(mySqlReader("user_id"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

    End Sub

    Private Sub btn_save_prin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_prin.Click
        If txt_user.Text <> "" Or txt_pass.Text <> "" Or txt_name.Text <> "" Or txt_position.Text <> "" Or txt_lastname.Text <> "" Then

            savedataid()
        Else
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub savedataid()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        'Dim po As String
        'If Sql.State = ConnectionState.Closed Then
        '    Sql.Open()
        'End If

        'Try
        '    If txt_user.Text <> "" Or txt_pass.Text <> "" Or txt_name.Text <> "" Or txt_position.Text <> "" Or txt_lastname.Text <> "" Then
        '        mySqlCommand.CommandText = "INSERT INTO member (user_id,password,name,lastname,position_name,position) VALUES ( '" & txt_user.Text & "', '" & txt_pass.Text & "','" & txt_name.Text & "', '" & txt_lastname.Text & "', '" & txt_position.Text & "','0');"
        '        mySqlCommand.CommandType = CommandType.Text
        '        mySqlCommand.Connection = Sql
        '        mySqlCommand.ExecuteNonQuery()
        '        MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Else
        '        MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try
        'Sql.Close()


        Dim check_sum As String = 0

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If
        'เช็คซ้ำ
        For i = 0 To ListViewuser.Items.Count - 1

            If txt_user.Text = ListViewuser.Items(i).SubItems(1).Text Then
                check_sum = "1"
            End If
        Next
        Try
            If idkey2 <> txt_user.Text And check_sum = "0" Then
                If txt_user.Text <> "" Or txt_pass.Text <> "" Or txt_name.Text <> "" Or txt_position.Text <> "" Or txt_lastname.Text <> "" Then
                    mySqlCommand.CommandText = "INSERT INTO member (user_id,password,name,lastname,position_name,position) VALUES ( '" & txt_user.Text & "', '" & txt_pass.Text & "','" & txt_name.Text & "', '" & txt_lastname.Text & "', '" & txt_position.Text & "','0');"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                    MessageBox.Show("เก็บเข้าฐานข้อมูลแล้ว", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    idkey3 = txt_user.Text
                Else
                    MessageBox.Show("กรุณาเช็คข้อมูลให้ถูกต้อง", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("กรุณาเช็คข้อมูลให้ถูกต้อง", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            Sql.Close()
        End Try
        cleardata()



        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM member order by idmember;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewuser.Items.Clear()
            While (mySqlReader.Read())

                With ListViewuser.Items.Add(mySqlReader("idmember"))
                    .SubItems.Add(mySqlReader("user_id"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub

    Private Sub btn_cencel_prin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cencel_prin.Click
        cleardata()
    End Sub
    Private Sub cleardata()
        txt_lastname.Text = ""
        txt_name.Text = ""
        txt_pass.Text = ""
        txt_position.Text = ""
        txt_user.Text = ""

    End Sub
End Class