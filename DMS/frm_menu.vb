﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Public Class btn_add_user
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub btnaddfloor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddfloor.Click
        If position_user = 1 Then
            Dim NextForm As frm_edit_floor = New frm_edit_floor(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub frm_menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)

            Me.Close()
        End Try
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        mySqlCommand.CommandText = "Select * from member where user_id like '" + id_user + "';"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try

            mySqlReader = mySqlCommand.ExecuteReader
            While mySqlReader.Read()
                Label4.Text = " " + mySqlReader("name") + "  " + mySqlReader("lastname")
                Label3.Text = " " + mySqlReader("position_name")

            End While


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Sql.Close()
    End Sub

    Private Sub btnadddepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadddepartment.Click
        If position_user = 1 Then
            Dim NextForm As frm_edit_section = New frm_edit_section(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnlocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If position_user = 1 Then
            Dim NextForm As frm_edit_location = New frm_edit_location(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        If position_user = 0 Or position_user = 1 Then
            Dim NextForm As frm_ADD_DEVICE = New frm_ADD_DEVICE(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnbrand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbrand.Click
        If position_user = 1 Then
            Dim NextForm As frm_edit_brand = New frm_edit_brand(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btntype_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btntype.Click
        If position_user = 1 Then
            Dim NextForm As frm_edit_type = New frm_edit_type(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnnextedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnextedit.Click
        If position_user = 0 Or position_user = 1 Then
            Dim NextForm As frm_MNG_DEVICE = New frm_MNG_DEVICE(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnadd_device_group_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd_device_group.Click
        If position_user = 0 Or position_user = 1 Then
            Dim NextForm As frm_INSTALLER = New frm_INSTALLER(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_regis.Click
        If position_user = 1 Then
            Dim NextForm As frm_ADD_USER = New frm_ADD_USER(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btneditsectiongroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneditsectiongroup.Click
        If position_user = 0 Or position_user = 1 Then
            Dim NextForm As frm_TRANSFER = New frm_TRANSFER(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()

        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnshowdevice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnshowdevice.Click
        If position_user = 0 Or position_user = 1 Then
            Dim NextForm As frm_SHOW = New frm_SHOW(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()
        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnaddhistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddhistory.Click
        If position_user = 0 Or position_user = 1 Then
            Dim NextForm As frm_Maintenance = New frm_Maintenance(id_user, position_user, mysqlpass, ipconnect, usernamedb, dbname)
            '  Dim NextForm As main_user = New main_user()
            NextForm.Show()
        Else
            MessageBox.Show("ท่านไม่มีสิทธิ์เข้าใช้งาน", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btn_loguot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_loguot.Click
        If MessageBox.Show("คุณต้องการออกจากโปรแกรมใช่หรือไม่", "ยืนยันออกจากโปรแกรม", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim NextForm As frmreport = New frmreport
        '  Dim NextForm As main_user = New main_user()
        NextForm.Show()
    End Sub
End Class