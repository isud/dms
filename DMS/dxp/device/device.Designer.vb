﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class device
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(device))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.sleDevcatid = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.sleSeller = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.dpWarrantyExp = New DevExpress.XtraEditors.DateEdit()
        Me.sleState = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit6View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.peDevpic = New DevExpress.XtraEditors.PictureEdit()
        Me.btnRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.txtNote = New DevExpress.XtraEditors.MemoEdit()
        Me.txtPowersupply = New DevExpress.XtraEditors.TextEdit()
        Me.sleIdtype = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit5View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.sleLocation = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.MaslocationBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsMasLocation = New DMS.dtsMasLocation()
        Me.SearchLookUpEdit4View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidlocation1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collocation_name1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidFloor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colfloor_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidsection = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsection_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collocadescription_name1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPrice = New DevExpress.XtraEditors.TextEdit()
        Me.txtWinapp = New DevExpress.XtraEditors.MemoEdit()
        Me.ckbDeepfreeze = New DevExpress.XtraEditors.CheckEdit()
        Me.ckbSystem = New DevExpress.XtraEditors.CheckEdit()
        Me.ckbLab = New DevExpress.XtraEditors.CheckEdit()
        Me.ckbAdmin = New DevExpress.XtraEditors.CheckEdit()
        Me.ckbPacs = New DevExpress.XtraEditors.CheckEdit()
        Me.ckbHims = New DevExpress.XtraEditors.CheckEdit()
        Me.txtOffice = New DevExpress.XtraEditors.TextEdit()
        Me.txtWindows = New DevExpress.XtraEditors.TextEdit()
        Me.txtComname = New DevExpress.XtraEditors.TextEdit()
        Me.txtSerialnumber = New DevExpress.XtraEditors.TextEdit()
        Me.txtCase = New DevExpress.XtraEditors.TextEdit()
        Me.txtCd = New DevExpress.XtraEditors.TextEdit()
        Me.txtSpec = New DevExpress.XtraEditors.MemoEdit()
        Me.txtMonitorsize = New DevExpress.XtraEditors.TextEdit()
        Me.slePrttype = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.sleIdbrand = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.txtIpaddress = New DevExpress.XtraEditors.TextEdit()
        Me.dpPurchase = New DevExpress.XtraEditors.DateEdit()
        Me.txtHdd = New DevExpress.XtraEditors.TextEdit()
        Me.txtVga = New DevExpress.XtraEditors.TextEdit()
        Me.txtRam = New DevExpress.XtraEditors.TextEdit()
        Me.txtMainboard = New DevExpress.XtraEditors.TextEdit()
        Me.txtModel = New DevExpress.XtraEditors.TextEdit()
        Me.txtCpu = New System.Windows.Forms.TextBox()
        Me.txtIdDataDevice = New DevExpress.XtraEditors.TextEdit()
        Me.gcDevice = New DevExpress.XtraGrid.GridControl()
        Me.DtsDevice1 = New DMS.dtsDevice()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.coliddata_device = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldevcatid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldevcatname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidtype = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltype_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidbrand = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbrand_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprttypeid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprttypename = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmodel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcpu = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmainboard = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colram = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colvgacard = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colharddisk = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstateid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstatename = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmonitorsize = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colipnumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpowersupply = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcase = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colserialnumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colspecification = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colnote = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcomname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colwindows = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coloffice = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colhims = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collabs = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpacs = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsystem = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coladmin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldeepfreeze = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colwindows_application = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprice = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldate_purchase = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colwarranty = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.cold_add = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.cold_update = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colusadd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colusedit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidlocation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collocadescription_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collocation_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsellerid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsellername = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldevpic = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colusaddName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coluseditName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.layoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleSeparator1 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleSeparator2 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.sleDevcatid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sleSeller.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpWarrantyExp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpWarrantyExp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sleState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit6View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peDevpic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNote.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPowersupply.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sleIdtype.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit5View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sleLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MaslocationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsMasLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWinapp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckbDeepfreeze.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckbSystem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckbLab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckbAdmin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckbPacs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckbHims.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWindows.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtComname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerialnumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSpec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonitorsize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slePrttype.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sleIdbrand.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIpaddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpPurchase.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpPurchase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMainboard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtModel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIdDataDevice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsDevice1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SimpleSeparator1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SimpleSeparator2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.sleDevcatid)
        Me.LayoutControl1.Controls.Add(Me.sleSeller)
        Me.LayoutControl1.Controls.Add(Me.dpWarrantyExp)
        Me.LayoutControl1.Controls.Add(Me.sleState)
        Me.LayoutControl1.Controls.Add(Me.peDevpic)
        Me.LayoutControl1.Controls.Add(Me.btnRefresh)
        Me.LayoutControl1.Controls.Add(Me.btnEdit)
        Me.LayoutControl1.Controls.Add(Me.btnAdd)
        Me.LayoutControl1.Controls.Add(Me.txtNote)
        Me.LayoutControl1.Controls.Add(Me.txtPowersupply)
        Me.LayoutControl1.Controls.Add(Me.sleIdtype)
        Me.LayoutControl1.Controls.Add(Me.sleLocation)
        Me.LayoutControl1.Controls.Add(Me.txtPrice)
        Me.LayoutControl1.Controls.Add(Me.txtWinapp)
        Me.LayoutControl1.Controls.Add(Me.ckbDeepfreeze)
        Me.LayoutControl1.Controls.Add(Me.ckbSystem)
        Me.LayoutControl1.Controls.Add(Me.ckbLab)
        Me.LayoutControl1.Controls.Add(Me.ckbAdmin)
        Me.LayoutControl1.Controls.Add(Me.ckbPacs)
        Me.LayoutControl1.Controls.Add(Me.ckbHims)
        Me.LayoutControl1.Controls.Add(Me.txtOffice)
        Me.LayoutControl1.Controls.Add(Me.txtWindows)
        Me.LayoutControl1.Controls.Add(Me.txtComname)
        Me.LayoutControl1.Controls.Add(Me.txtSerialnumber)
        Me.LayoutControl1.Controls.Add(Me.txtCase)
        Me.LayoutControl1.Controls.Add(Me.txtCd)
        Me.LayoutControl1.Controls.Add(Me.txtSpec)
        Me.LayoutControl1.Controls.Add(Me.txtMonitorsize)
        Me.LayoutControl1.Controls.Add(Me.slePrttype)
        Me.LayoutControl1.Controls.Add(Me.sleIdbrand)
        Me.LayoutControl1.Controls.Add(Me.txtIpaddress)
        Me.LayoutControl1.Controls.Add(Me.dpPurchase)
        Me.LayoutControl1.Controls.Add(Me.txtHdd)
        Me.LayoutControl1.Controls.Add(Me.txtVga)
        Me.LayoutControl1.Controls.Add(Me.txtRam)
        Me.LayoutControl1.Controls.Add(Me.txtMainboard)
        Me.LayoutControl1.Controls.Add(Me.txtModel)
        Me.LayoutControl1.Controls.Add(Me.txtCpu)
        Me.LayoutControl1.Controls.Add(Me.txtIdDataDevice)
        Me.LayoutControl1.Controls.Add(Me.gcDevice)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(640, 251, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'sleDevcatid
        '
        Me.sleDevcatid.Location = New System.Drawing.Point(123, 35)
        Me.sleDevcatid.Name = "sleDevcatid"
        Me.sleDevcatid.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.sleDevcatid.Properties.Appearance.Options.UseFont = True
        Me.sleDevcatid.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleDevcatid.Properties.NullText = "[โปรดเลือกหมวดอุปกรณ์]"
        Me.sleDevcatid.Properties.View = Me.SearchLookUpEdit1View
        Me.sleDevcatid.Size = New System.Drawing.Size(188, 22)
        Me.sleDevcatid.StyleController = Me.LayoutControl1
        Me.sleDevcatid.TabIndex = 50
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'sleSeller
        '
        Me.sleSeller.Location = New System.Drawing.Point(433, 489)
        Me.sleSeller.Name = "sleSeller"
        Me.sleSeller.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.sleSeller.Properties.Appearance.Options.UseFont = True
        Me.sleSeller.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleSeller.Properties.NullText = "[โปรดเลือกผู้ขายอุปกรณ์]"
        Me.sleSeller.Properties.View = Me.GridView2
        Me.sleSeller.Size = New System.Drawing.Size(197, 22)
        Me.sleSeller.StyleController = Me.LayoutControl1
        Me.sleSeller.TabIndex = 49
        '
        'GridView2
        '
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'dpWarrantyExp
        '
        Me.dpWarrantyExp.EditValue = Nothing
        Me.dpWarrantyExp.Location = New System.Drawing.Point(433, 463)
        Me.dpWarrantyExp.Name = "dpWarrantyExp"
        Me.dpWarrantyExp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.dpWarrantyExp.Properties.Appearance.Options.UseFont = True
        Me.dpWarrantyExp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpWarrantyExp.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpWarrantyExp.Size = New System.Drawing.Size(197, 22)
        Me.dpWarrantyExp.StyleController = Me.LayoutControl1
        Me.dpWarrantyExp.TabIndex = 48
        '
        'sleState
        '
        Me.sleState.Location = New System.Drawing.Point(433, 357)
        Me.sleState.Name = "sleState"
        Me.sleState.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sleState.Properties.Appearance.Options.UseFont = True
        Me.sleState.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleState.Properties.NullText = "[โปรดเลือกสถานะอุปกรณ์]"
        Me.sleState.Properties.View = Me.SearchLookUpEdit6View
        Me.sleState.Size = New System.Drawing.Size(197, 22)
        Me.sleState.StyleController = Me.LayoutControl1
        Me.sleState.TabIndex = 47
        '
        'SearchLookUpEdit6View
        '
        Me.SearchLookUpEdit6View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit6View.Name = "SearchLookUpEdit6View"
        Me.SearchLookUpEdit6View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit6View.OptionsView.ShowGroupPanel = False
        '
        'peDevpic
        '
        Me.peDevpic.Location = New System.Drawing.Point(123, 515)
        Me.peDevpic.Name = "peDevpic"
        Me.peDevpic.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.peDevpic.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.peDevpic.Size = New System.Drawing.Size(507, 212)
        Me.peDevpic.StyleController = Me.LayoutControl1
        Me.peDevpic.TabIndex = 46
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.Location = New System.Drawing.Point(27, 768)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(603, 23)
        Me.btnRefresh.StyleController = Me.LayoutControl1
        Me.btnRefresh.TabIndex = 45
        Me.btnRefresh.Text = "Refresh"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.Location = New System.Drawing.Point(330, 741)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(300, 23)
        Me.btnEdit.StyleController = Me.LayoutControl1
        Me.btnEdit.TabIndex = 44
        Me.btnEdit.Text = "แก้ไข"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(27, 741)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(299, 23)
        Me.btnAdd.StyleController = Me.LayoutControl1
        Me.btnAdd.TabIndex = 43
        Me.btnAdd.Text = "เพิ่ม"
        '
        'txtNote
        '
        Me.txtNote.Location = New System.Drawing.Point(433, 264)
        Me.txtNote.Name = "txtNote"
        Me.txtNote.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNote.Properties.Appearance.Options.UseFont = True
        Me.txtNote.Size = New System.Drawing.Size(197, 89)
        Me.txtNote.StyleController = Me.LayoutControl1
        Me.txtNote.TabIndex = 40
        '
        'txtPowersupply
        '
        Me.txtPowersupply.Location = New System.Drawing.Point(123, 243)
        Me.txtPowersupply.Name = "txtPowersupply"
        Me.txtPowersupply.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPowersupply.Properties.Appearance.Options.UseFont = True
        Me.txtPowersupply.Size = New System.Drawing.Size(188, 22)
        Me.txtPowersupply.StyleController = Me.LayoutControl1
        Me.txtPowersupply.TabIndex = 38
        '
        'sleIdtype
        '
        Me.sleIdtype.Location = New System.Drawing.Point(123, 61)
        Me.sleIdtype.Name = "sleIdtype"
        Me.sleIdtype.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.sleIdtype.Properties.Appearance.Options.UseFont = True
        Me.sleIdtype.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleIdtype.Properties.NullText = "[โปรดเลือกประเภทอุปกรณ์]"
        Me.sleIdtype.Properties.View = Me.SearchLookUpEdit5View
        Me.sleIdtype.Size = New System.Drawing.Size(188, 22)
        Me.sleIdtype.StyleController = Me.LayoutControl1
        Me.sleIdtype.TabIndex = 37
        '
        'SearchLookUpEdit5View
        '
        Me.SearchLookUpEdit5View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit5View.Name = "SearchLookUpEdit5View"
        Me.SearchLookUpEdit5View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit5View.OptionsView.ShowGroupPanel = False
        '
        'sleLocation
        '
        Me.sleLocation.Location = New System.Drawing.Point(433, 383)
        Me.sleLocation.Name = "sleLocation"
        Me.sleLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sleLocation.Properties.Appearance.Options.UseFont = True
        Me.sleLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleLocation.Properties.DataSource = Me.MaslocationBindingSource
        Me.sleLocation.Properties.DisplayMember = "location_name"
        Me.sleLocation.Properties.NullText = "[โปรดเลือกสถานที่เก็บ]"
        Me.sleLocation.Properties.ValueMember = "idlocation"
        Me.sleLocation.Properties.View = Me.SearchLookUpEdit4View
        Me.sleLocation.Size = New System.Drawing.Size(197, 22)
        Me.sleLocation.StyleController = Me.LayoutControl1
        Me.sleLocation.TabIndex = 36
        '
        'MaslocationBindingSource
        '
        Me.MaslocationBindingSource.DataMember = "maslocation"
        Me.MaslocationBindingSource.DataSource = Me.DtsMasLocation
        '
        'DtsMasLocation
        '
        Me.DtsMasLocation.DataSetName = "dtsMasLocation"
        Me.DtsMasLocation.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit4View
        '
        Me.SearchLookUpEdit4View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidlocation1, Me.collocation_name1, Me.colidFloor, Me.colfloor_name, Me.colidsection, Me.colsection_name, Me.collocadescription_name1})
        Me.SearchLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit4View.Name = "SearchLookUpEdit4View"
        Me.SearchLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit4View.OptionsView.ShowGroupPanel = False
        '
        'colidlocation1
        '
        Me.colidlocation1.FieldName = "idlocation"
        Me.colidlocation1.Name = "colidlocation1"
        Me.colidlocation1.Visible = True
        Me.colidlocation1.VisibleIndex = 0
        '
        'collocation_name1
        '
        Me.collocation_name1.FieldName = "location_name"
        Me.collocation_name1.Name = "collocation_name1"
        Me.collocation_name1.Visible = True
        Me.collocation_name1.VisibleIndex = 1
        '
        'colidFloor
        '
        Me.colidFloor.FieldName = "idFloor"
        Me.colidFloor.Name = "colidFloor"
        '
        'colfloor_name
        '
        Me.colfloor_name.FieldName = "floor_name"
        Me.colfloor_name.Name = "colfloor_name"
        Me.colfloor_name.Visible = True
        Me.colfloor_name.VisibleIndex = 2
        '
        'colidsection
        '
        Me.colidsection.FieldName = "idsection"
        Me.colidsection.Name = "colidsection"
        '
        'colsection_name
        '
        Me.colsection_name.FieldName = "section_name"
        Me.colsection_name.Name = "colsection_name"
        Me.colsection_name.Visible = True
        Me.colsection_name.VisibleIndex = 3
        '
        'collocadescription_name1
        '
        Me.collocadescription_name1.FieldName = "locadescription_name"
        Me.collocadescription_name1.Name = "collocadescription_name1"
        Me.collocadescription_name1.Visible = True
        Me.collocadescription_name1.VisibleIndex = 4
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(433, 437)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrice.Properties.Appearance.Options.UseFont = True
        Me.txtPrice.Size = New System.Drawing.Size(197, 22)
        Me.txtPrice.StyleController = Me.LayoutControl1
        Me.txtPrice.TabIndex = 35
        '
        'txtWinapp
        '
        Me.txtWinapp.Location = New System.Drawing.Point(433, 159)
        Me.txtWinapp.Name = "txtWinapp"
        Me.txtWinapp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWinapp.Properties.Appearance.Options.UseFont = True
        Me.txtWinapp.Size = New System.Drawing.Size(197, 101)
        Me.txtWinapp.StyleController = Me.LayoutControl1
        Me.txtWinapp.TabIndex = 33
        '
        'ckbDeepfreeze
        '
        Me.ckbDeepfreeze.Location = New System.Drawing.Point(430, 135)
        Me.ckbDeepfreeze.Name = "ckbDeepfreeze"
        Me.ckbDeepfreeze.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbDeepfreeze.Properties.Appearance.Options.UseFont = True
        Me.ckbDeepfreeze.Properties.Caption = "Deep Freeze"
        Me.ckbDeepfreeze.Size = New System.Drawing.Size(200, 20)
        Me.ckbDeepfreeze.StyleController = Me.LayoutControl1
        Me.ckbDeepfreeze.TabIndex = 1
        '
        'ckbSystem
        '
        Me.ckbSystem.Location = New System.Drawing.Point(430, 111)
        Me.ckbSystem.Name = "ckbSystem"
        Me.ckbSystem.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbSystem.Properties.Appearance.Options.UseFont = True
        Me.ckbSystem.Properties.Caption = "System"
        Me.ckbSystem.Size = New System.Drawing.Size(200, 20)
        Me.ckbSystem.StyleController = Me.LayoutControl1
        Me.ckbSystem.TabIndex = 2
        '
        'ckbLab
        '
        Me.ckbLab.Location = New System.Drawing.Point(430, 87)
        Me.ckbLab.Name = "ckbLab"
        Me.ckbLab.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbLab.Properties.Appearance.Options.UseFont = True
        Me.ckbLab.Properties.Caption = "Lab"
        Me.ckbLab.Size = New System.Drawing.Size(200, 20)
        Me.ckbLab.StyleController = Me.LayoutControl1
        Me.ckbLab.TabIndex = 3
        '
        'ckbAdmin
        '
        Me.ckbAdmin.Location = New System.Drawing.Point(337, 135)
        Me.ckbAdmin.Name = "ckbAdmin"
        Me.ckbAdmin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbAdmin.Properties.Appearance.Options.UseFont = True
        Me.ckbAdmin.Properties.Caption = "Admin"
        Me.ckbAdmin.Size = New System.Drawing.Size(89, 20)
        Me.ckbAdmin.StyleController = Me.LayoutControl1
        Me.ckbAdmin.TabIndex = 4
        '
        'ckbPacs
        '
        Me.ckbPacs.Location = New System.Drawing.Point(337, 111)
        Me.ckbPacs.Name = "ckbPacs"
        Me.ckbPacs.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbPacs.Properties.Appearance.Options.UseFont = True
        Me.ckbPacs.Properties.Caption = "PACS"
        Me.ckbPacs.Size = New System.Drawing.Size(89, 20)
        Me.ckbPacs.StyleController = Me.LayoutControl1
        Me.ckbPacs.TabIndex = 5
        '
        'ckbHims
        '
        Me.ckbHims.Location = New System.Drawing.Point(337, 87)
        Me.ckbHims.Name = "ckbHims"
        Me.ckbHims.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbHims.Properties.Appearance.Options.UseFont = True
        Me.ckbHims.Properties.Caption = "HIMS"
        Me.ckbHims.Size = New System.Drawing.Size(89, 20)
        Me.ckbHims.StyleController = Me.LayoutControl1
        Me.ckbHims.TabIndex = 31
        '
        'txtOffice
        '
        Me.txtOffice.Location = New System.Drawing.Point(433, 35)
        Me.txtOffice.Name = "txtOffice"
        Me.txtOffice.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOffice.Properties.Appearance.Options.UseFont = True
        Me.txtOffice.Size = New System.Drawing.Size(197, 22)
        Me.txtOffice.StyleController = Me.LayoutControl1
        Me.txtOffice.TabIndex = 29
        '
        'txtWindows
        '
        Me.txtWindows.Location = New System.Drawing.Point(433, 9)
        Me.txtWindows.Name = "txtWindows"
        Me.txtWindows.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWindows.Properties.Appearance.Options.UseFont = True
        Me.txtWindows.Size = New System.Drawing.Size(197, 22)
        Me.txtWindows.StyleController = Me.LayoutControl1
        Me.txtWindows.TabIndex = 28
        '
        'txtComname
        '
        Me.txtComname.Location = New System.Drawing.Point(123, 425)
        Me.txtComname.Name = "txtComname"
        Me.txtComname.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComname.Properties.Appearance.Options.UseFont = True
        Me.txtComname.Size = New System.Drawing.Size(188, 22)
        Me.txtComname.StyleController = Me.LayoutControl1
        Me.txtComname.TabIndex = 27
        '
        'txtSerialnumber
        '
        Me.txtSerialnumber.Location = New System.Drawing.Point(123, 399)
        Me.txtSerialnumber.Name = "txtSerialnumber"
        Me.txtSerialnumber.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerialnumber.Properties.Appearance.Options.UseFont = True
        Me.txtSerialnumber.Size = New System.Drawing.Size(188, 22)
        Me.txtSerialnumber.StyleController = Me.LayoutControl1
        Me.txtSerialnumber.TabIndex = 26
        '
        'txtCase
        '
        Me.txtCase.Location = New System.Drawing.Point(123, 373)
        Me.txtCase.Name = "txtCase"
        Me.txtCase.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCase.Properties.Appearance.Options.UseFont = True
        Me.txtCase.Size = New System.Drawing.Size(188, 22)
        Me.txtCase.StyleController = Me.LayoutControl1
        Me.txtCase.TabIndex = 25
        '
        'txtCd
        '
        Me.txtCd.Location = New System.Drawing.Point(123, 347)
        Me.txtCd.Name = "txtCd"
        Me.txtCd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCd.Properties.Appearance.Options.UseFont = True
        Me.txtCd.Size = New System.Drawing.Size(188, 22)
        Me.txtCd.StyleController = Me.LayoutControl1
        Me.txtCd.TabIndex = 24
        '
        'txtSpec
        '
        Me.txtSpec.Location = New System.Drawing.Point(123, 451)
        Me.txtSpec.Name = "txtSpec"
        Me.txtSpec.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpec.Properties.Appearance.Options.UseFont = True
        Me.txtSpec.Size = New System.Drawing.Size(188, 60)
        Me.txtSpec.StyleController = Me.LayoutControl1
        Me.txtSpec.TabIndex = 22
        '
        'txtMonitorsize
        '
        Me.txtMonitorsize.Location = New System.Drawing.Point(123, 321)
        Me.txtMonitorsize.Name = "txtMonitorsize"
        Me.txtMonitorsize.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonitorsize.Properties.Appearance.Options.UseFont = True
        Me.txtMonitorsize.Size = New System.Drawing.Size(188, 22)
        Me.txtMonitorsize.StyleController = Me.LayoutControl1
        Me.txtMonitorsize.TabIndex = 21
        '
        'slePrttype
        '
        Me.slePrttype.Location = New System.Drawing.Point(123, 295)
        Me.slePrttype.Name = "slePrttype"
        Me.slePrttype.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.slePrttype.Properties.Appearance.Options.UseFont = True
        Me.slePrttype.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slePrttype.Properties.NullText = "[โปรดเลือกประเภทเครื่องพิมพ์]"
        Me.slePrttype.Properties.View = Me.SearchLookUpEdit3View
        Me.slePrttype.Size = New System.Drawing.Size(188, 22)
        Me.slePrttype.StyleController = Me.LayoutControl1
        Me.slePrttype.TabIndex = 20
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'sleIdbrand
        '
        Me.sleIdbrand.Location = New System.Drawing.Point(123, 89)
        Me.sleIdbrand.Name = "sleIdbrand"
        Me.sleIdbrand.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.sleIdbrand.Properties.Appearance.Options.UseFont = True
        Me.sleIdbrand.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleIdbrand.Properties.NullText = "[โปรดเลือกยี่ห้อ]"
        Me.sleIdbrand.Properties.View = Me.SearchLookUpEdit2View
        Me.sleIdbrand.Size = New System.Drawing.Size(188, 22)
        Me.sleIdbrand.StyleController = Me.LayoutControl1
        Me.sleIdbrand.TabIndex = 19
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'txtIpaddress
        '
        Me.txtIpaddress.Location = New System.Drawing.Point(433, 61)
        Me.txtIpaddress.Name = "txtIpaddress"
        Me.txtIpaddress.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIpaddress.Properties.Appearance.Options.UseFont = True
        Me.txtIpaddress.Size = New System.Drawing.Size(197, 22)
        Me.txtIpaddress.StyleController = Me.LayoutControl1
        Me.txtIpaddress.TabIndex = 18
        '
        'dpPurchase
        '
        Me.dpPurchase.EditValue = Nothing
        Me.dpPurchase.Location = New System.Drawing.Point(433, 411)
        Me.dpPurchase.Name = "dpPurchase"
        Me.dpPurchase.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dpPurchase.Properties.Appearance.Options.UseFont = True
        Me.dpPurchase.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpPurchase.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpPurchase.Size = New System.Drawing.Size(197, 22)
        Me.dpPurchase.StyleController = Me.LayoutControl1
        Me.dpPurchase.TabIndex = 16
        '
        'txtHdd
        '
        Me.txtHdd.Location = New System.Drawing.Point(123, 269)
        Me.txtHdd.Name = "txtHdd"
        Me.txtHdd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtHdd.Properties.Appearance.Options.UseFont = True
        Me.txtHdd.Size = New System.Drawing.Size(188, 22)
        Me.txtHdd.StyleController = Me.LayoutControl1
        Me.txtHdd.TabIndex = 12
        '
        'txtVga
        '
        Me.txtVga.Location = New System.Drawing.Point(123, 217)
        Me.txtVga.Name = "txtVga"
        Me.txtVga.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtVga.Properties.Appearance.Options.UseFont = True
        Me.txtVga.Size = New System.Drawing.Size(188, 22)
        Me.txtVga.StyleController = Me.LayoutControl1
        Me.txtVga.TabIndex = 11
        '
        'txtRam
        '
        Me.txtRam.Location = New System.Drawing.Point(123, 191)
        Me.txtRam.Name = "txtRam"
        Me.txtRam.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtRam.Properties.Appearance.Options.UseFont = True
        Me.txtRam.Size = New System.Drawing.Size(188, 22)
        Me.txtRam.StyleController = Me.LayoutControl1
        Me.txtRam.TabIndex = 10
        '
        'txtMainboard
        '
        Me.txtMainboard.Location = New System.Drawing.Point(123, 165)
        Me.txtMainboard.Name = "txtMainboard"
        Me.txtMainboard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMainboard.Properties.Appearance.Options.UseFont = True
        Me.txtMainboard.Size = New System.Drawing.Size(188, 22)
        Me.txtMainboard.StyleController = Me.LayoutControl1
        Me.txtMainboard.TabIndex = 9
        '
        'txtModel
        '
        Me.txtModel.Location = New System.Drawing.Point(123, 115)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtModel.Properties.Appearance.Options.UseFont = True
        Me.txtModel.Size = New System.Drawing.Size(188, 22)
        Me.txtModel.StyleController = Me.LayoutControl1
        Me.txtModel.TabIndex = 8
        '
        'txtCpu
        '
        Me.txtCpu.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCpu.Location = New System.Drawing.Point(123, 141)
        Me.txtCpu.Name = "txtCpu"
        Me.txtCpu.Size = New System.Drawing.Size(188, 20)
        Me.txtCpu.TabIndex = 7
        '
        'txtIdDataDevice
        '
        Me.txtIdDataDevice.Location = New System.Drawing.Point(123, 9)
        Me.txtIdDataDevice.Name = "txtIdDataDevice"
        Me.txtIdDataDevice.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtIdDataDevice.Properties.Appearance.Options.UseFont = True
        Me.txtIdDataDevice.Properties.ReadOnly = True
        Me.txtIdDataDevice.Size = New System.Drawing.Size(188, 22)
        Me.txtIdDataDevice.StyleController = Me.LayoutControl1
        Me.txtIdDataDevice.TabIndex = 5
        '
        'gcDevice
        '
        Me.gcDevice.DataMember = "device"
        Me.gcDevice.DataSource = Me.DtsDevice1
        Me.gcDevice.Location = New System.Drawing.Point(644, 4)
        Me.gcDevice.MainView = Me.GridView1
        Me.gcDevice.Name = "gcDevice"
        Me.gcDevice.Size = New System.Drawing.Size(632, 792)
        Me.gcDevice.TabIndex = 4
        Me.gcDevice.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtsDevice1
        '
        Me.DtsDevice1.DataSetName = "dtsDevice"
        Me.DtsDevice1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.coliddata_device, Me.coldevcatid, Me.coldevcatname, Me.colidtype, Me.coltype_name, Me.colidbrand, Me.colbrand_name, Me.colprttypeid, Me.colprttypename, Me.colmodel, Me.colcpu, Me.colmainboard, Me.colram, Me.colvgacard, Me.colharddisk, Me.colstateid, Me.colstatename, Me.colmonitorsize, Me.colipnumber, Me.colpowersupply, Me.colcd, Me.colcase, Me.colserialnumber, Me.colspecification, Me.colnote, Me.colcomname, Me.colwindows, Me.coloffice, Me.colhims, Me.collabs, Me.colpacs, Me.colsystem, Me.coladmin, Me.coldeepfreeze, Me.colwindows_application, Me.colprice, Me.coldate_purchase, Me.colwarranty, Me.cold_add, Me.cold_update, Me.colusadd, Me.colusedit, Me.colidlocation, Me.collocadescription_name, Me.collocation_name, Me.colsellerid, Me.colsellername, Me.coldevpic, Me.colusaddName, Me.coluseditName})
        Me.GridView1.GridControl = Me.gcDevice
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsFind.AlwaysVisible = True
        '
        'coliddata_device
        '
        Me.coliddata_device.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coliddata_device.AppearanceCell.Options.UseFont = True
        Me.coliddata_device.Caption = "ID Device"
        Me.coliddata_device.FieldName = "iddata_device"
        Me.coliddata_device.Name = "coliddata_device"
        Me.coliddata_device.OptionsColumn.AllowEdit = False
        Me.coliddata_device.Visible = True
        Me.coliddata_device.VisibleIndex = 0
        '
        'coldevcatid
        '
        Me.coldevcatid.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldevcatid.AppearanceCell.Options.UseFont = True
        Me.coldevcatid.FieldName = "devcatid"
        Me.coldevcatid.Name = "coldevcatid"
        Me.coldevcatid.OptionsColumn.AllowEdit = False
        Me.coldevcatid.OptionsColumn.AllowShowHide = False
        '
        'coldevcatname
        '
        Me.coldevcatname.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldevcatname.AppearanceCell.Options.UseFont = True
        Me.coldevcatname.FieldName = "devcatname"
        Me.coldevcatname.Name = "coldevcatname"
        Me.coldevcatname.OptionsColumn.AllowEdit = False
        Me.coldevcatname.Visible = True
        Me.coldevcatname.VisibleIndex = 1
        '
        'colidtype
        '
        Me.colidtype.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colidtype.AppearanceCell.Options.UseFont = True
        Me.colidtype.FieldName = "idtype"
        Me.colidtype.Name = "colidtype"
        Me.colidtype.OptionsColumn.AllowEdit = False
        Me.colidtype.OptionsColumn.AllowShowHide = False
        '
        'coltype_name
        '
        Me.coltype_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coltype_name.AppearanceCell.Options.UseFont = True
        Me.coltype_name.FieldName = "type_name"
        Me.coltype_name.Name = "coltype_name"
        Me.coltype_name.OptionsColumn.AllowEdit = False
        Me.coltype_name.Visible = True
        Me.coltype_name.VisibleIndex = 2
        '
        'colidbrand
        '
        Me.colidbrand.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colidbrand.AppearanceCell.Options.UseFont = True
        Me.colidbrand.FieldName = "idbrand"
        Me.colidbrand.Name = "colidbrand"
        Me.colidbrand.OptionsColumn.AllowEdit = False
        Me.colidbrand.OptionsColumn.AllowShowHide = False
        '
        'colbrand_name
        '
        Me.colbrand_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colbrand_name.AppearanceCell.Options.UseFont = True
        Me.colbrand_name.FieldName = "brand_name"
        Me.colbrand_name.Name = "colbrand_name"
        Me.colbrand_name.OptionsColumn.AllowEdit = False
        Me.colbrand_name.Visible = True
        Me.colbrand_name.VisibleIndex = 3
        '
        'colprttypeid
        '
        Me.colprttypeid.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colprttypeid.AppearanceCell.Options.UseFont = True
        Me.colprttypeid.FieldName = "prttypeid"
        Me.colprttypeid.Name = "colprttypeid"
        Me.colprttypeid.OptionsColumn.AllowEdit = False
        Me.colprttypeid.OptionsColumn.AllowShowHide = False
        '
        'colprttypename
        '
        Me.colprttypename.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colprttypename.AppearanceCell.Options.UseFont = True
        Me.colprttypename.FieldName = "prttypename"
        Me.colprttypename.Name = "colprttypename"
        Me.colprttypename.OptionsColumn.AllowEdit = False
        Me.colprttypename.Visible = True
        Me.colprttypename.VisibleIndex = 4
        '
        'colmodel
        '
        Me.colmodel.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmodel.AppearanceCell.Options.UseFont = True
        Me.colmodel.FieldName = "model"
        Me.colmodel.Name = "colmodel"
        Me.colmodel.OptionsColumn.AllowEdit = False
        Me.colmodel.Visible = True
        Me.colmodel.VisibleIndex = 5
        '
        'colcpu
        '
        Me.colcpu.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colcpu.AppearanceCell.Options.UseFont = True
        Me.colcpu.FieldName = "cpu"
        Me.colcpu.Name = "colcpu"
        Me.colcpu.OptionsColumn.AllowEdit = False
        '
        'colmainboard
        '
        Me.colmainboard.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmainboard.AppearanceCell.Options.UseFont = True
        Me.colmainboard.FieldName = "mainboard"
        Me.colmainboard.Name = "colmainboard"
        Me.colmainboard.OptionsColumn.AllowEdit = False
        '
        'colram
        '
        Me.colram.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colram.AppearanceCell.Options.UseFont = True
        Me.colram.FieldName = "ram"
        Me.colram.Name = "colram"
        Me.colram.OptionsColumn.AllowEdit = False
        '
        'colvgacard
        '
        Me.colvgacard.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colvgacard.AppearanceCell.Options.UseFont = True
        Me.colvgacard.FieldName = "vgacard"
        Me.colvgacard.Name = "colvgacard"
        Me.colvgacard.OptionsColumn.AllowEdit = False
        '
        'colharddisk
        '
        Me.colharddisk.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colharddisk.AppearanceCell.Options.UseFont = True
        Me.colharddisk.FieldName = "harddisk"
        Me.colharddisk.Name = "colharddisk"
        Me.colharddisk.OptionsColumn.AllowEdit = False
        '
        'colstateid
        '
        Me.colstateid.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colstateid.AppearanceCell.Options.UseFont = True
        Me.colstateid.FieldName = "stateid"
        Me.colstateid.Name = "colstateid"
        Me.colstateid.OptionsColumn.AllowEdit = False
        Me.colstateid.OptionsColumn.AllowShowHide = False
        '
        'colstatename
        '
        Me.colstatename.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colstatename.AppearanceCell.Options.UseFont = True
        Me.colstatename.FieldName = "statename"
        Me.colstatename.Name = "colstatename"
        Me.colstatename.OptionsColumn.AllowEdit = False
        Me.colstatename.Visible = True
        Me.colstatename.VisibleIndex = 6
        '
        'colmonitorsize
        '
        Me.colmonitorsize.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmonitorsize.AppearanceCell.Options.UseFont = True
        Me.colmonitorsize.FieldName = "monitorsize"
        Me.colmonitorsize.Name = "colmonitorsize"
        Me.colmonitorsize.OptionsColumn.AllowEdit = False
        '
        'colipnumber
        '
        Me.colipnumber.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colipnumber.AppearanceCell.Options.UseFont = True
        Me.colipnumber.FieldName = "ipnumber"
        Me.colipnumber.Name = "colipnumber"
        Me.colipnumber.OptionsColumn.AllowEdit = False
        Me.colipnumber.Visible = True
        Me.colipnumber.VisibleIndex = 7
        '
        'colpowersupply
        '
        Me.colpowersupply.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpowersupply.AppearanceCell.Options.UseFont = True
        Me.colpowersupply.FieldName = "powersupply"
        Me.colpowersupply.Name = "colpowersupply"
        Me.colpowersupply.OptionsColumn.AllowEdit = False
        '
        'colcd
        '
        Me.colcd.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colcd.AppearanceCell.Options.UseFont = True
        Me.colcd.FieldName = "cd"
        Me.colcd.Name = "colcd"
        Me.colcd.OptionsColumn.AllowEdit = False
        '
        'colcase
        '
        Me.colcase.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colcase.AppearanceCell.Options.UseFont = True
        Me.colcase.FieldName = "case"
        Me.colcase.Name = "colcase"
        Me.colcase.OptionsColumn.AllowEdit = False
        '
        'colserialnumber
        '
        Me.colserialnumber.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colserialnumber.AppearanceCell.Options.UseFont = True
        Me.colserialnumber.FieldName = "serialnumber"
        Me.colserialnumber.Name = "colserialnumber"
        Me.colserialnumber.OptionsColumn.AllowEdit = False
        '
        'colspecification
        '
        Me.colspecification.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colspecification.AppearanceCell.Options.UseFont = True
        Me.colspecification.FieldName = "specification"
        Me.colspecification.Name = "colspecification"
        Me.colspecification.OptionsColumn.AllowEdit = False
        Me.colspecification.Visible = True
        Me.colspecification.VisibleIndex = 8
        '
        'colnote
        '
        Me.colnote.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colnote.AppearanceCell.Options.UseFont = True
        Me.colnote.FieldName = "note"
        Me.colnote.Name = "colnote"
        Me.colnote.OptionsColumn.AllowEdit = False
        Me.colnote.Visible = True
        Me.colnote.VisibleIndex = 9
        '
        'colcomname
        '
        Me.colcomname.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colcomname.AppearanceCell.Options.UseFont = True
        Me.colcomname.FieldName = "comname"
        Me.colcomname.Name = "colcomname"
        Me.colcomname.OptionsColumn.AllowEdit = False
        Me.colcomname.Visible = True
        Me.colcomname.VisibleIndex = 10
        '
        'colwindows
        '
        Me.colwindows.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colwindows.AppearanceCell.Options.UseFont = True
        Me.colwindows.FieldName = "windows"
        Me.colwindows.Name = "colwindows"
        Me.colwindows.OptionsColumn.AllowEdit = False
        '
        'coloffice
        '
        Me.coloffice.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coloffice.AppearanceCell.Options.UseFont = True
        Me.coloffice.FieldName = "office"
        Me.coloffice.Name = "coloffice"
        Me.coloffice.OptionsColumn.AllowEdit = False
        '
        'colhims
        '
        Me.colhims.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colhims.AppearanceCell.Options.UseFont = True
        Me.colhims.FieldName = "hims"
        Me.colhims.Name = "colhims"
        Me.colhims.OptionsColumn.AllowEdit = False
        Me.colhims.Visible = True
        Me.colhims.VisibleIndex = 11
        '
        'collabs
        '
        Me.collabs.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.collabs.AppearanceCell.Options.UseFont = True
        Me.collabs.FieldName = "labs"
        Me.collabs.Name = "collabs"
        Me.collabs.OptionsColumn.AllowEdit = False
        Me.collabs.Visible = True
        Me.collabs.VisibleIndex = 12
        '
        'colpacs
        '
        Me.colpacs.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpacs.AppearanceCell.Options.UseFont = True
        Me.colpacs.FieldName = "pacs"
        Me.colpacs.Name = "colpacs"
        Me.colpacs.OptionsColumn.AllowEdit = False
        Me.colpacs.Visible = True
        Me.colpacs.VisibleIndex = 13
        '
        'colsystem
        '
        Me.colsystem.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colsystem.AppearanceCell.Options.UseFont = True
        Me.colsystem.FieldName = "system"
        Me.colsystem.Name = "colsystem"
        Me.colsystem.OptionsColumn.AllowEdit = False
        Me.colsystem.Visible = True
        Me.colsystem.VisibleIndex = 14
        '
        'coladmin
        '
        Me.coladmin.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coladmin.AppearanceCell.Options.UseFont = True
        Me.coladmin.FieldName = "admin"
        Me.coladmin.Name = "coladmin"
        Me.coladmin.OptionsColumn.AllowEdit = False
        Me.coladmin.Visible = True
        Me.coladmin.VisibleIndex = 15
        '
        'coldeepfreeze
        '
        Me.coldeepfreeze.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldeepfreeze.AppearanceCell.Options.UseFont = True
        Me.coldeepfreeze.FieldName = "deepfreeze"
        Me.coldeepfreeze.Name = "coldeepfreeze"
        Me.coldeepfreeze.OptionsColumn.AllowEdit = False
        Me.coldeepfreeze.Visible = True
        Me.coldeepfreeze.VisibleIndex = 16
        '
        'colwindows_application
        '
        Me.colwindows_application.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colwindows_application.AppearanceCell.Options.UseFont = True
        Me.colwindows_application.FieldName = "windows_application"
        Me.colwindows_application.Name = "colwindows_application"
        Me.colwindows_application.OptionsColumn.AllowEdit = False
        '
        'colprice
        '
        Me.colprice.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colprice.AppearanceCell.Options.UseFont = True
        Me.colprice.FieldName = "price"
        Me.colprice.Name = "colprice"
        Me.colprice.OptionsColumn.AllowEdit = False
        '
        'coldate_purchase
        '
        Me.coldate_purchase.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldate_purchase.AppearanceCell.Options.UseFont = True
        Me.coldate_purchase.FieldName = "date_purchase"
        Me.coldate_purchase.Name = "coldate_purchase"
        Me.coldate_purchase.OptionsColumn.AllowEdit = False
        '
        'colwarranty
        '
        Me.colwarranty.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colwarranty.AppearanceCell.Options.UseFont = True
        Me.colwarranty.FieldName = "warranty"
        Me.colwarranty.Name = "colwarranty"
        Me.colwarranty.OptionsColumn.AllowEdit = False
        '
        'cold_add
        '
        Me.cold_add.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.cold_add.AppearanceCell.Options.UseFont = True
        Me.cold_add.FieldName = "d_add"
        Me.cold_add.Name = "cold_add"
        Me.cold_add.OptionsColumn.AllowEdit = False
        '
        'cold_update
        '
        Me.cold_update.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.cold_update.AppearanceCell.Options.UseFont = True
        Me.cold_update.FieldName = "d_update"
        Me.cold_update.Name = "cold_update"
        Me.cold_update.OptionsColumn.AllowEdit = False
        '
        'colusadd
        '
        Me.colusadd.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colusadd.AppearanceCell.Options.UseFont = True
        Me.colusadd.FieldName = "usadd"
        Me.colusadd.Name = "colusadd"
        Me.colusadd.OptionsColumn.AllowEdit = False
        Me.colusadd.OptionsColumn.AllowShowHide = False
        '
        'colusedit
        '
        Me.colusedit.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colusedit.AppearanceCell.Options.UseFont = True
        Me.colusedit.FieldName = "usedit"
        Me.colusedit.Name = "colusedit"
        Me.colusedit.OptionsColumn.AllowEdit = False
        Me.colusedit.OptionsColumn.AllowShowHide = False
        '
        'colidlocation
        '
        Me.colidlocation.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colidlocation.AppearanceCell.Options.UseFont = True
        Me.colidlocation.FieldName = "idlocation"
        Me.colidlocation.Name = "colidlocation"
        Me.colidlocation.OptionsColumn.AllowEdit = False
        Me.colidlocation.OptionsColumn.AllowShowHide = False
        '
        'collocadescription_name
        '
        Me.collocadescription_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.collocadescription_name.AppearanceCell.Options.UseFont = True
        Me.collocadescription_name.FieldName = "locadescription_name"
        Me.collocadescription_name.Name = "collocadescription_name"
        Me.collocadescription_name.OptionsColumn.AllowEdit = False
        '
        'collocation_name
        '
        Me.collocation_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.collocation_name.AppearanceCell.Options.UseFont = True
        Me.collocation_name.FieldName = "location_name"
        Me.collocation_name.Name = "collocation_name"
        Me.collocation_name.OptionsColumn.AllowEdit = False
        '
        'colsellerid
        '
        Me.colsellerid.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colsellerid.AppearanceCell.Options.UseFont = True
        Me.colsellerid.FieldName = "sellerid"
        Me.colsellerid.Name = "colsellerid"
        Me.colsellerid.OptionsColumn.AllowEdit = False
        Me.colsellerid.OptionsColumn.AllowShowHide = False
        '
        'colsellername
        '
        Me.colsellername.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colsellername.AppearanceCell.Options.UseFont = True
        Me.colsellername.FieldName = "sellername"
        Me.colsellername.Name = "colsellername"
        Me.colsellername.OptionsColumn.AllowEdit = False
        '
        'coldevpic
        '
        Me.coldevpic.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldevpic.AppearanceCell.Options.UseFont = True
        Me.coldevpic.FieldName = "devpic"
        Me.coldevpic.Name = "coldevpic"
        Me.coldevpic.OptionsColumn.AllowEdit = False
        '
        'colusaddName
        '
        Me.colusaddName.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colusaddName.AppearanceCell.Options.UseFont = True
        Me.colusaddName.FieldName = "usaddName"
        Me.colusaddName.Name = "colusaddName"
        Me.colusaddName.OptionsColumn.AllowEdit = False
        '
        'coluseditName
        '
        Me.coluseditName.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coluseditName.AppearanceCell.Options.UseFont = True
        Me.coluseditName.FieldName = "useditName"
        Me.coluseditName.Name = "coluseditName"
        Me.coluseditName.OptionsColumn.AllowEdit = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutControlGroup10, Me.SplitterItem1, Me.LayoutControlItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'layoutControlGroup10
        '
        Me.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10"
        Me.layoutControlGroup10.ExpandButtonVisible = True
        Me.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText
        Me.layoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.SimpleSeparator1, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem13, Me.SimpleSeparator2, Me.LayoutControlItem15, Me.LayoutControlItem2, Me.LayoutControlItem16, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem14, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.LayoutControlItem32, Me.LayoutControlItem34, Me.LayoutControlItem35, Me.LayoutControlItem36, Me.LayoutControlItem33, Me.LayoutControlItem38, Me.EmptySpaceItem2, Me.LayoutControlItem37, Me.LayoutControlItem39, Me.LayoutControlItem41, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.LayoutControlItem12, Me.LayoutControlItem17, Me.LayoutControlItem3})
        Me.layoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.layoutControlGroup10.Name = "layoutControlGroup10"
        Me.layoutControlGroup10.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.layoutControlGroup10.Size = New System.Drawing.Size(635, 796)
        Me.layoutControlGroup10.Text = "เพิ่ม/แก้ไข อุปกรณ์"
        Me.layoutControlGroup10.TextLocation = DevExpress.Utils.Locations.Left
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(288, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(22, 506)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.txtCpu
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 132)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(288, 24)
        Me.LayoutControlItem4.Text = "CPU"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem5.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem5.Control = Me.txtModel
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 106)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem5.Text = "รุ่น"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem6.Control = Me.txtMainboard
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 156)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem6.Text = "Mainboard"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem7.Control = Me.txtRam
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 182)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem7.Text = "RAM"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(93, 16)
        '
        'SimpleSeparator1
        '
        Me.SimpleSeparator1.AllowHotTrack = False
        Me.SimpleSeparator1.Location = New System.Drawing.Point(0, 78)
        Me.SimpleSeparator1.Name = "SimpleSeparator1"
        Me.SimpleSeparator1.Size = New System.Drawing.Size(288, 2)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem8.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem8.Control = Me.txtVga
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 208)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem8.Text = "VGA"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.txtHdd
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 260)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem9.Text = "Harddisk"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem13.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem13.Control = Me.dpPurchase
        Me.LayoutControlItem13.Location = New System.Drawing.Point(310, 402)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem13.Text = "วันที่ซื้อ"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(93, 16)
        '
        'SimpleSeparator2
        '
        Me.SimpleSeparator2.AllowHotTrack = False
        Me.SimpleSeparator2.Location = New System.Drawing.Point(310, 400)
        Me.SimpleSeparator2.Name = "SimpleSeparator2"
        Me.SimpleSeparator2.Size = New System.Drawing.Size(297, 2)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.txtIpaddress
        Me.LayoutControlItem15.Location = New System.Drawing.Point(310, 52)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem15.Text = "IP Address"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.txtIdDataDevice
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem2.Text = "ID"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem16.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem16.Control = Me.sleIdbrand
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 80)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem16.Text = "ยี่ห้อ"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem10.Control = Me.slePrttype
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 286)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem10.Text = "Printer Type"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem11.Control = Me.txtMonitorsize
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 312)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem11.Text = "ขนาดจอ"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.Control = Me.txtSpec
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 442)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(288, 64)
        Me.LayoutControlItem14.Text = "Specification"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem18.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem18.Control = Me.txtCd
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 338)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem18.Text = "CD/DVD"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem19.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem19.Control = Me.txtCase
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 364)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem19.Text = "Case"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem20.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem20.Control = Me.txtSerialnumber
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 390)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem20.Text = "Serial Number"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem21.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem21.Control = Me.txtComname
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem21.Text = "Computer Name"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem22.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem22.Control = Me.txtWindows
        Me.LayoutControlItem22.Location = New System.Drawing.Point(310, 0)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem22.Text = "Windows"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem23.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem23.Control = Me.txtOffice
        Me.LayoutControlItem23.Location = New System.Drawing.Point(310, 26)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem23.Text = "Office"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.ckbHims
        Me.LayoutControlItem25.Location = New System.Drawing.Point(310, 78)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(93, 24)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.ckbPacs
        Me.LayoutControlItem26.Location = New System.Drawing.Point(310, 102)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(93, 24)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.ckbAdmin
        Me.LayoutControlItem27.Location = New System.Drawing.Point(310, 126)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(93, 24)
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextVisible = False
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.ckbLab
        Me.LayoutControlItem28.Location = New System.Drawing.Point(403, 78)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(204, 24)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.ckbSystem
        Me.LayoutControlItem29.Location = New System.Drawing.Point(403, 102)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(204, 24)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = False
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.ckbDeepfreeze
        Me.LayoutControlItem30.Location = New System.Drawing.Point(403, 126)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(204, 24)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem32.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem32.Control = Me.txtWinapp
        Me.LayoutControlItem32.Location = New System.Drawing.Point(310, 150)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(297, 105)
        Me.LayoutControlItem32.Text = "โปรแกรม"
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem34.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem34.Control = Me.txtPrice
        Me.LayoutControlItem34.Location = New System.Drawing.Point(310, 428)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem34.Text = "ราคา"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem35.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem35.Control = Me.sleLocation
        Me.LayoutControlItem35.Location = New System.Drawing.Point(310, 374)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem35.Text = "สถานที่เก็บ"
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem36.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem36.Control = Me.sleIdtype
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem36.Text = "ประเภทอุปกรณ์"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem33.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem33.Control = Me.txtPowersupply
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 234)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem33.Text = "Power Supply"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem38.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem38.Control = Me.txtNote
        Me.LayoutControlItem38.Location = New System.Drawing.Point(310, 255)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(297, 93)
        Me.LayoutControlItem38.Text = "Note"
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 722)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(607, 10)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.btnAdd
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 732)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(303, 27)
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = False
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.btnEdit
        Me.LayoutControlItem39.Location = New System.Drawing.Point(303, 732)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(304, 27)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.btnRefresh
        Me.LayoutControlItem41.Location = New System.Drawing.Point(0, 759)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(607, 27)
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem41.TextVisible = False
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem42.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem42.Control = Me.peDevpic
        Me.LayoutControlItem42.Location = New System.Drawing.Point(0, 506)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(607, 216)
        Me.LayoutControlItem42.Text = "ภาพอุปกรณ์"
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem43.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem43.Control = Me.sleState
        Me.LayoutControlItem43.Location = New System.Drawing.Point(310, 348)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem43.Text = "สถานะ"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem12.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem12.Control = Me.dpWarrantyExp
        Me.LayoutControlItem12.Location = New System.Drawing.Point(310, 454)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem12.Text = "วันหมดประกัน"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem17.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem17.Control = Me.sleSeller
        Me.LayoutControlItem17.Location = New System.Drawing.Point(310, 480)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(297, 26)
        Me.LayoutControlItem17.Text = "ผู้ขาย"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.sleDevcatid
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem3.Text = "หมวดอุปกรณ์"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(93, 16)
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(635, 0)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(5, 796)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.gcDevice
        Me.LayoutControlItem1.Location = New System.Drawing.Point(640, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(636, 796)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'device
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "device"
        Me.Size = New System.Drawing.Size(1280, 800)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.sleDevcatid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sleSeller.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpWarrantyExp.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpWarrantyExp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sleState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit6View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peDevpic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNote.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPowersupply.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sleIdtype.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit5View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sleLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MaslocationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsMasLocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit4View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWinapp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckbDeepfreeze.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckbSystem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckbLab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckbAdmin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckbPacs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckbHims.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWindows.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtComname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerialnumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSpec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonitorsize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slePrttype.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sleIdbrand.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIpaddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpPurchase.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpPurchase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMainboard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtModel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIdDataDevice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsDevice1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SimpleSeparator1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SimpleSeparator2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtHdd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtVga As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtRam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMainboard As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtModel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCpu As TextBox
    Friend WithEvents txtIdDataDevice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcDevice As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents layoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleSeparator1 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtSpec As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtMonitorsize As DevExpress.XtraEditors.TextEdit
    Friend WithEvents slePrttype As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents sleIdbrand As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtIpaddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dpPurchase As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleSeparator2 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents sleIdtype As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit5View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents sleLocation As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit4View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtWinapp As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents ckbDeepfreeze As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckbSystem As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckbLab As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckbAdmin As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckbPacs As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckbHims As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtOffice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtWindows As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtComname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSerialnumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCase As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtNote As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtPowersupply As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DtsDevice1 As dtsDevice
    Friend WithEvents coliddata_device As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldevcatid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldevcatname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidtype As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltype_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidbrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbrand_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprttypeid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprttypename As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmodel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcpu As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmainboard As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colram As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colvgacard As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colharddisk As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstateid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstatename As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmonitorsize As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colipnumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpowersupply As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcase As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colserialnumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colspecification As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colnote As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcomname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colwindows As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coloffice As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colhims As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collabs As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpacs As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsystem As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coladmin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldeepfreeze As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colwindows_application As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprice As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldate_purchase As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colwarranty As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cold_add As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cold_update As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colusadd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colusedit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents peDevpic As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents btnRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents sleState As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit6View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents dpWarrantyExp As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents sleSeller As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colidlocation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collocadescription_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collocation_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsellerid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsellername As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldevpic As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colusaddName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coluseditName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sleDevcatid As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MaslocationBindingSource As BindingSource
    Friend WithEvents DtsMasLocation As dtsMasLocation
    Friend WithEvents colidlocation1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collocation_name1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidFloor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colfloor_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidsection As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsection_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collocadescription_name1 As DevExpress.XtraGrid.Columns.GridColumn
End Class
