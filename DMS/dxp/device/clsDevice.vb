﻿Imports System.Globalization

Public Class clsDevice
    Public db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsDevice


    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsDevice)
        dts = outerDts
    End Sub

#Region "var"
    Protected _iddata_device
    Protected _devcatid
    Protected _idtype
    Protected _idbrand
    Protected _model
    Protected _cpu
    Protected _mainboard
    Protected _ram
    Protected _vgacard
    Protected _harddisk
    Protected _prttypeid
    Protected _stateid
    Protected _monitorsize
    Protected _ipnumber
    Protected _powersupply
    Protected _cd
    Protected _case
    Protected _serialnumber
    Protected _specification
    Protected _note
    Protected _comname
    Protected _windows
    Protected _office
    Protected _hims As Integer
    Protected _lab As Integer
    Protected _pacs As Integer
    Protected _system As Integer
    Protected _admin As Integer
    Protected _deepfreeze As Integer
    Protected _windows_application
    Protected _price
    Protected _pass_connect
    Protected _pass_config
    Protected _date_purchase As DevExpress.XtraEditors.DateEdit
    Protected _warranty
    Protected _warrantyexpire As DevExpress.XtraEditors.DateEdit
    Protected _checkedfull
    Protected _usadd
    Protected _usedit
    Protected _devpic As DevExpress.XtraEditors.PictureEdit
    Protected _sellerid
    Protected _idlocation
#End Region

#Region "property"
    Public Property iddata_device
        Get
            Return _iddata_device
        End Get
        Set(value)
            _iddata_device = value
        End Set
    End Property

    Public Property devcatid
        Get
            Return _devcatid
        End Get
        Set(value)
            _devcatid = value
        End Set
    End Property

    Public Property idtype
        Get
            Return _idtype
        End Get
        Set(value)
            _idtype = value
        End Set
    End Property

    Public Property idbrand
        Get
            Return _idbrand
        End Get
        Set(value)
            _idbrand = value
        End Set
    End Property

    Public Property model
        Get
            Return _model
        End Get
        Set(value)
            _model = value
        End Set
    End Property

    Public Property cpu
        Get
            Return _cpu
        End Get
        Set(value)
            _cpu = value
        End Set
    End Property

    Public Property mainboard
        Get
            Return _mainboard
        End Get
        Set(value)
            _mainboard = value
        End Set
    End Property

    Public Property ram
        Get
            Return _ram
        End Get
        Set(value)
            _ram = value
        End Set
    End Property

    Public Property vgacard
        Get
            Return _vgacard
        End Get
        Set(value)
            _vgacard = value
        End Set
    End Property

    Public Property harddisk
        Get
            Return _harddisk
        End Get
        Set(value)
            _harddisk = value
        End Set
    End Property

    Public Property prttypeid
        Get
            Return _prttypeid
        End Get
        Set(value)
            _prttypeid = value
        End Set
    End Property

    Public Property stateid
        Get
            Return _stateid
        End Get
        Set(value)
            _stateid = value
        End Set
    End Property

    Public Property monitorsize
        Get
            Return _monitorsize
        End Get
        Set(value)
            _monitorsize = value
        End Set
    End Property

    Public Property ipnumber
        Get
            Return _ipnumber
        End Get
        Set(value)
            _ipnumber = value
        End Set
    End Property

    Public Property powersupply
        Get
            Return _powersupply
        End Get
        Set(value)
            _powersupply = value
        End Set
    End Property

    Public Property cd
        Get
            Return _cd
        End Get
        Set(value)
            _cd = value
        End Set
    End Property

    Public Property cases
        Get
            Return _case
        End Get
        Set(value)
            _case = value
        End Set
    End Property

    Public Property serialnumber
        Get
            Return _serialnumber
        End Get
        Set(value)
            _serialnumber = value
        End Set
    End Property

    Public Property specification
        Get
            Return _specification
        End Get
        Set(value)
            _specification = value
        End Set
    End Property

    Public Property note
        Get
            Return _note
        End Get
        Set(value)
            _note = value
        End Set
    End Property

    Public Property comname
        Get
            Return _comname
        End Get
        Set(value)
            _comname = value
        End Set
    End Property

    Public Property windows
        Get
            Return _windows
        End Get
        Set(value)
            _windows = value
        End Set
    End Property

    Public Property office
        Get
            Return _office
        End Get
        Set(value)
            _office = value
        End Set
    End Property

    Public Property hims As Boolean
        Get
            Return _hims
        End Get
        Set(value As Boolean)
            If value = True Then
                _hims = 1
            Else
                _hims = 0
            End If
        End Set
    End Property

    Public Property lab As Boolean
        Get
            Return _lab
        End Get
        Set(value As Boolean)
            If value = True Then
                _lab = 1
            Else
                _lab = 0
            End If
        End Set
    End Property

    Public Property pacs As Boolean
        Get
            Return _pacs
        End Get
        Set(value As Boolean)
            If value = True Then
                _pacs = 1
            Else
                _pacs = 0
            End If
        End Set
    End Property

    Public Property system As Boolean
        Get
            Return _system
        End Get
        Set(value As Boolean)
            If value = True Then
                _system = 1
            Else
                _system = 0
            End If
        End Set
    End Property

    Public Property admin As Boolean
        Get
            Return _admin
        End Get
        Set(value As Boolean)
            If value = True Then
                _admin = 1
            Else
                _admin = 0
            End If
        End Set
    End Property

    Public Property deepfreeze As Boolean
        Get
            Return _deepfreeze
        End Get
        Set(value As Boolean)
            If value = True Then
                _deepfreeze = 1
            Else
                _deepfreeze = 0
            End If
        End Set
    End Property

    Public Property windows_application
        Get
            Return _windows_application
        End Get
        Set(value)
            _windows_application = value
        End Set
    End Property

    Public Property price
        Get
            Return _price
        End Get
        Set(value)
            _price = value
        End Set
    End Property

    Public Property pass_connect
        Get
            Return _pass_connect
        End Get
        Set(value)
            _pass_connect = value
        End Set
    End Property

    Public Property pass_config
        Get
            Return _pass_config
        End Get
        Set(value)
            _pass_config = value
        End Set
    End Property

    Public Property date_purchase
        Get
            Return _date_purchase
        End Get
        Set(value)
            _date_purchase = value
        End Set
    End Property

    Public Property warranty
        Get
            Return _warranty
        End Get
        Set(value)
            _warranty = value
        End Set
    End Property

    Public Property warrantyexpire As DevExpress.XtraEditors.DateEdit
        Get
            Return _warrantyexpire
        End Get
        Set(value As DevExpress.XtraEditors.DateEdit)
            _warrantyexpire = value
        End Set
    End Property

    Public Property checkedfull
        Get
            Return _checkedfull
        End Get
        Set(value)
            _checkedfull = value
        End Set
    End Property

    Public Property usadd
        Get
            Return _usadd
        End Get
        Set(value)
            _usadd = value
        End Set
    End Property

    Public Property usedit
        Get
            Return _usedit
        End Get
        Set(value)
            _usedit = value
        End Set
    End Property

    Public Property devpic
        Get
            Return _devpic
        End Get
        Set(value)
            _devpic = value
        End Set
    End Property

    Public Property sellerid
        Get
            Return _sellerid
        End Get
        Set(value)
            _sellerid = value
        End Set
    End Property

    Public Property idlocation
        Get
            Return _idlocation
        End Get
        Set(value)
            _idlocation = value
        End Set
    End Property
#End Region

    Public Sub selectDevice()
        dts.Tables("device").Clear()

        Dim sql As String = "SELECT data_device.*, masdevcat.`devcatname`, mastype.`type_name`, masbrand.`brand_name`, masprintertype.`prttypename`, masstate.`statename`, CONCAT_WS(' ', member.name, member.`lname`) AS 'usaddName', CONCAT_WS(' ', member2.name, member2.`lname`) AS 'useditName', masseller.`sellername` FROM data_device LEFT JOIN masdevcat ON masdevcat.`devcatid` = data_device.`devcatid` LEFT JOIN mastype ON mastype.`idtype` = data_device.`idtype` LEFT JOIN masbrand ON masbrand.`idbrand` = data_device.`idbrand` LEFT JOIN masprintertype ON masprintertype.prttypeid = data_device.`prttypeid` LEFT JOIN masstate ON masstate.`stateid` = data_device.`stateid` LEFT JOIN member ON member.`idmember` = data_device.`usadd` LEFT JOIN member AS member2 ON member2.`idmember` = data_device.`usedit` LEFT JOIN masseller ON masseller.`sellerid` = data_device.`sellerid` LEFT JOIN maslocation ON maslocation.`idlocation` = data_device.`idlocation`;"
        db.GetTable(sql, dts.Tables("device"))

        db.Dispose()
    End Sub

End Class
