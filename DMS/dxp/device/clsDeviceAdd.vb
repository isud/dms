﻿Imports System.Globalization
Imports MySql.Data.MySqlClient
Imports System.Drawing
Imports System.IO

Public Class clsDeviceAdd : Inherits clsDevice

    Public Function addDevice() As Boolean
        Dim USCulture As New CultureInfo("en-US", True)

        Dim sql1 As String = ""
        Dim sql2 As String = ""

        Dim sql As String = ""

        sql1 += "devcatid, "
        If Convert.ToString(_devcatid) <> "" Then
            sql2 += "'" & _devcatid & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "idtype, "
        If Convert.ToString(_idtype) <> "" Then
            sql2 += "'" & _idtype & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "idbrand, "
        If Convert.ToString(_idbrand) <> "" Then
            sql2 += "'" & _idbrand & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "model, "
        If Convert.ToString(_model) <> "" Then
            sql2 += "'" & _model & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "cpu, "
        If Convert.ToString(_cpu) <> "" Then
            sql2 += "'" & _cpu & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "mainboard, "
        If Convert.ToString(_mainboard) <> "" Then
            sql2 += "'" & _mainboard & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "ram, "
        If Convert.ToString(_ram) <> "" Then
            sql2 += "'" & _ram & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "vgacard, "
        If Convert.ToString(_vgacard) <> "" Then
            sql2 += "'" & _vgacard & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "harddisk, "
        If Convert.ToString(_harddisk) <> "" Then
            sql2 += "'" & _harddisk & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "prttypeid, "
        If Convert.ToString(_prttypeid) <> "" Then
            sql2 += "'" & _prttypeid & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "stateid, "
        If Convert.ToString(_stateid) <> "" Then
            sql2 += "'" & _stateid & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "monitorsize, "
        If Convert.ToString(_monitorsize) <> "" Then
            sql2 += "'" & _monitorsize & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "ipnumber, "
        If Convert.ToString(_ipnumber) <> "" Then
            sql2 += "'" & _ipnumber & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "powersupply, "
        If Convert.ToString(_powersupply) <> "" Then
            sql2 += "'" & _powersupply & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "cd, "
        If Convert.ToString(_cd) <> "" Then
            sql2 += "'" & _cd & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "`case`, "
        If Convert.ToString(_case) <> "" Then
            sql2 += "'" & _case & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "serialnumber, "
        If Convert.ToString(_serialnumber) <> "" Then
            sql2 += "'" & _serialnumber & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "specification, "
        If Convert.ToString(_specification) <> "" Then
            sql2 += "'" & _specification & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "note, "
        If Convert.ToString(_note) <> "" Then
            sql2 += "'" & _note & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "comname, "
        If Convert.ToString(_comname) <> "" Then
            sql2 += "'" & _comname & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "windows, "
        If Convert.ToString(_windows) <> "" Then
            sql2 += "'" & _windows & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "office, "
        If Convert.ToString(_office) <> "" Then
            sql2 += "'" & _office & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "hims, "
        If Convert.ToString(_hims) <> "" Then
            sql2 += "'" & _hims & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "lab, "
        If Convert.ToString(_lab) <> "" Then
            sql2 += "'" & _lab & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "pacs, "
        If Convert.ToString(_pacs) <> "" Then
            sql2 += "'" & _pacs & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "system, "
        If Convert.ToString(_system) <> "" Then
            sql2 += "'" & _system & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "admin, "
        If Convert.ToString(_admin) <> "" Then
            sql2 += "'" & _admin & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "deepfreeze, "
        If Convert.ToString(_deepfreeze) <> "" Then
            sql2 += "'" & _deepfreeze & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "windows_application, "
        If Convert.ToString(_windows_application) <> "" Then
            sql2 += "'" & _windows_application & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "price, "
        If Convert.ToString(_price) <> "" Then
            sql2 += "'" & _price & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "date_purchase, "
        If _date_purchase.EditValue <> Nothing Then
            Dim d_purchase As Date = _date_purchase.EditValue
            sql2 += "'" & d_purchase.ToString("yyyy-MM-dd HH:mm:ss", USCulture) & "', "
        Else
            sql2 += "null, "
        End If

        'sql1 += "warranty, "
        'If Convert.ToString(_warranty) <> "" Then
        '    sql2 += "'" & _warranty & "', "
        'Else
        '    sql2 += "null, "
        'End If

        sql1 += "warrantyexpire, "
        If Convert.ToString(_warranty) <> "" Then
            Dim d_expire As Date = _warrantyexpire.EditValue
            sql2 += "'" & d_expire.ToString("yyyy-MM-dd HH:mm:ss", USCulture) & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "d_add, "
        sql2 += "NOW(), "

        sql1 += "usadd, "
        sql2 += "'1', "

        'sql1 += "devpic, "
        'If Convert.ToString(_devpic.EditValue) <> "" Then
        '    Dim pic As Image = _devpic.EditValue
        '    'sql2 += "'" & @pic & "', "
        'Else
        '    sql2 += "null, "
        'End If

        sql1 += "sellerid, "
        If Convert.ToString(_sellerid) <> "" Then
            sql2 += "'" & _sellerid & "', "
        Else
            sql2 += "null, "
        End If

        sql1 += "idlocation "
        If Convert.ToString(_idlocation) <> "" Then
            sql2 += "'" & _idlocation & "' "
        Else
            sql2 += "null "
        End If

        sql = "INSERT INTO data_device (" & sql1 & ") VALUES (" & sql2 & ");"

        Try
            Dim id As String
            id = db.ExecuteScalar(sql)

            If _devpic.EditValue IsNot Nothing Then
                'addDevicePic(id)
            End If

            MsgBox("เพิ่มอุปกรณ์เข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
            Return True
        Catch ex As Exception
            MsgBox(ex.ToString)
            Return False
        Finally
            db.Dispose()
        End Try
    End Function

    'Public Sub addDevicePic(iddata_device As String)
    '    Dim strConn As String = "server=" & My.Settings.IPDB & ";port =" & My.Settings.DBPORT & ";user id=" & My.Settings.DBUSER & ";password=" + My.Settings.DBPASSWORD + ";database=" & My.Settings.DBSCHEMA & ";Character Set =utf8 ;default command timeout=3600;"
    '    Dim Conn As New MySqlConnection(strConn)

    '    Dim SqlString As String
    '    SqlString = "UPDATE data_device SET devpic = " &
    '                "@Img WHERE iddata_device = '" & iddata_device & "'"
    '    Dim cmd As MySqlCommand = New MySqlCommand(SqlString, Conn)
    '    Dim Img As MySqlParameter = New MySqlParameter("@Img", MySqlDbType.LongBlob)




    '    'Dim FileSize As UInt32

    '    ''Conn.Close()

    '    'Dim mstream As New System.IO.MemoryStream()
    '    '_devpic.Image.Save(mstream, Imaging.ImageFormat.Jpeg)
    '    'Dim arrImage() As Byte = mstream.GetBuffer()

    '    'FileSize = mstream.Length
    '    'mstream.Close()


    '    ''Dim ms As New IO.MemoryStream()
    '    ''Dim imageIn As Image = _devpic.Image
    '    ''imageIn.Save(ms, Imaging.ImageFormat.Bmp)

    '    '''Dim x As Byte()
    '    '''Try
    '    '''    Using ms As New IO.MemoryStream()
    '    '''        _devpic.Image.Save(ms, Imaging.ImageFormat.Png)
    '    '''        x = ms.ToArray()
    '    '''    End Using
    '    '''Catch ex As Exception
    '    '''    MsgBox(ex.tostring)
    '    '''End Try

    '    '''''Create an empty stream in memory.'
    '    ''''Using stream As New IO.MemoryStream
    '    ''''    'Fill the stream with the binary data from the Image.'
    '    ''''    _devpic.Image.Save(stream, Imaging.ImageFormat.Jpeg)

    '    ''''    'Get an array of Bytes from the stream and assign to the parameter.'
    '    ''''End Using

    '    Dim fs As FileStream
    '    Dim fsImage As Byte()

    '    fs = New FileStream(_devpic.Image.Save(), FileMode.Open)
    '    fsImage = New Byte(fs.Length) {}
    '    fs.Read(fsImage, 0, fs.Length)
    '    fs.Close()


    '    Img.Value = fsImage
    '    cmd.Parameters.Add(Img)
    '    Try
    '        Conn.Open()
    '        cmd.ExecuteNonQuery()
    '        Conn.Close()
    '        MessageBox.Show("Save Image Complete", "Success")
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message.ToString(), "Error")
    '        Exit Sub
    '    End Try
    'End Sub
End Class
