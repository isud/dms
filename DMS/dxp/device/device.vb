﻿Public Class device
    Dim innerDtsDevice As New dtsDevice

    Dim objDevice As New clsDevice(innerDtsDevice)

    Private Sub getDevice()
        objDevice.selectDevice()
        gcDevice.DataSource = innerDtsDevice
    End Sub

    Private Sub masFloor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getDevice()

        getSleDevCat()
        getSleType()
        getSleBrand()
        getSlePrtType()
        getSleState()
        getSleLocation()
        getSleSeller()
    End Sub

#Region "sleData"
    Public Sub getSleDevCat()
        clsMasDevCat.getSleDevCat(sleDevcatid)
    End Sub


    Public Sub getSleType()
        clsMasType.getSleType(sleIdtype)
    End Sub

    Public Sub getSleBrand()
        clsMasBrand.getSleBrand(sleIdbrand)
    End Sub

    Public Sub getSlePrtType()
        clsMasPrtType.getSlePrtType(slePrttype)
    End Sub

    Public Sub getSleState()
        clsMasState.getSleState(sleState)
    End Sub

    Public Sub getSleLocation()
        clsMasLocation.getSleLocation(sleLocation)
    End Sub

    Public Sub getSleSeller()
        clsMasSeller.getSleSeller(sleSeller)
    End Sub

#End Region

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getDevice()
        txtIdDataDevice.Text = ""
        sleDevcatid.EditValue = Nothing
        sleIdtype.EditValue = Nothing
        sleIdbrand.EditValue = Nothing
        txtModel.Text = ""
        txtCpu.Text = ""
        txtMainboard.Text = ""
        txtRam.Text = ""
        txtVga.Text = ""
        txtPowersupply.Text = ""
        txtHdd.Text = ""
        slePrttype.EditValue = Nothing
        txtMonitorsize.Text = ""
        txtCd.Text = ""
        txtCase.Text = ""
        txtSerialnumber.Text = ""
        txtComname.Text = ""
        txtSpec.Text = ""
        txtWindows.Text = ""
        txtOffice.Text = ""
        txtIpaddress.Text = ""
        ckbHims.Checked = False
        ckbLab.Checked = False
        ckbPacs.Checked = False
        ckbSystem.Checked = False
        ckbAdmin.Checked = False
        ckbDeepfreeze.Checked = False
        txtWinapp.Text = ""
        txtNote.Text = ""
        sleState.EditValue = Nothing
        sleLocation.EditValue = Nothing
        dpPurchase.EditValue = Nothing
        txtPrice.Text = ""
        dpWarrantyExp.EditValue = Nothing
        sleSeller.EditValue = Nothing
        'peDevpic.EditValue = Nothing
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtIdDataDevice.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("iddata_device").ToString()

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("devcatid") IsNot DBNull.Value Then
            sleDevcatid.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("devcatid")
        Else
            sleDevcatid.EditValue = Nothing
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("idtype") IsNot DBNull.Value Then
            sleIdtype.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idtype")
        Else
            sleIdtype.EditValue = Nothing
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("idbrand") IsNot DBNull.Value Then
            sleIdtype.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idbrand")
        Else
            sleIdbrand.EditValue = Nothing
        End If

        txtModel.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("model").ToString()
        txtCpu.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("cpu").ToString()
        txtMainboard.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("mainboard").ToString()
        txtRam.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("ram").ToString()
        txtVga.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("vgacard").ToString()
        txtPowersupply.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("powersupply").ToString()
        txtHdd.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("harddisk").ToString()

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("prttypeid") IsNot DBNull.Value Then
            slePrttype.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("prttypeid")
        Else
            slePrttype.EditValue = Nothing
        End If

        txtMonitorsize.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("monitorsize").ToString()
        txtCd.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("cd").ToString()
        txtCase.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("case").ToString()
        txtSerialnumber.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("serialnumber").ToString()
        txtComname.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("comname").ToString()
        txtSpec.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("specification").ToString()
        txtWindows.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("windows").ToString()
        txtOffice.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("office").ToString()
        txtIpaddress.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("ipnumber").ToString()

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("hims") IsNot DBNull.Value Then
            ckbHims.Checked = GridView1.GetDataRow(GridView1.FocusedRowHandle)("hims")
        Else
            ckbHims.Checked = False
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("labs") IsNot DBNull.Value Then
            ckbLab.Checked = GridView1.GetDataRow(GridView1.FocusedRowHandle)("labs")
        Else
            ckbLab.Checked = False
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("pacs") IsNot DBNull.Value Then
            ckbPacs.Checked = GridView1.GetDataRow(GridView1.FocusedRowHandle)("pacs")
        Else
            ckbPacs.Checked = False
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("system") IsNot DBNull.Value Then
            ckbSystem.Checked = GridView1.GetDataRow(GridView1.FocusedRowHandle)("system")
        Else
            ckbSystem.Checked = False
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("admin") IsNot DBNull.Value Then
            ckbAdmin.Checked = GridView1.GetDataRow(GridView1.FocusedRowHandle)("admin")
        Else
            ckbAdmin.Checked = False
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("deepfreeze") IsNot DBNull.Value Then
            ckbDeepfreeze.Checked = GridView1.GetDataRow(GridView1.FocusedRowHandle)("deepfreeze")
        Else
            ckbDeepfreeze.Checked = False
        End If

        txtWinapp.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("windows_application").ToString()
        txtNote.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("note").ToString()

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("stateid") IsNot DBNull.Value Then
            sleState.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("stateid")
        Else
            sleState.EditValue = Nothing
        End If

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("idlocation") IsNot DBNull.Value Then
            sleLocation.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idlocation")
        Else
            sleLocation.EditValue = Nothing
        End If


        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("date_purchase") IsNot DBNull.Value Then
            dpPurchase.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("date_purchase")
        Else
            dpPurchase.EditValue = Nothing
        End If


        txtPrice.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("price").ToString()
        dpWarrantyExp.EditValue = Nothing
        sleSeller.EditValue = Nothing
        peDevpic.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("devpic")
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageDeviceData As New clsDeviceAdd

        objManageDeviceData.iddata_device = txtIdDataDevice
        objManageDeviceData.devcatid = sleDevcatid.EditValue
        objManageDeviceData.idtype = sleIdtype.EditValue
        objManageDeviceData.idbrand = sleIdbrand.EditValue
        objManageDeviceData.model = txtModel.Text.Trim
        objManageDeviceData.cpu = txtCpu.Text.Trim
        objManageDeviceData.mainboard = txtMainboard.Text.Trim
        objManageDeviceData.ram = txtRam.Text.Trim
        objManageDeviceData.vgacard = txtVga.Text.Trim
        objManageDeviceData.harddisk = txtHdd.Text.Trim
        objManageDeviceData.prttypeid = slePrttype.EditValue
        objManageDeviceData.stateid = sleState.EditValue
        objManageDeviceData.monitorsize = txtMonitorsize.Text.Trim
        objManageDeviceData.ipnumber = txtIpaddress.Text.Trim
        objManageDeviceData.powersupply = txtPowersupply.Text.Trim
        objManageDeviceData.cd = txtCd.Text.Trim
        objManageDeviceData.cases = txtCase.Text.Trim
        objManageDeviceData.serialnumber = txtSerialnumber.Text.Trim
        objManageDeviceData.specification = txtSpec.Text.Trim
        objManageDeviceData.note = txtNote.Text.Trim
        objManageDeviceData.comname = txtComname.Text.Trim
        objManageDeviceData.windows = txtWindows.Text.Trim
        objManageDeviceData.office = txtOffice.Text.Trim
        objManageDeviceData.hims = ckbHims.Checked
        objManageDeviceData.lab = ckbLab.Checked
        objManageDeviceData.pacs = ckbPacs.Checked
        objManageDeviceData.system = ckbSystem.Checked
        objManageDeviceData.admin = ckbAdmin.Checked
        objManageDeviceData.deepfreeze = ckbDeepfreeze.Checked
        objManageDeviceData.windows_application = txtWinapp.Text.Trim
        objManageDeviceData.price = txtPrice.Text
        objManageDeviceData.date_purchase = dpPurchase
        objManageDeviceData.warrantyexpire = dpWarrantyExp

        objManageDeviceData.usadd = ""

        objManageDeviceData.devpic = peDevpic

        objManageDeviceData.sellerid = sleSeller.EditValue
        objManageDeviceData.idlocation = sleLocation.EditValue

        If objManageDeviceData.addDevice() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageFloorData As New clsMasFloor

        objManageFloorData.idFloor = txtIdDataDevice.Tag

        If objManageFloorData.editFloorName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub


End Class
