﻿Public Class dxMain

    Private Sub btnClosePrg_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClosePrg.ItemClick
        Dim confirmClose As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการ LogOut และออกจากโปรแกรม", MsgBoxStyle.YesNo)

        If confirmClose = 6 Then
            Me.Close()
        End If
    End Sub


    Private Sub NavBarItem1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviDevice.LinkClicked
        Dim frm As New device
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviBrand_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviBrand.LinkClicked
        'disposeForm()

        Dim frm As New masBrand
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviMasType_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviMasType.LinkClicked
        Dim frm As New masType
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviFloor_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviFloor.LinkClicked
        Dim frm As New masFloor
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviSection_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviSection.LinkClicked
        Dim frm As New masSection
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviMasLocation_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviMasLocation.LinkClicked
        Dim frm As New masLocation
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub



    Private Sub nviOfficer_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviOfficer.LinkClicked
        Dim frm As New officer
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviMember_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviMember.LinkClicked
        Dim frm As New member
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub

    Private Sub nviMasDevCat_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviMasDevCat.LinkClicked
        Dim frm As New masDevCat
        frm.Dock = DockStyle.Fill
        PanelControl1.Controls.Clear()
        PanelControl1.Controls.Add(frm)
    End Sub
End Class