﻿Public Class clsMasLocation
    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMasLocation

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMasLocation)
        dts = outerDts
    End Sub

    Private _idlocation
    Private _location_name
    Private _idsection
    Private _idFloor
    Private _description_name

    Public Property idlocation
        Get
            Return _idlocation
        End Get
        Set(value)
            _idlocation = value
        End Set
    End Property

    Public Property location_name
        Get
            Return _location_name
        End Get
        Set(value)
            _location_name = value
        End Set
    End Property

    Public Property idsection
        Get
            Return _idsection
        End Get
        Set(value)
            _idsection = value
        End Set
    End Property

    Public Property idFloor
        Get
            Return _idFloor
        End Get
        Set(value)
            _idFloor = value
        End Set
    End Property

    Public Property description_name
        Get
            Return _description_name
        End Get
        Set(value)
            _description_name = value
        End Set
    End Property


    Public Sub selectLocationName()
        dts.Tables("maslocation").Clear()

        Dim sql As String = "SELECT maslocation.*, massection.`section_name`, masfloor.`floor_name` FROM maslocation LEFT JOIN massection ON massection.`idSECTION` = maslocation.`idsection` LEFT JOIN masfloor ON masfloor.`idFloor` = massection.`idFloor` ;"
        db.GetTable(sql, dts.Tables("maslocation"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleLocation(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasLocation

        Dim sql As String
        sql = "SELECT maslocation.*, masfloor.`floor_name`, massection.`section_name` FROM maslocation LEFT JOIN masfloor ON masfloor.`idFloor` = maslocation.`idFloor` LEFT JOIN massection ON massection.`idSECTION` = maslocation.`idsection`;"

        db.GetTable(sql, dts.Tables("maslocation"))

        sle.Properties.DataSource = dts.Tables("maslocation")
        sle.Properties.DisplayMember = "location_name"
        sle.Properties.ValueMember = "idlocation"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addLocationName() As Boolean
        If Convert.ToString(_location_name) = "" Then
            MsgBox("โปรดระบุชื่อสถานที่", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idFloor) = "" Then
                MsgBox("โปรดระบุ floor", MsgBoxStyle.Exclamation)
                Return False
            Else
                If Convert.ToString(_idsection) = "" Then
                    MsgBox("โปรดระบุแผนก", MsgBoxStyle.Exclamation)
                    Return False
                Else
                    Dim sqlCheckDup As String = "SELECT idlocation FROM maslocation WHERE location_name = '" & _location_name & "'"
                    Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                    If dtCheckDup.Rows.Count > 0 Then
                        MsgBox("มีชื่อสถานที่นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                        Return False
                    Else
                        Dim sql1 As String = ""
                        Dim sql2 As String = ""

                        Dim sql As String = ""

                        sql1 += "location_name, "
                        sql2 += "'" & _location_name & "', "

                        sql1 += "idFloor, "
                        sql2 += "'" & _idFloor & "', "

                        sql1 += "idsection, "
                        sql2 += "'" & _idsection & "', "


                        sql1 += "description_name "
                        If Convert.ToString(_description_name) = "" Then
                            sql2 += "null "
                        Else
                            sql2 += "'" & _description_name & "' "
                        End If

                        sql = "INSERT INTO maslocation (" & sql1 & ") VALUES (" & sql2 & ");"

                        Try
                            db.ExecuteNonQuery(sql)
                            MsgBox("เพิ่มชื่อสถานที่เข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                            Return True
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                            Return False
                        Finally
                            db.Dispose()
                        End Try
                    End If


                End If

            End If
        End If
    End Function


    Public Function editLocationName() As Boolean
        If Convert.ToString(_location_name) = "" Then
            MsgBox("โปรดระบุชื่อสถานที่", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idFloor) = "" Then
                MsgBox("โปรดระบุ floor", MsgBoxStyle.Exclamation)
                Return False
            Else
                If Convert.ToString(_idsection) = "" Then
                    MsgBox("โปรดระบุแผนก", MsgBoxStyle.Exclamation)
                    Return False
                Else
                    If Convert.ToString(_idlocation) = "" Then
                        MsgBox("โปรดระบุ idLocation", MsgBoxStyle.Exclamation)
                        Return False
                    Else

                        Dim sqlCheckDup As String = "SELECT idlocation FROM maslocation WHERE location_name = '" & _location_name & "' AND idlocation <> '" & _idlocation & "'"
                        Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                        If dtCheckDup.Rows.Count > 0 Then
                            MsgBox("มีชื่อสถานที่นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                            Return False
                        Else


                            Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                            If confirmEdit = 6 Then
                                Dim sql As String = ""

                                sql += "UPDATE maslocation SET "

                                sql += "location_name = '" & _location_name & "', "
                                sql += "idFloor = '" & _idFloor & "', "
                                sql += "idsection = '" & _idsection & "', "


                                If Convert.ToString(_description_name) = "" Then
                                    sql += "description_name = null "
                                Else
                                    sql += "description_name = '" & description_name & "' "
                                End If

                                sql += "WHERE idlocation = '" & _idlocation & "';"

                                Try
                                    db.ExecuteNonQuery(sql)
                                    MsgBox("แก้ไขชื่อสถานที่แล้ว", MsgBoxStyle.Exclamation)
                                    Return True
                                Catch ex As Exception
                                    MsgBox(ex.ToString)
                                    Return False
                                Finally
                                    db.Dispose()
                                End Try
                            Else
                                db.Dispose()
                                Return False
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Function
End Class
