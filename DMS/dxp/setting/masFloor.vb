﻿Public Class masFloor
    Dim innerDtsMasFloor As New dtsMasFloor

    Dim objMasFloor As New clsMasFloor(innerDtsMasFloor)

    Private Sub getMasFloor()
        objMasFloor.selectFloorName()
        gcMasFloor.DataSource = innerDtsMasFloor
    End Sub

    Private Sub masFloor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMasFloor()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMasFloor()
        txtFloorName.Text = ""
        txtFloorName.Tag = ""
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtFloorName.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("floor_name").ToString()
        txtFloorName.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idFloor").ToString()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageFloorData As New clsMasFloor

        objManageFloorData.floor_name = txtFloorName.Text.Trim

        If objManageFloorData.addFloorName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageFloorData As New clsMasFloor

        objManageFloorData.idFloor = txtFloorName.Tag
        objManageFloorData.floor_name = txtFloorName.Text.Trim

        If objManageFloorData.editFloorName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
