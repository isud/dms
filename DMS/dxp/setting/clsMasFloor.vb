﻿Public Class clsMasFloor

    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMasFloor

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMasFloor)
        dts = outerDts
    End Sub

    Private _idFloor
    Private _floor_name

    Public Property idFloor
        Get
            Return _idFloor
        End Get
        Set(value)
            _idFloor = value
        End Set
    End Property

    Public Property floor_name
        Get
            Return _floor_name
        End Get
        Set(value)
            _floor_name = value
        End Set
    End Property


    Public Sub selectFloorName()
        dts.Tables("masfloor").Clear()

        Dim sql As String = "SELECT * FROM masfloor;"
        db.GetTable(sql, dts.Tables("masfloor"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleFloor(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasFloor

        Dim sql As String
        sql = "SELECT idFloor, floor_name FROM masfloor;"

        db.GetTable(sql, dts.Tables("masfloor"))

        sle.Properties.DataSource = dts.Tables("masfloor")
        sle.Properties.DisplayMember = "floor_name"
        sle.Properties.ValueMember = "idFloor"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addFloorName() As Boolean
        If Convert.ToString(_floor_name) = "" Then
            MsgBox("โปรดระบุชื่อ floor", MsgBoxStyle.Exclamation)
            Return False
        Else
            Dim sqlCheckDup As String = "SELECT idFloor FROM masfloor WHERE floor_name = '" & _floor_name & "'"
            Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

            If dtCheckDup.Rows.Count > 0 Then
                MsgBox("มี floor นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                Return False
            Else
                Dim sql1 As String = ""
                Dim sql2 As String = ""

                Dim sql As String = ""

                sql1 += "floor_name "
                sql2 += "'" & _floor_name & "' "

                sql = "INSERT INTO masfloor (" & sql1 & ") VALUES (" & sql2 & ");"

                Try
                    db.ExecuteNonQuery(sql)
                    MsgBox("เพิ่มชื่อ floor เข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                    Return True
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    Return False
                Finally
                    db.Dispose()
                End Try
            End If
        End If
    End Function


    Public Function editFloorName() As Boolean
        If Convert.ToString(_floor_name) = "" Then
            MsgBox("โปรดระบุชื่อ floor", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idFloor) = "" Then
                MsgBox("โปรดระบุ idFloor", MsgBoxStyle.Exclamation)
                Return False
            Else

                Dim sqlCheckDup As String = "SELECT idFloor FROM masfloor WHERE floor_name = '" & _floor_name & "' AND idFloor <> '" & _idFloor & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มี floor นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else

                    Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                    If confirmEdit = 6 Then
                        Dim sql As String = ""

                        sql += "UPDATE masfloor SET "

                        sql += "floor_name = '" & _floor_name & "' "
                        sql += "WHERE idFloor = '" & _idFloor & "';"

                        Try
                            db.ExecuteNonQuery(sql)
                            MsgBox("แก้ไขชื่อ floor แล้ว", MsgBoxStyle.Exclamation)
                            Return True
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                            Return False
                        Finally
                            db.Dispose()
                        End Try
                    Else
                        db.Dispose()
                        Return False
                    End If
                End If
            End If
        End If
    End Function
End Class
