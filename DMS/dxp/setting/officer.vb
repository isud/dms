﻿Public Class officer
    Dim innerDtsOfficer As New dtsOfficer

    Dim objOfficer As New clsOfficer(innerDtsOfficer)

    Private Sub getOfficer()
        objOfficer.selectOfficer()
        gcOfficer.DataSource = innerDtsOfficer
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getOfficer()
        txtOfficer_name.Text = ""
        txtOfficer_name.Tag = ""
    End Sub

    Private Sub officer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getOfficer()
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtOfficer_name.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("officer_name").ToString()
        txtOfficer_name.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idofficer").ToString()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageOfficerData As New clsOfficer

        objManageOfficerData.officer_name = txtOfficer_name.Text.Trim

        If objManageOfficerData.addOfficer() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageOfficerData As New clsOfficer

        objManageOfficerData.idofficer = txtOfficer_name.Tag
        objManageOfficerData.officer_name = txtOfficer_name.Text.Trim

        If objManageOfficerData.editOfficer() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
