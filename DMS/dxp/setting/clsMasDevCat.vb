﻿Public Class clsMasDevCat

    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMasDevCat

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMasDevCat)
        dts = outerDts
    End Sub

    Private _devcatid
    Private _devcatname

    Public Property devcatid
        Get
            Return _devcatid
        End Get
        Set(value)
            _devcatid = value
        End Set
    End Property

    Public Property devcatname
        Get
            Return _devcatname
        End Get
        Set(value)
            _devcatname = value
        End Set
    End Property


    Public Sub selectDevCat()
        dts.Tables("masdevcat").Clear()

        Dim sql As String = "SELECT * FROM masdevcat;"
        db.GetTable(sql, dts.Tables("masdevcat"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleDevCat(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasDevCat

        Dim sql As String
        sql = "SELECT devcatid, devcatname FROM masdevcat;"

        db.GetTable(sql, dts.Tables("masdevcat"))

        sle.Properties.DataSource = dts.Tables("masdevcat")
        sle.Properties.DisplayMember = "devcatname"
        sle.Properties.ValueMember = "devcatid"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addDevCat() As Boolean
        If Convert.ToString(_devcatname) = "" Then
            MsgBox("โปรดระบุชื่อหมวดอุปกรณ์", MsgBoxStyle.Exclamation)
            Return False
        Else
            Dim sqlCheckDup As String = "SELECT devcatid FROM masdevcat WHERE devcatname = '" & _devcatname & "'"
            Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

            If dtCheckDup.Rows.Count > 0 Then
                MsgBox("มีหมวดอุปกรณ์นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                Return False
            Else
                Dim sql1 As String = ""
                Dim sql2 As String = ""

                Dim sql As String = ""

                sql1 += "devcatname "
                sql2 += "'" & _devcatname & "' "

                sql = "INSERT INTO masdevcat (" & sql1 & ") VALUES (" & sql2 & ");"

                Try
                    db.ExecuteNonQuery(sql)
                    MsgBox("เพิ่มชื่อหมวดอุปกรณ์เข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                    Return True
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    Return False
                Finally
                    db.Dispose()
                End Try
            End If
        End If
    End Function


    Public Function editFloorName() As Boolean
        If Convert.ToString(_devcatname) = "" Then
            MsgBox("โปรดระบุชื่อหมวดอุปกรณ์", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_devcatid) = "" Then
                MsgBox("โปรดระบุ idMasdevcat", MsgBoxStyle.Exclamation)
                Return False
            Else

                Dim sqlCheckDup As String = "SELECT devcatid FROM masdevcat WHERE devcatname = '" & _devcatname & "' AND devcatid <> '" & _devcatid & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มีหมวดอุปกรณ์นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else

                    Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                    If confirmEdit = 6 Then
                        Dim sql As String = ""

                        sql += "UPDATE masdevcat SET "

                        sql += "devcatname = '" & _devcatname & "' "
                        sql += "WHERE devcatid = '" & _devcatid & "';"

                        Try
                            db.ExecuteNonQuery(sql)
                            MsgBox("แก้ไขชื่อหมวดอุปกรณ์แล้ว", MsgBoxStyle.Exclamation)
                            Return True
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                            Return False
                        Finally
                            db.Dispose()
                        End Try
                    Else
                        db.Dispose()
                        Return False
                    End If
                End If
            End If
        End If
    End Function
End Class
