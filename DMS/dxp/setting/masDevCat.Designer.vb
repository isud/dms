﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class masDevCat
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(masDevCat))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.btnRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.txtDevCatName = New DevExpress.XtraEditors.TextEdit()
        Me.gcMasDevCat = New DevExpress.XtraGrid.GridControl()
        Me.DtsMasDevCat1 = New DMS.dtsMasDevCat()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.coldevcatid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldevcatname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.layoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtDevCatName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcMasDevCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsMasDevCat1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.btnRefresh)
        Me.LayoutControl1.Controls.Add(Me.btnEdit)
        Me.LayoutControl1.Controls.Add(Me.btnAdd)
        Me.LayoutControl1.Controls.Add(Me.txtDevCatName)
        Me.LayoutControl1.Controls.Add(Me.gcMasDevCat)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(870, 289, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.Location = New System.Drawing.Point(37, 69)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(259, 23)
        Me.btnRefresh.StyleController = Me.LayoutControl1
        Me.btnRefresh.TabIndex = 8
        Me.btnRefresh.Text = "Refresh"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.Location = New System.Drawing.Point(169, 42)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(127, 23)
        Me.btnEdit.StyleController = Me.LayoutControl1
        Me.btnEdit.TabIndex = 7
        Me.btnEdit.Text = "แก้ไข"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(37, 42)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(128, 23)
        Me.btnAdd.StyleController = Me.LayoutControl1
        Me.btnAdd.TabIndex = 6
        Me.btnAdd.Text = "เพิ่ม"
        '
        'txtDevCatName
        '
        Me.txtDevCatName.Location = New System.Drawing.Point(111, 16)
        Me.txtDevCatName.Name = "txtDevCatName"
        Me.txtDevCatName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.txtDevCatName.Properties.Appearance.Options.UseFont = True
        Me.txtDevCatName.Size = New System.Drawing.Size(185, 22)
        Me.txtDevCatName.StyleController = Me.LayoutControl1
        Me.txtDevCatName.TabIndex = 5
        '
        'gcMasDevCat
        '
        Me.gcMasDevCat.DataMember = "masdevcat"
        Me.gcMasDevCat.DataSource = Me.DtsMasDevCat1
        Me.gcMasDevCat.Location = New System.Drawing.Point(317, 4)
        Me.gcMasDevCat.MainView = Me.GridView1
        Me.gcMasDevCat.Name = "gcMasDevCat"
        Me.gcMasDevCat.Size = New System.Drawing.Size(959, 792)
        Me.gcMasDevCat.TabIndex = 4
        Me.gcMasDevCat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtsMasDevCat1
        '
        Me.DtsMasDevCat1.DataSetName = "dtsMasDevCat"
        Me.DtsMasDevCat1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridView1.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.coldevcatid, Me.coldevcatname})
        Me.GridView1.GridControl = Me.gcMasDevCat
        Me.GridView1.Name = "GridView1"
        '
        'coldevcatid
        '
        Me.coldevcatid.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldevcatid.AppearanceCell.Options.UseFont = True
        Me.coldevcatid.Caption = "ID Devcat"
        Me.coldevcatid.FieldName = "devcatid"
        Me.coldevcatid.Name = "coldevcatid"
        Me.coldevcatid.OptionsColumn.AllowEdit = False
        Me.coldevcatid.OptionsColumn.AllowShowHide = False
        Me.coldevcatid.Visible = True
        Me.coldevcatid.VisibleIndex = 0
        Me.coldevcatid.Width = 186
        '
        'coldevcatname
        '
        Me.coldevcatname.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldevcatname.AppearanceCell.Options.UseFont = True
        Me.coldevcatname.Caption = "หมวดอุปกรณ์"
        Me.coldevcatname.FieldName = "devcatname"
        Me.coldevcatname.Name = "coldevcatname"
        Me.coldevcatname.OptionsColumn.AllowEdit = False
        Me.coldevcatname.OptionsColumn.AllowShowHide = False
        Me.coldevcatname.Visible = True
        Me.coldevcatname.VisibleIndex = 1
        Me.coldevcatname.Width = 755
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutControlGroup10, Me.LayoutControlItem1, Me.SplitterItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'layoutControlGroup10
        '
        Me.layoutControlGroup10.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.layoutControlGroup10.AppearanceGroup.Options.UseFont = True
        Me.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10"
        Me.layoutControlGroup10.ExpandButtonVisible = True
        Me.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText
        Me.layoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5})
        Me.layoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.layoutControlGroup10.Name = "layoutControlGroup10"
        Me.layoutControlGroup10.Size = New System.Drawing.Size(308, 796)
        Me.layoutControlGroup10.Text = "เพิ่ม/แก้ไข หมวดอุปกรณ์"
        Me.layoutControlGroup10.TextLocation = DevExpress.Utils.Locations.Left
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.txtDevCatName
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(263, 26)
        Me.LayoutControlItem2.Text = "หมวดอุปกรณ์"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(71, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.btnAdd
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(132, 27)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.btnEdit
        Me.LayoutControlItem4.Location = New System.Drawing.Point(132, 26)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(131, 27)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.btnRefresh
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 53)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(263, 719)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.gcMasDevCat
        Me.LayoutControlItem1.Location = New System.Drawing.Point(313, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(963, 796)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(308, 0)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(5, 796)
        '
        'masDevCat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "masDevCat"
        Me.Size = New System.Drawing.Size(1280, 800)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txtDevCatName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcMasDevCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsMasDevCat1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents btnRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtDevCatName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcMasDevCat As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents layoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents DtsMasDevCat1 As dtsMasDevCat
    Friend WithEvents coldevcatid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldevcatname As DevExpress.XtraGrid.Columns.GridColumn
End Class
