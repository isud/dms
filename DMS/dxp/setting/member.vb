﻿Public Class member
    Dim innerDtsMember As New dtsMember

    Dim objMember As New clsMember(innerDtsMember)

    Private Sub getMember()
        objMember.selectMember()
        gcMember.DataSource = innerDtsMember
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMember()
        txtUsername.Text = ""
        txtUsername.Tag = ""
        txtPasswd.Text = ""
        txtConPasswd.Text = ""
    End Sub

    Private Sub member_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMember()
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtUsername.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("username").ToString()
        txtUsername.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idmember").ToString()

        txtPasswd.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("password").ToString()
        txtConPasswd.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("password").ToString()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageMemberData As New clsMember

        objManageMemberData.username = txtUsername.Text.Trim
        objManageMemberData.password = txtPasswd.Text.Trim
        objManageMemberData.confpassword = txtConPasswd.Text.Trim

        If objManageMemberData.addMember() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageMemberData As New clsMember

        objManageMemberData.idmember = txtUsername.Tag
        objManageMemberData.username = txtUsername.Text.Trim
        objManageMemberData.password = txtPasswd.Text.Trim
        objManageMemberData.confpassword = txtConPasswd.Text.Trim

        If objManageMemberData.editMember() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
