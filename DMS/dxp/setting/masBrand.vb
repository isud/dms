﻿Public Class masBrand
    Dim innerDtsMasBrand As New dtsMasBrand

    Dim objMasBrand As New clsMasBrand(innerDtsMasBrand)

    Private Sub getMasBrand()
        objMasBrand.selectBrandName()
        gcMasBrand.DataSource = innerDtsMasBrand
    End Sub

    Private Sub masBrand_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMasBrand()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMasBrand()
        txtBrand_name.Text = ""
        txtBrand_name.Tag = ""

    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtBrand_name.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("brand_name").ToString()
        txtBrand_name.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idbrand").ToString()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageBrandData As New clsMasBrand

        objManageBrandData.brand_name = txtBrand_name.Text.Trim

        If objManageBrandData.addBrandName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageBrandData As New clsMasBrand

        objManageBrandData.idbrand = txtBrand_name.Tag
        objManageBrandData.brand_name = txtBrand_name.Text.Trim

        If objManageBrandData.editBrandName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
