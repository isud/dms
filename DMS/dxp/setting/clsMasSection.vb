﻿Public Class clsMasSection
    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMasSection

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMasSection)
        dts = outerDts
    End Sub

    Private _idSECTION
    Private _section_name
    Private _idFloor

    Public Property idSECTION
        Get
            Return _idSECTION
        End Get
        Set(value)
            _idSECTION = value
        End Set
    End Property

    Public Property section_name
        Get
            Return _section_name
        End Get
        Set(value)
            _section_name = value
        End Set
    End Property

    Public Property idFloor
        Get
            Return _idFloor
        End Get
        Set(value)
            _idFloor = value
        End Set
    End Property


    Public Sub selectSectionName()
        dts.Tables("massection").Clear()

        Dim sql As String = "SELECT massection.*, masfloor.`floor_name` FROM massection LEFT JOIN masfloor ON masfloor.`idFloor` = massection.`idFloor` ;"
        db.GetTable(sql, dts.Tables("massection"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleSection(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasSection

        Dim sql As String
        sql = "SELECT massection.*, masfloor.`floor_name` FROM massection LEFT JOIN masfloor ON masfloor.`idFloor` = massection.`idFloor`;"

        db.GetTable(sql, dts.Tables("massection"))

        sle.Properties.DataSource = dts.Tables("massection")
        sle.Properties.DisplayMember = "section_name"
        sle.Properties.ValueMember = "idSECTION"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addSectionName() As Boolean
        If Convert.ToString(_section_name) = "" Then
            MsgBox("โปรดระบุชื่อแผนก", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idFloor) = "" Then
                MsgBox("โปรดระบุ floor", MsgBoxStyle.Exclamation)
                Return False
            Else
                Dim sqlCheckDup As String = "SELECT idSECTION FROM massection WHERE section_name = '" & _section_name & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มีชื่อแผนกนี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else
                    Dim sql1 As String = ""
                    Dim sql2 As String = ""

                    Dim sql As String = ""

                    sql1 += "section_name, "
                    sql2 += "'" & _section_name & "', "

                    sql1 += "idFloor "
                    sql2 += "'" & _idFloor & "' "

                    sql = "INSERT INTO massection (" & sql1 & ") VALUES (" & sql2 & ");"

                    Try
                        db.ExecuteNonQuery(sql)
                        MsgBox("เพิ่มชื่อแผนกเข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                        Return True
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                        Return False
                    Finally
                        db.Dispose()
                    End Try
                End If
            End If
        End If
    End Function


    Public Function editSectionName() As Boolean
        If Convert.ToString(_section_name) = "" Then
            MsgBox("โปรดระบุชื่อแผนก", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idSECTION) = "" Then
                MsgBox("โปรดระบุ idSection", MsgBoxStyle.Exclamation)
                Return False
            Else

                Dim sqlCheckDup As String = "SELECT idSECTION FROM massection WHERE section_name = '" & _section_name & "' AND idSECTION <> '" & _idSECTION & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มี ชื่อแผนกนี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else

                    If Convert.ToString(_idFloor) = "" Then
                        MsgBox("โปรดระบุ floor", MsgBoxStyle.Exclamation)
                        Return False
                    Else
                        Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                        If confirmEdit = 6 Then
                            Dim sql As String = ""

                            sql += "UPDATE massection SET "

                            sql += "section_name = '" & _section_name & "', "
                            sql += "idFloor = '" & _idFloor & "' "
                            sql += "WHERE idSECTION = '" & _idSECTION & "';"

                            Try
                                db.ExecuteNonQuery(sql)
                                MsgBox("แก้ไขชื่อแผนกแล้ว", MsgBoxStyle.Exclamation)
                                Return True
                            Catch ex As Exception
                                MsgBox(ex.ToString)
                                Return False
                            Finally
                                db.Dispose()
                            End Try
                        Else
                            db.Dispose()
                            Return False
                        End If
                    End If
                End If
            End If
        End If
    End Function
End Class
