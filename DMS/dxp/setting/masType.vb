﻿Public Class masType
    Dim innerDtsMasType As New dtsMasType

    Dim objMasType As New clsMasType(innerDtsMasType)

    Private Sub getMasType()
        objMasType.selectTypeName()
        gcMasType.DataSource = innerDtsMasType
    End Sub

    Private Sub masType_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMasType()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMasType()
        txtTypeName.Text = ""
        txtTypeName.Tag = ""
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtTypeName.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("type_name").ToString()
        txtTypeName.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idtype").ToString()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageTypeData As New clsMasType

        objManageTypeData.type_name = txtTypeName.Text.Trim

        If objManageTypeData.addTypeName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageTypeData As New clsMasType

        objManageTypeData.idtype = txtTypeName.Tag
        objManageTypeData.type_name = txtTypeName.Text.Trim

        If objManageTypeData.editTypeName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
