﻿Public Class clsMasSeller
    Public Shared Sub getSleSeller(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasSeller

        Dim sql As String
        sql = "SELECT sellerid, sellername FROM masseller;"

        db.GetTable(sql, dts.Tables("masseller"))

        sle.Properties.DataSource = dts.Tables("masseller")
        sle.Properties.DisplayMember = "sellername"
        sle.Properties.ValueMember = "sellerid"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub
End Class
