﻿Public Class masLocation
    Dim innerDtsMasLocation As New dtsMasLocation

    Dim objMasLocation As New clsMasLocation(innerDtsMasLocation)

    Private Sub getMasLocation()
        objMasLocation.selectLocationName()
        gcMasLocation.DataSource = innerDtsMasLocation
    End Sub

    Private Sub getSleFloor()
        clsMasFloor.getSleFloor(sleIdFloor)
    End Sub

    Private Sub getSleSection()
        clsMasSection.getSleSection(sleIdSection)
    End Sub

    Private Sub masLocation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMasLocation()
        getSleSection()
        getSleFloor()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMasLocation()
        txtLocation_name.Text = ""
        txtLocation_name.Tag = ""
        sleIdSection.EditValue = Nothing
        sleIdFloor.EditValue = Nothing
        txtDescription.Text = ""
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtLocation_name.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("location_name").ToString()
        txtLocation_name.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idlocation").ToString()
        sleIdSection.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idsection").ToString()
        sleIdFloor.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idFloor").ToString()
        txtDescription.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("description_name").ToString()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageLocationData As New clsMasLocation

        objManageLocationData.location_name = txtLocation_name.Text.Trim
        objManageLocationData.idsection = sleIdSection.EditValue
        objManageLocationData.idFloor = sleIdFloor.EditValue
        objManageLocationData.description_name = txtDescription.Text.Trim

        If objManageLocationData.addLocationName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageLocationData As New clsMasLocation

        objManageLocationData.idlocation = txtLocation_name.Tag
        objManageLocationData.location_name = txtLocation_name.Text.Trim
        objManageLocationData.idsection = sleIdSection.EditValue
        objManageLocationData.idFloor = sleIdFloor.EditValue
        objManageLocationData.description_name = txtDescription.Text.Trim

        If objManageLocationData.editLocationName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
