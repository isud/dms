﻿Public Class clsMember
    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMember

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMember)
        dts = outerDts
    End Sub

    Private _idmember
    Private _username
    Private _password
    Private _confpassword

    Public Property idmember
        Get
            Return _idmember
        End Get
        Set(value)
            _idmember = value
        End Set
    End Property

    Public Property username
        Get
            Return _username
        End Get
        Set(value)
            _username = value
        End Set
    End Property

    Public Property password
        Get
            Return _password
        End Get
        Set(value)
            _password = value
        End Set
    End Property

    Public Property confpassword
        Get
            Return _confpassword
        End Get
        Set(value)
            _confpassword = value
        End Set
    End Property


    Public Sub selectMember()
        dts.Tables("member").Clear()

        Dim sql As String = "SELECT * FROM member;"
        db.GetTable(sql, dts.Tables("member"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleMember(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasFloor

        Dim sql As String
        sql = "SELECT idmember, username FROM masfloor;"

        db.GetTable(sql, dts.Tables("member"))

        sle.Properties.DataSource = dts.Tables("member")
        sle.Properties.DisplayMember = "username"
        sle.Properties.ValueMember = "idmember"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addMember() As Boolean
        If Convert.ToString(_username) = "" Then
            MsgBox("โปรดระบุ username", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_password) = "" Then
                MsgBox("โปรดระบุ password", MsgBoxStyle.Exclamation)
                Return False
            Else
                If Convert.ToString(_confpassword) = "" Then
                    MsgBox("โปรดระบุ confirm password", MsgBoxStyle.Exclamation)
                    Return False
                Else
                    If Convert.ToString(_password) <> Convert.ToString(_confpassword) Then
                        MsgBox("โปรดระบุ password ให้ตรงกัน", MsgBoxStyle.Exclamation)
                        Return False
                    Else
                        Dim sqlCheckDup As String = "SELECT idmember FROM member WHERE username = '" & _username & "'"
                        Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                        If dtCheckDup.Rows.Count > 0 Then
                            MsgBox("มี username นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                            Return False
                        Else
                            Dim sql1 As String = ""
                            Dim sql2 As String = ""

                            Dim sql As String = ""

                            sql1 += "username, "
                            sql2 += "'" & _username & "', "

                            sql1 += "password "
                            sql2 += "'" & _password & "' "

                            sql = "INSERT INTO member (" & sql1 & ") VALUES (" & sql2 & ");"

                            Try
                                db.ExecuteNonQuery(sql)
                                MsgBox("เพิ่มสมาชิกเข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                                Return True
                            Catch ex As Exception
                                MsgBox(ex.ToString)
                                Return False
                            Finally
                                db.Dispose()
                            End Try
                        End If
                    End If
                End If
            End If

        End If
    End Function


    Public Function editMember() As Boolean
        If Convert.ToString(_idmember) = "" Then
            MsgBox("โปรดระบุ idMember", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_username) = "" Then
                MsgBox("โปรดระบุ username", MsgBoxStyle.Exclamation)
                Return False
            Else
                If Convert.ToString(_password) = "" Then
                    MsgBox("โปรดระบุ password", MsgBoxStyle.Exclamation)
                    Return False
                Else
                    If Convert.ToString(_confpassword) = "" Then
                        MsgBox("โปรดระบุ confirm password", MsgBoxStyle.Exclamation)
                        Return False
                    Else
                        If Convert.ToString(_password) <> Convert.ToString(_confpassword) Then
                            MsgBox("โปรดระบุ password ให้ตรงกัน", MsgBoxStyle.Exclamation)
                            Return False
                        Else

                            Dim sqlCheckDup As String = "SELECT idmember FROM member WHERE username = '" & _username & "' AND idmember <> '" & _idmember & "'"
                            Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                            If dtCheckDup.Rows.Count > 0 Then
                                MsgBox("มี username นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                                Return False
                            Else

                                Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                                If confirmEdit = 6 Then
                                    Dim sql As String = ""

                                    sql += "UPDATE member SET "

                                    sql += "username = '" & _username & "', "
                                    sql += "password = '" & _password & "' "

                                    sql += "WHERE idmember = '" & _idmember & "';"

                                    Try
                                        db.ExecuteNonQuery(sql)
                                        MsgBox("แก้ไขสมาชิกแล้ว", MsgBoxStyle.Exclamation)
                                        Return True
                                    Catch ex As Exception
                                        MsgBox(ex.ToString)
                                        Return False
                                    Finally
                                        db.Dispose()
                                    End Try
                                Else
                                    db.Dispose()
                                    Return False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Function
End Class
