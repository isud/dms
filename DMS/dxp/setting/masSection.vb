﻿Public Class masSection
    Dim innerDtsMasSection As New dtsMasSection

    Dim objMasSection As New clsMasSection(innerDtsMasSection)

    Private Sub getMasSection()
        objMasSection.selectSectionName()
        gcSection.DataSource = innerDtsMasSection
    End Sub

    Private Sub getSleFloor()
        clsMasFloor.getSleFloor(sleFloor)
    End Sub

    Private Sub masSection_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMasSection()
        getSleFloor()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMasSection()
        txtSection_name.Text = ""
        txtSection_name.Tag = ""
        sleFloor.EditValue = Nothing
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtSection_name.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("section_name").ToString()
        txtSection_name.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idSECTION").ToString()
        sleFloor.EditValue = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idFloor").ToString()

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageSectionData As New clsMasSection

        objManageSectionData.section_name = txtSection_name.Text.Trim
        objManageSectionData.idFloor = sleFloor.EditValue


        If objManageSectionData.addSectionName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageSectionData As New clsMasSection

        objManageSectionData.idSECTION = txtSection_name.Tag
        objManageSectionData.section_name = txtSection_name.Text.Trim
        objManageSectionData.idFloor = sleFloor.EditValue


        If objManageSectionData.editSectionName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
