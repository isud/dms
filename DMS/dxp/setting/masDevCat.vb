﻿Public Class masDevCat
    Dim innerDtsMasDevCat As New dtsMasDevCat

    Dim objMasDevCat As New clsMasDevCat(innerDtsMasDevCat)

    Private Sub getMasDevCat()
        objMasDevCat.selectDevCat()
        gcMasDevCat.DataSource = innerDtsMasDevCat
    End Sub

    Private Sub masFloor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getMasDevCat()
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        getMasDevCat()
        txtDevCatName.Text = ""
        txtDevCatName.Tag = ""
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        txtDevCatName.Text = GridView1.GetDataRow(GridView1.FocusedRowHandle)("devcatname").ToString()
        txtDevCatName.Tag = GridView1.GetDataRow(GridView1.FocusedRowHandle)("devcatid").ToString()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim objManageDevCatData As New clsMasDevCat

        objManageDevCatData.devcatname = txtDevCatName.Text.Trim

        If objManageDevCatData.addDevCat() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim objManageDevCatData As New clsMasDevCat

        objManageDevCatData.devcatid = txtDevCatName.Tag
        objManageDevCatData.devcatname = txtDevCatName.Text.Trim

        If objManageDevCatData.editFloorName() = True Then
            btnRefresh_Click(Nothing, Nothing)
        End If
    End Sub
End Class
