﻿Public Class clsOfficer
    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsOfficer

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsOfficer)
        dts = outerDts
    End Sub

    Private _idofficer
    Private _officer_name

    Public Property idofficer
        Get
            Return _idofficer
        End Get
        Set(value)
            _idofficer = value
        End Set
    End Property

    Public Property officer_name
        Get
            Return _officer_name
        End Get
        Set(value)
            _officer_name = value
        End Set
    End Property


    Public Sub selectOfficer()
        dts.Tables("officer").Clear()

        Dim sql As String = "SELECT * FROM officer;"
        db.GetTable(sql, dts.Tables("officer"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleOfficer(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasFloor

        Dim sql As String
        sql = "SELECT * FROM officer;"

        db.GetTable(sql, dts.Tables("officer"))

        sle.Properties.DataSource = dts.Tables("officer")
        sle.Properties.DisplayMember = "officer_name"
        sle.Properties.ValueMember = "idofficer"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addOfficer() As Boolean
        If Convert.ToString(_officer_name) = "" Then
            MsgBox("โปรดระบุชื่อพนักงาน", MsgBoxStyle.Exclamation)
            Return False
        Else
            Dim sqlCheckDup As String = "SELECT idofficer FROM officer WHERE officer_name = '" & _officer_name & "'"
            Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

            If dtCheckDup.Rows.Count > 0 Then
                MsgBox("มีพนักงานนี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                Return False
            Else
                Dim sql1 As String = ""
                Dim sql2 As String = ""

                Dim sql As String = ""

                sql1 += "officer_name "
                sql2 += "'" & _officer_name & "' "

                sql = "INSERT INTO officer (" & sql1 & ") VALUES (" & sql2 & ");"

                Try
                    db.ExecuteNonQuery(sql)
                    MsgBox("เพิ่มพนักงานเข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                    Return True
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    Return False
                Finally
                    db.Dispose()
                End Try
            End If
        End If
    End Function


    Public Function editOfficer() As Boolean
        If Convert.ToString(_officer_name) = "" Then
            MsgBox("โปรดระบุชื่อพนักงาน", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idofficer) = "" Then
                MsgBox("โปรดระบุ idOfficer", MsgBoxStyle.Exclamation)
                Return False
            Else

                Dim sqlCheckDup As String = "SELECT idofficer FROM officer WHERE officer_name = '" & _officer_name & "' AND idofficer <> '" & _idofficer & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มีพนักงานนี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else

                    Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                    If confirmEdit = 6 Then
                        Dim sql As String = ""

                        sql += "UPDATE officer SET "

                        sql += "officer_name = '" & _officer_name & "' "
                        sql += "WHERE idofficer = '" & _idofficer & "';"

                        Try
                            db.ExecuteNonQuery(sql)
                            MsgBox("แก้ไขพนักงานแล้ว", MsgBoxStyle.Exclamation)
                            Return True
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                            Return False
                        Finally
                            db.Dispose()
                        End Try
                    Else
                        db.Dispose()
                        Return False
                    End If
                End If
            End If
        End If
    End Function
End Class
