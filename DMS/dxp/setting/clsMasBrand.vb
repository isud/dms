﻿Public Class clsMasBrand


    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMasBrand

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMasBrand)
        dts = outerDts
    End Sub

    Private _idbrand
    Private _brand_name

    Public Property idbrand
        Get
            Return _idbrand
        End Get
        Set(value)
            _idbrand = value
        End Set
    End Property

    Public Property brand_name
        Get
            Return _brand_name
        End Get
        Set(value)
            _brand_name = value
        End Set
    End Property


    Public Sub selectBrandName()
        dts.Tables("masbrand").Clear()

        Dim sql As String = "SELECT * FROM masbrand;"
        db.GetTable(sql, dts.Tables("masbrand"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleBrand(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasBrand

        Dim sql As String
        sql = "SELECT idbrand, brand_name FROM masbrand;"

        db.GetTable(sql, dts.Tables("masbrand"))

        sle.Properties.DataSource = dts.Tables("masbrand")
        sle.Properties.DisplayMember = "brand_name"
        sle.Properties.ValueMember = "idbrand"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addBrandName() As Boolean
        If Convert.ToString(_brand_name) = "" Then
            MsgBox("โปรดระบุชื่อ ยี่ห้อ/Brand", MsgBoxStyle.Exclamation)
            Return False
        Else
            Dim sqlCheckDup As String = "SELECT idbrand FROM masbrand WHERE brand_name = '" & _brand_name & "'"
            Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

            If dtCheckDup.Rows.Count > 0 Then
                MsgBox("มี ยี่ห้อ/Brand นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                Return False
            Else
                Dim sql1 As String = ""
                Dim sql2 As String = ""

                Dim sql As String = ""

                sql1 += "brand_name "
                sql2 += "'" & _brand_name & "' "

                sql = "INSERT INTO masbrand (" & sql1 & ") VALUES (" & sql2 & ");"

                Try
                    db.ExecuteNonQuery(sql)
                    MsgBox("เพิ่มชื่อ ยี่ห้อ/Brand เข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                    Return True
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    Return False
                Finally
                    db.Dispose()
                End Try
            End If
        End If
    End Function


    Public Function editBrandName() As Boolean
        If Convert.ToString(_brand_name) = "" Then
            MsgBox("โปรดระบุชื่อ ยี่ห้อ/Brand", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idbrand) = "" Then
                MsgBox("โปรดระบุ idBrand", MsgBoxStyle.Exclamation)
                Return False
            Else

                Dim sqlCheckDup As String = "SELECT idbrand FROM masbrand WHERE brand_name = '" & _brand_name & "' AND idbrand <> '" & _idbrand & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มี ยี่ห้อ/Brand นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else

                    Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                    If confirmEdit = 6 Then
                        Dim sql As String = ""

                        sql += "UPDATE masbrand SET "

                        sql += "brand_name = '" & _brand_name & "' "
                        sql += "WHERE idbrand = '" & _idbrand & "';"

                        Try
                            db.ExecuteNonQuery(sql)
                            MsgBox("แก้ไขชื่อ ยี่ห้อ/Brand แล้ว", MsgBoxStyle.Exclamation)
                            Return True
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                            Return False
                        Finally
                            db.Dispose()
                        End Try
                    Else
                        db.Dispose()
                        Return False
                    End If
                End If
            End If
        End If
    End Function
End Class
