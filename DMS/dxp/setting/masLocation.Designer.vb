﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class masLocation
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(masLocation))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.sleIdFloor = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.MasfloorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsMasFloor = New DMS.dtsMasFloor()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.sleIdSection = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.MassectionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsMasSection = New DMS.dtsMasSection()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.btnRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEdit = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAdd = New DevExpress.XtraEditors.SimpleButton()
        Me.txtDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.txtLocation_name = New DevExpress.XtraEditors.TextEdit()
        Me.gcMasLocation = New DevExpress.XtraGrid.GridControl()
        Me.DtsMasLocation1 = New DMS.dtsMasLocation()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidlocation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collocation_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidFloor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colfloor_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidsection = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsection_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldescription_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.layoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.sleIdFloor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasfloorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsMasFloor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sleIdSection.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MassectionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsMasSection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLocation_name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcMasLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsMasLocation1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.sleIdFloor)
        Me.LayoutControl1.Controls.Add(Me.sleIdSection)
        Me.LayoutControl1.Controls.Add(Me.btnRefresh)
        Me.LayoutControl1.Controls.Add(Me.btnEdit)
        Me.LayoutControl1.Controls.Add(Me.btnAdd)
        Me.LayoutControl1.Controls.Add(Me.txtDescription)
        Me.LayoutControl1.Controls.Add(Me.txtLocation_name)
        Me.LayoutControl1.Controls.Add(Me.gcMasLocation)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(864, 206, 389, 424)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'sleIdFloor
        '
        Me.sleIdFloor.Location = New System.Drawing.Point(89, 97)
        Me.sleIdFloor.Name = "sleIdFloor"
        Me.sleIdFloor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sleIdFloor.Properties.Appearance.Options.UseFont = True
        Me.sleIdFloor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleIdFloor.Properties.DataSource = Me.MasfloorBindingSource
        Me.sleIdFloor.Properties.DisplayMember = "floor_name"
        Me.sleIdFloor.Properties.NullText = "[โปรดเลือกชั้น]"
        Me.sleIdFloor.Properties.ValueMember = "idFloor"
        Me.sleIdFloor.Properties.View = Me.SearchLookUpEdit2View
        Me.sleIdFloor.Size = New System.Drawing.Size(280, 22)
        Me.sleIdFloor.StyleController = Me.LayoutControl1
        Me.sleIdFloor.TabIndex = 11
        '
        'MasfloorBindingSource
        '
        Me.MasfloorBindingSource.DataMember = "masfloor"
        Me.MasfloorBindingSource.DataSource = Me.DtsMasFloor
        '
        'DtsMasFloor
        '
        Me.DtsMasFloor.DataSetName = "dtsMasFloor"
        Me.DtsMasFloor.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'sleIdSection
        '
        Me.sleIdSection.Location = New System.Drawing.Point(89, 71)
        Me.sleIdSection.Name = "sleIdSection"
        Me.sleIdSection.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sleIdSection.Properties.Appearance.Options.UseFont = True
        Me.sleIdSection.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.sleIdSection.Properties.DataSource = Me.MassectionBindingSource
        Me.sleIdSection.Properties.DisplayMember = "section_name"
        Me.sleIdSection.Properties.NullText = "[โปรดเลือกแผนก]"
        Me.sleIdSection.Properties.ValueMember = "idSECTION"
        Me.sleIdSection.Properties.View = Me.SearchLookUpEdit1View
        Me.sleIdSection.Size = New System.Drawing.Size(280, 22)
        Me.sleIdSection.StyleController = Me.LayoutControl1
        Me.sleIdSection.TabIndex = 10
        '
        'MassectionBindingSource
        '
        Me.MassectionBindingSource.DataMember = "massection"
        Me.MassectionBindingSource.DataSource = Me.DtsMasSection
        '
        'DtsMasSection
        '
        Me.DtsMasSection.DataSetName = "dtsMasSection"
        Me.DtsMasSection.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'btnRefresh
        '
        Me.btnRefresh.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Appearance.Options.UseFont = True
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.Location = New System.Drawing.Point(24, 359)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(345, 23)
        Me.btnRefresh.StyleController = Me.LayoutControl1
        Me.btnRefresh.TabIndex = 9
        Me.btnRefresh.Text = "Refresh"
        '
        'btnEdit
        '
        Me.btnEdit.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Appearance.Options.UseFont = True
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.Location = New System.Drawing.Point(198, 332)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(171, 23)
        Me.btnEdit.StyleController = Me.LayoutControl1
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "แก้ไข"
        '
        'btnAdd
        '
        Me.btnAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Appearance.Options.UseFont = True
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(24, 332)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(170, 23)
        Me.btnAdd.StyleController = Me.LayoutControl1
        Me.btnAdd.TabIndex = 7
        Me.btnAdd.Text = "เพิ่ม"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(89, 123)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Properties.Appearance.Options.UseFont = True
        Me.txtDescription.Size = New System.Drawing.Size(280, 205)
        Me.txtDescription.StyleController = Me.LayoutControl1
        Me.txtDescription.TabIndex = 6
        '
        'txtLocation_name
        '
        Me.txtLocation_name.Location = New System.Drawing.Point(89, 45)
        Me.txtLocation_name.Name = "txtLocation_name"
        Me.txtLocation_name.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation_name.Properties.Appearance.Options.UseFont = True
        Me.txtLocation_name.Size = New System.Drawing.Size(280, 22)
        Me.txtLocation_name.StyleController = Me.LayoutControl1
        Me.txtLocation_name.TabIndex = 5
        '
        'gcMasLocation
        '
        Me.gcMasLocation.DataMember = "maslocation"
        Me.gcMasLocation.DataSource = Me.DtsMasLocation1
        Me.gcMasLocation.Location = New System.Drawing.Point(390, 12)
        Me.gcMasLocation.MainView = Me.GridView1
        Me.gcMasLocation.Name = "gcMasLocation"
        Me.gcMasLocation.Size = New System.Drawing.Size(878, 776)
        Me.gcMasLocation.TabIndex = 4
        Me.gcMasLocation.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtsMasLocation1
        '
        Me.DtsMasLocation1.DataSetName = "dtsMasLocation"
        Me.DtsMasLocation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridView1.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidlocation, Me.collocation_name, Me.colidFloor, Me.colfloor_name, Me.colidsection, Me.colsection_name, Me.coldescription_name})
        Me.GridView1.GridControl = Me.gcMasLocation
        Me.GridView1.Name = "GridView1"
        '
        'colidlocation
        '
        Me.colidlocation.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colidlocation.AppearanceCell.Options.UseFont = True
        Me.colidlocation.Caption = "ID Location"
        Me.colidlocation.FieldName = "idlocation"
        Me.colidlocation.Name = "colidlocation"
        Me.colidlocation.OptionsColumn.AllowEdit = False
        Me.colidlocation.OptionsColumn.AllowShowHide = False
        Me.colidlocation.Visible = True
        Me.colidlocation.VisibleIndex = 0
        Me.colidlocation.Width = 112
        '
        'collocation_name
        '
        Me.collocation_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.collocation_name.AppearanceCell.Options.UseFont = True
        Me.collocation_name.Caption = "ชื่อสถานที่"
        Me.collocation_name.FieldName = "location_name"
        Me.collocation_name.Name = "collocation_name"
        Me.collocation_name.OptionsColumn.AllowEdit = False
        Me.collocation_name.OptionsColumn.AllowShowHide = False
        Me.collocation_name.Visible = True
        Me.collocation_name.VisibleIndex = 1
        Me.collocation_name.Width = 208
        '
        'colidFloor
        '
        Me.colidFloor.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colidFloor.AppearanceCell.Options.UseFont = True
        Me.colidFloor.FieldName = "idFloor"
        Me.colidFloor.Name = "colidFloor"
        Me.colidFloor.OptionsColumn.AllowEdit = False
        Me.colidFloor.OptionsColumn.AllowShowHide = False
        '
        'colfloor_name
        '
        Me.colfloor_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colfloor_name.AppearanceCell.Options.UseFont = True
        Me.colfloor_name.Caption = "ชั้น(Floor)"
        Me.colfloor_name.FieldName = "floor_name"
        Me.colfloor_name.Name = "colfloor_name"
        Me.colfloor_name.OptionsColumn.AllowEdit = False
        Me.colfloor_name.OptionsColumn.AllowShowHide = False
        Me.colfloor_name.Visible = True
        Me.colfloor_name.VisibleIndex = 2
        Me.colfloor_name.Width = 208
        '
        'colidsection
        '
        Me.colidsection.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colidsection.AppearanceCell.Options.UseFont = True
        Me.colidsection.FieldName = "idsection"
        Me.colidsection.Name = "colidsection"
        Me.colidsection.OptionsColumn.AllowEdit = False
        Me.colidsection.OptionsColumn.AllowShowHide = False
        '
        'colsection_name
        '
        Me.colsection_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colsection_name.AppearanceCell.Options.UseFont = True
        Me.colsection_name.Caption = "แผนก"
        Me.colsection_name.FieldName = "section_name"
        Me.colsection_name.Name = "colsection_name"
        Me.colsection_name.OptionsColumn.AllowEdit = False
        Me.colsection_name.OptionsColumn.AllowShowHide = False
        Me.colsection_name.Visible = True
        Me.colsection_name.VisibleIndex = 3
        Me.colsection_name.Width = 208
        '
        'coldescription_name
        '
        Me.coldescription_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldescription_name.AppearanceCell.Options.UseFont = True
        Me.coldescription_name.Caption = "รายละเอียด"
        Me.coldescription_name.FieldName = "description_name"
        Me.coldescription_name.Name = "coldescription_name"
        Me.coldescription_name.OptionsColumn.AllowEdit = False
        Me.coldescription_name.OptionsColumn.AllowShowHide = False
        Me.coldescription_name.Visible = True
        Me.coldescription_name.VisibleIndex = 4
        Me.coldescription_name.Width = 212
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutControlGroup10, Me.LayoutControlItem1, Me.SplitterItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'layoutControlGroup10
        '
        Me.layoutControlGroup10.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup10.AppearanceGroup.Options.UseFont = True
        Me.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10"
        Me.layoutControlGroup10.ExpandButtonVisible = True
        Me.layoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.EmptySpaceItem1, Me.LayoutControlItem7, Me.LayoutControlItem8})
        Me.layoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.layoutControlGroup10.Name = "layoutControlGroup10"
        Me.layoutControlGroup10.Size = New System.Drawing.Size(373, 780)
        Me.layoutControlGroup10.Text = "เพิ่ม/แก้ไข สถานที่"
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.txtLocation_name
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(349, 26)
        Me.LayoutControlItem2.Text = "ชื่อสถานที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(62, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.txtDescription
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(349, 209)
        Me.LayoutControlItem3.Text = "รายละเอียด"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(62, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.btnAdd
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 287)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(174, 27)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.btnEdit
        Me.LayoutControlItem5.Location = New System.Drawing.Point(174, 287)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(175, 27)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.btnRefresh
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 314)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(349, 27)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 341)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(349, 394)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem7.Control = Me.sleIdSection
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(349, 26)
        Me.LayoutControlItem7.Text = "แผนก"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(62, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem8.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem8.Control = Me.sleIdFloor
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(349, 26)
        Me.LayoutControlItem8.Text = "ชั้น(Floor)"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(62, 16)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.gcMasLocation
        Me.LayoutControlItem1.Location = New System.Drawing.Point(378, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(882, 780)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(373, 0)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(5, 780)
        '
        'masLocation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "masLocation"
        Me.Size = New System.Drawing.Size(1280, 800)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.sleIdFloor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasfloorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsMasFloor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sleIdSection.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MassectionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsMasSection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLocation_name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcMasLocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsMasLocation1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents btnRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEdit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtLocation_name As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcMasLocation As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents layoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents sleIdFloor As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents sleIdSection As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MasfloorBindingSource As BindingSource
    Friend WithEvents DtsMasFloor As dtsMasFloor
    Friend WithEvents MassectionBindingSource As BindingSource
    Friend WithEvents DtsMasSection As dtsMasSection
    Friend WithEvents DtsMasLocation1 As dtsMasLocation
    Friend WithEvents colidlocation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collocation_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidFloor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colfloor_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidsection As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsection_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldescription_name As DevExpress.XtraGrid.Columns.GridColumn
End Class
