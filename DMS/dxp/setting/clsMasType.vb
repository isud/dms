﻿Public Class clsMasType
    Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection

    Dim dts As New dtsMasType

    Public Sub New()

    End Sub

    Public Sub New(ByRef outerDts As dtsMasType)
        dts = outerDts
    End Sub

    Private _idtype
    Private _type_name

    Public Property idtype
        Get
            Return _idtype
        End Get
        Set(value)
            _idtype = value
        End Set
    End Property

    Public Property type_name
        Get
            Return _type_name
        End Get
        Set(value)
            _type_name = value
        End Set
    End Property


    Public Sub selectTypeName()
        dts.Tables("mastype").Clear()

        Dim sql As String = "SELECT * FROM mastype;"
        db.GetTable(sql, dts.Tables("mastype"))

        db.Dispose()
    End Sub

    Public Shared Sub getSleType(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasType

        Dim sql As String
        sql = "SELECT idtype, type_name FROM mastype;"

        db.GetTable(sql, dts.Tables("mastype"))

        sle.Properties.DataSource = dts.Tables("mastype")
        sle.Properties.DisplayMember = "type_name"
        sle.Properties.ValueMember = "idtype"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

    Public Function addTypeName() As Boolean
        If Convert.ToString(_type_name) = "" Then
            MsgBox("โปรดระบุชื่อประเภทอุปกรณ์", MsgBoxStyle.Exclamation)
            Return False
        Else
            Dim sqlCheckDup As String = "SELECT idtype FROM mastype WHERE type_name = '" & _type_name & "'"
            Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

            If dtCheckDup.Rows.Count > 0 Then
                MsgBox("มีชื่อประเภทนี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                Return False
            Else
                Dim sql1 As String = ""
                Dim sql2 As String = ""

                Dim sql As String = ""

                sql1 += "type_name "
                sql2 += "'" & _type_name & "' "

                sql = "INSERT INTO mastype (" & sql1 & ") VALUES (" & sql2 & ");"

                Try
                    db.ExecuteNonQuery(sql)
                    MsgBox("เพิ่มชื่อประเภทอุปกรณ์เข้าสู่ระบบแล้ว", MsgBoxStyle.Exclamation)
                    Return True
                Catch ex As Exception
                    MsgBox(ex.ToString)
                    Return False
                Finally
                    db.Dispose()
                End Try
            End If
        End If
    End Function


    Public Function editTypeName() As Boolean
        If Convert.ToString(_type_name) = "" Then
            MsgBox("โปรดระบุชื่อประเภทอุปกรณ์", MsgBoxStyle.Exclamation)
            Return False
        Else
            If Convert.ToString(_idtype) = "" Then
                MsgBox("โปรดระบุ idType", MsgBoxStyle.Exclamation)
                Return False
            Else

                Dim sqlCheckDup As String = "SELECT idtype FROM mastype WHERE type_name = '" & _type_name & "' AND idtype <> '" & _idtype & "'"
                Dim dtCheckDup As DataTable = db.GetTable(sqlCheckDup)

                If dtCheckDup.Rows.Count > 0 Then
                    MsgBox("มีประเภทอุปกรณ์นี้อยู่ในระบบแล้ว โปรดใช้ชื่ออื่น", MsgBoxStyle.Exclamation)
                    Return False
                Else

                    Dim confirmEdit As Integer = MsgBox("ท่านแน่ใจหรือไม่ว่าต้องการแก้ไขข้อมูลดังกล่าว", MsgBoxStyle.YesNo)

                    If confirmEdit = 6 Then
                        Dim sql As String = ""

                        sql += "UPDATE mastype SET "

                        sql += "type_name = '" & _type_name & "' "
                        sql += "WHERE idtype = '" & _idtype & "';"

                        Try
                            db.ExecuteNonQuery(sql)
                            MsgBox("แก้ไขชื่อประเภทอุปกรณ์แล้ว", MsgBoxStyle.Exclamation)
                            Return True
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                            Return False
                        Finally
                            db.Dispose()
                        End Try
                    Else
                        db.Dispose()
                        Return False
                    End If


                End If
            End If
        End If
    End Function
End Class
