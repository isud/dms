﻿Public Class clsMasPrtType

    Public Shared Sub getSlePrtType(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasPrtType

        Dim sql As String
        sql = "SELECT prttypeid, prttypename FROM masprintertype;"

        db.GetTable(sql, dts.Tables("masprttype"))

        sle.Properties.DataSource = dts.Tables("masprttype")
        sle.Properties.DisplayMember = "prttypename"
        sle.Properties.ValueMember = "prttypeid"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub

End Class
