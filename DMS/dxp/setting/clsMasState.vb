﻿Public Class clsMasState
    Public Shared Sub getSleState(ByRef sle As DevExpress.XtraEditors.SearchLookUpEdit)
        Dim db As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim dts As New dtsMasState

        Dim sql As String
        sql = "SELECT stateid, statename FROM masstate;"

        db.GetTable(sql, dts.Tables("masstate"))

        sle.Properties.DataSource = dts.Tables("masstate")
        sle.Properties.DisplayMember = "statename"
        sle.Properties.ValueMember = "stateid"

        sle.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup

        db.Dispose()
    End Sub
End Class
