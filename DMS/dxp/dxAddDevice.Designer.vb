﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class dxAddDevice
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.RadioGroup2 = New DevExpress.XtraEditors.RadioGroup()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl6 = New DevExpress.XtraLayout.LayoutControl()
        Me.DateEdit6 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit5 = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem87 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem88 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleButton12 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton11 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit6 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit57 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit56 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit55 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit54 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit43 = New DevExpress.XtraEditors.TextEdit()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit3 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit42 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit41 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit40 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit39 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit37 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit25 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit31 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit27 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit22 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit35 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit33 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit32 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit30 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit21 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit38 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit24 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit29 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit36 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit28 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit23 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit26 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit34 = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit4 = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit46 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit48 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit47 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEdit4 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit44 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton8 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit45 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit53 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit52 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit49 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit51 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit50 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEdit5 = New DevExpress.XtraEditors.MemoEdit()
        Me.SimpleButton10 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton9 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.tabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.layoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.layoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem46 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem48 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem50 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem11 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem47 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem12 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem49 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem52 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem13 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem51 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem54 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem55 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem56 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem57 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem53 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem14 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem58 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem15 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem59 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem60 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem61 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem16 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem62 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem63 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem64 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem66 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem70 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem18 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem65 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem17 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem67 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem68 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem19 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem20 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem69 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem71 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem72 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem74 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem21 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem75 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem22 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem76 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem77 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem23 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem24 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem79 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem80 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem81 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem82 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem25 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem26 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem78 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem83 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem84 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem27 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem85 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem28 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem86 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DateTimeChartRangeControlClient1 = New DevExpress.XtraEditors.DateTimeChartRangeControlClient()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.RadioGroup2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl6.SuspendLayout()
        CType(Me.DateEdit6.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit5.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit57.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit56.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit55.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit54.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit43.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit42.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit41.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit40.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit39.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit37.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit25.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit31.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit27.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit22.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit35.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit33.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit32.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit30.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit21.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit38.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit24.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit29.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit36.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit28.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit23.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit26.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit34.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit4.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit46.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit48.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit47.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit44.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit45.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit53.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit52.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit49.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit51.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit50.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTimeChartRangeControlClient1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.RadioGroup2)
        Me.LayoutControl1.Controls.Add(Me.GroupControl3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton12)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton11)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit6)
        Me.LayoutControl1.Controls.Add(Me.TextEdit57)
        Me.LayoutControl1.Controls.Add(Me.TextEdit56)
        Me.LayoutControl1.Controls.Add(Me.TextEdit55)
        Me.LayoutControl1.Controls.Add(Me.TextEdit54)
        Me.LayoutControl1.Controls.Add(Me.TextEdit43)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton6)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton5)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit3)
        Me.LayoutControl1.Controls.Add(Me.TextEdit42)
        Me.LayoutControl1.Controls.Add(Me.TextEdit41)
        Me.LayoutControl1.Controls.Add(Me.TextEdit40)
        Me.LayoutControl1.Controls.Add(Me.TextEdit39)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit1)
        Me.LayoutControl1.Controls.Add(Me.GroupControl1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit19)
        Me.LayoutControl1.Controls.Add(Me.TextEdit18)
        Me.LayoutControl1.Controls.Add(Me.TextEdit17)
        Me.LayoutControl1.Controls.Add(Me.TextEdit16)
        Me.LayoutControl1.Controls.Add(Me.TextEdit15)
        Me.LayoutControl1.Controls.Add(Me.TextEdit14)
        Me.LayoutControl1.Controls.Add(Me.TextEdit13)
        Me.LayoutControl1.Controls.Add(Me.TextEdit12)
        Me.LayoutControl1.Controls.Add(Me.TextEdit11)
        Me.LayoutControl1.Controls.Add(Me.TextEdit10)
        Me.LayoutControl1.Controls.Add(Me.TextEdit9)
        Me.LayoutControl1.Controls.Add(Me.TextEdit8)
        Me.LayoutControl1.Controls.Add(Me.TextEdit7)
        Me.LayoutControl1.Controls.Add(Me.TextEdit6)
        Me.LayoutControl1.Controls.Add(Me.TextEdit5)
        Me.LayoutControl1.Controls.Add(Me.TextEdit4)
        Me.LayoutControl1.Controls.Add(Me.TextEdit3)
        Me.LayoutControl1.Controls.Add(Me.TextEdit2)
        Me.LayoutControl1.Controls.Add(Me.TextEdit1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit37)
        Me.LayoutControl1.Controls.Add(Me.TextEdit25)
        Me.LayoutControl1.Controls.Add(Me.TextEdit31)
        Me.LayoutControl1.Controls.Add(Me.TextEdit27)
        Me.LayoutControl1.Controls.Add(Me.TextEdit20)
        Me.LayoutControl1.Controls.Add(Me.TextEdit22)
        Me.LayoutControl1.Controls.Add(Me.TextEdit35)
        Me.LayoutControl1.Controls.Add(Me.TextEdit33)
        Me.LayoutControl1.Controls.Add(Me.TextEdit32)
        Me.LayoutControl1.Controls.Add(Me.TextEdit30)
        Me.LayoutControl1.Controls.Add(Me.TextEdit21)
        Me.LayoutControl1.Controls.Add(Me.TextEdit38)
        Me.LayoutControl1.Controls.Add(Me.TextEdit24)
        Me.LayoutControl1.Controls.Add(Me.TextEdit29)
        Me.LayoutControl1.Controls.Add(Me.TextEdit36)
        Me.LayoutControl1.Controls.Add(Me.TextEdit28)
        Me.LayoutControl1.Controls.Add(Me.TextEdit23)
        Me.LayoutControl1.Controls.Add(Me.TextEdit26)
        Me.LayoutControl1.Controls.Add(Me.TextEdit34)
        Me.LayoutControl1.Controls.Add(Me.GroupControl2)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit2)
        Me.LayoutControl1.Controls.Add(Me.TextEdit46)
        Me.LayoutControl1.Controls.Add(Me.TextEdit48)
        Me.LayoutControl1.Controls.Add(Me.TextEdit47)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit4)
        Me.LayoutControl1.Controls.Add(Me.TextEdit44)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton7)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton8)
        Me.LayoutControl1.Controls.Add(Me.TextEdit45)
        Me.LayoutControl1.Controls.Add(Me.TextEdit53)
        Me.LayoutControl1.Controls.Add(Me.TextEdit52)
        Me.LayoutControl1.Controls.Add(Me.TextEdit49)
        Me.LayoutControl1.Controls.Add(Me.TextEdit51)
        Me.LayoutControl1.Controls.Add(Me.TextEdit50)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit5)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton10)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton9)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(551, 337, 326, 311)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'RadioGroup2
        '
        Me.RadioGroup2.Location = New System.Drawing.Point(120, 159)
        Me.RadioGroup2.Name = "RadioGroup2"
        Me.RadioGroup2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioGroup2.Properties.Appearance.Options.UseFont = True
        Me.RadioGroup2.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "OS"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Application")})
        Me.RadioGroup2.Size = New System.Drawing.Size(365, 64)
        Me.RadioGroup2.StyleController = Me.LayoutControl1
        Me.RadioGroup2.TabIndex = 46
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.LayoutControl6)
        Me.GroupControl3.Location = New System.Drawing.Point(24, 49)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(461, 106)
        Me.GroupControl3.TabIndex = 45
        Me.GroupControl3.Text = "Choose Date"
        '
        'LayoutControl6
        '
        Me.LayoutControl6.Controls.Add(Me.DateEdit6)
        Me.LayoutControl6.Controls.Add(Me.DateEdit5)
        Me.LayoutControl6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl6.Location = New System.Drawing.Point(2, 23)
        Me.LayoutControl6.Name = "LayoutControl6"
        Me.LayoutControl6.Root = Me.LayoutControlGroup9
        Me.LayoutControl6.Size = New System.Drawing.Size(457, 81)
        Me.LayoutControl6.TabIndex = 0
        Me.LayoutControl6.Text = "LayoutControl6"
        '
        'DateEdit6
        '
        Me.DateEdit6.EditValue = Nothing
        Me.DateEdit6.Location = New System.Drawing.Point(95, 38)
        Me.DateEdit6.Name = "DateEdit6"
        Me.DateEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateEdit6.Properties.Appearance.Options.UseFont = True
        Me.DateEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit6.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit6.Size = New System.Drawing.Size(350, 22)
        Me.DateEdit6.StyleController = Me.LayoutControl6
        Me.DateEdit6.TabIndex = 5
        '
        'DateEdit5
        '
        Me.DateEdit5.EditValue = Nothing
        Me.DateEdit5.Location = New System.Drawing.Point(95, 12)
        Me.DateEdit5.Name = "DateEdit5"
        Me.DateEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateEdit5.Properties.Appearance.Options.UseFont = True
        Me.DateEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Size = New System.Drawing.Size(350, 22)
        Me.DateEdit5.StyleController = Me.LayoutControl6
        Me.DateEdit5.TabIndex = 4
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup9.GroupBordersVisible = False
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem87, Me.LayoutControlItem88})
        Me.LayoutControlGroup9.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup9.Name = "LayoutControlGroup9"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(457, 81)
        Me.LayoutControlGroup9.TextVisible = False
        '
        'LayoutControlItem87
        '
        Me.LayoutControlItem87.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem87.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem87.Control = Me.DateEdit5
        Me.LayoutControlItem87.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem87.Name = "LayoutControlItem87"
        Me.LayoutControlItem87.Size = New System.Drawing.Size(437, 26)
        Me.LayoutControlItem87.Text = "วันที่ซื้อ"
        Me.LayoutControlItem87.TextSize = New System.Drawing.Size(80, 16)
        '
        'LayoutControlItem88
        '
        Me.LayoutControlItem88.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem88.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem88.Control = Me.DateEdit6
        Me.LayoutControlItem88.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem88.Name = "LayoutControlItem88"
        Me.LayoutControlItem88.Size = New System.Drawing.Size(437, 35)
        Me.LayoutControlItem88.Text = "หมดประกันเมื่อ"
        Me.LayoutControlItem88.TextSize = New System.Drawing.Size(80, 16)
        '
        'SimpleButton12
        '
        Me.SimpleButton12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton12.Appearance.Options.UseFont = True
        Me.SimpleButton12.Location = New System.Drawing.Point(24, 434)
        Me.SimpleButton12.Name = "SimpleButton12"
        Me.SimpleButton12.Size = New System.Drawing.Size(226, 23)
        Me.SimpleButton12.StyleController = Me.LayoutControl1
        Me.SimpleButton12.TabIndex = 44
        Me.SimpleButton12.Text = "Save"
        '
        'SimpleButton11
        '
        Me.SimpleButton11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton11.Appearance.Options.UseFont = True
        Me.SimpleButton11.Location = New System.Drawing.Point(254, 434)
        Me.SimpleButton11.Name = "SimpleButton11"
        Me.SimpleButton11.Size = New System.Drawing.Size(231, 23)
        Me.SimpleButton11.StyleController = Me.LayoutControl1
        Me.SimpleButton11.TabIndex = 43
        Me.SimpleButton11.Text = "Cancel"
        '
        'MemoEdit6
        '
        Me.MemoEdit6.Location = New System.Drawing.Point(120, 341)
        Me.MemoEdit6.Name = "MemoEdit6"
        Me.MemoEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MemoEdit6.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit6.Size = New System.Drawing.Size(365, 79)
        Me.MemoEdit6.StyleController = Me.LayoutControl1
        Me.MemoEdit6.TabIndex = 42
        '
        'TextEdit57
        '
        Me.TextEdit57.Location = New System.Drawing.Point(120, 279)
        Me.TextEdit57.Name = "TextEdit57"
        Me.TextEdit57.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit57.Properties.Appearance.Options.UseFont = True
        Me.TextEdit57.Size = New System.Drawing.Size(365, 22)
        Me.TextEdit57.StyleController = Me.LayoutControl1
        Me.TextEdit57.TabIndex = 41
        '
        'TextEdit56
        '
        Me.TextEdit56.Location = New System.Drawing.Point(120, 253)
        Me.TextEdit56.Name = "TextEdit56"
        Me.TextEdit56.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit56.Properties.Appearance.Options.UseFont = True
        Me.TextEdit56.Size = New System.Drawing.Size(365, 22)
        Me.TextEdit56.StyleController = Me.LayoutControl1
        Me.TextEdit56.TabIndex = 40
        '
        'TextEdit55
        '
        Me.TextEdit55.Location = New System.Drawing.Point(120, 227)
        Me.TextEdit55.Name = "TextEdit55"
        Me.TextEdit55.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit55.Properties.Appearance.Options.UseFont = True
        Me.TextEdit55.Size = New System.Drawing.Size(365, 22)
        Me.TextEdit55.StyleController = Me.LayoutControl1
        Me.TextEdit55.TabIndex = 39
        '
        'TextEdit54
        '
        Me.TextEdit54.Location = New System.Drawing.Point(120, 305)
        Me.TextEdit54.Name = "TextEdit54"
        Me.TextEdit54.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit54.Properties.Appearance.Options.UseFont = True
        Me.TextEdit54.Size = New System.Drawing.Size(365, 22)
        Me.TextEdit54.StyleController = Me.LayoutControl1
        Me.TextEdit54.TabIndex = 38
        '
        'TextEdit43
        '
        Me.TextEdit43.Location = New System.Drawing.Point(120, 311)
        Me.TextEdit43.Name = "TextEdit43"
        Me.TextEdit43.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit43.Properties.Appearance.Options.UseFont = True
        Me.TextEdit43.Size = New System.Drawing.Size(375, 22)
        Me.TextEdit43.StyleController = Me.LayoutControl1
        Me.TextEdit43.TabIndex = 37
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(120, 49)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioGroup1.Properties.Appearance.Options.UseFont = True
        Me.RadioGroup1.Properties.Columns = 1
        Me.RadioGroup1.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.[Default]
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Laser Printer"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Dot Matrix"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ink Jet")})
        Me.RadioGroup1.Size = New System.Drawing.Size(375, 154)
        Me.RadioGroup1.StyleController = Me.LayoutControl1
        Me.RadioGroup1.TabIndex = 36
        '
        'SimpleButton6
        '
        Me.SimpleButton6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton6.Appearance.Options.UseFont = True
        Me.SimpleButton6.Location = New System.Drawing.Point(237, 498)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(258, 23)
        Me.SimpleButton6.StyleController = Me.LayoutControl1
        Me.SimpleButton6.TabIndex = 35
        Me.SimpleButton6.Text = "Cancel"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton5.Appearance.Options.UseFont = True
        Me.SimpleButton5.Location = New System.Drawing.Point(24, 498)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(209, 23)
        Me.SimpleButton5.StyleController = Me.LayoutControl1
        Me.SimpleButton5.TabIndex = 34
        Me.SimpleButton5.Text = "Save"
        '
        'MemoEdit3
        '
        Me.MemoEdit3.Location = New System.Drawing.Point(120, 347)
        Me.MemoEdit3.Name = "MemoEdit3"
        Me.MemoEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MemoEdit3.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit3.Size = New System.Drawing.Size(375, 137)
        Me.MemoEdit3.StyleController = Me.LayoutControl1
        Me.MemoEdit3.TabIndex = 33
        '
        'TextEdit42
        '
        Me.TextEdit42.Location = New System.Drawing.Point(120, 259)
        Me.TextEdit42.Name = "TextEdit42"
        Me.TextEdit42.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit42.Properties.Appearance.Options.UseFont = True
        Me.TextEdit42.Size = New System.Drawing.Size(375, 22)
        Me.TextEdit42.StyleController = Me.LayoutControl1
        Me.TextEdit42.TabIndex = 32
        '
        'TextEdit41
        '
        Me.TextEdit41.Location = New System.Drawing.Point(120, 233)
        Me.TextEdit41.Name = "TextEdit41"
        Me.TextEdit41.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit41.Properties.Appearance.Options.UseFont = True
        Me.TextEdit41.Size = New System.Drawing.Size(375, 22)
        Me.TextEdit41.StyleController = Me.LayoutControl1
        Me.TextEdit41.TabIndex = 31
        '
        'TextEdit40
        '
        Me.TextEdit40.Location = New System.Drawing.Point(120, 207)
        Me.TextEdit40.Name = "TextEdit40"
        Me.TextEdit40.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit40.Properties.Appearance.Options.UseFont = True
        Me.TextEdit40.Size = New System.Drawing.Size(375, 22)
        Me.TextEdit40.StyleController = Me.LayoutControl1
        Me.TextEdit40.TabIndex = 30
        '
        'TextEdit39
        '
        Me.TextEdit39.Location = New System.Drawing.Point(120, 285)
        Me.TextEdit39.Name = "TextEdit39"
        Me.TextEdit39.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit39.Properties.Appearance.Options.UseFont = True
        Me.TextEdit39.Size = New System.Drawing.Size(375, 22)
        Me.TextEdit39.StyleController = Me.LayoutControl1
        Me.TextEdit39.TabIndex = 29
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(924, 179)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(176, 23)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 28
        Me.SimpleButton3.Text = "Save"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(1075, 174)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(181, 23)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 27
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(890, 174)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(181, 23)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 26
        Me.SimpleButton1.Text = "Save"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(1104, 179)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(152, 23)
        Me.SimpleButton4.StyleController = Me.LayoutControl1
        Me.SimpleButton4.TabIndex = 27
        Me.SimpleButton4.Text = "Cancel"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(526, 241)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Size = New System.Drawing.Size(300, 128)
        Me.MemoEdit1.StyleController = Me.LayoutControl1
        Me.MemoEdit1.TabIndex = 25
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.LayoutControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(890, 49)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(366, 121)
        Me.GroupControl1.TabIndex = 24
        Me.GroupControl1.Text = "เลือกวันที่"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.LayoutControl3)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(2, 23)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup8
        Me.LayoutControl2.Size = New System.Drawing.Size(362, 96)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.DateEdit2)
        Me.LayoutControl3.Controls.Add(Me.DateEdit1)
        Me.LayoutControl3.Location = New System.Drawing.Point(12, 12)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.Root
        Me.LayoutControl3.Size = New System.Drawing.Size(338, 72)
        Me.LayoutControl3.TabIndex = 4
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(75, 38)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(251, 22)
        Me.DateEdit2.StyleController = Me.LayoutControl3
        Me.DateEdit2.TabIndex = 5
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(75, 12)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(251, 22)
        Me.DateEdit1.StyleController = Me.LayoutControl3
        Me.DateEdit1.TabIndex = 4
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem23, Me.LayoutControlItem24})
        Me.Root.Location = New System.Drawing.Point(0, 0)
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(338, 72)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem23.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem23.Control = Me.DateEdit1
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(318, 26)
        Me.LayoutControlItem23.Text = "วันที่ซื้อ"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(60, 16)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem24.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem24.Control = Me.DateEdit2
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(318, 26)
        Me.LayoutControlItem24.Text = "หมดประกัน"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(60, 16)
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup8.GroupBordersVisible = False
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem22})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(362, 96)
        Me.LayoutControlGroup8.TextVisible = False
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.LayoutControl3
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(342, 76)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'TextEdit19
        '
        Me.TextEdit19.Location = New System.Drawing.Point(526, 205)
        Me.TextEdit19.Name = "TextEdit19"
        Me.TextEdit19.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit19.Properties.Appearance.Options.UseFont = True
        Me.TextEdit19.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit19.StyleController = Me.LayoutControl1
        Me.TextEdit19.TabIndex = 23
        '
        'TextEdit18
        '
        Me.TextEdit18.Location = New System.Drawing.Point(526, 383)
        Me.TextEdit18.Name = "TextEdit18"
        Me.TextEdit18.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit18.Properties.Appearance.Options.UseFont = True
        Me.TextEdit18.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit18.StyleController = Me.LayoutControl1
        Me.TextEdit18.TabIndex = 21
        '
        'TextEdit17
        '
        Me.TextEdit17.Location = New System.Drawing.Point(526, 179)
        Me.TextEdit17.Name = "TextEdit17"
        Me.TextEdit17.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit17.Properties.Appearance.Options.UseFont = True
        Me.TextEdit17.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit17.StyleController = Me.LayoutControl1
        Me.TextEdit17.TabIndex = 20
        '
        'TextEdit16
        '
        Me.TextEdit16.Location = New System.Drawing.Point(526, 153)
        Me.TextEdit16.Name = "TextEdit16"
        Me.TextEdit16.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit16.Properties.Appearance.Options.UseFont = True
        Me.TextEdit16.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit16.StyleController = Me.LayoutControl1
        Me.TextEdit16.TabIndex = 19
        '
        'TextEdit15
        '
        Me.TextEdit15.Location = New System.Drawing.Point(526, 127)
        Me.TextEdit15.Name = "TextEdit15"
        Me.TextEdit15.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit15.Properties.Appearance.Options.UseFont = True
        Me.TextEdit15.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit15.StyleController = Me.LayoutControl1
        Me.TextEdit15.TabIndex = 18
        '
        'TextEdit14
        '
        Me.TextEdit14.Location = New System.Drawing.Point(526, 101)
        Me.TextEdit14.Name = "TextEdit14"
        Me.TextEdit14.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit14.Properties.Appearance.Options.UseFont = True
        Me.TextEdit14.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit14.StyleController = Me.LayoutControl1
        Me.TextEdit14.TabIndex = 17
        '
        'TextEdit13
        '
        Me.TextEdit13.Location = New System.Drawing.Point(526, 75)
        Me.TextEdit13.Name = "TextEdit13"
        Me.TextEdit13.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit13.Properties.Appearance.Options.UseFont = True
        Me.TextEdit13.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit13.StyleController = Me.LayoutControl1
        Me.TextEdit13.TabIndex = 16
        '
        'TextEdit12
        '
        Me.TextEdit12.Location = New System.Drawing.Point(526, 49)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit12.Properties.Appearance.Options.UseFont = True
        Me.TextEdit12.Size = New System.Drawing.Size(300, 22)
        Me.TextEdit12.StyleController = Me.LayoutControl1
        Me.TextEdit12.TabIndex = 15
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(120, 309)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit11.Properties.Appearance.Options.UseFont = True
        Me.TextEdit11.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit11.StyleController = Me.LayoutControl1
        Me.TextEdit11.TabIndex = 14
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(120, 283)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit10.Properties.Appearance.Options.UseFont = True
        Me.TextEdit10.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit10.StyleController = Me.LayoutControl1
        Me.TextEdit10.TabIndex = 13
        '
        'TextEdit9
        '
        Me.TextEdit9.Location = New System.Drawing.Point(120, 257)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit9.Properties.Appearance.Options.UseFont = True
        Me.TextEdit9.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit9.StyleController = Me.LayoutControl1
        Me.TextEdit9.TabIndex = 12
        '
        'TextEdit8
        '
        Me.TextEdit8.Location = New System.Drawing.Point(120, 231)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit8.Properties.Appearance.Options.UseFont = True
        Me.TextEdit8.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit8.StyleController = Me.LayoutControl1
        Me.TextEdit8.TabIndex = 11
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(120, 205)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit7.Properties.Appearance.Options.UseFont = True
        Me.TextEdit7.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit7.StyleController = Me.LayoutControl1
        Me.TextEdit7.TabIndex = 10
        '
        'TextEdit6
        '
        Me.TextEdit6.Location = New System.Drawing.Point(120, 179)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit6.StyleController = Me.LayoutControl1
        Me.TextEdit6.TabIndex = 9
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(120, 153)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit5.Properties.Appearance.Options.UseFont = True
        Me.TextEdit5.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit5.StyleController = Me.LayoutControl1
        Me.TextEdit5.TabIndex = 8
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(120, 127)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit4.Properties.Appearance.Options.UseFont = True
        Me.TextEdit4.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit4.StyleController = Me.LayoutControl1
        Me.TextEdit4.TabIndex = 7
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(120, 101)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit3.StyleController = Me.LayoutControl1
        Me.TextEdit3.TabIndex = 6
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(120, 49)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit2.StyleController = Me.LayoutControl1
        Me.TextEdit2.TabIndex = 5
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(120, 75)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Size = New System.Drawing.Size(254, 22)
        Me.TextEdit1.StyleController = Me.LayoutControl1
        Me.TextEdit1.TabIndex = 4
        '
        'TextEdit37
        '
        Me.TextEdit37.Location = New System.Drawing.Point(120, 75)
        Me.TextEdit37.Name = "TextEdit37"
        Me.TextEdit37.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit37.Properties.Appearance.Options.UseFont = True
        Me.TextEdit37.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit37.StyleController = Me.LayoutControl1
        Me.TextEdit37.TabIndex = 4
        '
        'TextEdit25
        '
        Me.TextEdit25.Location = New System.Drawing.Point(120, 49)
        Me.TextEdit25.Name = "TextEdit25"
        Me.TextEdit25.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit25.Properties.Appearance.Options.UseFont = True
        Me.TextEdit25.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit25.StyleController = Me.LayoutControl1
        Me.TextEdit25.TabIndex = 5
        '
        'TextEdit31
        '
        Me.TextEdit31.Location = New System.Drawing.Point(120, 101)
        Me.TextEdit31.Name = "TextEdit31"
        Me.TextEdit31.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit31.Properties.Appearance.Options.UseFont = True
        Me.TextEdit31.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit31.StyleController = Me.LayoutControl1
        Me.TextEdit31.TabIndex = 6
        '
        'TextEdit27
        '
        Me.TextEdit27.Location = New System.Drawing.Point(120, 127)
        Me.TextEdit27.Name = "TextEdit27"
        Me.TextEdit27.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit27.Properties.Appearance.Options.UseFont = True
        Me.TextEdit27.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit27.StyleController = Me.LayoutControl1
        Me.TextEdit27.TabIndex = 7
        '
        'TextEdit20
        '
        Me.TextEdit20.Location = New System.Drawing.Point(120, 153)
        Me.TextEdit20.Name = "TextEdit20"
        Me.TextEdit20.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit20.Properties.Appearance.Options.UseFont = True
        Me.TextEdit20.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit20.StyleController = Me.LayoutControl1
        Me.TextEdit20.TabIndex = 8
        '
        'TextEdit22
        '
        Me.TextEdit22.Location = New System.Drawing.Point(120, 179)
        Me.TextEdit22.Name = "TextEdit22"
        Me.TextEdit22.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit22.Properties.Appearance.Options.UseFont = True
        Me.TextEdit22.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit22.StyleController = Me.LayoutControl1
        Me.TextEdit22.TabIndex = 9
        '
        'TextEdit35
        '
        Me.TextEdit35.Location = New System.Drawing.Point(120, 205)
        Me.TextEdit35.Name = "TextEdit35"
        Me.TextEdit35.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit35.Properties.Appearance.Options.UseFont = True
        Me.TextEdit35.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit35.StyleController = Me.LayoutControl1
        Me.TextEdit35.TabIndex = 10
        '
        'TextEdit33
        '
        Me.TextEdit33.Location = New System.Drawing.Point(120, 231)
        Me.TextEdit33.Name = "TextEdit33"
        Me.TextEdit33.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit33.Properties.Appearance.Options.UseFont = True
        Me.TextEdit33.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit33.StyleController = Me.LayoutControl1
        Me.TextEdit33.TabIndex = 11
        '
        'TextEdit32
        '
        Me.TextEdit32.Location = New System.Drawing.Point(120, 257)
        Me.TextEdit32.Name = "TextEdit32"
        Me.TextEdit32.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit32.Properties.Appearance.Options.UseFont = True
        Me.TextEdit32.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit32.StyleController = Me.LayoutControl1
        Me.TextEdit32.TabIndex = 12
        '
        'TextEdit30
        '
        Me.TextEdit30.Location = New System.Drawing.Point(120, 283)
        Me.TextEdit30.Name = "TextEdit30"
        Me.TextEdit30.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit30.Properties.Appearance.Options.UseFont = True
        Me.TextEdit30.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit30.StyleController = Me.LayoutControl1
        Me.TextEdit30.TabIndex = 13
        '
        'TextEdit21
        '
        Me.TextEdit21.Location = New System.Drawing.Point(120, 309)
        Me.TextEdit21.Name = "TextEdit21"
        Me.TextEdit21.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit21.Properties.Appearance.Options.UseFont = True
        Me.TextEdit21.Size = New System.Drawing.Size(259, 22)
        Me.TextEdit21.StyleController = Me.LayoutControl1
        Me.TextEdit21.TabIndex = 14
        '
        'TextEdit38
        '
        Me.TextEdit38.Location = New System.Drawing.Point(522, 49)
        Me.TextEdit38.Name = "TextEdit38"
        Me.TextEdit38.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit38.Properties.Appearance.Options.UseFont = True
        Me.TextEdit38.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit38.StyleController = Me.LayoutControl1
        Me.TextEdit38.TabIndex = 15
        '
        'TextEdit24
        '
        Me.TextEdit24.Location = New System.Drawing.Point(522, 75)
        Me.TextEdit24.Name = "TextEdit24"
        Me.TextEdit24.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit24.Properties.Appearance.Options.UseFont = True
        Me.TextEdit24.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit24.StyleController = Me.LayoutControl1
        Me.TextEdit24.TabIndex = 16
        '
        'TextEdit29
        '
        Me.TextEdit29.Location = New System.Drawing.Point(522, 101)
        Me.TextEdit29.Name = "TextEdit29"
        Me.TextEdit29.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit29.Properties.Appearance.Options.UseFont = True
        Me.TextEdit29.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit29.StyleController = Me.LayoutControl1
        Me.TextEdit29.TabIndex = 17
        '
        'TextEdit36
        '
        Me.TextEdit36.Location = New System.Drawing.Point(522, 127)
        Me.TextEdit36.Name = "TextEdit36"
        Me.TextEdit36.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit36.Properties.Appearance.Options.UseFont = True
        Me.TextEdit36.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit36.StyleController = Me.LayoutControl1
        Me.TextEdit36.TabIndex = 18
        '
        'TextEdit28
        '
        Me.TextEdit28.Location = New System.Drawing.Point(522, 153)
        Me.TextEdit28.Name = "TextEdit28"
        Me.TextEdit28.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit28.Properties.Appearance.Options.UseFont = True
        Me.TextEdit28.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit28.StyleController = Me.LayoutControl1
        Me.TextEdit28.TabIndex = 19
        '
        'TextEdit23
        '
        Me.TextEdit23.Location = New System.Drawing.Point(522, 179)
        Me.TextEdit23.Name = "TextEdit23"
        Me.TextEdit23.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit23.Properties.Appearance.Options.UseFont = True
        Me.TextEdit23.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit23.StyleController = Me.LayoutControl1
        Me.TextEdit23.TabIndex = 20
        '
        'TextEdit26
        '
        Me.TextEdit26.Location = New System.Drawing.Point(522, 442)
        Me.TextEdit26.Name = "TextEdit26"
        Me.TextEdit26.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit26.Properties.Appearance.Options.UseFont = True
        Me.TextEdit26.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit26.StyleController = Me.LayoutControl1
        Me.TextEdit26.TabIndex = 21
        '
        'TextEdit34
        '
        Me.TextEdit34.Location = New System.Drawing.Point(522, 205)
        Me.TextEdit34.Name = "TextEdit34"
        Me.TextEdit34.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextEdit34.Properties.Appearance.Options.UseFont = True
        Me.TextEdit34.Size = New System.Drawing.Size(316, 22)
        Me.TextEdit34.StyleController = Me.LayoutControl1
        Me.TextEdit34.TabIndex = 23
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.LayoutControl4)
        Me.GroupControl2.Location = New System.Drawing.Point(924, 49)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(332, 126)
        Me.GroupControl2.TabIndex = 24
        Me.GroupControl2.Text = "เลือกวันที่"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.LayoutControl5)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(2, 23)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup11
        Me.LayoutControl4.Size = New System.Drawing.Size(328, 101)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl2"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.DateEdit3)
        Me.LayoutControl5.Controls.Add(Me.DateEdit4)
        Me.LayoutControl5.Location = New System.Drawing.Point(12, 12)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup10
        Me.LayoutControl5.Size = New System.Drawing.Size(304, 77)
        Me.LayoutControl5.TabIndex = 4
        Me.LayoutControl5.Text = "LayoutControl3"
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = Nothing
        Me.DateEdit3.Location = New System.Drawing.Point(75, 38)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateEdit3.Properties.Appearance.Options.UseFont = True
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Size = New System.Drawing.Size(217, 22)
        Me.DateEdit3.StyleController = Me.LayoutControl5
        Me.DateEdit3.TabIndex = 5
        '
        'DateEdit4
        '
        Me.DateEdit4.EditValue = Nothing
        Me.DateEdit4.Location = New System.Drawing.Point(75, 12)
        Me.DateEdit4.Name = "DateEdit4"
        Me.DateEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateEdit4.Properties.Appearance.Options.UseFont = True
        Me.DateEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Size = New System.Drawing.Size(217, 22)
        Me.DateEdit4.StyleController = Me.LayoutControl5
        Me.DateEdit4.TabIndex = 4
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup10.GroupBordersVisible = False
        Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem27, Me.LayoutControlItem28})
        Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup10.Name = "Root"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(304, 77)
        Me.LayoutControlGroup10.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem27.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem27.Control = Me.DateEdit4
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem27.Name = "LayoutControlItem23"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(284, 26)
        Me.LayoutControlItem27.Text = "วันที่ซื้อ"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(60, 16)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem28.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem28.Control = Me.DateEdit3
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem28.Name = "LayoutControlItem24"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(284, 31)
        Me.LayoutControlItem28.Text = "หมดประกัน"
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(60, 16)
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup11.GroupBordersVisible = False
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem29})
        Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup11.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(328, 101)
        Me.LayoutControlGroup11.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.LayoutControl5
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem22"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(308, 81)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = False
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(522, 244)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MemoEdit2.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit2.Size = New System.Drawing.Size(316, 184)
        Me.MemoEdit2.StyleController = Me.LayoutControl1
        Me.MemoEdit2.TabIndex = 25
        '
        'TextEdit46
        '
        Me.TextEdit46.Location = New System.Drawing.Point(120, 49)
        Me.TextEdit46.Name = "TextEdit46"
        Me.TextEdit46.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit46.Properties.Appearance.Options.UseFont = True
        Me.TextEdit46.Size = New System.Drawing.Size(361, 22)
        Me.TextEdit46.StyleController = Me.LayoutControl1
        Me.TextEdit46.TabIndex = 30
        '
        'TextEdit48
        '
        Me.TextEdit48.Location = New System.Drawing.Point(120, 75)
        Me.TextEdit48.Name = "TextEdit48"
        Me.TextEdit48.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit48.Properties.Appearance.Options.UseFont = True
        Me.TextEdit48.Size = New System.Drawing.Size(361, 22)
        Me.TextEdit48.StyleController = Me.LayoutControl1
        Me.TextEdit48.TabIndex = 31
        '
        'TextEdit47
        '
        Me.TextEdit47.Location = New System.Drawing.Point(120, 101)
        Me.TextEdit47.Name = "TextEdit47"
        Me.TextEdit47.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit47.Properties.Appearance.Options.UseFont = True
        Me.TextEdit47.Size = New System.Drawing.Size(361, 22)
        Me.TextEdit47.StyleController = Me.LayoutControl1
        Me.TextEdit47.TabIndex = 32
        '
        'MemoEdit4
        '
        Me.MemoEdit4.Location = New System.Drawing.Point(120, 189)
        Me.MemoEdit4.Name = "MemoEdit4"
        Me.MemoEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MemoEdit4.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit4.Size = New System.Drawing.Size(361, 125)
        Me.MemoEdit4.StyleController = Me.LayoutControl1
        Me.MemoEdit4.TabIndex = 33
        '
        'TextEdit44
        '
        Me.TextEdit44.Location = New System.Drawing.Point(120, 127)
        Me.TextEdit44.Name = "TextEdit44"
        Me.TextEdit44.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit44.Properties.Appearance.Options.UseFont = True
        Me.TextEdit44.Size = New System.Drawing.Size(361, 22)
        Me.TextEdit44.StyleController = Me.LayoutControl1
        Me.TextEdit44.TabIndex = 29
        '
        'SimpleButton7
        '
        Me.SimpleButton7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton7.Appearance.Options.UseFont = True
        Me.SimpleButton7.Location = New System.Drawing.Point(24, 328)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(212, 23)
        Me.SimpleButton7.StyleController = Me.LayoutControl1
        Me.SimpleButton7.TabIndex = 34
        Me.SimpleButton7.Text = "Save"
        '
        'SimpleButton8
        '
        Me.SimpleButton8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton8.Appearance.Options.UseFont = True
        Me.SimpleButton8.Location = New System.Drawing.Point(240, 328)
        Me.SimpleButton8.Name = "SimpleButton8"
        Me.SimpleButton8.Size = New System.Drawing.Size(241, 23)
        Me.SimpleButton8.StyleController = Me.LayoutControl1
        Me.SimpleButton8.TabIndex = 35
        Me.SimpleButton8.Text = "Cancel"
        '
        'TextEdit45
        '
        Me.TextEdit45.Location = New System.Drawing.Point(120, 153)
        Me.TextEdit45.Name = "TextEdit45"
        Me.TextEdit45.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit45.Properties.Appearance.Options.UseFont = True
        Me.TextEdit45.Size = New System.Drawing.Size(361, 22)
        Me.TextEdit45.StyleController = Me.LayoutControl1
        Me.TextEdit45.TabIndex = 37
        '
        'TextEdit53
        '
        Me.TextEdit53.Location = New System.Drawing.Point(120, 49)
        Me.TextEdit53.Name = "TextEdit53"
        Me.TextEdit53.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit53.Properties.Appearance.Options.UseFont = True
        Me.TextEdit53.Size = New System.Drawing.Size(348, 22)
        Me.TextEdit53.StyleController = Me.LayoutControl1
        Me.TextEdit53.TabIndex = 30
        '
        'TextEdit52
        '
        Me.TextEdit52.Location = New System.Drawing.Point(120, 75)
        Me.TextEdit52.Name = "TextEdit52"
        Me.TextEdit52.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit52.Properties.Appearance.Options.UseFont = True
        Me.TextEdit52.Size = New System.Drawing.Size(348, 22)
        Me.TextEdit52.StyleController = Me.LayoutControl1
        Me.TextEdit52.TabIndex = 31
        '
        'TextEdit49
        '
        Me.TextEdit49.Location = New System.Drawing.Point(120, 101)
        Me.TextEdit49.Name = "TextEdit49"
        Me.TextEdit49.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit49.Properties.Appearance.Options.UseFont = True
        Me.TextEdit49.Size = New System.Drawing.Size(348, 22)
        Me.TextEdit49.StyleController = Me.LayoutControl1
        Me.TextEdit49.TabIndex = 32
        '
        'TextEdit51
        '
        Me.TextEdit51.Location = New System.Drawing.Point(120, 127)
        Me.TextEdit51.Name = "TextEdit51"
        Me.TextEdit51.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit51.Properties.Appearance.Options.UseFont = True
        Me.TextEdit51.Size = New System.Drawing.Size(348, 22)
        Me.TextEdit51.StyleController = Me.LayoutControl1
        Me.TextEdit51.TabIndex = 29
        '
        'TextEdit50
        '
        Me.TextEdit50.Location = New System.Drawing.Point(120, 153)
        Me.TextEdit50.Name = "TextEdit50"
        Me.TextEdit50.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit50.Properties.Appearance.Options.UseFont = True
        Me.TextEdit50.Size = New System.Drawing.Size(348, 22)
        Me.TextEdit50.StyleController = Me.LayoutControl1
        Me.TextEdit50.TabIndex = 37
        '
        'MemoEdit5
        '
        Me.MemoEdit5.Location = New System.Drawing.Point(120, 189)
        Me.MemoEdit5.Name = "MemoEdit5"
        Me.MemoEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MemoEdit5.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit5.Size = New System.Drawing.Size(348, 110)
        Me.MemoEdit5.StyleController = Me.LayoutControl1
        Me.MemoEdit5.TabIndex = 33
        '
        'SimpleButton10
        '
        Me.SimpleButton10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton10.Appearance.Options.UseFont = True
        Me.SimpleButton10.Location = New System.Drawing.Point(24, 313)
        Me.SimpleButton10.Name = "SimpleButton10"
        Me.SimpleButton10.Size = New System.Drawing.Size(205, 23)
        Me.SimpleButton10.StyleController = Me.LayoutControl1
        Me.SimpleButton10.TabIndex = 34
        Me.SimpleButton10.Text = "Save"
        '
        'SimpleButton9
        '
        Me.SimpleButton9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton9.Appearance.Options.UseFont = True
        Me.SimpleButton9.Location = New System.Drawing.Point(233, 313)
        Me.SimpleButton9.Name = "SimpleButton9"
        Me.SimpleButton9.Size = New System.Drawing.Size(235, 23)
        Me.SimpleButton9.StyleController = Me.LayoutControl1
        Me.SimpleButton9.TabIndex = 35
        Me.SimpleButton9.Text = "Cancel"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.tabbedControlGroup1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'tabbedControlGroup1
        '
        Me.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1"
        Me.tabbedControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.tabbedControlGroup1.Name = "tabbedControlGroup1"
        Me.tabbedControlGroup1.SelectedTabPage = Me.layoutControlGroup3
        Me.tabbedControlGroup1.SelectedTabPageIndex = 0
        Me.tabbedControlGroup1.Size = New System.Drawing.Size(1260, 780)
        Me.tabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutControlGroup3, Me.layoutControlGroup4, Me.LayoutControlGroup2, Me.LayoutControlGroup5, Me.LayoutControlGroup6, Me.LayoutControlGroup7})
        '
        'layoutControlGroup3
        '
        Me.layoutControlGroup3.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup3.AppearanceGroup.Options.UseFont = True
        Me.layoutControlGroup3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup3.AppearanceItemCaption.Options.UseFont = True
        Me.layoutControlGroup3.AppearanceTabPage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup3.AppearanceTabPage.HeaderActive.Options.UseFont = True
        Me.layoutControlGroup3.CustomizationFormText = "Tab1"
        Me.layoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.EmptySpaceItem1, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem20, Me.EmptySpaceItem2, Me.LayoutControlItem21, Me.LayoutControlItem19, Me.EmptySpaceItem4, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.EmptySpaceItem3, Me.EmptySpaceItem5, Me.EmptySpaceItem6})
        Me.layoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.layoutControlGroup3.Name = "layoutControlGroup3"
        Me.layoutControlGroup3.Size = New System.Drawing.Size(1236, 731)
        Me.layoutControlGroup3.Text = "Computer"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.TextEdit1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem1.Text = "Model"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.TextEdit2
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem2.Text = "Brand"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.TextEdit3
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem3.Text = "CPU*"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.TextEdit4
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem4.Text = "Mainboard"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.TextEdit5
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 104)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem5.Text = "Ram"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.TextEdit6
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 130)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem6.Text = "Harddisk"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.TextEdit7
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 156)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem7.Text = "VGA Card"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.TextEdit8
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 182)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem8.Text = "Power Supply"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.TextEdit9
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 208)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem9.Text = "CD/DVD"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.TextEdit10
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 234)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem10.Text = "Case"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.TextEdit11
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 260)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(354, 471)
        Me.LayoutControlItem11.Text = "Serial Number"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(354, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(52, 731)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.TextEdit12
        Me.LayoutControlItem12.Location = New System.Drawing.Point(406, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem12.Text = "Computer Name"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.TextEdit13
        Me.LayoutControlItem13.Location = New System.Drawing.Point(406, 26)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem13.Text = "IP Address"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.TextEdit14
        Me.LayoutControlItem14.Location = New System.Drawing.Point(406, 52)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem14.Text = "Windows"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.TextEdit15
        Me.LayoutControlItem15.Location = New System.Drawing.Point(406, 78)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem15.Text = "Office"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.TextEdit16
        Me.LayoutControlItem16.Location = New System.Drawing.Point(406, 104)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem16.Text = "Other"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.TextEdit17
        Me.LayoutControlItem17.Location = New System.Drawing.Point(406, 130)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem17.Text = "Detail"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.TextEdit18
        Me.LayoutControlItem18.Location = New System.Drawing.Point(406, 334)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem18.Text = "ชื่อผู้ขาย"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TextEdit19
        Me.LayoutControlItem20.Location = New System.Drawing.Point(406, 156)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem20.Text = "Price"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(806, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(60, 731)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.GroupControl1
        Me.LayoutControlItem21.Location = New System.Drawing.Point(866, 0)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(370, 125)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem19.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem19.Control = Me.MemoEdit1
        Me.LayoutControlItem19.Location = New System.Drawing.Point(406, 192)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(400, 132)
        Me.LayoutControlItem19.Text = "Note"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(866, 152)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(370, 579)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.SimpleButton1
        Me.LayoutControlItem25.Location = New System.Drawing.Point(866, 125)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(185, 27)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.SimpleButton2
        Me.LayoutControlItem26.Location = New System.Drawing.Point(1051, 125)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(185, 27)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(406, 182)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(400, 10)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(406, 324)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(400, 10)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = False
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(406, 360)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(400, 371)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'layoutControlGroup4
        '
        Me.layoutControlGroup4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup4.AppearanceItemCaption.Options.UseFont = True
        Me.layoutControlGroup4.AppearanceTabPage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup4.AppearanceTabPage.HeaderActive.Options.UseFont = True
        Me.layoutControlGroup4.CustomizationFormText = "Tab2"
        Me.layoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem31, Me.LayoutControlItem30, Me.LayoutControlItem32, Me.LayoutControlItem33, Me.LayoutControlItem34, Me.LayoutControlItem35, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.LayoutControlItem38, Me.LayoutControlItem39, Me.EmptySpaceItem7, Me.LayoutControlItem41, Me.LayoutControlItem40, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.LayoutControlItem44, Me.LayoutControlItem45, Me.LayoutControlItem46, Me.LayoutControlItem48, Me.EmptySpaceItem10, Me.LayoutControlItem50, Me.EmptySpaceItem11, Me.LayoutControlItem47, Me.EmptySpaceItem12, Me.EmptySpaceItem8, Me.LayoutControlItem49, Me.LayoutControlItem52, Me.EmptySpaceItem13, Me.LayoutControlItem51})
        Me.layoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.layoutControlGroup4.Name = "layoutControlGroup4"
        Me.layoutControlGroup4.Size = New System.Drawing.Size(1236, 731)
        Me.layoutControlGroup4.Text = "Computer Rent"
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.TextEdit25
        Me.LayoutControlItem31.CustomizationFormText = "Brand"
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem31.Text = "Brand"
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.TextEdit37
        Me.LayoutControlItem30.CustomizationFormText = "Model"
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem30.Text = "Model"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.TextEdit31
        Me.LayoutControlItem32.CustomizationFormText = "CPU*"
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem32.Text = "CPU*"
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.TextEdit27
        Me.LayoutControlItem33.CustomizationFormText = "Mainboard"
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem33.Text = "Mainboard"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.TextEdit20
        Me.LayoutControlItem34.CustomizationFormText = "Ram"
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 104)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem34.Text = "Ram"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.TextEdit22
        Me.LayoutControlItem35.CustomizationFormText = "Harddisk"
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 130)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem35.Text = "Harddisk"
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.TextEdit35
        Me.LayoutControlItem36.CustomizationFormText = "VGA Card"
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 156)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem36.Text = "VGA Card"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.TextEdit33
        Me.LayoutControlItem37.CustomizationFormText = "Power Supply"
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 182)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem37.Text = "Power Supply"
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.TextEdit32
        Me.LayoutControlItem38.CustomizationFormText = "CD/DVD"
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 208)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem38.Text = "CD/DVD"
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.TextEdit30
        Me.LayoutControlItem39.CustomizationFormText = "Case"
        Me.LayoutControlItem39.Location = New System.Drawing.Point(0, 234)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem39.Text = "Case"
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(359, 0)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(43, 731)
        Me.EmptySpaceItem7.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.TextEdit38
        Me.LayoutControlItem41.CustomizationFormText = "Computer Name"
        Me.LayoutControlItem41.Location = New System.Drawing.Point(402, 0)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem41.Text = "Computer Name"
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.TextEdit21
        Me.LayoutControlItem40.CustomizationFormText = "Serial Number"
        Me.LayoutControlItem40.Location = New System.Drawing.Point(0, 260)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(359, 471)
        Me.LayoutControlItem40.Text = "Serial Number"
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.Control = Me.TextEdit24
        Me.LayoutControlItem42.CustomizationFormText = "IP Address"
        Me.LayoutControlItem42.Location = New System.Drawing.Point(402, 26)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem42.Text = "IP Address"
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.Control = Me.TextEdit29
        Me.LayoutControlItem43.CustomizationFormText = "Windows"
        Me.LayoutControlItem43.Location = New System.Drawing.Point(402, 52)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem43.Text = "Windows"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.Control = Me.TextEdit36
        Me.LayoutControlItem44.CustomizationFormText = "Office"
        Me.LayoutControlItem44.Location = New System.Drawing.Point(402, 78)
        Me.LayoutControlItem44.Name = "LayoutControlItem44"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem44.Text = "Office"
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.Control = Me.TextEdit28
        Me.LayoutControlItem45.CustomizationFormText = "Other"
        Me.LayoutControlItem45.Location = New System.Drawing.Point(402, 104)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem45.Text = "Other"
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem46
        '
        Me.LayoutControlItem46.Control = Me.TextEdit23
        Me.LayoutControlItem46.CustomizationFormText = "Detail"
        Me.LayoutControlItem46.Location = New System.Drawing.Point(402, 130)
        Me.LayoutControlItem46.Name = "LayoutControlItem46"
        Me.LayoutControlItem46.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem46.Text = "Detail"
        Me.LayoutControlItem46.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem48
        '
        Me.LayoutControlItem48.Control = Me.TextEdit34
        Me.LayoutControlItem48.CustomizationFormText = "Price"
        Me.LayoutControlItem48.Location = New System.Drawing.Point(402, 156)
        Me.LayoutControlItem48.Name = "LayoutControlItem48"
        Me.LayoutControlItem48.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem48.Text = "Price"
        Me.LayoutControlItem48.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem10
        '
        Me.EmptySpaceItem10.AllowHotTrack = False
        Me.EmptySpaceItem10.CustomizationFormText = "EmptySpaceItem3"
        Me.EmptySpaceItem10.Location = New System.Drawing.Point(402, 182)
        Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem10.Size = New System.Drawing.Size(416, 13)
        Me.EmptySpaceItem10.Text = "EmptySpaceItem3"
        Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem50
        '
        Me.LayoutControlItem50.Control = Me.MemoEdit2
        Me.LayoutControlItem50.CustomizationFormText = "Note"
        Me.LayoutControlItem50.Location = New System.Drawing.Point(402, 195)
        Me.LayoutControlItem50.Name = "LayoutControlItem50"
        Me.LayoutControlItem50.Size = New System.Drawing.Size(416, 188)
        Me.LayoutControlItem50.Text = "Note"
        Me.LayoutControlItem50.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem11
        '
        Me.EmptySpaceItem11.AllowHotTrack = False
        Me.EmptySpaceItem11.CustomizationFormText = "EmptySpaceItem5"
        Me.EmptySpaceItem11.Location = New System.Drawing.Point(402, 383)
        Me.EmptySpaceItem11.Name = "EmptySpaceItem11"
        Me.EmptySpaceItem11.Size = New System.Drawing.Size(416, 10)
        Me.EmptySpaceItem11.Text = "EmptySpaceItem5"
        Me.EmptySpaceItem11.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem47
        '
        Me.LayoutControlItem47.Control = Me.TextEdit26
        Me.LayoutControlItem47.CustomizationFormText = "ชื่อผู้ขาย"
        Me.LayoutControlItem47.Location = New System.Drawing.Point(402, 393)
        Me.LayoutControlItem47.Name = "LayoutControlItem47"
        Me.LayoutControlItem47.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem47.Text = "ชื่อผู้ขาย"
        Me.LayoutControlItem47.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem12
        '
        Me.EmptySpaceItem12.AllowHotTrack = False
        Me.EmptySpaceItem12.CustomizationFormText = "EmptySpaceItem6"
        Me.EmptySpaceItem12.Location = New System.Drawing.Point(402, 419)
        Me.EmptySpaceItem12.Name = "EmptySpaceItem12"
        Me.EmptySpaceItem12.Size = New System.Drawing.Size(416, 312)
        Me.EmptySpaceItem12.Text = "EmptySpaceItem6"
        Me.EmptySpaceItem12.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = False
        Me.EmptySpaceItem8.CustomizationFormText = "EmptySpaceItem2"
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(818, 0)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(82, 731)
        Me.EmptySpaceItem8.Text = "EmptySpaceItem2"
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem49
        '
        Me.LayoutControlItem49.Control = Me.GroupControl2
        Me.LayoutControlItem49.CustomizationFormText = "LayoutControlItem21"
        Me.LayoutControlItem49.Location = New System.Drawing.Point(900, 0)
        Me.LayoutControlItem49.Name = "LayoutControlItem49"
        Me.LayoutControlItem49.Size = New System.Drawing.Size(336, 130)
        Me.LayoutControlItem49.Text = "LayoutControlItem21"
        Me.LayoutControlItem49.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem49.TextVisible = False
        '
        'LayoutControlItem52
        '
        Me.LayoutControlItem52.Control = Me.SimpleButton4
        Me.LayoutControlItem52.Location = New System.Drawing.Point(1080, 130)
        Me.LayoutControlItem52.Name = "LayoutControlItem52"
        Me.LayoutControlItem52.Size = New System.Drawing.Size(156, 27)
        Me.LayoutControlItem52.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem52.TextVisible = False
        '
        'EmptySpaceItem13
        '
        Me.EmptySpaceItem13.AllowHotTrack = False
        Me.EmptySpaceItem13.Location = New System.Drawing.Point(900, 157)
        Me.EmptySpaceItem13.Name = "EmptySpaceItem13"
        Me.EmptySpaceItem13.Size = New System.Drawing.Size(336, 574)
        Me.EmptySpaceItem13.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem51
        '
        Me.LayoutControlItem51.Control = Me.SimpleButton3
        Me.LayoutControlItem51.Location = New System.Drawing.Point(900, 130)
        Me.LayoutControlItem51.Name = "LayoutControlItem51"
        Me.LayoutControlItem51.Size = New System.Drawing.Size(180, 27)
        Me.LayoutControlItem51.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem51.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup2.AppearanceTabPage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup2.AppearanceTabPage.HeaderActive.Options.UseFont = True
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem54, Me.LayoutControlItem55, Me.LayoutControlItem56, Me.LayoutControlItem57, Me.EmptySpaceItem9, Me.LayoutControlItem53, Me.EmptySpaceItem14, Me.LayoutControlItem58, Me.EmptySpaceItem15, Me.LayoutControlItem59, Me.LayoutControlItem60, Me.LayoutControlItem61, Me.EmptySpaceItem16})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1236, 731)
        Me.LayoutControlGroup2.Text = "Printer"
        '
        'LayoutControlItem54
        '
        Me.LayoutControlItem54.Control = Me.TextEdit40
        Me.LayoutControlItem54.Location = New System.Drawing.Point(0, 158)
        Me.LayoutControlItem54.Name = "LayoutControlItem54"
        Me.LayoutControlItem54.Size = New System.Drawing.Size(475, 26)
        Me.LayoutControlItem54.Text = "Brand"
        Me.LayoutControlItem54.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem55
        '
        Me.LayoutControlItem55.Control = Me.TextEdit41
        Me.LayoutControlItem55.Location = New System.Drawing.Point(0, 184)
        Me.LayoutControlItem55.Name = "LayoutControlItem55"
        Me.LayoutControlItem55.Size = New System.Drawing.Size(475, 26)
        Me.LayoutControlItem55.Text = "Model*"
        Me.LayoutControlItem55.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem56
        '
        Me.LayoutControlItem56.Control = Me.TextEdit42
        Me.LayoutControlItem56.Location = New System.Drawing.Point(0, 210)
        Me.LayoutControlItem56.Name = "LayoutControlItem56"
        Me.LayoutControlItem56.Size = New System.Drawing.Size(475, 26)
        Me.LayoutControlItem56.Text = "Serial Number"
        Me.LayoutControlItem56.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem57
        '
        Me.LayoutControlItem57.Control = Me.MemoEdit3
        Me.LayoutControlItem57.Location = New System.Drawing.Point(0, 298)
        Me.LayoutControlItem57.Name = "LayoutControlItem57"
        Me.LayoutControlItem57.Size = New System.Drawing.Size(475, 141)
        Me.LayoutControlItem57.Text = "Note"
        Me.LayoutControlItem57.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = False
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(0, 439)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(475, 10)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem53
        '
        Me.LayoutControlItem53.Control = Me.TextEdit39
        Me.LayoutControlItem53.Location = New System.Drawing.Point(0, 236)
        Me.LayoutControlItem53.Name = "LayoutControlItem53"
        Me.LayoutControlItem53.Size = New System.Drawing.Size(475, 26)
        Me.LayoutControlItem53.Text = "Detail"
        Me.LayoutControlItem53.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem14
        '
        Me.EmptySpaceItem14.AllowHotTrack = False
        Me.EmptySpaceItem14.Location = New System.Drawing.Point(0, 288)
        Me.EmptySpaceItem14.Name = "EmptySpaceItem14"
        Me.EmptySpaceItem14.Size = New System.Drawing.Size(475, 10)
        Me.EmptySpaceItem14.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem58
        '
        Me.LayoutControlItem58.Control = Me.SimpleButton5
        Me.LayoutControlItem58.Location = New System.Drawing.Point(0, 449)
        Me.LayoutControlItem58.Name = "LayoutControlItem58"
        Me.LayoutControlItem58.Size = New System.Drawing.Size(213, 27)
        Me.LayoutControlItem58.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem58.TextVisible = False
        '
        'EmptySpaceItem15
        '
        Me.EmptySpaceItem15.AllowHotTrack = False
        Me.EmptySpaceItem15.Location = New System.Drawing.Point(0, 476)
        Me.EmptySpaceItem15.Name = "EmptySpaceItem15"
        Me.EmptySpaceItem15.Size = New System.Drawing.Size(475, 255)
        Me.EmptySpaceItem15.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem59
        '
        Me.LayoutControlItem59.Control = Me.SimpleButton6
        Me.LayoutControlItem59.Location = New System.Drawing.Point(213, 449)
        Me.LayoutControlItem59.Name = "LayoutControlItem59"
        Me.LayoutControlItem59.Size = New System.Drawing.Size(262, 27)
        Me.LayoutControlItem59.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem59.TextVisible = False
        '
        'LayoutControlItem60
        '
        Me.LayoutControlItem60.Control = Me.RadioGroup1
        Me.LayoutControlItem60.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem60.Name = "LayoutControlItem60"
        Me.LayoutControlItem60.Size = New System.Drawing.Size(475, 158)
        Me.LayoutControlItem60.Text = "เลือกประเภท"
        Me.LayoutControlItem60.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem61
        '
        Me.LayoutControlItem61.Control = Me.TextEdit43
        Me.LayoutControlItem61.Location = New System.Drawing.Point(0, 262)
        Me.LayoutControlItem61.Name = "LayoutControlItem61"
        Me.LayoutControlItem61.Size = New System.Drawing.Size(475, 26)
        Me.LayoutControlItem61.Text = "Price"
        Me.LayoutControlItem61.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem16
        '
        Me.EmptySpaceItem16.AllowHotTrack = False
        Me.EmptySpaceItem16.Location = New System.Drawing.Point(475, 0)
        Me.EmptySpaceItem16.Name = "EmptySpaceItem16"
        Me.EmptySpaceItem16.Size = New System.Drawing.Size(761, 731)
        Me.EmptySpaceItem16.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup5.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup5.AppearanceTabPage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup5.AppearanceTabPage.HeaderActive.Options.UseFont = True
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem62, Me.LayoutControlItem63, Me.LayoutControlItem64, Me.LayoutControlItem66, Me.LayoutControlItem70, Me.EmptySpaceItem18, Me.LayoutControlItem65, Me.EmptySpaceItem17, Me.LayoutControlItem67, Me.LayoutControlItem68, Me.EmptySpaceItem19, Me.EmptySpaceItem20})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(1236, 731)
        Me.LayoutControlGroup5.Text = "Monitor"
        '
        'LayoutControlItem62
        '
        Me.LayoutControlItem62.Control = Me.TextEdit46
        Me.LayoutControlItem62.CustomizationFormText = "Brand"
        Me.LayoutControlItem62.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem62.Name = "LayoutControlItem62"
        Me.LayoutControlItem62.Size = New System.Drawing.Size(461, 26)
        Me.LayoutControlItem62.Text = "Brand"
        Me.LayoutControlItem62.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem63
        '
        Me.LayoutControlItem63.Control = Me.TextEdit48
        Me.LayoutControlItem63.CustomizationFormText = "Model"
        Me.LayoutControlItem63.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem63.Name = "LayoutControlItem63"
        Me.LayoutControlItem63.Size = New System.Drawing.Size(461, 26)
        Me.LayoutControlItem63.Text = "Model*"
        Me.LayoutControlItem63.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem64
        '
        Me.LayoutControlItem64.Control = Me.TextEdit47
        Me.LayoutControlItem64.CustomizationFormText = "Serial Number"
        Me.LayoutControlItem64.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem64.Name = "LayoutControlItem64"
        Me.LayoutControlItem64.Size = New System.Drawing.Size(461, 26)
        Me.LayoutControlItem64.Text = "Serial Number"
        Me.LayoutControlItem64.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem66
        '
        Me.LayoutControlItem66.Control = Me.TextEdit44
        Me.LayoutControlItem66.CustomizationFormText = "Detail"
        Me.LayoutControlItem66.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem66.Name = "LayoutControlItem66"
        Me.LayoutControlItem66.Size = New System.Drawing.Size(461, 26)
        Me.LayoutControlItem66.Text = "Detail"
        Me.LayoutControlItem66.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem70
        '
        Me.LayoutControlItem70.Control = Me.TextEdit45
        Me.LayoutControlItem70.CustomizationFormText = "Price"
        Me.LayoutControlItem70.Location = New System.Drawing.Point(0, 104)
        Me.LayoutControlItem70.Name = "LayoutControlItem70"
        Me.LayoutControlItem70.Size = New System.Drawing.Size(461, 26)
        Me.LayoutControlItem70.Text = "Price"
        Me.LayoutControlItem70.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem18
        '
        Me.EmptySpaceItem18.AllowHotTrack = False
        Me.EmptySpaceItem18.CustomizationFormText = "EmptySpaceItem14"
        Me.EmptySpaceItem18.Location = New System.Drawing.Point(0, 130)
        Me.EmptySpaceItem18.Name = "EmptySpaceItem18"
        Me.EmptySpaceItem18.Size = New System.Drawing.Size(461, 10)
        Me.EmptySpaceItem18.Text = "EmptySpaceItem14"
        Me.EmptySpaceItem18.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem65
        '
        Me.LayoutControlItem65.Control = Me.MemoEdit4
        Me.LayoutControlItem65.CustomizationFormText = "Note"
        Me.LayoutControlItem65.Location = New System.Drawing.Point(0, 140)
        Me.LayoutControlItem65.Name = "LayoutControlItem65"
        Me.LayoutControlItem65.Size = New System.Drawing.Size(461, 129)
        Me.LayoutControlItem65.Text = "Note"
        Me.LayoutControlItem65.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem17
        '
        Me.EmptySpaceItem17.AllowHotTrack = False
        Me.EmptySpaceItem17.CustomizationFormText = "EmptySpaceItem9"
        Me.EmptySpaceItem17.Location = New System.Drawing.Point(0, 269)
        Me.EmptySpaceItem17.Name = "EmptySpaceItem17"
        Me.EmptySpaceItem17.Size = New System.Drawing.Size(461, 10)
        Me.EmptySpaceItem17.Text = "EmptySpaceItem9"
        Me.EmptySpaceItem17.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem67
        '
        Me.LayoutControlItem67.Control = Me.SimpleButton7
        Me.LayoutControlItem67.CustomizationFormText = "LayoutControlItem58"
        Me.LayoutControlItem67.Location = New System.Drawing.Point(0, 279)
        Me.LayoutControlItem67.Name = "LayoutControlItem67"
        Me.LayoutControlItem67.Size = New System.Drawing.Size(216, 27)
        Me.LayoutControlItem67.Text = "LayoutControlItem58"
        Me.LayoutControlItem67.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem67.TextVisible = False
        '
        'LayoutControlItem68
        '
        Me.LayoutControlItem68.Control = Me.SimpleButton8
        Me.LayoutControlItem68.CustomizationFormText = "LayoutControlItem59"
        Me.LayoutControlItem68.Location = New System.Drawing.Point(216, 279)
        Me.LayoutControlItem68.Name = "LayoutControlItem68"
        Me.LayoutControlItem68.Size = New System.Drawing.Size(245, 27)
        Me.LayoutControlItem68.Text = "LayoutControlItem59"
        Me.LayoutControlItem68.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem68.TextVisible = False
        '
        'EmptySpaceItem19
        '
        Me.EmptySpaceItem19.AllowHotTrack = False
        Me.EmptySpaceItem19.CustomizationFormText = "EmptySpaceItem15"
        Me.EmptySpaceItem19.Location = New System.Drawing.Point(0, 306)
        Me.EmptySpaceItem19.Name = "EmptySpaceItem19"
        Me.EmptySpaceItem19.Size = New System.Drawing.Size(461, 425)
        Me.EmptySpaceItem19.Text = "EmptySpaceItem15"
        Me.EmptySpaceItem19.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem20
        '
        Me.EmptySpaceItem20.AllowHotTrack = False
        Me.EmptySpaceItem20.CustomizationFormText = "EmptySpaceItem16"
        Me.EmptySpaceItem20.Location = New System.Drawing.Point(461, 0)
        Me.EmptySpaceItem20.Name = "EmptySpaceItem20"
        Me.EmptySpaceItem20.Size = New System.Drawing.Size(775, 731)
        Me.EmptySpaceItem20.Text = "EmptySpaceItem16"
        Me.EmptySpaceItem20.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup6.AppearanceTabPage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup6.AppearanceTabPage.HeaderActive.Options.UseFont = True
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem69, Me.LayoutControlItem71, Me.LayoutControlItem72, Me.LayoutControlItem73, Me.LayoutControlItem74, Me.EmptySpaceItem21, Me.LayoutControlItem75, Me.EmptySpaceItem22, Me.LayoutControlItem76, Me.LayoutControlItem77, Me.EmptySpaceItem23, Me.EmptySpaceItem24})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(1236, 731)
        Me.LayoutControlGroup6.Text = "Other"
        '
        'LayoutControlItem69
        '
        Me.LayoutControlItem69.Control = Me.TextEdit53
        Me.LayoutControlItem69.CustomizationFormText = "Brand"
        Me.LayoutControlItem69.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem69.Name = "LayoutControlItem69"
        Me.LayoutControlItem69.Size = New System.Drawing.Size(448, 26)
        Me.LayoutControlItem69.Text = "Brand"
        Me.LayoutControlItem69.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem71
        '
        Me.LayoutControlItem71.Control = Me.TextEdit52
        Me.LayoutControlItem71.CustomizationFormText = "Model"
        Me.LayoutControlItem71.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem71.Name = "LayoutControlItem71"
        Me.LayoutControlItem71.Size = New System.Drawing.Size(448, 26)
        Me.LayoutControlItem71.Text = "Model*"
        Me.LayoutControlItem71.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem72
        '
        Me.LayoutControlItem72.Control = Me.TextEdit49
        Me.LayoutControlItem72.CustomizationFormText = "Serial Number"
        Me.LayoutControlItem72.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem72.Name = "LayoutControlItem72"
        Me.LayoutControlItem72.Size = New System.Drawing.Size(448, 26)
        Me.LayoutControlItem72.Text = "Serial Number"
        Me.LayoutControlItem72.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.Control = Me.TextEdit51
        Me.LayoutControlItem73.CustomizationFormText = "Detail"
        Me.LayoutControlItem73.Location = New System.Drawing.Point(0, 78)
        Me.LayoutControlItem73.Name = "LayoutControlItem73"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(448, 26)
        Me.LayoutControlItem73.Text = "Detail"
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem74
        '
        Me.LayoutControlItem74.Control = Me.TextEdit50
        Me.LayoutControlItem74.CustomizationFormText = "Price"
        Me.LayoutControlItem74.Location = New System.Drawing.Point(0, 104)
        Me.LayoutControlItem74.Name = "LayoutControlItem74"
        Me.LayoutControlItem74.Size = New System.Drawing.Size(448, 26)
        Me.LayoutControlItem74.Text = "Price"
        Me.LayoutControlItem74.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem21
        '
        Me.EmptySpaceItem21.AllowHotTrack = False
        Me.EmptySpaceItem21.CustomizationFormText = "EmptySpaceItem14"
        Me.EmptySpaceItem21.Location = New System.Drawing.Point(0, 130)
        Me.EmptySpaceItem21.Name = "EmptySpaceItem21"
        Me.EmptySpaceItem21.Size = New System.Drawing.Size(448, 10)
        Me.EmptySpaceItem21.Text = "EmptySpaceItem14"
        Me.EmptySpaceItem21.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem75
        '
        Me.LayoutControlItem75.Control = Me.MemoEdit5
        Me.LayoutControlItem75.CustomizationFormText = "Note"
        Me.LayoutControlItem75.Location = New System.Drawing.Point(0, 140)
        Me.LayoutControlItem75.Name = "LayoutControlItem75"
        Me.LayoutControlItem75.Size = New System.Drawing.Size(448, 114)
        Me.LayoutControlItem75.Text = "Note"
        Me.LayoutControlItem75.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem22
        '
        Me.EmptySpaceItem22.AllowHotTrack = False
        Me.EmptySpaceItem22.CustomizationFormText = "EmptySpaceItem9"
        Me.EmptySpaceItem22.Location = New System.Drawing.Point(0, 254)
        Me.EmptySpaceItem22.Name = "EmptySpaceItem22"
        Me.EmptySpaceItem22.Size = New System.Drawing.Size(448, 10)
        Me.EmptySpaceItem22.Text = "EmptySpaceItem9"
        Me.EmptySpaceItem22.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem76
        '
        Me.LayoutControlItem76.Control = Me.SimpleButton10
        Me.LayoutControlItem76.CustomizationFormText = "LayoutControlItem58"
        Me.LayoutControlItem76.Location = New System.Drawing.Point(0, 264)
        Me.LayoutControlItem76.Name = "LayoutControlItem76"
        Me.LayoutControlItem76.Size = New System.Drawing.Size(209, 27)
        Me.LayoutControlItem76.Text = "LayoutControlItem58"
        Me.LayoutControlItem76.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem76.TextVisible = False
        '
        'LayoutControlItem77
        '
        Me.LayoutControlItem77.Control = Me.SimpleButton9
        Me.LayoutControlItem77.CustomizationFormText = "LayoutControlItem59"
        Me.LayoutControlItem77.Location = New System.Drawing.Point(209, 264)
        Me.LayoutControlItem77.Name = "LayoutControlItem77"
        Me.LayoutControlItem77.Size = New System.Drawing.Size(239, 27)
        Me.LayoutControlItem77.Text = "LayoutControlItem59"
        Me.LayoutControlItem77.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem77.TextVisible = False
        '
        'EmptySpaceItem23
        '
        Me.EmptySpaceItem23.AllowHotTrack = False
        Me.EmptySpaceItem23.CustomizationFormText = "EmptySpaceItem15"
        Me.EmptySpaceItem23.Location = New System.Drawing.Point(0, 291)
        Me.EmptySpaceItem23.Name = "EmptySpaceItem23"
        Me.EmptySpaceItem23.Size = New System.Drawing.Size(448, 440)
        Me.EmptySpaceItem23.Text = "EmptySpaceItem15"
        Me.EmptySpaceItem23.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem24
        '
        Me.EmptySpaceItem24.AllowHotTrack = False
        Me.EmptySpaceItem24.CustomizationFormText = "EmptySpaceItem16"
        Me.EmptySpaceItem24.Location = New System.Drawing.Point(448, 0)
        Me.EmptySpaceItem24.Name = "EmptySpaceItem24"
        Me.EmptySpaceItem24.Size = New System.Drawing.Size(788, 731)
        Me.EmptySpaceItem24.Text = "EmptySpaceItem16"
        Me.EmptySpaceItem24.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup7.AppearanceTabPage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup7.AppearanceTabPage.HeaderActive.Options.UseFont = True
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem79, Me.LayoutControlItem80, Me.LayoutControlItem81, Me.LayoutControlItem82, Me.EmptySpaceItem25, Me.EmptySpaceItem26, Me.LayoutControlItem78, Me.LayoutControlItem83, Me.LayoutControlItem84, Me.EmptySpaceItem27, Me.LayoutControlItem85, Me.EmptySpaceItem28, Me.LayoutControlItem86})
        Me.LayoutControlGroup7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup7.Name = "LayoutControlGroup7"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(1236, 731)
        Me.LayoutControlGroup7.Text = "License"
        '
        'LayoutControlItem79
        '
        Me.LayoutControlItem79.Control = Me.TextEdit55
        Me.LayoutControlItem79.Location = New System.Drawing.Point(0, 178)
        Me.LayoutControlItem79.Name = "LayoutControlItem79"
        Me.LayoutControlItem79.Size = New System.Drawing.Size(465, 26)
        Me.LayoutControlItem79.Text = "Brand*"
        Me.LayoutControlItem79.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem80
        '
        Me.LayoutControlItem80.Control = Me.TextEdit56
        Me.LayoutControlItem80.Location = New System.Drawing.Point(0, 204)
        Me.LayoutControlItem80.Name = "LayoutControlItem80"
        Me.LayoutControlItem80.Size = New System.Drawing.Size(465, 26)
        Me.LayoutControlItem80.Text = "Detail"
        Me.LayoutControlItem80.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem81
        '
        Me.LayoutControlItem81.Control = Me.TextEdit57
        Me.LayoutControlItem81.Location = New System.Drawing.Point(0, 230)
        Me.LayoutControlItem81.Name = "LayoutControlItem81"
        Me.LayoutControlItem81.Size = New System.Drawing.Size(465, 26)
        Me.LayoutControlItem81.Text = "Price"
        Me.LayoutControlItem81.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem82
        '
        Me.LayoutControlItem82.Control = Me.MemoEdit6
        Me.LayoutControlItem82.Location = New System.Drawing.Point(0, 292)
        Me.LayoutControlItem82.Name = "LayoutControlItem82"
        Me.LayoutControlItem82.Size = New System.Drawing.Size(465, 83)
        Me.LayoutControlItem82.Text = "Note"
        Me.LayoutControlItem82.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem25
        '
        Me.EmptySpaceItem25.AllowHotTrack = False
        Me.EmptySpaceItem25.Location = New System.Drawing.Point(0, 282)
        Me.EmptySpaceItem25.Name = "EmptySpaceItem25"
        Me.EmptySpaceItem25.Size = New System.Drawing.Size(465, 10)
        Me.EmptySpaceItem25.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem26
        '
        Me.EmptySpaceItem26.AllowHotTrack = False
        Me.EmptySpaceItem26.Location = New System.Drawing.Point(0, 375)
        Me.EmptySpaceItem26.Name = "EmptySpaceItem26"
        Me.EmptySpaceItem26.Size = New System.Drawing.Size(465, 10)
        Me.EmptySpaceItem26.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem78
        '
        Me.LayoutControlItem78.Control = Me.TextEdit54
        Me.LayoutControlItem78.Location = New System.Drawing.Point(0, 256)
        Me.LayoutControlItem78.Name = "LayoutControlItem78"
        Me.LayoutControlItem78.Size = New System.Drawing.Size(465, 26)
        Me.LayoutControlItem78.Text = "Amount"
        Me.LayoutControlItem78.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem83
        '
        Me.LayoutControlItem83.Control = Me.SimpleButton11
        Me.LayoutControlItem83.Location = New System.Drawing.Point(230, 385)
        Me.LayoutControlItem83.Name = "LayoutControlItem83"
        Me.LayoutControlItem83.Size = New System.Drawing.Size(235, 27)
        Me.LayoutControlItem83.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem83.TextVisible = False
        '
        'LayoutControlItem84
        '
        Me.LayoutControlItem84.Control = Me.SimpleButton12
        Me.LayoutControlItem84.Location = New System.Drawing.Point(0, 385)
        Me.LayoutControlItem84.Name = "LayoutControlItem84"
        Me.LayoutControlItem84.Size = New System.Drawing.Size(230, 27)
        Me.LayoutControlItem84.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem84.TextVisible = False
        '
        'EmptySpaceItem27
        '
        Me.EmptySpaceItem27.AllowHotTrack = False
        Me.EmptySpaceItem27.Location = New System.Drawing.Point(0, 412)
        Me.EmptySpaceItem27.Name = "EmptySpaceItem27"
        Me.EmptySpaceItem27.Size = New System.Drawing.Size(465, 319)
        Me.EmptySpaceItem27.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem85
        '
        Me.LayoutControlItem85.Control = Me.GroupControl3
        Me.LayoutControlItem85.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem85.Name = "LayoutControlItem85"
        Me.LayoutControlItem85.Size = New System.Drawing.Size(465, 110)
        Me.LayoutControlItem85.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem85.TextVisible = False
        '
        'EmptySpaceItem28
        '
        Me.EmptySpaceItem28.AllowHotTrack = False
        Me.EmptySpaceItem28.Location = New System.Drawing.Point(465, 0)
        Me.EmptySpaceItem28.Name = "EmptySpaceItem28"
        Me.EmptySpaceItem28.Size = New System.Drawing.Size(771, 731)
        Me.EmptySpaceItem28.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem86
        '
        Me.LayoutControlItem86.Control = Me.RadioGroup2
        Me.LayoutControlItem86.Location = New System.Drawing.Point(0, 110)
        Me.LayoutControlItem86.Name = "LayoutControlItem86"
        Me.LayoutControlItem86.Size = New System.Drawing.Size(465, 68)
        Me.LayoutControlItem86.Text = "Choose Type"
        Me.LayoutControlItem86.TextSize = New System.Drawing.Size(93, 16)
        '
        'dxAddDevice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "dxAddDevice"
        Me.Size = New System.Drawing.Size(1280, 800)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.RadioGroup2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl6.ResumeLayout(False)
        CType(Me.DateEdit6.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit5.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit57.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit56.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit55.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit54.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit43.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit42.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit41.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit40.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit39.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit37.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit25.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit31.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit27.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit22.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit35.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit33.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit32.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit30.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit21.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit38.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit24.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit29.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit36.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit28.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit23.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit26.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit34.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit4.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit46.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit48.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit47.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit44.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit45.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit53.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit52.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit49.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit51.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit50.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTimeChartRangeControlClient1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents layoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents layoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateTimeChartRangeControlClient1 As DevExpress.XtraEditors.DateTimeChartRangeControlClient
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit37 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit25 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit31 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit27 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit22 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit35 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit33 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit32 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit30 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit21 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit38 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit24 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit29 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit36 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit28 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit23 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit26 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit34 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit4 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem46 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem48 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem50 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem11 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem47 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem12 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem49 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem52 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem13 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem51 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit42 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit41 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit40 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit39 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem54 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem55 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem56 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem57 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem53 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem14 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem58 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem15 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem59 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem60 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit43 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem61 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem16 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TextEdit46 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit48 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit47 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemoEdit4 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit44 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit45 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem62 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem63 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem64 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem66 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem70 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem18 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem65 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem17 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem67 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem68 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem19 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem20 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TextEdit53 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit52 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit49 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit51 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit50 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemoEdit5 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SimpleButton10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem69 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem71 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem72 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem74 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem21 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem75 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem22 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem76 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem77 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem23 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem24 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TextEdit54 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem78 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioGroup2 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit6 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit57 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit56 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit55 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem79 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem80 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem81 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem82 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem25 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem26 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem83 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem84 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem27 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem85 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem28 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem86 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl6 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents DateEdit6 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit5 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem87 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem88 As DevExpress.XtraLayout.LayoutControlItem
End Class
