﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_edit_section
    'ประกาศตัวแปรตัวเชื่อมต่อ
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Dim idkey2 As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub frm_edit_section_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'เชื่อมต่อฐานข้อมูล

        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)

            Me.Close()
        End Try

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor);"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewsection.Items.Clear()
            While (MySqlReader.Read())

                With ListViewsection.Items.Add(MySqlReader("idsection"))
                    .SubItems.Add(MySqlReader("floor_name"))
                    .SubItems.Add(MySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewfloor.Items.Clear()
            While (MySqlReader.Read())

                With ListViewfloor.Items.Add(MySqlReader("idfloor"))
                    .SubItems.Add(MySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_add_section_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_section.Click
        If txt_add_section.Text <> "" Then

            savedata()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะบันทึก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub savedata()
        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Try
            If txt_add_section.Text <> "" Then
                MySqlCommand.CommandText = "INSERT INTO section (section_name,idfloor) VALUES ('" & txt_add_section.Text & "','" & txt_id_floor1.Text & "' );"
                MySqlCommand.CommandType = CommandType.Text
                MySqlCommand.Connection = sql
                MySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM floor order by idfloor;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewfloor.Items.Clear()
            While (MySqlReader.Read())

                With ListViewfloor.Items.Add(MySqlReader("idfloor"))
                    .SubItems.Add(MySqlReader("floor_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        MySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor);"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            ListViewsection.Items.Clear()
            While (MySqlReader.Read())

                With ListViewsection.Items.Add(MySqlReader("idsection"))
                    .SubItems.Add(MySqlReader("floor_name"))
                    .SubItems.Add(MySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
        txt_floor1.Text = ""
        txt_add_section.Text = ""
        txt_id_floor1.Text = ""

    End Sub

    Private Sub ListViewfloor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewfloor.Click
        idkey = ListViewfloor.SelectedItems(0).SubItems(0).Text

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        MySqlCommand.CommandText = "SELECT * FROM floor  where idfloor = '" & idkey & "' ;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            While (MySqlReader.Read())
                txt_id_floor1.Text = (MySqlReader("idfloor"))
                txt_floor1.Text = (MySqlReader("floor_name"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_home_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_home.Click
        Me.Hide()
    End Sub

    Private Sub ListViewsection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewsection.Click
        idkey2 = ListViewsection.SelectedItems(0).SubItems(0).Text

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        MySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor)and idsection = '" & idkey2 & "' ;"
        MySqlCommand.Connection = sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            While (MySqlReader.Read())
                txt_id_floor2.Text = (MySqlReader("idfloor"))
                txt_floor2.Text = (MySqlReader("floor_name"))
                txt_edit_section.Text = (MySqlReader("section_name"))
                txt_id_section.Text = (MySqlReader("idsection"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_clear_section_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txt_floor1.Text = ""
        txt_id_floor1.Text = ""
        txt_add_section.Text = ""
    End Sub
    Private Sub savedatasection()

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        Dim commandText2 As String

        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then

            If txt_id_floor2.Text <> "" Then
                Try
                    commandText2 = "UPDATE section SET section_name = '" & txt_edit_section.Text & "'  WHERE idsection = " & txt_id_section.Text & "; "
                    mySqlCommand.CommandText = commandText2
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = sql
                    mySqlCommand.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If
            End If
        End If


        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor);"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewsection.Items.Clear()
            While (mySqlReader.Read())

                With ListViewsection.Items.Add(mySqlReader("idsection"))
                    .SubItems.Add(mySqlReader("floor_name"))
                    .SubItems.Add(mySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
    End Sub

    Private Sub btn_save_section_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save_section.Click
        If txt_id_floor2.Text <> "" Then

            savedatasection()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btn_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete.Click
        If txt_id_section.Text <> "" Then

            DeleteData()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบออก", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
    Private Sub DeleteData()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object
        respone = MsgBox("ต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt_id_section.Text <> "" Then

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If
                Try

                    mySqlCommand.CommandText = "DELETE FROM section where idsection = '" & txt_id_section.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = sql

                    mySqlCommand.ExecuteNonQuery()
                    sql.Close()
                Catch ex As Exception

                    MsgBox(ex.ToString)
                    Exit Sub
                End Try

                If sql.State = ConnectionState.Closed Then
                    sql.Open()
                End If
            End If
        End If
        If sql.State = ConnectionState.Closed Then
            sql.Open()
        End If

        mySqlCommand.CommandText = "SELECT * FROM section join floor where (section.idfloor = floor.idfloor);"
        mySqlCommand.Connection = sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListViewsection.Items.Clear()
            While (mySqlReader.Read())

                With ListViewsection.Items.Add(mySqlReader("idsection"))
                    .SubItems.Add(mySqlReader("floor_name"))
                    .SubItems.Add(mySqlReader("section_name"))
                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        sql.Close()
        txt_floor2.Text = ""
        txt_edit_section.Text = ""
        txt_id_floor2.Text = ""
        txt_id_section.Text = ""
    End Sub

    Private Sub btn_delete_section_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete_section.Click
        txt_floor1.Text = ""
        txt_add_section.Text = ""
        txt_id_floor1.Text = ""
        txt_floor2.Text = ""
        txt_edit_section.Text = ""
        txt_id_floor2.Text = ""
        txt_id_section.Text = ""
    End Sub
End Class