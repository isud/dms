﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Data
Public Class frm_edit_brand
    Public Sql As MySqlConnection
    Public Path_SQL As String
    Dim mysql As MySqlConnection
    Dim id_user As String
    Dim position_user As String
    Dim positionname As String
    Dim mysqlpass As String
    Dim ipconnect As String
    Dim usernamedb As String
    Dim dbname As String
    Dim idkey As String
    Public Sub New(ByRef IdUser As String, ByRef position As String, ByRef mysql_pass As String, ByRef ip_connect As String, ByRef user_namedb As String, ByRef db_name As String)
        InitializeComponent()
        id_user = IdUser
        position_user = position
        mysqlpass = mysql_pass
        ipconnect = ip_connect
        usernamedb = user_namedb
        dbname = db_name
    End Sub
    Private Sub btn_add_brand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_brand.Click
        Dim Input As String
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Try
            Input = InputBox("เพิ่มข้อมูลยี่ห้อ")
            If Input.Length > 0 Then

                mySqlCommand.CommandText = "INSERT INTO brand (brand_name) VALUES ('" & Input & "');"
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()
                MessageBox.Show("เก็บเข้าฐานข้อมูลเรียบร้อย", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()

        showdatabrand()
    End Sub
    Private Sub showdatabrand()
        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader


        mySqlCommand.CommandText = "SELECT * FROM brand order by idbrand;"
        mySqlCommand.Connection = Sql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            ListViewbrand.Items.Clear()

            While (mySqlReader.Read())

                With ListViewbrand.Items.Add(mySqlReader("idbrand"))
                    .SubItems.Add(mySqlReader("brand_name"))

                End With
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub


    Private Sub frm_edit_brand_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'เชื่อมต่อฐานข้อมูล

        Sql = New MySqlConnection
        Sql.ConnectionString = "server=" + ipconnect + ";user id=" + usernamedb + ";password=" + mysqlpass + ";database=" + dbname + ";Character Set =utf8;"
        Try
            Sql.Open()
            '    MsgBox("CONNECTED TO DATABASE")
        Catch ex As Exception
            MsgBox("Can't Connect to database" + ex.Message)

            Me.Close()
        End Try
        showdatabrand()
    End Sub

    Private Sub btn_delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_delete.Click
        If txt_id_brand.Text <> "" Then

            DeleteData()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะลบ", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub DeleteData()

        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim respone As Object

        respone = MsgBox("คุณต้องการลบข้อมูลนี้หรือไม่", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            If txt_id_brand.Text <> "" Then

                If Sql.State = ConnectionState.Closed Then
                    Sql.Open()
                End If

                Try

                    mySqlCommand.CommandText = "DELETE FROM brand where idbrand = '" & txt_id_brand.Text & "';"
                    mySqlCommand.CommandType = CommandType.Text
                    mySqlCommand.Connection = Sql
                    mySqlCommand.ExecuteNonQuery()
                    txt_id_brand.Text = ""
                    txt_brand.Text = ""
                Catch ex As Exception

                    MsgBox(ex.ToString)

                End Try
                Sql.Close()

                'txt_count.Text = ListViewshow.Items.Count.ToString
                showdatabrand()
                'เรียกข้อมูลของ brand มาแสดงใน listviewbrand

            End If
        End If
    End Sub

    Private Sub btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_save.Click
        If txt_id_brand.Text <> "" Then

            savedatabrand()
        Else
            MessageBox.Show("กรุณาเลือกข้อมูลในตารางที่จะแก้ไข", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub savedatabrand()
        Dim mySqlCommand As New MySqlCommand
        Dim mySqlAdaptor As New MySqlDataAdapter
        Dim mySqlReader As MySqlDataReader
        Dim size As String
        Dim condition As String
        Dim respone As Object

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim commandText2 As String
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")

        If respone = 1 Then
            Try
                commandText2 = "UPDATE brand SET brand_name = '" & txt_brand.Text & "'  WHERE idbrand = " & txt_id_brand.Text & "; "

                mySqlCommand.CommandText = commandText2
                mySqlCommand.CommandType = CommandType.Text
                mySqlCommand.Connection = Sql
                mySqlCommand.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        Sql.Close()
        txt_brand.Text = ""
        txt_id_brand.Text = ""
        showdatabrand()
    End Sub

    Private Sub btn_home_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_home.Click
        Me.Hide()
    End Sub

    Private Sub ListViewbrand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListViewbrand.Click
        idkey = ListViewbrand.SelectedItems(0).SubItems(0).Text

        If Sql.State = ConnectionState.Closed Then
            Sql.Open()
        End If

        Dim MySqlCommand As New MySqlCommand
        Dim MySqlAdaptor As New MySqlDataAdapter
        Dim MySqlReader As MySqlDataReader

        MySqlCommand.CommandText = "SELECT * FROM brand  where idbrand = '" & idkey & "' ;"
        MySqlCommand.Connection = Sql
        MySqlAdaptor.SelectCommand = MySqlCommand

        Try
            MySqlReader = MySqlCommand.ExecuteReader
            While (MySqlReader.Read())
                txt_id_brand.Text = (MySqlReader("idbrand"))
                txt_brand.Text = (MySqlReader("brand_name"))
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Sql.Close()
    End Sub
End Class